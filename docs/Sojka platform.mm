<map version="freeplane 1.5.9">
<!--To view this file, download free mind mapping software Freeplane from http://freeplane.sourceforge.net -->
<node TEXT="Sojka platform" FOLDED="false" ID="ID_1723255651" CREATED="1283093380553" MODIFIED="1472306897517"><hook NAME="MapStyle">
    <properties fit_to_viewport="false;"/>

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node" STYLE="oval" UNIFORM_SHAPE="true" VGAP_QUANTITY="24.0 pt">
<font SIZE="24"/>
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="default" MAX_WIDTH="600.0 px" COLOR="#000000" STYLE="as_parent">
<font NAME="SansSerif" SIZE="10" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details"/>
<stylenode LOCALIZED_TEXT="defaultstyle.attributes">
<font SIZE="9"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.note"/>
<stylenode LOCALIZED_TEXT="defaultstyle.floating">
<edge STYLE="hide_edge"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="styles.topic" COLOR="#18898b" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subtopic" COLOR="#cc3300" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subsubtopic" COLOR="#669900">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.important">
<icon BUILTIN="yes"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" COLOR="#000000">
<font SIZE="18"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" COLOR="#0033ff">
<font SIZE="16"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" COLOR="#00b439">
<font SIZE="14"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" COLOR="#990000">
<font SIZE="12"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" COLOR="#111111">
<font SIZE="10"/>
</stylenode>
</stylenode>
</stylenode>
</map_styles>
</hook>
<hook NAME="AutomaticEdgeColor" COUNTER="2" RULE="ON_BRANCH_CREATION"/>
<node TEXT="Objects" POSITION="right" ID="ID_1542953460" CREATED="1472306898186" MODIFIED="1472306901640">
<edge COLOR="#808080"/>
<node TEXT="JobListing" ID="ID_1427726328" CREATED="1472306908433" MODIFIED="1472306965844" LINK="JobListing.mm"/>
<node TEXT="Resume" ID="ID_840876649" CREATED="1472307001551" MODIFIED="1472307004267"/>
<node TEXT="Recommendation" ID="ID_1273148467" CREATED="1472307022983" MODIFIED="1472307027391">
<node TEXT="JobListingRecommendation" ID="ID_311529018" CREATED="1472307036037" MODIFIED="1472307051637">
<node TEXT="" ID="ID_1143871248" CREATED="1472307095267" MODIFIED="1472307095267">
<node ID="ID_1485918864" TREE_ID="ID_1427726328"/>
</node>
</node>
<node TEXT="CompanyRecommendation" ID="ID_1314322578" CREATED="1472307053716" MODIFIED="1472307062647"/>
</node>
<node TEXT="Company" ID="ID_933731838" CREATED="1472307132381" MODIFIED="1472307137520"/>
<node TEXT="Hirer" ID="ID_1479172229" CREATED="1472307146223" MODIFIED="1472307151138">
<node TEXT="Employer" ID="ID_1212136983" CREATED="1472310303325" MODIFIED="1472310308524"/>
<node TEXT="Recruiter" ID="ID_1131224567" CREATED="1472310308963" MODIFIED="1472310315937"/>
</node>
<node TEXT="Candidate" ID="ID_6470326" CREATED="1472307180534" MODIFIED="1472307186340"/>
<node TEXT="Contractor" ID="ID_84726958" CREATED="1472311364769" MODIFIED="1472311369109"/>
<node TEXT="Teacher" ID="ID_1319964424" CREATED="1472307186975" MODIFIED="1472307191948"/>
</node>
</node>
</map>
