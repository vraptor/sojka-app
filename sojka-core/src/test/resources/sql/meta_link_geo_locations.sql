INSERT INTO GEO_LOCATIONS (UUID, VERSION, NAME, PLACE_ID)
VALUES ('57972460-cb06-4371-bff3-eeb044ca7f81', 0, 'Praha 2', 'ChIJuZQJdKqQC0cR_D5f878nJWU');
INSERT INTO GEO_LOCATIONS (UUID, VERSION, NAME, PLACE_ID)
VALUES ('5e453d04-e808-4f29-9125-4df970f6e859', 0, 'Praha', 'ChIJi3lwCZyTC0cRkEAWZg-vAAQ');
INSERT INTO GEO_LOCATIONS (UUID, VERSION, NAME, PLACE_ID)
VALUES ('b394cc94-703d-4088-a99f-de19f7bf760c', 0, 'Praha 1', 'ChIJP9OBE-mUC0cR8gwkXBCvACY');

INSERT INTO META_LINK (UUID, VERSION, ADDRESS, BASE_URI, COMPANY, CREATED, HREF, TITLE, HTTP_STATUS, SUMMARY, UPDATED)
VALUES ('3c883de2-bf1d-4372-a0ea-99adde674f27', 1, 'Praha - Pankrác', 'https://www.monster.cz', 'ManpowerGroup s.r.o.',
                                                '2017-03-22 07:47:16.012000',
                                                'http://prace.monster.cz/jsi-li-fresh-graduate-s-prax%C3%AD-ve-financ%C3%ADch-hledaj%C3%ADc%C3%AD-pr%C3%A1ci-v-mlad%C3%A9m-multilingual-kolektivu-de-pr%C3%A1ce-praha-pankr%C3%A1c-180961246.aspx?mescoid=1300117001001&jobPosition=20',
                                                'Jsi-li "FRESH GRADUATE" s praxí ve financích hledající práci v mladém multilingual kolektivu - de...',
                                                0, NULL, '2017-03-22 07:47:16.018000');
INSERT INTO META_LINK (UUID, VERSION, ADDRESS, BASE_URI, COMPANY, CREATED, HREF, TITLE, HTTP_STATUS, SUMMARY, UPDATED)
VALUES ('4870ec77-e93c-4d20-aacd-e7b2ec1f9b5f', 3, 'Praha - Pankrác', 'https://www.monster.cz', 'ManpowerGroup s.r.o.',
                                                '2017-03-28 22:33:31.602000',
                                                'http://prace.monster.cz/gl-specialista-s-flexibiln%C3%AD-pracovn%C3%AD-dobou-pr%C3%A1ce-praha-pankr%C3%A1c-182333557.aspx?mescoid=9901112000004&jobPosition=12',
                                                'GL specialista s flexibilní pracovní dobou', 0, NULL,
                                                '2017-03-28 22:33:34.139000');
INSERT INTO META_LINK (UUID, VERSION, ADDRESS, BASE_URI, COMPANY, CREATED, HREF, TITLE, HTTP_STATUS, SUMMARY, UPDATED)
VALUES ('66450ef9-a96b-42e2-be95-8559e75a9ff0', 6, '- Praha 1 - Praha 2', 'http://www.careerjet.cz',
                                                'Zatisi Catering Group a.s.', '2017-03-29 06:28:02.268000',
                                                'http://www.careerjet.cz/job/7268b42c3b94449c5bb2f072c6e0db80.html',
                                                'Kuchař/kuchařka – práce jen v týdnu, přes den', 0, NULL,
                                                '2017-03-29 21:30:40.682000');
INSERT INTO META_LINK (UUID, VERSION, ADDRESS, BASE_URI, COMPANY, CREATED, HREF, TITLE, HTTP_STATUS, SUMMARY, UPDATED)
VALUES ('7eb78459-2cb7-4f28-82c0-b99c1d0e5447', 1, 'Letiště Václava Havla - Praha 6', 'https://www.monster.cz',
                                                'Hays Czech Republic, s.r.o.', '2017-03-22 07:47:36.568000',
                                                'http://prace.monster.cz/shop-assistant-pr%C3%A1ce-leti%C5%A1t%C4%9B-v%C3%A1clava-havla-praha-6-181527728.aspx?mescoid=4100671001001&jobPosition=8',
                                                'Shop Assistant', 0, NULL, '2017-03-22 07:47:36.573000');
INSERT INTO META_LINK (UUID, VERSION, ADDRESS, BASE_URI, COMPANY, CREATED, HREF, TITLE, HTTP_STATUS, SUMMARY, UPDATED)
VALUES
  ('d6c67101-34cd-4f9f-9799-b6c65006e918', 5, '- Královéhradecký kraj - Pardubický kraj', 'http://www.careerjet.cz',
                                           'Advantage Consulting, s.r.o.', '2017-03-30 09:41:05.439000',
                                           'http://www.careerjet.cz/job/5d44c320de1a8e7826908eefa5c3a99e.html',
                                           'VEDOUCÍ PROJEKTŮ S AJ', 0, NULL, '2017-03-30 21:43:55.269000');
INSERT INTO META_LINK (UUID, VERSION, ADDRESS, BASE_URI, COMPANY, CREATED, HREF, TITLE, HTTP_STATUS, SUMMARY, UPDATED)
VALUES ('fc866405-5101-4ff4-89b4-0d39dd2d9c7d', 29, 'Praha - Pankrác', 'https://www.monster.cz', 'ManpowerGroup s.r.o.',
                                                '2017-03-28 21:26:28.128000',
                                                'http://prace.monster.cz/gl-specialista-s-flexibiln%C3%AD-pracovn%C3%AD-dobou-pr%C3%A1ce-praha-pankr%C3%A1c-182333557.aspx?mescoid=9901112000004&jobPosition=11',
                                                'GL specialista s flexibilní pracovní dobou', 0, NULL,
                                                '2017-03-28 22:33:37.931000');

INSERT INTO META_LINK_GEO_LOCATIONS (META_LINK_UUID, GEO_LOCATION_UUID)
VALUES ('fc866405-5101-4ff4-89b4-0d39dd2d9c7d', '57972460-cb06-4371-bff3-eeb044ca7f81');
INSERT INTO META_LINK_GEO_LOCATIONS (META_LINK_UUID, GEO_LOCATION_UUID)
VALUES ('7eb78459-2cb7-4f28-82c0-b99c1d0e5447', '5e453d04-e808-4f29-9125-4df970f6e859');