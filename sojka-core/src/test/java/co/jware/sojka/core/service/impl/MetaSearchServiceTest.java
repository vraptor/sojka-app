package co.jware.sojka.core.service.impl;

import co.jware.sojka.core.TestConfig;
import co.jware.sojka.core.domain.search.MetaSearch;
import co.jware.sojka.core.service.entity.MetaSearchService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = TestConfig.class)
@ActiveProfiles("test")
@Transactional
public class MetaSearchServiceTest {
    @Autowired
    private MetaSearchService metaSearchService;
    @PersistenceContext
    private
    EntityManager em;

    @Test
    public void getSearch() throws Exception {
        MetaSearch search = MetaSearch.builder()
                .addKeywords("java", "python")
                .build();
        metaSearchService.storeSearch(search.withKeywords("python", "java"));
        metaSearchService.storeSearch(search.withKeywords("java", "javascript"));
        em.flush();
        Optional<MetaSearch> result = metaSearchService.getSearch(search);
        assertThat(result.isPresent()).isTrue();
    }

}