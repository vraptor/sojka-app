package co.jware.sojka.core.domain.search;

import org.junit.Test;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;

import static org.assertj.core.api.Assertions.assertThat;


public class MetaSearchResultTest {
    @Test
    public void baseUri() throws Exception {
        URI fullUri = UriComponentsBuilder.fromHttpUrl("http://noop.io/somepath?q=3")
                .build()
                .encode()
                .toUri();
        URI uri = MetaSearchResult.builder().uri(fullUri).statusCode(200).build().baseUri();
        assertThat(uri.toString()).isEqualToIgnoringCase("http://noop.io");
    }

}