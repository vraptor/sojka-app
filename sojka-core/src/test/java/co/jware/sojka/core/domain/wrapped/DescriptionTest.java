package co.jware.sojka.core.domain.wrapped;

import org.junit.Test;

import static org.junit.Assert.assertEquals;


public class DescriptionTest {

    @Test
    public void testToString() throws Exception {
        Description description = Description.of("Some Description");
        String toString = description.toString();
        assertEquals("Description{value=Some Description}",toString);
    }
}