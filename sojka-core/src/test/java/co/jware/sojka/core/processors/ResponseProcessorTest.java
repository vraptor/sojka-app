package co.jware.sojka.core.processors;

import co.jware.sojka.core.Fakers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import co.jware.sojka.core.domain.JobListing;
import co.jware.sojka.core.domain.JobResponse;
import co.jware.sojka.core.domain.party.Candidate;
import co.jware.sojka.core.domain.party.Hirer;
import co.jware.sojka.core.exceptions.DuplicateJobResponseException;
import co.jware.sojka.core.exceptions.JobListingExpiredException;
import mockit.Injectable;
import mockit.integration.junit4.JMockit;

import java.time.OffsetDateTime;

import static java.time.temporal.ChronoUnit.DAYS;
import static org.junit.Assert.*;

@RunWith(JMockit.class)
public class ResponseProcessorTest {
    //    @Tested
    private ResponseProcessor responseProcessor = new ResponseProcessor();

    private JobListing jobListing;
    private Candidate candidate;
    @Injectable
    private Hirer owner;

    @Before
    public void setUp() throws Exception {
        jobListing = JobListing.builder()
                .name("FHKSDJFSDK")
                .owner(Fakers.party()).build();
        candidate = Candidate.builder()
                .firstName("John")
                .lastName("Doe")
                .email("john.doe@jware.co")
                .build();
    }

    @Test
    public void respondToJob() throws Exception {
        JobResponse response = responseProcessor.responseToJob(JobListing.NULL, candidate);
        assertNotNull(response);
        assertNotNull(response.jobListing());
    }

    @Test
    public void respondToJob_free_capacity_decreased() throws Exception {
        JobListing listing = JobListing.NULL
                .withCapacity(4)
                .withFreeCapacity(2);
        JobResponse response = responseProcessor.responseToJob(listing, candidate);
        JobListing resultJobListing = response.jobListing();
        assertEquals(1, resultJobListing.freeCapacity());
    }

    @Test(expected = IllegalArgumentException.class)
    public void respondToJob_no_free_capacity() throws Exception {
        JobListing listing = JobListing.NULL
                .withCapacity(4)
                .withFreeCapacity(0);
        responseProcessor.responseToJob(listing, candidate);
    }

    @Test(expected = JobListingExpiredException.class)
    public void respondToJob_expired_lob_listing() throws Exception {
        OffsetDateTime from = OffsetDateTime.now()
                .minus(3, DAYS);
        OffsetDateTime to = OffsetDateTime.now()
                .minus(1, DAYS);
        JobListing jobListing = this.jobListing.withValidFrom(from).withValidTo(to);
        responseProcessor.responseToJob(jobListing, candidate);
    }

    @Test
    public void respondToJob_respondent() throws Exception {
        JobResponse response = responseProcessor.responseToJob(jobListing, candidate);
        assertEquals(candidate, response.respondent());
    }

    @Test(expected = DuplicateJobResponseException.class)
    public void respondToJobDuplicate() throws Exception {
        responseProcessor.responseToJob(jobListing, candidate);
        responseProcessor.responseToJob(jobListing, candidate);
    }

    @Test
    public void processJobResponse() throws Exception {
        JobResponse jobResponse = JobResponse.builder()
                .jobListing(jobListing)
                .respondent(candidate)
                .build();
        JobResponse response = responseProcessor.processJobResponse(jobResponse);
        assertTrue(jobResponse.status().ordinal() < response.status().ordinal());
    }


}
