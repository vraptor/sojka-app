package co.jware.sojka.core;

import co.jware.sojka.core.service.MetaLinkService;
import co.jware.sojka.repository.CandidateRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.autoconfigure.jackson.Jackson2ObjectMapperBuilderCustomizer;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.*;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;

@Configuration
@Profile("hsql")
@EntityScan(basePackages = "co.jware.sojka.entities")
@EnableJpaRepositories(basePackageClasses = CandidateRepository.class)
@ComponentScan(basePackageClasses = MetaLinkService.class)
public class HsqlTestConfig {

    @Bean
    public ObjectMapper objectMapper() {
        return Jackson2ObjectMapperBuilder.json()
                .failOnEmptyBeans(false)
                .build();
    }

    @Bean
    public Jackson2ObjectMapperBuilderCustomizer jackson2ObjectMapperBuilderCustomizer() {
        return builder -> builder.failOnEmptyBeans(false);
    }

    @Bean
    @Primary
    @ConfigurationProperties(prefix = "spring.datasource")
    public DataSourceProperties hsqlDataSourceProperties() {
        DataSourceProperties dataSourceProperties = new DataSourceProperties() {
        };
        return dataSourceProperties;
    }

//    @Bean
//    @Primary
//    @DependsOn("hsqlServer")
//    public DataSource dataSource(DataSourceProperties properties) {
//        return properties.initializeDataSourceBuilder().build();
//    }
//
//    @Bean
//    public HsqlProperties hsqlProperties() {
//        HsqlProperties properties = new HsqlProperties();
//        properties.setProperty("server.remote_open", true);
//        properties.setProperty("server.no_system_exit", true);
//        return properties;
//    }
//
//    @Bean(initMethod = "start", destroyMethod = "shutdown")
//    public Server hsqlServer(HsqlProperties properties) throws Exception {
//        Server server = new Server();
//        server.setProperties(properties);
//        return server;
//    }
}
