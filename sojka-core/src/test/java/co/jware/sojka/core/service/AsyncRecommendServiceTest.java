package co.jware.sojka.core.service;

import co.jware.sojka.core.TestConfig;
import mockit.integration.junit4.JMockit;
import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.rules.SpringClassRule;
import org.springframework.test.context.junit4.rules.SpringMethodRule;

@SpringBootTest(classes = TestConfig.class)
@ActiveProfiles("test")
@RunWith(JMockit.class)
public class AsyncRecommendServiceTest {

    @ClassRule
    public static final SpringClassRule springClassRule = new SpringClassRule();
    @Rule
    public SpringMethodRule springMethodRule = new SpringMethodRule();
    @Autowired
    RecommendService recommendService;

    @Test
    public void recommendViaReceiverEmail() throws Exception {
//        PersonServiceImpl personService = new PersonServiceImpl();
//        new Expectations(PersonServiceImpl.class) {{
//            personService.findByEmail(anyString);
//            result = new PersonEntity();
//        }};
//        new MockUp<PersonRepository>() {
//            @Mock
//            PersonEntity findByEmail(String email) {
//                return new PersonEntity();
//            }
//        }.getMockInstance();
//        Candidate sender = Candidate.NULL.withEmail("jane@jware.co");
//        CompletableFuture<Recommendation> future =
//                recommendService.recommendViaReceiverEmail("_unknown", sender, JobListing.NULL);
//        future.thenAccept((r) -> {
//            assertNotNull(r);
//        }).join();
    }

}