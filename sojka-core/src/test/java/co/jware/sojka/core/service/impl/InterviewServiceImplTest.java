
package co.jware.sojka.core.service.impl;

import java.util.Date;
import java.util.Optional;
import java.util.UUID;

import co.jware.sojka.core.Fakers;
import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.rules.SpringClassRule;
import org.springframework.test.context.junit4.rules.SpringMethodRule;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Sets;

import co.jware.sojka.core.TestConfig;
import co.jware.sojka.core.domain.Interview;
import co.jware.sojka.core.domain.JobListing;
import co.jware.sojka.core.domain.party.Candidate;
import co.jware.sojka.core.domain.party.Hirer;
import co.jware.sojka.core.domain.wrapped.Note;
import co.jware.sojka.core.service.InterviewService;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(classes = TestConfig.class)
@Transactional
@ActiveProfiles("test")
public class InterviewServiceImplTest {

    @Rule
    public SpringMethodRule methodRule = new SpringMethodRule();
    @ClassRule
    public static final SpringClassRule classRule = new SpringClassRule();
    @Autowired
    InterviewService interviewService;
    @Autowired
    private ObjectMapper objectMapper;

    @Test
    public void createInterview() throws Exception {
    }

    @Test
    public void setResult() throws Exception {
    }

    @Test
    public void loadInterview() throws Exception {
    }

    @Test
    public void changeInterviewStatus() throws Exception {
    }

    @Test
    public void checkIfExists() throws Exception {
    }

    @Test
    public void findInterviewsLike() throws Exception {
    }

    @Test
    public void findByUuid() throws Exception {
    }

    @Test
    public void findByCandidate() throws Exception {
    }

    @Test
    public void findByJbListing() throws Exception {
    }

    @Test
    public void scheduleInterview() throws Exception {
        Interview interview = Interview.builder()
                .candidate(Candidate.NULL.withUuid(UUID.randomUUID()))
                .jobListing(JobListing.NULL
                        .withUuid(UUID.randomUUID())
                        .withOwner(Fakers.hirer()))
                .interviewDate(new Date())
                .order(1)
                .notes(Sets.newHashSet(Note.of("Interview Note")))
                .build();
        Optional<Interview> result = interviewService.scheduleInterview(interview);
        String asString = objectMapper.writeValueAsString(interview);
        assertThat(result).isNotNull();
    }

}