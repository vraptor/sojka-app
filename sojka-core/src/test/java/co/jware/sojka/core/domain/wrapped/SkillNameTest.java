package co.jware.sojka.core.domain.wrapped;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;

import static org.assertj.core.api.Assertions.assertThat;

public class SkillNameTest {
    private ObjectMapper objectMapper;

    @Before
    public void setUp() throws Exception {
        objectMapper = Jackson2ObjectMapperBuilder.json().build();
    }

    @Test
    public void serialize() throws Exception {
        String asString = objectMapper.writeValueAsString(SkillName.of("Javascript"));
        assertThat(asString).contains("Javascript");
    }
}