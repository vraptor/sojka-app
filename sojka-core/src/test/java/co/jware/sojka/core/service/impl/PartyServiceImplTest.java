package co.jware.sojka.core.service.impl;

import co.jware.sojka.core.BaseDataJpaTest;
import co.jware.sojka.core.Fakers;
import co.jware.sojka.core.domain.party.Candidate;
import co.jware.sojka.core.domain.party.External;
import co.jware.sojka.core.domain.party.Party;
import co.jware.sojka.core.domain.party.Person;
import co.jware.sojka.core.service.PartyService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@DataJpaTest
@ContextConfiguration
@Transactional
public class PartyServiceImplTest extends BaseDataJpaTest {

    @Autowired
    private PartyService partyService;

    @Test
    public void findByEmail() throws Exception {
        Party result = partyService.findByEmail("candidate@sojka.co")
                .orElseThrow(IllegalStateException::new);
        assertThat(result).isInstanceOf(Person.class);
    }

    @Test
    public void findByEmail_external() throws Exception {
        Party result = partyService.findByEmail("john.doe@acme.co")
                .orElseThrow(IllegalStateException::new);
        assertThat(result).isInstanceOf(External.class);
    }

    @Test
    public void createParty() throws Exception {
        Party party = partyService.createParty(Fakers.external());
        assertThat(party).isInstanceOf(External.class);
    }

    @Test
    public void createParty_person() throws Exception {
        Party party = partyService.createParty(Fakers.candidate());
        assertThat(party).isInstanceOf(Candidate.class);
    }
}
