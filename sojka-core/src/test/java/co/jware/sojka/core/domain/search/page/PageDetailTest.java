package co.jware.sojka.core.domain.search.page;

import co.jware.sojka.core.Fakers;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.flipkart.zjsonpatch.DiffFlags;
import org.junit.Test;
import com.flipkart.zjsonpatch.JsonDiff;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
public class PageDetailTest {

    @Autowired
    ObjectMapper mapper;

    @Test
    public void diff() throws Exception {


        PageDetail pageDetail = Fakers.pageDetail();
        PageDetail targetPageDetail = Fakers.pageDetail();
        JsonNode sourceJsonNode = mapper.valueToTree(pageDetail);
        JsonNode targetJsonNode = mapper.valueToTree(targetPageDetail);
        JsonNode diffJsonNode = JsonDiff.asJson(sourceJsonNode, targetJsonNode);
        List diffList = mapper.treeToValue(diffJsonNode, ArrayList.class);
        assertThat(diffList).isNotEmpty();
    }
}
