package co.jware.sojka.core.service.impl;

import co.jware.sojka.entities.core.UserBo;
import co.jware.sojka.repository.UserRepository;
import mockit.Expectations;
import mockit.Injectable;
import mockit.Tested;
import mockit.integration.junit4.JMockit;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;

@RunWith(JMockit.class)
public class DefaultUserDetailsServiceTest {
    @Tested(fullyInitialized = true, availableDuringSetup = true)
    DefaultUserDetailsService detailsService;
    @Injectable
    UserRepository userRepository;
    @Injectable
    PasswordEncoder passwordEncoder;
    @Injectable
    JdbcTemplate jdbcTemplate;

    @Test(expected = IllegalArgumentException.class)
    public void loadUserByUsername_null() throws Exception {
        detailsService.loadUserByUsername(null);
    }

    @Test(expected = UsernameNotFoundException.class)
    public void loadUserByUsername_not_found() throws Exception {
        /// COVERAGE:OFF
        new Expectations() {{
            userRepository.findByUsername(anyString);
            result = null;
        }};
        /// COVERAGE:ON
        detailsService.loadUserByUsername("unknown");
    }

    @Test()
    public void loadUserByUsername() throws Exception {
        new Expectations() {{
            userRepository.findByUsername(anyString);
            result = new UserBo();
        }};
        detailsService.loadUserByUsername("known");
    }
}