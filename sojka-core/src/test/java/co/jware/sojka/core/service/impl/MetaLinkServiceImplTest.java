package co.jware.sojka.core.service.impl;

import co.jware.sojka.core.BaseDataJpaTest;
import co.jware.sojka.core.Fakers;
import co.jware.sojka.core.domain.search.MetaLink;
import co.jware.sojka.core.domain.search.MetaLinksFilter;
import co.jware.sojka.core.service.MetaLinkService;
import co.jware.sojka.core.service.MetaSearchLinkService;
import co.jware.sojka.repository.search.MetaLinkRepository;
import com.hazelcast.core.HazelcastInstance;
import io.vertx.core.eventbus.EventBus;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import javax.transaction.Transactional;
import java.util.Arrays;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;

@RunWith(SpringRunner.class)
@Transactional
@ContextConfiguration
public class MetaLinkServiceImplTest extends BaseDataJpaTest {
    @Autowired
    private MetaLinkService metaLinkService;
    @Autowired
    private MetaLinkRepository metaLinkRepository;

    @Test
    public void filterLinks() throws Exception {
        MetaLinksFilter filter = MetaLinksFilter.builder()
                .addKeywords("java", "javascript")
                .addLocations("praha", "brno")
                .build();

        Arrays.stream(new int[]{1, 2, 3, 4, 5}).forEach(i -> {
            MetaLink metaLink = Fakers.metaLink().withLocations("praha").withKeywords("java", "javascript");
            metaLinkService.saveLink(metaLink);
        });

        metaLinkRepository.flush();
        Page<MetaLink> metaLinks = metaLinkService.filterLinks(filter, new PageRequest(0, 10));
        assertThat(metaLinks.hasContent()).isTrue();
    }
}
