package co.jware.sojka.core.domain;

import java.time.Duration;
import java.time.LocalDate;

import org.junit.Test;
import org.objenesis.strategy.StdInstantiatorStrategy;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;

import co.jware.sojka.core.domain.party.Teacher;

import static org.assertj.core.api.Assertions.assertThat;


public class CourseTest {
    @Test
    public void totalDuration() throws Exception {
        CourseTopic topicOne = CourseTopic.builder()
                .name("NONAME").duration(Duration.ofMinutes(30))
                .build();
        CourseTopic topicTwo = CourseTopic.builder()
                .name("OTHER").duration(Duration.ofMinutes(15))
                .build();
        Course course = Course.builder()
                .title("NONAME")
                .owner(Teacher.NULL)
                .courseDate(LocalDate.now())
                .seats(9)
                .addCourseTopics(topicOne, topicTwo)
                .build();
        assertThat(course.totalDuration()).isEqualTo(Duration.ofMinutes(45));
    }

    @Test
    public void totalDuration_no_topics() throws Exception {
        Course course = Course.builder()
                .title("NONAME")
                .owner(Teacher.NULL)
                .courseDate(LocalDate.now())
                .seats(9)
                .build();
        assertThat(course.totalDuration()).isEqualTo(Duration.ofMinutes(0));
    }

    @Test
    public void serialization() throws Exception {
        Kryo kryo = new Kryo();
        kryo.setInstantiatorStrategy(new StdInstantiatorStrategy());
        Output output = new Output(2048);
        CourseTopic topic = CourseTopic.builder()
                .name("Preamble")
                .duration(Duration.ofMinutes(45))
                .build();
        Course course = Course.builder()
                .owner(Teacher.NULL)
                .title("Boring one")
                .seats(25)
                .courseDate(LocalDate.now())
                .addCourseTopic(topic)
                .build();
        kryo.writeObject(output, course);
        byte[] bytes = output.getBuffer();
        Course result = kryo.readObject(new Input(bytes), Course.class);
        assertThat(result).isEqualTo(course);

    }
}