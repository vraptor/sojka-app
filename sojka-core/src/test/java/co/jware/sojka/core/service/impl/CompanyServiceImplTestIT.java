package co.jware.sojka.core.service.impl;

import co.jware.sojka.core.TestConfig;
import co.jware.sojka.core.domain.Company;
import co.jware.sojka.core.service.entity.CompanyService;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.rules.SpringClassRule;
import org.springframework.test.context.junit4.rules.SpringMethodRule;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(classes = TestConfig.class)
@Transactional
@ActiveProfiles("test")
public class CompanyServiceImplTestIT {
    @Rule
    public SpringMethodRule methodRule = new SpringMethodRule();
    @ClassRule
    public static final SpringClassRule classRule = new SpringClassRule();
    @Autowired
    CompanyService companyService;
    private Company company;

    @Before
    public void setUp() throws Exception {
        company = Company.builder().name("Damage Inc.").build();
    }

    @Test
    public void getOrCreate() throws Exception {
        Optional<Company> result = companyService.getOrCreate(company);
        assertThat(result.isPresent()).isTrue();
    }

    @Test
    public void loadByExample_non_existing() throws Exception {
        Optional<Company> company = ((CompanyServiceImpl) companyService).loadByExample(this.company);
        assertThat(company.isPresent()).isFalse();
    }

    @Test
    public void loadByExample_existing() throws Exception {
        Optional<Company> result = companyService.getOrCreate(this.company)
                .flatMap(c -> ((CompanyServiceImpl) companyService).loadByExample(c));
        assertThat(result.isPresent()).isTrue();
    }
}