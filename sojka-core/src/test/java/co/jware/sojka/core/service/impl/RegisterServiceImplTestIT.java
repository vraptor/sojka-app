package co.jware.sojka.core.service.impl;

import co.jware.sojka.core.BaseDataJpaTest;
import co.jware.sojka.core.service.PersonService;
import co.jware.sojka.core.service.entity.AccountService;
import co.jware.sojka.core.service.entity.AccountServiceImpl;
import co.jware.sojka.core.service.entity.ExternalService;
import co.jware.sojka.core.service.entity.ExternalServiceImpl;
import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.junit4.rules.SpringClassRule;
import org.springframework.test.context.junit4.rules.SpringMethodRule;
import org.springframework.transaction.annotation.Transactional;

import co.jware.sojka.core.TestConfig;
import co.jware.sojka.core.domain.Account;
import co.jware.sojka.core.enums.RegistrationType;
import co.jware.sojka.core.service.RegisterService;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@ContextConfiguration
@Transactional
public class RegisterServiceImplTestIT extends BaseDataJpaTest {

    @Autowired
    private
    RegisterService registerService;

    @Test(expected = RuntimeException.class)
    public void register_null_type() throws Exception {
        registerService.register("john", "doe", "jdoe@un.net", null);
    }

    @Test()
    public void register_candidate() throws Exception {
        Account account = registerService.register("john", "doe", "jdoe@un.net",
                RegistrationType.CANDIDATE);
        assertThat(account).isNotNull();
    }

    @Test()
    public void register_hirer() throws Exception {
        Account account = registerService.register("john", "doe", "jdoe@un.net",
                RegistrationType.HIRER);
        assertThat(account).isNotNull();
    }

    @Test()
    public void register_teacher() throws Exception {
        Account account = registerService.register("john", "doe", "jdoe@un.net",
                RegistrationType.TEACHER);
        assertThat(account).isNotNull();
    }

    @Test
    public void attachUser() throws Exception {
        Account account = registerService.register("a", "b", "a.b@x.net", RegistrationType.CANDIDATE);
        Account result = registerService.attachUser(account);
        assertThat(account).isNotEqualTo(result);
    }
}
