package co.jware.sojka.core.service.entity;

import co.jware.sojka.core.BaseDataJpaTest;
import co.jware.sojka.core.Fakers;
import co.jware.sojka.core.TestConfig;
import co.jware.sojka.core.domain.JobListing;
import co.jware.sojka.core.service.impl.CompanyServiceImpl;
import co.jware.sojka.utils.CreatorConfig;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.Page;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
@Transactional
@ContextConfiguration
public class JobListingServiceImplTest extends BaseDataJpaTest {
    @Autowired
    private JobListingService jobListingService;

    @Test
    public void findJobListings() throws Exception {
        Page<JobListing> page = jobListingService.findJobListings();
        assertTrue(page.hasContent());
    }

    @Test
    public void findByUuid() throws Exception {
    }

    @Test
    public void createJobListing() throws Exception {
    }

    @Test
    public void getOrCreate() throws Exception {
    }

    @Test
    public void findByOwner() throws Exception {
    }

    @Test
    public void checkOwnerEligibility() throws Exception {
    }

    @Test
    public void update() throws Exception {
        JobListing jobListing = jobListingService.createJobListing(Fakers.jobListing())
                .orElseThrow(IllegalStateException::new);
        JobListing toUpdate = jobListing.
                withActive(true)
                .withCapacity(3).withFreeCapacity(2)
                .withVersion(jobListing.version());
        JobListing updated = jobListingService
                .update(toUpdate);
        assertThat(updated).isNotEqualTo(jobListing);
    }

    @Test
    public void findByLink() throws Exception {
    }

    @Test
    public void fromMetaLink() throws Exception {
        JobListing jobListing = jobListingService.fromMetaLink(Fakers.metaLink());
        assertThat(jobListing).isNotNull();
    }
}
