package co.jware.sojka.core.domain;

import java.util.UUID;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.objenesis.strategy.StdInstantiatorStrategy;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;

import co.jware.sojka.core.domain.party.Candidate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
public class AccountTest {
    @Autowired
    private ObjectMapper mapper;

    @Test
    public void serialize_candidate() throws Exception {
        User user = User.builder()
                .username("noop")
                .build();
        Account account = Account.builder()
                .owner(Candidate.NULL)
                .user(user)
                .build();
        String asString = mapper.writeValueAsString(account);
        assertThat(asString).isNotBlank();
    }

    @Test
    public void serialization() throws Exception {
        ThreadLocal<Kryo> kryos = ThreadLocal.withInitial(() -> {
            Kryo kryo = new Kryo();
            kryo.setInstantiatorStrategy(new StdInstantiatorStrategy());
            return kryo;
        });
        Kryo kryo = kryos.get();
        Output out = new Output(2048);
        Account account = Account.builder()
                .uuid(UUID.randomUUID())
                .owner(Candidate.NULL)
                .build();
        kryo.writeObject(out, account);
        byte[] bytes = out.getBuffer();
        Account result = kryo.readObject(new Input(bytes), Account.class);
        assertThat(account).isEqualTo(result);
    }
}
