package co.jware.sojka.core.converters;

import co.jware.sojka.core.domain.*;
import co.jware.sojka.core.domain.party.Candidate;
import co.jware.sojka.core.domain.party.Hirer;
import co.jware.sojka.core.domain.wrapped.SkillName;
import co.jware.sojka.entities.core.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.core.convert.ConversionService;
import org.springframework.format.support.DefaultFormattingConversionService;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static co.jware.sojka.core.enums.SkillLevel.EXPERT;
import static co.jware.sojka.core.enums.SkillLevel.INTERMEDIATE;
import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
public class ConversionsTest {

    @Autowired
    ConversionService conversionService;

    @Test
    public void canConvert_jobListing() throws Exception {
        assertThat(conversionService.canConvert(JobListing.class, JobListingBo.class))
                .isTrue();
    }


    @Test
    public void convertJobListing() throws Exception {
        Location location = Location.builder()
                .city("")
                .region("")
                .country("CZ")
                .build();
        JobListing jobListing = JobListing.builder()
                .name("WHO_CARES")
                .owner(Hirer.NULL)
                .addLocation(location)
                .build();
        JobListingBo entity = conversionService.convert(jobListing, JobListingBo.class);
        assertThat(entity).isNotNull();
        assertThat(entity.getOwner()).isNotNull();
    }

//    @Test
//    public void convertPersonalRecommendation() throws Exception {
//        Recommendation<JobListing> recommendation = Recommendation
//                .<JobListing>builder()
//                .subject(JobListing.NULL)
//                .sender(Hirer.NULL)
//                .receiver(Candidate.NULL)
//                .build();
//        RecommendationEntity entity = conversionService.convert(recommendation, RecommendationEntity.class);
//        assertThat(entity).isNotNull();
//    }

    @Test
    public void convertResume() throws Exception {
        Resume resume = Resume.builder()
                .owner(Candidate.NULL)
                .addSkills(Skill.of(SkillName.of("Java"), EXPERT, 12), Skill.of(SkillName.of("JavaScript"), INTERMEDIATE, 3))
                .addTag(Tag.builder().name("JAVA").build())
                .build();
        ResumeBo entity = conversionService.convert(resume, ResumeBo.class);
        assertThat(entity).isNotNull();

    }

    @TestConfiguration
    static class TestConfig {

        @Bean
        public ConversionService conversionService(List<BaseConverter> converters) {
            DefaultFormattingConversionService conversionService = new DefaultFormattingConversionService();
            if (converters != null) {
                converters.forEach((converter) -> {
                    converter.setConversionService(conversionService);
                    conversionService.addConverter(converter);
                });
            }
            return conversionService;
        }

        @Bean
        public BaseConverter<JobListing, JobListingBo> jobListingConverter() {
            return new JobListingConverter();
        }

        @Bean
        public BaseConverter<Hirer, HirerBo> hirerConverter() {
            return new BaseConverter<Hirer, HirerBo>() {
            };
        }

        @Bean
        public BaseConverter<Candidate, CandidateBo> candidateConverter() {
            return new BaseConverter<Candidate, CandidateBo>() {
            };
        }

        @Bean
        public BaseConverter<Location, EmbeddedAddress> locationConverter() {
            return new LocationConverter();
        }

        @Bean
        public BaseConverter<Skill, EmbeddedSkill> skillConverter() {
            return new BaseConverter<Skill, EmbeddedSkill>() {
            };
        }

        @Bean
        public BaseConverter<Resume, ResumeBo> resumeConverter() {
            return new BaseConverter<Resume, ResumeBo>() {
            };
        }
    }
}
