package co.jware.sojka.core.service.geo;

import co.jware.sojka.core.domain.geo.GeoLocation;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
public class GoogleMapsServiceTest {
    @Autowired
    private GoogleMapsService service;

    @Test
    public void geocode() throws Exception {
        Set<GeoLocation> geoLocations = service.geocode("Praha-Strašnice");
        assertThat(geoLocations).isNotEmpty();
    }
}
