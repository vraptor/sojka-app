package co.jware.sojka.core.domain.search;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
public class MetaLinksFilterTest {

    private ObjectMapper mapper;

    @Before
    public void setUp() throws Exception {
        mapper = Jackson2ObjectMapperBuilder.json().build();
    }

    @Test
    public void toMetaSearch() throws Exception {
        MetaLinksFilter filter = MetaLinksFilter.builder()
                .addLocations("praha", "brno")
                .addKeyword("java")
                .build();
        MetaSearch metaSearch = mapper.convertValue(filter, MetaSearch.class);
        assertThat(metaSearch.locations()).containsOnly("brno", "praha");
        assertThat(metaSearch.keywords()).containsOnly("java");
    }

    @Test
    public void toMetaSearch_empty() throws Exception {
        MetaLinksFilter filter = MetaLinksFilter.builder()
                .build();
        MetaSearch metaSearch = mapper.convertValue(filter, MetaSearch.class);
        assertThat(metaSearch.locations()).isEmpty();
        assertThat(metaSearch.keywords()).isEmpty();
    }

    @Test
    public void toMetaSearch_no_keywords() throws Exception {
        MetaLinksFilter filter = MetaLinksFilter.builder()
                .addLocations("praha", "brno")
                .build();
        MetaSearch metaSearch = mapper.convertValue(filter, MetaSearch.class);
        assertThat(metaSearch.locations()).containsOnly("brno", "praha");
        assertThat(metaSearch.keywords()).isEmpty();
    }

    @Test
    public void toMetaSearch_no_locations() throws Exception {
        MetaLinksFilter filter = MetaLinksFilter.builder()
                .addKeywords("java", "docker")
                .build();
        MetaSearch metaSearch = mapper.convertValue(filter, MetaSearch.class);
        assertThat(metaSearch.locations()).isEmpty();
        assertThat(metaSearch.keywords()).containsOnly("docker", "java");
    }
}