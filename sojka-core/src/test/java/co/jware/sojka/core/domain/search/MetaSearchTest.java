package co.jware.sojka.core.domain.search;

import co.jware.sojka.core.enums.search.MetaSearchSource;
import org.junit.Before;
import org.junit.Test;

import java.util.EnumSet;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;


public class MetaSearchTest {

    private MetaSearch search;

    @Before
    public void setUp() throws Exception {
        search = MetaSearch.builder()
                .addKeyword("java")
                .version(1L)
                .build();
    }

    @Test
    public void testEquals() throws Exception {
        assertThat(search.equals(search.withVersion(2L))).isTrue();
    }

    @Test
    public void testHashCode() throws Exception {
        assertThat(search.hashCode())
                .isEqualTo(search.withVersion(2L).hashCode());
    }

    @Test
    public void skipNullKeywords() throws Exception {
        MetaSearch metaSearch = search.withKeywords(null, null, "javascript");
        assertThat(metaSearch.keywords())
                .containsExactlyInAnyOrder("javascript");
        metaSearch = MetaSearch.builder()
                .addKeywords("docker", null, null, "java")
                .build();
        assertThat(metaSearch.keywords())
                .containsExactlyInAnyOrder("docker", "java");
    }

    @Test
    public void equals() throws Exception {
        MetaSearch metaSearchOne = MetaSearch.builder()
                .page(1)
                .sources(EnumSet.of(MetaSearchSource.ANY))
                .addKeyword("java").build();
        MetaSearch metaSearchTwo = MetaSearch.builder()
                .page(1)
                .sources(EnumSet.of(MetaSearchSource.ANY))
                .addKeyword("java").build();
        assertThat(metaSearchOne.equals(metaSearchTwo))
                .isTrue();
        assertThat(metaSearchOne.equals(metaSearchTwo.withUuid(UUID.randomUUID())))
                .isTrue();
    }
}
