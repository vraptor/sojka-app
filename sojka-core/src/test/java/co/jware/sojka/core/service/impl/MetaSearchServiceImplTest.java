package co.jware.sojka.core.service.impl;

import co.jware.sojka.core.TestConfig;
import co.jware.sojka.core.domain.search.MetaSearch;
import co.jware.sojka.core.service.entity.MetaSearchService;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = TestConfig.class)
@Transactional
public class MetaSearchServiceImplTest {
    @Autowired
    private MetaSearchService metaSearchService;

    @Test
    public void findOrCreate_empty() throws Exception {
        MetaSearch empty = MetaSearch.builder().build();
        MetaSearch result = metaSearchService.findOrCreate(empty);
        assertThat(result).isNotNull();
        MetaSearch actual = metaSearchService.findOrCreate(empty);
        assertThat(actual).isEqualTo(result);
    }

    @Test
    @Ignore
    public void findOrCreate() throws Exception {
        MetaSearch search = MetaSearch.builder()
                .addKeywords("java", "javascript")
                .build();
        metaSearchService.storeSearch(search);
        metaSearchService.storeSearch(search.withKeywords("java"));
        MetaSearch result = metaSearchService.findOrCreate(search);
        MetaSearch actual = metaSearchService.findOrCreate(search);
        assertThat(actual).isEqualTo(result);
    }

    @Test
    public void findOrCreate_ambigious() throws Exception {

    }
}