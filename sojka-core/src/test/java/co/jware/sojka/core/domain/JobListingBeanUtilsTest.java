package co.jware.sojka.core.domain;

import com.esotericsoftware.reflectasm.MethodAccess;
import org.apache.commons.lang3.text.WordUtils;
import org.junit.Test;

import com.esotericsoftware.reflectasm.FieldAccess;

import java.util.Arrays;

import static java.util.stream.Collectors.toMap;

public class JobListingBeanUtilsTest {
    @Test
    public void name() throws Exception {
        FieldAccess access = FieldAccess.get(JobListing.class);
        String[] names = access.getFieldNames();
        Arrays.stream(names)
                .forEach(System.out::println);
        MethodAccess methodAccess = MethodAccess.get(JobListing.class);
        String[] methodNames = methodAccess.getMethodNames();
        Arrays.stream(methodNames)
                .distinct()
                .filter(n -> (n.startsWith("with")))
                .map(methodAccess::getIndex)
                .sorted().collect(toMap(s -> s, s -> methodNames[s]))
                .forEach((k, v) -> System.out.println(String.format("%d %s", k, v)));
        Class[][] parameterTypes = methodAccess.getParameterTypes();
        Arrays.stream(parameterTypes)
                .forEach(c -> {
                    System.out.println(c);
                    Arrays.stream(c)
                            .forEach(cc -> System.out.println(cc.getCanonicalName()));
                });
    }
}