package co.jware.sojka.core.domain.pipelines;

import co.jware.sojka.core.domain.party.Hirer;
import co.jware.sojka.core.domain.party.Party;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@ContextConfiguration
public class PipelineTest {
    @Autowired
    private ObjectMapper mapper;

    @Test
    public void ownerPipeline() throws Exception {
        OwnerPipeline ownerPipeline = OwnerPipeline
                .builder()
                .name("BOGUS")
                .owner((Party) Hirer.NULL)
                .build();
        String asString = mapper.writeValueAsString(ownerPipeline);
        System.err.println(asString);
    }
}
