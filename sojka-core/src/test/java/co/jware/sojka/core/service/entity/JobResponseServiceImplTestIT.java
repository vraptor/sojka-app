package co.jware.sojka.core.service.entity;

import java.util.Optional;
import java.util.UUID;

import co.jware.sojka.core.Fakers;
import co.jware.sojka.core.domain.party.Candidate;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import co.jware.sojka.core.TestConfig;
import co.jware.sojka.core.domain.JobListing;
import co.jware.sojka.core.domain.JobResponse;
import co.jware.sojka.core.domain.party.Party;
import co.jware.sojka.core.service.PersonService;
import co.jware.sojka.entities.core.CandidateBo;
import co.jware.sojka.entities.core.JobListingBo;
import co.jware.sojka.utils.Creator;
import co.jware.sojka.utils.CreatorConfig;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@DataJpaTest
@ContextConfiguration(classes = {TestConfig.class, CreatorConfig.class})
public class JobResponseServiceImplTestIT {

    private Candidate candidate;

    @Before
    public void setUp() throws Exception {
        candidate = Fakers.candidate().withUuid(UUID.randomUUID());
    }

    @Test
    public void findByCandidate() throws Exception {
    }

    @Test
    public void getInterviews() throws Exception {
    }

    @Test
    public void getTests() throws Exception {
    }

    @Test
    public void findJobResponse() throws Exception {
    }

    @Autowired
    private JobResponseService jobResponseService;

    @Autowired
    private PersonService personService;

    @Autowired
    private JobListingService jobListingService;
    @Autowired
    private Creator creator;

    @Test
    public void createJobResponse() throws Exception {
        CandidateBo candidateEntity = creator.save(new CandidateBo());
        JobListingBo jobListingEntity = creator.save(new JobListingBo());
        Party candidate = personService.findByUuid(candidateEntity.getUuid());
        Optional<JobListing> jobListing = jobListingService.findByUuid(jobListingEntity.getUuid());
        Optional<JobResponse> jobResponse = jobResponseService.createJobResponse(JobResponse
                .builder()
                .respondent(candidate)
                .jobListing(jobListing.orElseThrow(RuntimeException::new))
                .build());
        JobResponse result = jobResponse.map(r -> r).orElseThrow(RuntimeException::new);
        assertThat(result).isNotNull();
    }

    @Test
    public void findByRespondent() throws Exception {
        Page<JobResponse> page = jobResponseService.findByRespondent(candidate,
                new PageRequest(0, 10));
        assertThat(page.hasContent()).isFalse();
    }

    @Test
    public void findByJobListing() throws Exception {
        JobListing jobListing = Fakers.jobListing().withUuid(UUID.randomUUID());
        Page<JobResponse> page = jobResponseService.findByJobListing(jobListing, new PageRequest(0, 01));
        assertThat(page.hasContent()).isFalse();
    }

}