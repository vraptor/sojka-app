package co.jware.sojka.core.processors;

import co.jware.sojka.core.domain.Tag;
import co.jware.sojka.core.domain.wrapped.TagWeight;
import mockit.Tested;
import mockit.integration.junit4.JMockit;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.math.BigDecimal;
import java.util.Set;

import static com.google.common.collect.Sets.newHashSet;
import static org.junit.Assert.assertEquals;

@RunWith(JMockit.class)
public class DefaultTagProcessorTest {
    @Tested
    DefaultTagProcessor tagProcessor;

    @Test
    public void evaluateTags_nulls() throws Exception {
        Set<Tag> etalonTags = null;
        Set<Tag> tags = null;
        BigDecimal weight = tagProcessor.evaluateTags(tags, etalonTags);
        assertEquals(BigDecimal.ZERO, weight);
    }

    @Test
    public void evaluateTags_equal_sets() throws Exception {
        Tag tagA = Tag.builder()
                .name("A Tag")
                .weight(TagWeight.of(new BigDecimal(1.52)))
                .build();
        Tag tagB = tagA.withName("B Tag")
                .withWeight(TagWeight.of(new BigDecimal(1.77)));
        BigDecimal tagWeight = tagProcessor.evaluateTags(
                newHashSet(tagB, tagA),
                newHashSet(tagA, tagB));
        assertEquals(1.65d, tagWeight.doubleValue(), 0.1d);
    }


    @Test
    public void evaluateTags() throws Exception {
        Tag tagA = Tag.builder()
                .name("A Tag")
                .weight(TagWeight.of(new BigDecimal(1.52)))
                .build();
        Tag tagB = tagA.withName("B Tag")
                .withWeight(TagWeight.of(new BigDecimal(1.77)));
    }
}