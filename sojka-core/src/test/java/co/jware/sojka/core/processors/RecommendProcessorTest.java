package co.jware.sojka.core.processors;

import mockit.Tested;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import co.jware.sojka.core.domain.JobListing;
import co.jware.sojka.core.domain.Recommendation;
import co.jware.sojka.core.domain.Recommendation;
import co.jware.sojka.core.domain.party.Party;
import co.jware.sojka.core.domain.party.Person;
import co.jware.sojka.core.enums.RecommendationStatus;
import co.jware.sojka.core.exceptions.InvalidStatusRequestedException;
import mockit.Injectable;
import mockit.Mock;
import mockit.MockUp;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import static co.jware.sojka.core.enums.RecommendationStatus.PENDING;
import static co.jware.sojka.core.enums.RecommendationStatus.RESPONSE_SENT;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.isA;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
public class RecommendProcessorTest {

    @Injectable
    private Person sender;
    @Injectable
    private Party receiver;
    @Tested
    private RecommendProcessor processor;
    @Injectable
    private JobListing jobListing;

    @Test
    public void createRecommendation() throws Exception {
        Recommendation recommendation = processor.createRecommendation(sender,
                receiver, jobListing).join();
        assertThat(recommendation, isA(Recommendation.class));
        assertThat(recommendation.sender(), notNullValue());
        assertThat(recommendation.receiver(), notNullValue());
//        assertThat(recommendation.subject(), notNullValue());

    }

    private Recommendation buildRecommendation() {
        return Recommendation.<JobListing>builder()
                .subject(jobListing)
                .sender(sender)
                .receiver(receiver)
                .build();
    }

    @Test
    public void createRecommendation_status() throws Exception {
        Recommendation recommendation = processor.createRecommendation(sender, receiver, jobListing).join();
        assertTrue(recommendation.status() == PENDING);
    }

    @Test
    public void recommendationSenderAndReceiverAreDifferent() throws Exception {
        Recommendation recommendation = (Recommendation) processor.createRecommendation(sender,
                receiver, jobListing).join();
        assertNotEquals(recommendation.sender(), recommendation.receiver());
    }

    @Test(expected = IllegalStateException.class)
    @Ignore
    public void createRecommendationFailsOnSameSenderAndReceiver() throws Exception {
        processor.createRecommendation(sender, sender, jobListing);
    }

    @Test
    public void addRecommendation_duplicate(@Injectable Recommendation recommendation) throws Exception {
        assertTrue(processor.addRecommendation(recommendation));
        assertFalse(processor.addRecommendation(recommendation));
    }

    @Test
    public void recommendationHasUuid() throws Exception {
        Recommendation recommendation = buildRecommendation();
        assertNull(recommendation.uuid());
    }

    @Test
    public void updateStatus() throws Exception {
        Recommendation recOne = buildRecommendation();
        Recommendation result = processor.updateStatus(recOne, RESPONSE_SENT);
        assertTrue(result.status() == RESPONSE_SENT);
    }

    @Test(expected = InvalidStatusRequestedException.class)
    public void updateStatus_not_allowed() throws Exception {
        Recommendation recOne = buildRecommendation();
        Recommendation result = processor.updateStatus(recOne, RESPONSE_SENT);
        assertTrue(result.status() == RESPONSE_SENT);
        processor.updateStatus(result, PENDING);
    }

    @Test
    public void updateStatus_same_status() throws Exception {
        Recommendation recOne = buildRecommendation();
        Recommendation result = processor.updateStatus(recOne, RESPONSE_SENT);
        assertTrue(result.status() == RESPONSE_SENT);
        processor.updateStatus(result, RESPONSE_SENT);
    }

    @Test
    public void updateStatus_null_status() throws Exception {
        Recommendation recOne = buildRecommendation();
        Recommendation result = processor.updateStatus(recOne, null);
        assertEquals(recOne, result);
    }


}
