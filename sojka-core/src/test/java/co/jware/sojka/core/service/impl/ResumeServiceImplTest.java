package co.jware.sojka.core.service.impl;

import co.jware.sojka.core.BaseDataJpaTest;
import co.jware.sojka.core.Fakers;
import co.jware.sojka.core.domain.Resume;
import co.jware.sojka.core.domain.party.Candidate;
import co.jware.sojka.core.domain.party.Person;
import co.jware.sojka.core.domain.party.Submitter;
import co.jware.sojka.core.service.PersonService;
import co.jware.sojka.core.service.ResumeService;
import co.jware.sojka.core.service.TransactionHelper;
import co.jware.sojka.core.service.entity.AccountService;
import co.jware.sojka.core.service.entity.AccountServiceImpl;
import co.jware.sojka.repository.ResumeRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;


@RunWith(SpringRunner.class)
@Transactional
@ContextConfiguration
public class ResumeServiceImplTest extends BaseDataJpaTest {
    @Autowired
    private ResumeService resumeService;

    @Autowired
    private PersonService personService;
    private Person submitter;
    private Person candidate;

    @Before
    public void setUp() throws Exception {
        submitter = personService.createPerson(Fakers.hirer());
        candidate = personService.createPerson(Fakers.candidate());
    }

    @Test
    public void createResume() throws Exception {
    }

    @Test
    public void hasSupportedSubmitter() throws Exception {
    }

    @Test
    public void submitResume() throws Exception {
        Resume resume = Fakers.resume()
                .withSubmitter((Submitter) submitter)
                .withOwner((Candidate) candidate);
        Optional<Resume> result = resumeService.submitResume(resume);
        result.ifPresent(r -> {
            assertThat(r.uuid()).isNotNull();
        });
    }

    @Test
    public void findByOwner() throws Exception {
        Optional<Resume> resumeOptional = resumeService.submitResume(Fakers.resume()
                .withOwner((Candidate) candidate)
                .withSubmitter(((Submitter) submitter)));
        assertThat(resumeOptional.isPresent()).isTrue();
        resumeOptional.ifPresent(r -> {
            Candidate candidate = Optional.ofNullable(r.owner())
                    .orElseThrow(IllegalStateException::new);
            assertThat(candidate.uuid()).isNotNull();
        });
        Page<Resume> page = resumeService.findByOwner((Candidate) candidate, new PageRequest(0, 5));
        assertThat(page.hasContent()).isTrue();
    }

    @Test
    public void findByOwner_not_found() throws Exception {
        Candidate candidate = Fakers.candidate().withUuid(UUID.randomUUID());
        Page<Resume> page = resumeService.findByOwner(candidate, new PageRequest(0, 5));
        assertThat(page).isNotNull();
        assertThat(page.hasContent()).isFalse();
    }

    @Test
    public void findBySubmitter() throws Exception {
        Optional<Resume> resumeOptional = resumeService.submitResume(Fakers.resume()
                .withOwner((Candidate) candidate)
                .withSubmitter((Submitter) submitter));
        assertThat(resumeOptional.isPresent()).isTrue();
        resumeOptional.ifPresent(r -> {
            Candidate candidate = Optional.ofNullable(r.owner())
                    .orElseThrow(IllegalStateException::new);
            assertThat(candidate.uuid()).isNotNull();
        });
        Page<Resume> page = resumeService.findBySubmitter((Submitter) submitter, new PageRequest(0, 5));
        assertThat(page.hasContent()).isTrue();
    }

    @Test
    public void findResume() throws Exception {
    }
}
