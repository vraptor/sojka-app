package co.jware.sojka.core.domain.invitation;

import co.jware.sojka.core.domain.*;
import co.jware.sojka.core.domain.party.Candidate;
import co.jware.sojka.core.domain.party.Hirer;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.jackson.Jackson2ObjectMapperBuilderCustomizer;
import org.springframework.boot.test.autoconfigure.json.JsonTest;
import org.springframework.boot.test.json.JacksonTester;
import org.springframework.boot.test.json.JsonContent;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.Instant;
import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.util.Date;

import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_EMPTY;
import static com.fasterxml.jackson.databind.SerializationFeature.WRITE_DATES_AS_TIMESTAMPS;
import static org.assertj.core.api.Java6Assertions.assertThat;

@RunWith(SpringRunner.class)
@JsonTest()
public class InvitationJsonTest {

    @Value("classpath:/json/invitation-evaluation.json")
    Resource invitationEvaluationResource;

    @Value("classpath:/json/invitation-interview.json")
    Resource invitationInterviewResource;

    @Autowired
    private JacksonTester<Invitation> tester;

    @Test
    public void serialize_Evaluation() throws Exception {
        Candidate candidate = Candidate.builder()
                .firstName("a")
                .lastName("z")
                .email("x@y.net")
                .build();
        Evaluation event = Evaluation.builder()
                .candidate(candidate)
                .title("primary")
                .testDate(Date.from(Instant.now()))
                .order(1)
                .build();
        Invitation<Evaluation> invitation = EvaluationInvitation.builder()
                .event(event)
                .receiver(candidate)
                .sender(Hirer.NULL)
                .eventDateTime(OffsetDateTime.now().plusDays(3))
                .build();
        JsonContent<Invitation> jsonContent = tester.write(invitation);
        String json = jsonContent.getJson();
        assertThat(json).isNotEmpty();
        assertThat(jsonContent).extractingJsonPathValue("@.event.@type")
                .isEqualTo("EVALUATION");
    }

    @Test
    public void serialize_Interview() throws Exception {
        Interview interview = Interview.builder()
                .candidate(Candidate.NULL)
                .jobListing(JobListing.NULL)
                .interviewDate(Date.from(OffsetDateTime.now().plusDays(3).toInstant()))
                .order(1)
                .build();
        Invitation<Interview> invitation = InterviewInvitation.builder()
                .receiver(interview.candidate())
                .sender(Hirer.NULL)
                .event(interview).eventDateTime(OffsetDateTime.ofInstant(interview.interviewDate().toInstant(), ZoneId.systemDefault()))
                .build();
        JsonContent<Invitation> jsonContent = tester.write(invitation);
        String json = jsonContent.getJson();
        assertThat(json).isNotEmpty();
        assertThat(jsonContent)
                .extractingJsonPathValue("@.event.@type")
                .isEqualTo("INTERVIEW");
    }

    @Test
    public void deserialize_Interview() throws Exception {
        Invitation invitation = tester.read(invitationInterviewResource).getObject();
        assertThat(invitation).isNotNull();
        assertThat(invitation.event()).isInstanceOf(Interview.class);
    }

    @Test
    public void deserialize_Evaluation() throws Exception {
        Invitation invitation = tester.read(invitationEvaluationResource).getObject();
        assertThat(invitation.event()).isInstanceOf(Evaluation.class);
    }
}
