package co.jware.sojka.core.domain.pipelines;

import co.jware.sojka.core.domain.JobListing;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
public class CardTest {
    @Autowired
    private ObjectMapper mapper;

    @Test
    public void name() throws Exception {
        Card<JobListing> card = JobListingCard.builder()
                .content(JobListing.NULL)
                .build();
        String asString = mapper.writeValueAsString(card);
        System.out.println(asString);
    }
}
