package co.jware.sojka.core.service.entity;

import java.util.UUID;

import org.junit.ClassRule;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.rules.SpringClassRule;
import org.springframework.test.context.junit4.rules.SpringMethodRule;
import org.springframework.transaction.annotation.Transactional;

import co.jware.sojka.core.TestConfig;
import co.jware.sojka.core.domain.Company;
import co.jware.sojka.core.domain.party.Hirer;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(classes = TestConfig.class)
@Transactional
@ActiveProfiles("test")
public class HirerServiceImplTestIT {

    @Rule
    public SpringMethodRule methodRule = new SpringMethodRule();
    @ClassRule
    public static final SpringClassRule classRule = new SpringClassRule();
    @Autowired
    HirerService hirerService;

    @Test
    public void getOrCreate() throws Exception {
        Hirer hirer = Hirer.builder()
                .firstName("A")
                .lastName("B")
                .email("a@x.net")
                .uuid(UUID.randomUUID())
                .build();
        Hirer result = hirerService.getOrCreate(hirer);
        assertThat(result.uuid()).isNotNull();
    }
    @Test
    @Ignore
    public void getOrCreate_with_company() throws Exception {
        Company company = Company.builder().name("Damage Inc.").build();
        Hirer hirer = Hirer.builder()
                .firstName("A")
                .lastName("B")
                .email("a@x.net")
                .uuid(UUID.randomUUID())
                .company(company)
                .build();
        Hirer result = hirerService.getOrCreate(hirer);
        assertThat(result).isNotEqualTo(hirer);
    }
}