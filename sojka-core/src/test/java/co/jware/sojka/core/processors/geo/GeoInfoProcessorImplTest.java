package co.jware.sojka.core.processors.geo;

import io.reactivex.Flowable;
import io.reactivex.schedulers.Schedulers;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
public class GeoInfoProcessorImplTest {
    @Test
    public void name() throws Exception {
        Flowable.range(1, 10)
                .flatMap(i -> Flowable.just(i)
                        .subscribeOn(Schedulers.computation())
                        .map(n -> {
                            System.out.println(Thread.currentThread().getName());
                            return n;
                        }))
                .subscribe(System.out::println);
        Thread.sleep(50);
    }

}