package co.jware.sojka.core.service.entity;

import co.jware.sojka.core.BaseDataJpaTest;
import co.jware.sojka.core.domain.Account;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@Transactional
@ContextConfiguration
public class AccountServiceImplTestIT extends BaseDataJpaTest {
    @Autowired
    private AccountService accountService;


    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Test
    public void findByOwnerUuid() throws Exception {
        List<String> uuids = jdbcTemplate.queryForList("select owner_id from account", String.class);
        UUID uuid = UUID.fromString(uuids.get(0));
        Optional<Account> account = accountService.findByOwnerUuid(uuid);
        assertThat(account.isPresent()).isTrue();
    }

    @Test
    public void findByUsername() throws Exception {
        List<String> userNames = jdbcTemplate.queryForList("select username from users", String.class);
        Optional<Account> account = accountService.findByUsername(userNames.get(0));
        assertThat(account.isPresent()).isTrue();
    }
}
