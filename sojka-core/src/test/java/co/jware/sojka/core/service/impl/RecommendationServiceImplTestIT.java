package co.jware.sojka.core.service.impl;

import co.jware.sojka.core.BaseDataJpaTest;
import co.jware.sojka.core.Fakers;
import co.jware.sojka.core.domain.JobListing;
import co.jware.sojka.core.domain.Recommendation;
import co.jware.sojka.core.domain.party.Candidate;
import co.jware.sojka.core.domain.party.External;
import co.jware.sojka.core.domain.party.Party;
import co.jware.sojka.core.domain.party.Person;
import co.jware.sojka.core.service.PersonService;
import co.jware.sojka.core.service.entity.*;
import co.jware.sojka.entities.core.AccountBo;
import co.jware.sojka.repository.RecommendationRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@ContextConfiguration
public class RecommendationServiceImplTestIT extends BaseDataJpaTest {

    @Autowired
    private RecommendationService service;

    @Autowired
    private JobListingService jobListingService;
    @Autowired
    private PersonService personService;
    @Autowired
    private ExternalService externalService;
    @Autowired
    private JdbcTemplate jdbcTemplate;

    private JobListing jobListing;
    private Person candidate;
    private External external;

    @Before
    public void setUp() throws Exception {
        jobListing = jobListingService.createJobListing(Fakers.jobListing())
                .orElseThrow(IllegalStateException::new);
        candidate = personService.createPerson(Fakers.candidate());
        external = externalService.createExternal(Fakers.external());
    }

    @Test
    public void findPersonRecommendations() throws Exception {
    }

    @Test
    public void findByReceiver() throws Exception {
        Candidate candidate = Fakers.candidate().withUuid(UUID.randomUUID());
        Page<Recommendation> page = service.findByReceiver(candidate,  PageRequest.of(0, 10));
        assertThat(page.hasContent()).isFalse();
    }

    @Test
    public void findBySender() throws Exception {
        Page<Recommendation> page = service.findBySender(candidate, new PageRequest(0, 10));
        assertThat(page.hasContent()).isFalse();
    }

    @Test
    public void findByUuid() throws Exception {
    }

    @Test
    public void createRecommendation() throws Exception {
        Party receiver = personService.findByEmail("candidate@sojka.co")
                .orElseThrow(IllegalStateException::new);
        JobListing subject = jobListingService.createJobListing(Fakers.jobListing())
                .orElseThrow(IllegalStateException::new);
        External sender = externalService.createExternal(Fakers.external());
        Recommendation<Object> recommendation = Recommendation.builder()
                .subject(subject)
                .receiver(receiver)
                .sender(sender)
                .build();
        service.createRecommendation(recommendation);
    }

    @Test
    public void findRecommendation() throws Exception {
    }

    @Test
    public void recommendSubject() throws Exception {
        Recommendation recommendation = service.recommendSubject(candidate, external, jobListing.uuid());
        assertThat(recommendation).isNotNull();

    }

    @Test
    public void delete() throws Exception {
        List<AccountBo> accounts = jdbcTemplate.query("select * from account", new BeanPropertyRowMapper<>(AccountBo.class));
        assertThat(accounts).isNotNull();
    }

    @Test
    public void findBySubject() throws Exception {
        Recommendation recommendation = service.recommendSubject(candidate, external, jobListing.uuid());
        assertThat(recommendation).isNotNull();
        Page<Recommendation> page = service.findBySubject(jobListing.uuid(), new PageRequest(0, 10));
        assertThat(page.hasContent()).isTrue();
    }

    @Test
    public void findByJobListing() throws Exception {
        Recommendation recommendation = service.recommendSubject(candidate, external, jobListing.uuid());
        assertThat(recommendation).isNotNull();
        Page<Recommendation> page = service.findByJobListing(jobListing, new PageRequest(0, 10));
        assertThat(page.hasContent()).isTrue();
    }
}
