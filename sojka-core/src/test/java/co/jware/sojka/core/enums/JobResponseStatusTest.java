package co.jware.sojka.core.enums;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;

import java.util.Arrays;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;


public class JobResponseStatusTest {
    @Test
    public void next_last() throws Exception {
        JobResponseStatus actual = JobResponseStatus.OFFER_REJECTED.next();
        JobResponseStatus expected = JobResponseStatus.OFFER_REJECTED;
        assertEquals(expected, actual);
    }

    @Test
    public void next() throws Exception {
        JobResponseStatus actual = JobResponseStatus.PENDING.next();
        JobResponseStatus expected = JobResponseStatus.RESPONSE_SENT;
        assertEquals(expected, actual);
    }

    @Test
    public void key() throws Exception {
        boolean allMatch = Arrays.stream(JobResponseStatus.values())
                .allMatch(s -> StringUtils.isNoneBlank(s.key()));
        assertThat(allMatch).isTrue();
    }
}