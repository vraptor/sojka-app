package co.jware.sojka.core.service.pipelines.impl;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Stream;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.fasterxml.jackson.databind.ObjectMapper;

import co.jware.sojka.core.BaseDataJpaTest;
import co.jware.sojka.core.domain.Account;
import co.jware.sojka.core.domain.JobListing;
import co.jware.sojka.core.domain.party.Candidate;
import co.jware.sojka.core.domain.party.Hirer;
import co.jware.sojka.core.domain.pipelines.JobPipeline;
import co.jware.sojka.core.domain.pipelines.OwnerPipeline;
import co.jware.sojka.core.domain.pipelines.PersonPipeline;
import co.jware.sojka.core.domain.pipelines.Stage;
import co.jware.sojka.core.domain.pipelines.SystemPipeline;
import co.jware.sojka.core.service.entity.AccountService;
import co.jware.sojka.core.service.pipelines.PipelineService;

import static java.util.stream.Collectors.toList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@ContextConfiguration
@Ignore
public class PipelineServiceImplTest extends BaseDataJpaTest {

    @Autowired
    private PipelineService pipelineService;
    @Autowired
    private AccountService accountService;

    @Test
    public void generateOwnerPipeline() throws Exception {
        SystemPipeline systemPipeline = SystemPipeline.builder()
                .name("_SYSTEM_")
                .uuid(UUID.randomUUID())
                .build();
        OwnerPipeline ownerPipeline = pipelineService.generateOwnerPipeline(systemPipeline, Hirer.NULL);
        assertThat(systemPipeline.uuid()).isNotEqualTo(ownerPipeline.uuid());
        assertThat(ownerPipeline.parent()).isEqualTo(systemPipeline);
    }

    @Test
    public void generateJobPipeline() throws Exception {
        OwnerPipeline ownerPipeline = OwnerPipeline.builder()
                .uuid(UUID.randomUUID())
                .name("_OWNER_")
                .owner(Hirer.NULL)
                .build();
        JobPipeline jobPipeline = pipelineService.generateJobPipeline(ownerPipeline, JobListing.NULL);
        assertThat(jobPipeline.uuid()).isNotEqualTo(ownerPipeline.uuid());
        assertThat(jobPipeline.parent()).isEqualTo(ownerPipeline);
    }

    @Test
    public void generatePersonPipeline() throws Exception {
        JobPipeline jobPipeline = JobPipeline.builder()
                .uuid(UUID.randomUUID())
                .name("_JOB_")
                .jobListing(JobListing.NULL)
                .build();
        PersonPipeline personPipeline = pipelineService.generatePersonPipeline(jobPipeline, Candidate.NULL);
        assertThat(personPipeline.uuid()).isNotEqualTo(jobPipeline.uuid());
        assertThat(personPipeline.parent()).isEqualTo(jobPipeline);
    }

    @Test
    public void createPipeline() throws Exception {
        SystemPipeline pipeline = SystemPipeline.builder()
                .name("BOGUS")
                .build();
        Account account = Account.builder()
                .owner(Hirer.NULL)
                .build();
        when(accountService.findByUsername(anyString()))
                .thenReturn(Optional.of(account));
        pipelineService.createPipeline(pipeline, "hirer@sojka.co");
    }

    @Test
    @Ignore
    public void stages() throws Exception {
        int pos[] = {0};
        List<Stage> stages = Stream.of("stage1", "stage2", "stage3")
                .map(name -> Stage.builder().name(name).position(pos[0]++).build())
                .collect(toList());
        SystemPipeline systemPipeline = SystemPipeline.builder().name("_SYSTEM_")
                .stages(stages)
                .build();
        OwnerPipeline ownerPipeline = pipelineService.generateOwnerPipeline(systemPipeline, Hirer.NULL);
        List<UUID> newUuidList = ownerPipeline.stages().stream()
                .map(Stage::uuid).collect(toList());
        List<UUID> oldUuidList = systemPipeline.stages().stream()
                .map(Stage::uuid).collect(toList());
        assertThat(newUuidList).doesNotContainAnyElementsOf(oldUuidList);
    }
}
