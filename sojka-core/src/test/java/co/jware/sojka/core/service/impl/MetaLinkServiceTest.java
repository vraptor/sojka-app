package co.jware.sojka.core.service.impl;

import co.jware.sojka.core.BaseDataJpaTest;
import co.jware.sojka.core.domain.search.MetaLink;
import co.jware.sojka.core.service.MetaLinkService;
import co.jware.sojka.core.service.MetaSearchLinkService;
import com.hazelcast.core.HazelcastInstance;
import io.reactivex.observers.TestObserver;
import io.reactivex.subscribers.TestSubscriber;
import io.vertx.core.eventbus.EventBus;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@Sql(scripts = "classpath:/sql/meta_link_geo_locations.sql")
@Transactional
@ContextConfiguration
public class MetaLinkServiceTest extends BaseDataJpaTest {
    @Autowired
    private MetaLinkService metaLinkService;

    @Test
    public void findUnresolvedGeoInfo() throws Exception {
        Page<MetaLink> page = metaLinkService.findUnresolvedGeoInfo(new PageRequest(0, 20));
        assertThat(page.hasContent()).isTrue();
    }

    @Test

    public void findResolvedGeoInfo() throws Exception {
        Page<MetaLink> page = metaLinkService.findResolvedGeoInfo("praha", new PageRequest(0, 20));
        assertThat(page.hasContent()).isTrue();

    }

    @Test
    @Sql(scripts = "classpath:sql/meta_links.sql")
    public void allMetaLinks() throws Exception {
        TestObserver<MetaLink> ts = new TestObserver<>();
        metaLinkService.allMetaLinks(0)
                .filter(m -> m.address() != null)
                .filter(m -> m.address()
                        .toLowerCase()
                        .contains("praha"))
                .doOnNext(System.out::println)
                .take(50)
                .subscribe(ts);
        ts.assertComplete();
        ts.assertValueCount(11);
    }

    @Test
    public void findResolvedGeoInfo_multiple() throws Exception {
        Page<MetaLink> page = metaLinkService.findResolvedGeoInfo("- Praha - Praha 2", new PageRequest(0, 20));
        assertThat(page.hasContent()).isTrue();
    }

    @Test
    @Ignore
    public void name() throws Exception {
        MetaLink metaLink = MetaLink.builder()
                .href("n/a")
                .title("n/a")
                .build();
        TestSubscriber<MetaLink> ts = metaLinkService.metaLinks(metaLink, new PageRequest(0, 20))
                .take(35)
                .test();
        ts.awaitTerminalEvent();
        ts.assertValueCount(35);
    }

    @Profile("hsql")
    @Configuration
    static class TestConfig {
        @MockBean
        private HazelcastInstance hazelcastInstance;
        @MockBean
        private EventBus eventBus;
        @MockBean
        private MetaSearchLinkService metaSearchLinkService;

        @Bean
        public MetaLinkService metaLinkService() {
            return new MetaLinkServiceImpl();
        }

    }
}