package co.jware.sojka.core.domain.search;

import java.util.Set;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import co.jware.sojka.core.Fakers;
import org.javers.core.Javers;
import org.javers.core.JaversBuilder;
import org.javers.core.diff.Diff;
import org.junit.Before;
import org.junit.Test;

import com.fasterxml.jackson.core.type.TypeReference;
import com.google.common.collect.Sets;
import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;

import static org.assertj.core.api.Assertions.assertThat;

public class MetaLinkTest {

    private final TypeReference<MetaLink> typeRef = new TypeReference<MetaLink>() {
    };
    private MetaLink metaLink;
    private Javers javers;

    @Before
    public void setUp() throws Exception {
        metaLink = MetaLink.builder()
                .href("n/a")
                .title("n/a")
                .addKeyword("javascript")
                .build();
        javers = JaversBuilder.javers().build();
    }

    @Test
    public void hazelcastMap() throws Exception {
        HazelcastInstance hazelcast = Hazelcast.newHazelcastInstance();
        IMap<MetaLink, Boolean> metaLinksMap = hazelcast.getMap("meta_links");
        boolean result = metaLinksMap.tryPut(metaLink, true, 500, TimeUnit.MILLISECONDS);
        assertThat(result).isTrue();
        MetaLink metaLinkKeywords = MetaLink.copyOf(metaLink).withKeywords("java");
        result = metaLinksMap.tryPut(metaLinkKeywords, true, 500, TimeUnit.MILLISECONDS);
        assertThat(result).isTrue();
        assertThat(metaLinksMap.containsKey(metaLink));
    }

    @Test
    public void equals_keywords() throws Exception {
        Set<String> keywords = metaLink.keywords();
        keywords = Sets.newHashSet(keywords);
        keywords.add("docker");
        MetaLink metaLinkKeywords = this.metaLink.withKeywords(keywords);
        assertThat(metaLink).isNotEqualTo(metaLinkKeywords);
    }

    @Test
    public void equals_version() throws Exception {
        MetaLink metaLinkVersioned = this.metaLink.withVersion(17L);
        assertThat(metaLink).isEqualTo(metaLinkVersioned);
    }

    @Test
    public void equals_uuid() throws Exception {
        MetaLink metaLinkOne = this.metaLink.withUuid(UUID.randomUUID());
        MetaLink metaLinkTwo = this.metaLink.withUuid(UUID.randomUUID());
        assertThat(metaLinkOne).isEqualTo(metaLinkTwo);
    }

    @Test
    public void diff() throws Exception {
        MetaLink left = Fakers.metaLink().withUuid(UUID.randomUUID());
        Diff diff = javers.compare(left.withKeywords("java", "javascript"), Fakers.metaLink().withAddress("Praha"));
        assertThat(diff).isNotNull();
        assertThat(diff.hasChanges()).isTrue();
    }

    @Test
    public void diff_no_changes() throws Exception {
        MetaLink metaLink = Fakers.metaLink();
        Diff diff = javers.compare(metaLink, metaLink);
        assertThat(diff.hasChanges()).isFalse();
    }
}