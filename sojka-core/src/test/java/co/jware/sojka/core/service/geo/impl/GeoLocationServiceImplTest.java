package co.jware.sojka.core.service.geo.impl;

import java.util.Set;

import co.jware.sojka.core.BaseDataJpaTest;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.fasterxml.jackson.databind.ObjectMapper;

import co.jware.sojka.core.service.geo.GeoLocationService;
import co.jware.sojka.core.service.geo.GoogleMapsService;
import co.jware.sojka.repository.geo.GeoLocationRepository;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@ContextConfiguration
public class GeoLocationServiceImplTest extends BaseDataJpaTest {

    @Autowired
    private GeoLocationServiceImpl service;

    @Test
    public void preProcessAddress_dashes() throws Exception {
        String address = "- Břeclav, Jihomoravský - Mikulov, Jihomoravský";
        Set<String> locations = service.preProcessAddress(address);
        assertThat(locations).hasSize(2);
        assertThat(locations)
                .containsExactlyInAnyOrder("Břeclav, Jihomoravský", "Mikulov, Jihomoravský");
    }

    @Test
    public void preProcessAddress_dash_only() throws Exception {
        String address = "- ";
        Set<String> locations = service.preProcessAddress(address);
        assertThat(locations).isEmpty();
    }

    @Test
    public void preProcessAddress_null() throws Exception {
        Set<String> locations = service.preProcessAddress(null);
        assertThat(locations).isEmpty();
    }

    @Test
    public void preProcessAddress_n_a() throws Exception {
        Set<String> locations = service.preProcessAddress("n/a");
        assertThat(locations).isEmpty();
    }

    @Test
    public void preProcessAddress_plus() throws Exception {
        String address = "Moravskoslezský kraj + 2 další lokality";
        Set<String> locations = service.preProcessAddress(address);
        assertThat(locations).hasSize(1);
        assertThat(locations)
                .containsExactlyInAnyOrder("Moravskoslezský kraj");
    }
}
