package co.jware.sojka.core.domain.invitation;

import co.jware.sojka.core.domain.Evaluation;
import co.jware.sojka.core.domain.EvaluationInvitation;
import co.jware.sojka.core.domain.Invitation;
import co.jware.sojka.core.domain.party.Candidate;
import co.jware.sojka.core.domain.party.Hirer;
import org.junit.Test;

import java.time.Instant;
import java.time.OffsetDateTime;
import java.util.Date;

import static org.assertj.core.api.Java6Assertions.assertThat;


public class InvitationTest {
    @Test
    public void create() throws Exception {
        Candidate candidate = Candidate.builder()
                .firstName("a")
                .lastName("z")
                .email("x@y.net")
                .build();
        Evaluation event = Evaluation.builder()
                .candidate(candidate)
                .title("primary")
                .testDate(Date.from(Instant.now()))
                .order(1)
                .build();
        Invitation<Evaluation> invitation = EvaluationInvitation.builder()
                .event(event)
                .receiver(candidate)
                .sender(Hirer.NULL)
                .eventDateTime(OffsetDateTime.now().plusDays(3))
                .build();
        assertThat(invitation).isNotNull();
    }
}