package co.jware.sojka.core.domain.wrapped;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;

import static org.assertj.core.api.Assertions.assertThat;

public class NoteTest {

    private ObjectMapper objectMapper;

    @Before
    public void setUp() throws Exception {
        objectMapper = Jackson2ObjectMapperBuilder.json()
                .build();
    }

    @Test
    public void serialize_deserialize() throws Exception {
        Note note = Note.of("Some text");
        String asString = objectMapper.writeValueAsString(note);
        assertThat(asString).isNotNull();
        Note result = objectMapper.readValue(asString, Note.class);
        assertThat(result.value()).isEqualToIgnoringCase("Some text");
    }
}