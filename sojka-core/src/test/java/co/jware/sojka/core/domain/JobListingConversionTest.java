package co.jware.sojka.core.domain;

import co.jware.sojka.entities.core.JobListingBo;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.OffsetDateTime;

import static com.fasterxml.jackson.databind.SerializationFeature.WRITE_DATES_AS_TIMESTAMPS;
import static org.assertj.core.api.Java6Assertions.assertThat;

@RunWith(SpringRunner.class)
public class JobListingConversionTest {
    @Autowired
    private ObjectMapper mapper;


    @Test
    public void toEntity() throws Exception {
        JobListingBo entity = mapper.convertValue(JobListing.NULL.withValidFrom(OffsetDateTime.now()), JobListingBo.class);
        assertThat(entity).isNotNull();
    }

    @Test
    public void toEntity_via_string() throws Exception {
        String asString = mapper.writeValueAsString(JobListing.NULL.withValidFrom(OffsetDateTime.now()));
        JobListingBo entity = mapper.readValue(asString, JobListingBo.class);
        assertThat(entity).isNotNull();
    }
}
