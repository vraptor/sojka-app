package co.jware.sojka.core.domain.favorite;

import co.jware.sojka.core.domain.Company;
import co.jware.sojka.core.domain.JobListing;
import co.jware.sojka.core.domain.party.Candidate;
import co.jware.sojka.core.domain.party.Hirer;
import org.junit.Test;

import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;


public class FavoriteTest {
    @Test
    public void jobListingFavorite() throws Exception {
        Favorite<JobListing> jobListingFavorite = Favorite.<JobListing>builder()
                .subject(JobListing.NULL)
                .owner(Candidate.NULL)
                .build();
        Class<?> clazz = jobListingFavorite.subject().getClass();
        assertThat(clazz, notNullValue());
    }

    @Test
    public void companyFavorite() throws Exception {
        Company company = Company.builder()
                .name("Damage Inc.")
                .build();
        Favorite<Company> companyFavorite = Favorite.<Company>builder()
                .subject(company)
                .owner(Hirer.NULL)
                .build();
        assertNotNull(companyFavorite);
    }

    @Test
    public void candidateFavorite() throws Exception {
        Favorite<Candidate> candidateFavorite = Favorite.<Candidate>builder()
                .subject(Candidate.NULL)
                .owner(Candidate.NULL.withEmail("joe@a.net"))
                .build();
        assertThat(candidateFavorite, notNullValue());
        assertThat(candidateFavorite.subject(), not(is(candidateFavorite.owner())));
    }
}