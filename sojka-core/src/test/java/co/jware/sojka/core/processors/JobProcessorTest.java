package co.jware.sojka.core.processors;


import java.util.Optional;
import java.util.UUID;

import co.jware.sojka.core.domain.party.Party;
import org.junit.Test;
import org.junit.runner.RunWith;

import co.jware.sojka.core.domain.JobListing;
import co.jware.sojka.core.domain.party.Hirer;
import co.jware.sojka.core.exceptions.AccountNotPresentException;
import co.jware.sojka.core.exceptions.DuplicateJobPostException;
import co.jware.sojka.core.service.entity.JobListingService;
import mockit.Expectations;
import mockit.Injectable;
import mockit.Tested;
import mockit.integration.junit4.JMockit;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

@RunWith(JMockit.class)
public class JobProcessorTest {
    @Tested
    DefaultJobProcessor jobProcessor;
    @Injectable
    JobListingService jobListingService;

    @Test
    public void postJobListing(@Injectable Hirer owner) throws Exception {
        new Expectations() {{
            owner.registered();
            result = true;
        }};
        JobListing jobListing = jobProcessor.postJobListing(
                JobListing.builder()
                        .name("DGSKAJGDASJ")
                        .owner(owner).build());
        assertNotNull(jobListing);
    }

    @Test(expected = AccountNotPresentException.class)
    public void postJobListingNoAccount() throws Exception {
        jobProcessor.postJobListing(JobListing.builder()
                .link("__FAKE__")
                .name("__UNKNOWN__")
                .owner((Party) Hirer.NULL)
                .build());
    }

//    @Test(expected = BillingInfoNotPresentException.class)
//    public void postJobBillingInfoNotPresent(@Injectable Hirer owner) throws Exception {
//        new Expectations() {{
//            owner.registered();
//            result = true;
//        }};
//        JobListing jobListing = JobListing.builder().build();
//        jobProcessor.postJobListing(jobListing.withOwner(owner));
//    }

    @Test(expected = DuplicateJobPostException.class)
    public void postJobListing_duplicate(@Injectable Hirer owner) throws Exception {
        JobListing jobListing = JobListing.NULL
                .withOwner(owner)
                .withUuid(UUID.randomUUID());
        new Expectations() {{
            owner.registered();
            result = true;
        }};
        jobProcessor.postJobListing(jobListing);
        jobProcessor.postJobListing(jobListing);
    }

    @Test(expected = IllegalArgumentException.class)
    public void findByUuid_invalid_uuid() throws Exception {
        jobProcessor.findByUuid("");
    }

    @Test
    public void findByUuid_not_found() throws Exception {
        UUID uuid = UUID.randomUUID();
        JobListing jobListing = jobProcessor.findByUuid(uuid.toString());
        assertNull(jobListing);
    }
}
