package co.jware.sojka.core.service.impl;

import org.junit.Before;
import org.junit.Test;

import co.jware.sojka.core.domain.JobListing;
import co.jware.sojka.core.domain.JobResponse;
import co.jware.sojka.core.domain.Recommendation;
import co.jware.sojka.core.domain.Recommendation;
import co.jware.sojka.core.domain.party.Candidate;
import co.jware.sojka.core.enums.RecommendationStatus;
import co.jware.sojka.core.processors.RecommendProcessor;
import co.jware.sojka.core.processors.ResponseProcessor;
import mockit.Expectations;
import mockit.Injectable;
import mockit.MockUp;
import mockit.Tested;

import static co.jware.sojka.core.enums.RecommendationStatus.PENDING;
import static co.jware.sojka.core.enums.RecommendationStatus.RESPONSE_SENT;
import static org.junit.Assert.assertNotNull;

public class ResponseServiceImplTest {
    @Tested
    ResponseServiceImpl responseService;
    @Injectable
    RecommendProcessor recommendProcessor;
    @Injectable
    ResponseProcessor responseProcessor;
    private Candidate candidate;

    @Before
    public void setUp() throws Exception {
        candidate = Candidate.builder()
                .email("")
                .firstName("")
                .lastName("")
                .build();
    }

    @Test
    public void respondToRecommendation(@Injectable Recommendation recommendation) throws Exception {
        new Expectations() {{
            recommendation.status();
            result = PENDING;
            recommendProcessor.updateStatus((Recommendation) any, (RecommendationStatus) any);
            times = 1;
        }};
        JobResponse response = responseService.respondToRecommendation(recommendation);
        assertNotNull(response);
    }

    @Test(expected = IllegalArgumentException.class)
    public void respondToRecommendationBadStatus(final @Injectable Recommendation recommendation) throws Exception {
        new MockUp<Recommendation>() {
            RecommendationStatus status() {
                return RESPONSE_SENT;
            }
        };
        JobResponse response = responseService.respondToRecommendation(recommendation);
        assertNotNull(response);
    }

    @Test
    public void respondToJobListing() throws Exception {
        JobResponse jobResponse = responseService.respondToJobListing(JobListing.NULL, candidate);
        assertNotNull(jobResponse);
    }

    @Test(expected = IllegalArgumentException.class)
    public void respondToJobListing_expired(@Injectable JobListing jobListing) throws Exception {
        new Expectations() {{
            jobListing.isExpired();
            result = true;
        }};
        responseService.respondToJobListing(jobListing, candidate);
    }

    @Test(expected = IllegalArgumentException.class)
    public void respondToJobListing_no_capacity(@Injectable JobListing jobListing) throws Exception {
        new Expectations() {{
            jobListing.hasFreeCapacity();
            result = false;
        }};
        responseService.respondToJobListing(jobListing, candidate);
    }
}