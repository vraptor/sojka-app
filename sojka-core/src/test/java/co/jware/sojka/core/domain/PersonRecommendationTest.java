package co.jware.sojka.core.domain;

import co.jware.sojka.core.TestConfig;
import co.jware.sojka.core.domain.party.Candidate;
import co.jware.sojka.core.domain.party.External;
import co.jware.sojka.core.domain.wrapped.RecommendationWeight;
import co.jware.sojka.entities.core.JobListingBo;
import co.jware.sojka.entities.core.RecommendationBo;
import co.jware.sojka.utils.Creator;
import co.jware.sojka.utils.CreatorConfig;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@DataJpaTest
@ContextConfiguration(classes = {TestConfig.class})
public class PersonRecommendationTest {

    @Autowired
    ObjectMapper objectMapper;
    @Autowired
    Creator creator;
    private Recommendation<JobListing> recommendation;

    @Before
    public void setUp() throws Exception {
        recommendation = Recommendation.<JobListing>builder()
                .subject(JobListing.NULL)
                .sender(Candidate.NULL)
                .receiver(External.NULL)
                .weight(RecommendationWeight.of(1.25))
                .build();
    }

    @Test
    public void serialize() throws Exception {
        String asString = objectMapper.writeValueAsString(recommendation);
        assertThat(asString).isNotEmpty();
    }

    @Test
    public void deserialize() throws Exception {
        String asString = objectMapper.writeValueAsString(recommendation);
        Recommendation personRecommendation = objectMapper.readValue(asString, Recommendation.class);
        assertThat(personRecommendation).isNotNull();
    }

    @Test
    @Ignore
    public void fromEntity() throws Exception {
        RecommendationBo personRecommendationEntity = new RecommendationBo();
        personRecommendationEntity.setSubject(creator.save(new JobListingBo()));
        RecommendationBo recommendationEntity = creator.save(personRecommendationEntity);
        Recommendation personRecommendation = objectMapper.convertValue(recommendationEntity, Recommendation.class);
        assertThat(personRecommendation).isNotNull();
    }
}
