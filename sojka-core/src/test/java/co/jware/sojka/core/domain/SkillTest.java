package co.jware.sojka.core.domain;

import co.jware.sojka.core.domain.wrapped.SkillName;
import co.jware.sojka.core.enums.SkillLevel;
import co.jware.sojka.entities.core.EmbeddedSkill;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;


public class SkillTest {

    @Test
    public void equals() throws Exception {
        Skill skillOne = Skill.of(SkillName.of("JavaScript"), SkillLevel.INTERMEDIATE, 2);
        assertTrue(skillOne.equals(skillOne));
        assertFalse(skillOne.equals(Skill.of(SkillName.of("Java"), SkillLevel.INTERMEDIATE, 2)));
    }

    @Test
    public void name() throws Exception {
        ObjectMapper objectMapper = Jackson2ObjectMapperBuilder.json().build();
        Skill skill = Skill.of(SkillName.of("Java"), SkillLevel.EXPERT, 12);
        String asString = objectMapper.writeValueAsString(skill);
        assertThat(asString).isNotEmpty();
        EmbeddedSkill embeddedSkill = objectMapper.readValue(asString, EmbeddedSkill.class);
        assertThat(embeddedSkill).isNotNull();
    }
}