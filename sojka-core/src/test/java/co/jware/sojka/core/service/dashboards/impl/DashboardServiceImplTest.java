package co.jware.sojka.core.service.dashboards.impl;

import co.jware.sojka.core.Fakers;
import co.jware.sojka.core.TestConfig;
import co.jware.sojka.core.domain.Account;
import co.jware.sojka.core.domain.dashboards.Dashboard;
import co.jware.sojka.core.domain.party.*;
import co.jware.sojka.core.domain.pipelines.Label;
import co.jware.sojka.core.service.PartyService;
import co.jware.sojka.core.service.PersonService;
import co.jware.sojka.core.service.dashboards.DashboardService;
import co.jware.sojka.core.service.entity.AccountService;
import co.jware.sojka.repository.DashboardRepository;
import co.jware.sojka.repository.PersonRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.util.Optional;
import java.util.UUID;

import static org.assertj.core.api.Java6Assertions.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = TestConfig.class)
@Transactional
public class DashboardServiceImplTest {
    @Autowired
    private DashboardService dashboardService;
    @Autowired
    private PersonService personService;
    @Autowired
    private PartyService partyService;
    @MockBean
    private AccountService accountService;
    @Autowired
    private PersonRepository personRepository;
    @Autowired
    private DashboardRepository dashboardRepository;

    @Test
    public void getByOwner() throws Exception {
    }

    @Test
    @Transactional
    public void createDashboard() throws Exception {
        Hirer owner = Hirer.NULL;
        Person person = personService.createPerson(owner);
        personRepository.flush();
        Account account = Account.builder().owner(person).build();
        when(accountService.findByOwner(any()))
                .thenReturn(Optional.of(account));
        Dashboard dashboard = Dashboard.builder()
                .owner(person)
                .build();
        Dashboard result = dashboardService.createDashboard(dashboard);
        assertThat(dashboard).isEqualTo(result);
        assertThat(result.uuid()).isNotNull();
    }

    @Test
    public void saveDashboard() throws Exception {
        Dashboard dashboard = Dashboard.builder().owner(Fakers.hirer()).build();
        Dashboard result = dashboardService.saveDashboard(dashboard);
        assertThat((result.uuid())).isNotNull();
    }

    @Test
    public void updateDashboard() throws Exception {
        Party owner = Fakers.hirer();

        Dashboard dashboard = Dashboard.builder().owner(partyService.createParty(owner)).build();
        Dashboard result = dashboardService.saveDashboard(dashboard);
        dashboardRepository.flush();
        Dashboard toUpdate = result.withLabels(Label.of("RED_LABEL", "RED"));
        Dashboard updated = dashboardService.updateDashboard(toUpdate);
        assertThat(result).isNotEqualTo(updated);
    }

    @Test
    public void getDashboard() throws Exception {

    }

    @Test
    public void deleteDashboard() throws Exception {
        Dashboard dashboard = Dashboard.builder().owner(Fakers.hirer()).build();
        Dashboard result = dashboardService.saveDashboard(dashboard);
        dashboardService.deleteDashboard(result.uuid());
    }

    @Configuration
    static class TestConfig {

    }
}