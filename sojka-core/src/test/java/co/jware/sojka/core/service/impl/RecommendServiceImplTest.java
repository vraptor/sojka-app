package co.jware.sojka.core.service.impl;

import org.junit.Test;

import co.jware.sojka.core.domain.JobListing;
import co.jware.sojka.core.domain.party.Party;
import co.jware.sojka.core.processors.RecommendProcessor;
import co.jware.sojka.core.service.PersonService;
import mockit.Injectable;
import mockit.Tested;


public class RecommendServiceImplTest {
    @Tested
    RecommendServiceImpl recommendService;
    @Injectable
    private JobListing jobListing;
    @Injectable
    private Party sender;
    @Injectable
    RecommendProcessor recommendProcessor;
    @Injectable
    private PersonService personService;

    public RecommendServiceImplTest() {
    }

    @Test
    public void recommendViaReceiverEmail() throws Exception {

    }

    @Test
    public void recommendViaReceiverEmail_external() throws Exception {

    }
}