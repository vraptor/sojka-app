package co.jware.sojka.core;


import co.jware.sojka.core.domain.*;
import co.jware.sojka.core.domain.dashboards.Dashboard;
import co.jware.sojka.core.domain.party.Candidate;
import co.jware.sojka.core.domain.party.External;
import co.jware.sojka.core.domain.party.Hirer;
import co.jware.sojka.core.domain.party.Party;
import co.jware.sojka.core.domain.pipelines.JobPipeline;
import co.jware.sojka.core.domain.pipelines.OwnerPipeline;
import co.jware.sojka.core.domain.search.MetaLink;
import co.jware.sojka.core.domain.search.page.PageDetail;
import co.jware.sojka.core.domain.search.page.PageMetaData;
import co.jware.sojka.core.enums.HirerType;
import co.jware.sojka.core.enums.RecommendationType;
import com.github.javafaker.Faker;

import java.util.Locale;

public abstract class Fakers {

    private static Faker faker = new Faker();


    public static Resume resume() {
        return Resume.builder()
                .owner(Fakers.candidate())
                .submitter(Fakers.hirer())
                .build();
    }

    public static JobResponse jobResponse() {
        return JobResponse.builder()
                .jobListing(Fakers.jobListing())
                .respondent(Fakers.candidate())
                .build();
    }

    public static JobListing jobListing() {
        return JobListing.builder()
                .name(faker.app().name())
                .owner(Fakers.hirer())
                .build();
    }

    public static Hirer hirer() {
        return Hirer.builder()
                .firstName(faker.name().firstName())
                .lastName(faker.name().lastName())
                .email(faker.internet().emailAddress())
                .build();
    }

    public static Candidate candidate() {
        return Candidate.builder()
                .firstName(faker.name().firstName())
                .lastName(faker.name().lastName())
                .email(faker.internet().emailAddress())
                .build();
    }

    public static External external() {
        return External.builder()
                .email(faker.internet()
                        .emailAddress())
                .build();
    }

    public static Dashboard dashboard() {
        return Dashboard.builder()
                .owner(Fakers.candidate())
                .addPipeline(Fakers.jobPipeline())
                .build();
    }

    private static JobPipeline jobPipeline() {
        return JobPipeline.builder()
                .jobListing(Fakers.jobListing())
                .build();
    }

    public static Account account() {
        return Account.builder()
                .user(Fakers.user())
                .owner(Fakers.candidate())
                .build();
    }

    public static User user() {
        return User.builder()
                .username(faker.internet().emailAddress())
                .build();
    }

    public static <S> Recommendation personRecommendation() {
        return Recommendation.builder()
                .subject(Fakers.jobListing())
                .sender(Fakers.candidate())
                .receiver(Fakers.external())
                .type(RecommendationType.PERSON)
                .build();
    }

    public static OwnerPipeline ownerPipeline() {
        return OwnerPipeline.builder().build();
    }

    public static PageDetail pageDetail() {
        return PageDetail.builder()
                .url(faker.internet().url())
                .title(faker.lorem().word())
                .richText(faker.lorem().paragraph())
                .pageMetaData(Fakers.pageMetaData())
                .build();
    }

    public static PageMetaData pageMetaData() {
        return PageMetaData.builder()
                .locale(Locale.getDefault())
                .company(faker.company().name())
                .hirerType(HirerType.UNKNOWN)
                .build();
    }

    public static MetaLink metaLink() {
        return MetaLink.builder()
                .href(faker.internet().url())
                .title(faker.lorem().paragraph(1))
                .build();
    }

    public static Party party() {
        return external();
    }
}
