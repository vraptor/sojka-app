package co.jware.sojka;


import com.github.vincentrussell.json.datagenerator.impl.JsonDataGeneratorImpl;
import org.springframework.core.io.Resource;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;

public class DataGenerator {

    Resource usersJson;

    public DataGenerator(Resource usersJson) {
        this.usersJson = usersJson;
    }

    public void generateUsers() throws Exception {
        InputStream in = usersJson.getInputStream();
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        JsonDataGeneratorImpl jsonDataGenerator = new JsonDataGeneratorImpl();
        jsonDataGenerator.generateTestDataJson(usersJson.getFile(), baos);
        System.out.println(new String(baos.toByteArray()));
    }
}
