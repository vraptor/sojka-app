package co.jware.sojka.entities.core;


import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;

import java.io.IOException;

public class EmbeddedRecWeightDeserializer extends JsonDeserializer<EmbeddedRecWeight> {
    @Override
    public EmbeddedRecWeight deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException {
        JsonNode node = jp.getCodec().readTree(jp);
        double value = node.asDouble();
        EmbeddedRecWeight recWeight = new EmbeddedRecWeight();
        recWeight.setValue(value);
        return recWeight;
    }
}
