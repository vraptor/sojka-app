package co.jware.sojka.repository;

import co.jware.sojka.core.TestConfig;
import co.jware.sojka.entities.core.UserBo;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.jpa.repository.JpaContext;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import static org.junit.Assert.assertNotNull;

@SpringBootTest(classes = TestConfig.class)
@RunWith(SpringRunner.class)
@Transactional
@ActiveProfiles("test")
public class UserRepositoryTest {
    @Autowired
    UserRepository userRepository;
    @Autowired
    JdbcTemplate jdbcTemplate;
    @Autowired
    JpaContext jpaContext;

    @Test
    public void save() throws Exception {
        assertNotNull(userRepository);
        UserBo entity = new UserBo();
        entity.setUsername("vraptor@jware.co");
        entity.setPassword("changeit");
        entity.setEnabled(true);
        entity = userRepository.save(entity);
        jpaContext.getEntityManagerByManagedType(UserBo.class).flush();
        assertNotNull(userRepository.findById(entity.getUuid()));
    }
}