package co.jware.sojka.repository;

import co.jware.sojka.core.TestConfig;
import co.jware.sojka.core.domain.Resume;
import co.jware.sojka.core.domain.party.Candidate;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@DataJpaTest
@SpringBootTest(classes = TestConfig.class)
@Transactional
public class ResumeRepositoryTest {
    @Autowired
    private ResumeRepository repository;
    @Autowired
    private ObjectMapper mapper;

    @Test
    public void save() throws Exception {
        Resume resume = Resume.builder().owner(Candidate.NULL).build();
        String asString = mapper.writeValueAsString(resume);
        assertThat(asString).isNotBlank();
    }

    @Configuration
    static class TestConfig {

    }
}