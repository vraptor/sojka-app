package co.jware.sojka.repository.search;

import co.jware.sojka.core.BaseDataJpaTest;
import co.jware.sojka.entities.core.metasearch.MetaLinkBo;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
public class MetaLinkRepositoryTest extends BaseDataJpaTest {
    @Autowired
    private MetaLinkRepository repository;

    @Test
    public void findAllByMissingPageDetail() throws Exception {

        Page<MetaLinkBo> page = repository.findAllByMissingPageDetail(new PageRequest(0, 20));
        assertThat(page).isNotNull();
    }
}