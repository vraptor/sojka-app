package co.jware.sojka.repository.geo;


import co.jware.sojka.core.TestConfig;
import co.jware.sojka.entities.core.geo.OsmResponseBo;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringRunner.class)
@DataJpaTest()
@ContextConfiguration(classes = TestConfig.class)
@Transactional()

public class OsmResponseRepositoryTest {

    @Autowired
    private OsmResponseRepository repository;

    @Test
    @Rollback(false)
    public void save() {
        repository.save(new OsmResponseBo());
    }
}
