package co.jware.sojka.repository.page;

import co.jware.sojka.core.BaseDataJpaTest;
import co.jware.sojka.entities.core.metasearch.page.PageDetailBo;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
public class PageDetailRepositoryTestIT extends BaseDataJpaTest {
    @Autowired
    PageDetailRepository repository;

    @Test
    public void save() throws Exception {
        PageDetailBo pageDetailEntity = new PageDetailBo();
        pageDetailEntity.setUuid(UUID.randomUUID());
        PageDetailBo entity = repository.save(pageDetailEntity);
        assertThat(entity.getUuid()).isNotNull();
    }

    @Test
    public void exists() throws Exception {
        boolean exists = repository.existsById(UUID.randomUUID());
        assertThat(exists).isFalse();
    }
}