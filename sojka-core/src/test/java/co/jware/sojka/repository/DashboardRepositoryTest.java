package co.jware.sojka.repository;

import co.jware.sojka.core.TestConfig;

import co.jware.sojka.core.domain.dashboards.Dashboard;
import co.jware.sojka.core.domain.party.Party;
import co.jware.sojka.core.domain.wrapped.Description;
import co.jware.sojka.core.enums.OwnerType;
import co.jware.sojka.core.service.PersonService;
import co.jware.sojka.entities.core.dashboard.DashboardBo;
import co.jware.sojka.entities.core.dashboard.QDashboardBo;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@DataJpaTest()
@SpringBootTest(classes = TestConfig.class)
@Transactional
public class DashboardRepositoryTest {
    @Autowired
    private ObjectMapper mapper;
    @Autowired
    private DashboardRepository repository;
    @Autowired
    private PersonService personService;

    @Test
    public void save_owner_candidate() throws Exception {

        Party owner = personService.findByEmail("hr@sojka.co")
                .orElseThrow(IllegalArgumentException::new);
        Dashboard dashboard = Dashboard.builder()
                .owner(owner)
                .description(Description.of("Simple Dashboard"))
                .build();
        DashboardBo entity = mapper.convertValue(dashboard, DashboardBo.class);
        DashboardBo result = repository.saveAndFlush(entity);
        assertThat(result.getUuid()).isNotNull();
        QDashboardBo $ = QDashboardBo.dashboardBo;
        List<DashboardBo> entities = (List<DashboardBo>) repository.findAll($.ownerType.eq(OwnerType.HIRER));
        assertThat(entities).isNotEmpty();
    }
}