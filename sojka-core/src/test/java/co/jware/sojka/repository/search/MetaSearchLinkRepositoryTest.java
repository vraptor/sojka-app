package co.jware.sojka.repository.search;

import co.jware.sojka.core.TestConfig;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@DataJpaTest()
@ContextConfiguration(classes = TestConfig.class)
public class MetaSearchLinkRepositoryTest {
    @Autowired
    MetaSearchLinkRepository repository;

    @Test
    public void findByMetaSearchUuid() throws Exception {
//        UUID uuid = UUID.fromString("8e38f90e-3ec5-441e-a649-77ea1f3cbb29");
//        List<MetaSearchLinkEntity> result = repository.findByMetaSearchUuid(uuid);
//        assertThat(result).isNotEmpty();
    }

}