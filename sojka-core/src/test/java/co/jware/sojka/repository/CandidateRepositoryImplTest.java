package co.jware.sojka.repository;

import co.jware.sojka.core.TestConfig;
import co.jware.sojka.entities.core.CandidateBo;
import co.jware.sojka.entities.core.ResumeBo;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;
import java.util.stream.Stream;

import static org.junit.Assert.assertNotNull;

@SpringBootTest(classes = TestConfig.class)
@RunWith(SpringRunner.class)
@ActiveProfiles("test")
@Transactional
public class CandidateRepositoryImplTest {
    @Autowired
    CandidateRepository repository;

    @Test
    public void getResumesByOwner() throws Exception {
        CandidateBo owner = new CandidateBo();
        owner.setUuid(UUID.randomUUID());
        owner.setVersion(1L);
        Stream<ResumeBo> resumes = repository.getResumesByOwner(owner);
        assertNotNull(resumes);
    }
}