package co.jware.sojka.repository;

import co.jware.sojka.core.TestConfig;
import co.jware.sojka.core.domain.JobListing;
import co.jware.sojka.core.domain.party.Candidate;
import co.jware.sojka.core.domain.pipelines.CandidateCard;
import co.jware.sojka.core.domain.pipelines.Card;
import co.jware.sojka.core.domain.pipelines.JobListingCard;
import co.jware.sojka.core.domain.pipelines.Stage;
import co.jware.sojka.entities.core.pipelines.CardBo;
import co.jware.sojka.entities.core.pipelines.StageBo;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
//@SpringBootTest(classes = TestConfig.class)
@DataJpaTest
@Transactional
public class CardRepositoryTest {
    @Autowired
    private CardRepository cardRepository;
    @Autowired
    private StageRepository stageRepository;
    @Autowired
    private ObjectMapper mapper;

    @Test
    public void findAllByStage() throws Exception {
        Stage stage = Stage.builder()
                .addCard(JobListingCard.builder()
                        .content(JobListing.NULL)
                        .build())
                .name("STAGE")
                .position(1)
                .build();
        String asString = mapper.writeValueAsString(stage);
        assertThat(asString).isNotBlank();
        StageBo entity = mapper.convertValue(stage, StageBo.class);
        StageBo stageEntity = stageRepository.saveAndFlush(entity);
        List<CardBo> stages = cardRepository.findAllByStage(stageEntity);
        assertThat(stages).hasSize(1);
    }


    @Test
    public void save_card_candidate() throws Exception {
        Card<Candidate> card = CandidateCard.builder()
                .content(Candidate.NULL).build();
        String asString = mapper.writeValueAsString(card);
        assertThat(asString).isNotBlank();
        CardBo entity = cardRepository.save(mapper.convertValue(card, CardBo.class));
        assertThat(entity.getUuid()).isNotNull();
    }

    @Configuration
    static class TestConfig {


    }


}
