package co.jware.sojka.repository;

import co.jware.sojka.core.BaseDataJpaTest;
import co.jware.sojka.core.domain.Account;
import co.jware.sojka.entities.core.AccountBo;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@Transactional
public class AccountRepositoryTestIT extends BaseDataJpaTest {
    @Autowired
    private AccountRepository repository;
    @Autowired
    private JdbcTemplate jdbcTemplate;
    @Autowired
    private ObjectMapper mapper;

    @Test
    public void findByOwnerUuid() throws Exception {
        List<String> uuids = jdbcTemplate.queryForList("select owner_id from account", String.class);
        UUID uuid = UUID.fromString(uuids.get(0));
        AccountBo entity = repository.findByOwnerUuid(uuid)
                .orElseThrow(IllegalStateException::new);
        assertThat(entity).isNotNull();
        Account account = mapper.convertValue(entity, Account.class);
        assertThat(account).isNotNull();
    }

    @Configuration
    static class TestConfig {
    }
}