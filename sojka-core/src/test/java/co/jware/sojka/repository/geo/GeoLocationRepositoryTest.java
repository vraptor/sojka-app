package co.jware.sojka.repository.geo;

import co.jware.sojka.core.BaseDataJpaTest;
import co.jware.sojka.entities.core.geo.GeoLocationBo;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@Transactional
public class GeoLocationRepositoryTest extends BaseDataJpaTest {
    @Autowired
    private GeoLocationRepository repository;

    @Test
    @Rollback
    public void save() throws Exception {
        GeoLocationBo entity = new GeoLocationBo();
        entity.setName("Praha 2");
        entity.setPlaceId("ChIJuZQJdKqQC0cR_D5f878nJWU");
        repository.save(entity);
    }

    @Test
    public void exists() throws Exception {
        boolean exists = repository.existsById(UUID.randomUUID());
        assertThat(exists).isFalse();
    }
}