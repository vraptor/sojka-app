package co.jware.sojka.utils;

import co.jware.sojka.entities.Persistent;
import co.jware.sojka.repository.ResumeRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;

import java.text.DateFormat;

@Configuration
@ComponentScan
@EnableJpaRepositories(basePackageClasses = ResumeRepository.class)
@EntityScan(basePackageClasses = Persistent.class)
public class CreatorConfig {
//    @Bean
//    public ObjectMapper objectMapper() {
//        return new Jackson2ObjectMapperBuilder()
//                .json()
//                .simpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
//                .build();
//    }
}
