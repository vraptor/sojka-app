package co.jware.sojka.utils;

import org.junit.Ignore;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;


public class CastUtilsTest {
    @Test
    public void as() throws Exception {
        Object input = "alabala";
        String result = CastUtils.as(String.class, input);
        assertEquals("alabala", result);
    }

    @Test
    public void as_null() throws Exception {
        assertNull(CastUtils.as(String.class, null));
    }
    @Test
    public void as_null_null() throws Exception {
        assertNull(CastUtils.as(null, null));
    }
}