package co.jware.sojka.utils;

import co.jware.sojka.entities.core.JobListingBo;
import co.jware.sojka.entities.core.JobResponseBo;
import co.jware.sojka.entities.core.ResumeBo;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@DataJpaTest
@ContextConfiguration(classes = {CreatorConfig.class})
public class CreatorTest {
    @Autowired
    private Creator creator;

    @Test
    public void save_resume() throws Exception {
        ResumeBo entity = creator.save(new ResumeBo());
        assertThat(entity.isNew()).isFalse();
        assertThat(entity.getSubmitter().isNew()).isFalse();
    }
    @Test
    public void save_job_listing() throws Exception {
        JobListingBo entity = creator.save(new JobListingBo());
        assertThat(entity.isNew()).isFalse();
        assertThat(entity.getOwner().isNew()).isFalse();
    }
    @Test
    @Ignore
    public void save_job_response() throws Exception {
        JobResponseBo entity = creator.save(new JobResponseBo());
        assertThat(entity.isNew()).isFalse();
        assertThat(entity.getJobListing().isNew()).isFalse();
        assertThat(entity.getRespondent().isNew()).isFalse();
    }
}