package co.jware.sojka.utils;

import co.jware.sojka.entities.Persistent;
import co.jware.sojka.entities.core.JobListingBo;
import co.jware.sojka.entities.core.PersonBo;
import com.fasterxml.classmate.ResolvedType;
import com.fasterxml.classmate.TypeResolver;
import com.github.javafaker.Faker;
import com.google.common.collect.Maps;
import com.google.common.reflect.TypeToken;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.util.ReflectionUtils;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.validation.constraints.NotNull;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

@Component
public class Creator implements ApplicationContextAware {

    private final Faker faker = new Faker();
    @Autowired
    private List<JpaRepository> repositories;
    private HashMap<Class, JpaRepository> repositoriesMap;
    private ApplicationContext context;

    @Override
    public void setApplicationContext(ApplicationContext context) throws BeansException {
        this.context = context;
    }

    @PostConstruct
    public void init() {
        repositoriesMap = Maps.newHashMap();
        repositories.forEach((repo) -> {
            TypeResolver typeResolver = new TypeResolver();
            ResolvedType resolvedType = typeResolver.resolve(repo.getClass());
            List<ResolvedType> types = resolvedType.typeParametersFor(JpaRepository.class);
            Class<?> entityClass = types.get(0).getErasedType();
            repositoriesMap.putIfAbsent(entityClass, repo);
        });
    }

    public JpaRepository getRepositoryFor(Class entityClass) {
        return repositoriesMap.get(entityClass);
    }

    public <T extends Persistent> T save(T entity) {
//        fillEntity(entity);
        Type type = TypeToken.of(entity.getClass()).getType();
        Field[] fields = entity.getClass().getDeclaredFields();
        Stream.of(fields)
                .filter(field -> Persistent.class.isAssignableFrom(field.getType()))
                .filter(field -> {
                    Annotation[] annotations = field.getDeclaredAnnotations();
                    return Arrays.stream(annotations)
                            .map(Annotation::getClass)
                            .anyMatch(NotNull.class::isAssignableFrom);
                })
                .forEach(f -> {
                    try {
                        f.setAccessible(true);
                        if (null == f.get(entity)) {
                            T value = (T) f.getType().newInstance();
//                            fillEntity(value);
                            ReflectionUtils.setField(f, entity, this.save(value));
                        }
                    } catch (InstantiationException | IllegalAccessException e) {
                        throw new RuntimeException(e);
                    }
                });
        return (T) Optional.ofNullable(getRepositoryFor(entity.getClass()))
                .map(repo -> {
                    try {
                        return repo.save(entity);
                    } catch (Exception e) {
                        throw new RuntimeException(e);
                    }
                })
                .orElseThrow(IllegalArgumentException::new);
    }

//    private <T extends Persistent> void fillEntity(T entity) {
//        if (entity instanceof PersonBo) {
//            fillPersonEntity((PersonBo) entity);
//        } else if (entity instanceof JobListingBo) {
//            JobListingBo jobListingEntity = (JobListingBo) entity;
//            jobListingEntity.setName(faker.lorem().word());
//            jobListingEntity.setCapacity(1);
//
//        }
//    }

//    private <T extends Persistent> void fillPersonEntity(PersonBo entity) {
//        PersonBo personEntity = entity;
//        personEntity.setEmail(faker.internet().emailAddress());
//        if (personEntity instanceof Named) {
//            Named named = (Named) personEntity;
//            named.setFirstName(faker.address().firstName());
//            named.setLastName(faker.address().lastName());
//        }
//    }
}
