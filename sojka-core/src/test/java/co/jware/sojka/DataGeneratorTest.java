package co.jware.sojka;

import org.junit.Test;
import org.springframework.core.io.ClassPathResource;


public class DataGeneratorTest {
    @Test
    public void generateUsers() throws Exception {
        ClassPathResource json = new ClassPathResource("/json/users-gen.json");
        DataGenerator generator = new DataGenerator(json);
        generator.generateUsers();
    }


}