package co.jware.sojka.core.processors

import co.jware.sojka.core.domain.JobListing
import co.jware.sojka.core.domain.Recommendation
import co.jware.sojka.core.domain.party.Party
import co.jware.sojka.core.domain.party.Person
import co.jware.sojka.core.enums.RecommendationStatus
import co.jware.sojka.core.exceptions.InvalidStatusRequestedException
import mockit.Mocked
import mockit.integration.junit4.JMockit
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(JMockit)
public class TestRecommendProcessor {
    @Mocked
    Person sender
    @Mocked
    Party receiver
    @Mocked
    JobListing jobListing

    def recommendation
    def processor = [] as RecommendProcessor

    @Before
    public void setUp() throws Exception {
        recommendation = Recommendation.<JobListing> builder().
                sender(sender)
                .receiver(receiver)
                .subject(jobListing)
                .build()
    }

    @Test(expected = InvalidStatusRequestedException)
    public void updateStatus() throws Exception {
        processor.updateStatus(recommendation, RecommendationStatus.INTERVIEW)
    }

}