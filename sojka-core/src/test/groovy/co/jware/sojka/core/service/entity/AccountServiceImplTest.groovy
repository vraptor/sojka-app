package co.jware.sojka.core.service.entity

import co.jware.sojka.core.BaseDataJpaTest
import co.jware.sojka.core.domain.Account
import co.jware.sojka.core.domain.User
import co.jware.sojka.core.domain.party.Candidate
import co.jware.sojka.core.domain.party.Party
import co.jware.sojka.core.service.PersonService
import co.jware.sojka.core.service.impl.PersonServiceImpl
import co.jware.sojka.entities.core.AccountBo
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.data.jpa.repository.JpaContext
import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.test.jdbc.JdbcTestUtils
import org.springframework.transaction.annotation.Transactional

@RunWith(SpringRunner)
@Transactional
@ContextConfiguration
class AccountServiceImplTest extends BaseDataJpaTest {

    @Autowired
    AccountService accountService
    @Autowired
    UserService userService
    @Autowired
    PersonService personService
    @Autowired
    JdbcTemplate jdbcTemplate
    @Autowired
    JpaContext jpaContext

    @Test
    public void createAccount_candidate() throws Exception {
        def user = userService.createUser(User.builder().username("bogus@looser.net").build())
        def owner = personService.createPerson(Candidate.builder().firstName("a").lastName("b")
                .email("bogus@looser.net").build())
        def rows = JdbcTestUtils.countRowsInTable(jdbcTemplate, "account")
        Account account = doCreate(owner, user)
        assert account
        jpaContext.getEntityManagerByManagedType(AccountBo).flush()
        assert rows + 1 == JdbcTestUtils.countRowsInTable(jdbcTemplate, "account")
    }

    @Test
    void createAccount_no_user() throws Exception {
        def user = Optional.empty();
        def owner = personService.findByEmail('demo@sojka.co')
        def person = owner.orElseThrow({ new IllegalStateException() })
        Optional.ofNullable(person.uuid())
                .map({ u -> jdbcTemplate.update("delete from account where owner_id =?", u.toString()) })
        def rows = JdbcTestUtils.countRowsInTable(jdbcTemplate, "account")
        Account account = doCreate(person)
        assert account
        jpaContext.getEntityManagerByManagedType(AccountBo).flush()
        assert rows + 1 == JdbcTestUtils.countRowsInTable(jdbcTemplate, "account")
    }

    @Test
    void createAccount_hirer() throws Exception {
        def user = userService.findByUsername('demo@sojka.co')
        def owner = personService.findByEmail('hr@sojka.co')
                .orElseThrow({ new IllegalStateException() })
        Optional.ofNullable(owner.uuid())
                .map({ u -> jdbcTemplate.update("delete from account where owner_id =?", u.toString()) })
        def rows = JdbcTestUtils.countRowsInTable(jdbcTemplate, "account")
        Account account = doCreate(owner, user)
        assert account
        jpaContext.getEntityManagerByManagedType(AccountBo).flush()
        assert rows + 1 == JdbcTestUtils.countRowsInTable(jdbcTemplate, "account")
    }

    @Test
    void createAccount_teacher() throws Exception {
        def user = userService.findByUsername('demo@sojka.co')
        def owner = personService.findByEmail('teacher@sojka.co').
                orElseThrow({ new IllegalStateException() })
        Optional.ofNullable(owner.uuid())
                .map({ u -> jdbcTemplate.update("delete from account where owner_id =?", u.toString()) })
        def rows = JdbcTestUtils.countRowsInTable(jdbcTemplate, "account")
        Account account = doCreate(owner, user)
        assert account
        jpaContext.getEntityManagerByManagedType(AccountBo).flush()
        assert rows + 1 == JdbcTestUtils.countRowsInTable(jdbcTemplate, "account")
    }

    @Test(expected = IllegalArgumentException)
    public void createAccount_external() throws Exception {
        def user = userService.findByUsername('demo@sojka.co')
        def owner = personService.findByEmail('john.doe@acme.co').orElseThrow({ new IllegalStateException() })
        def rows = JdbcTestUtils.countRowsInTable(jdbcTemplate, "account")
        Account account = doCreate(owner, user)
        assert account
        jpaContext.getEntityManagerByManagedType(AccountBo).flush()
        assert rows + 1 == JdbcTestUtils.countRowsInTable(jdbcTemplate, "account")
    }

    @Test
    public void findByUser() throws Exception {
        def user = userService.findByUsername('demo@sojka.co')
        Optional<Account> account = accountService.findByUser(user)
        assert account.isPresent()
    }

    @Test
    public void findByOwner() throws Exception {
        def owner = personService.findByEmail('hr@sojka.co')
        accountService.findByOwner(owner.orElseThrow({ new IllegalStateException() }))
    }

    private Account doCreate(Party owner, User user) {
        def rawAccount = Account.builder()
                .owner(owner)
                .user(user)
                .build()
        def account = accountService.createAccount(rawAccount)
        return account
    }

    private Account doCreate(Party owner) {
        def rawAccount = Account.builder()
                .owner(owner)
                .build()
        def account = accountService.createAccount(rawAccount)
        return account
    }
}
