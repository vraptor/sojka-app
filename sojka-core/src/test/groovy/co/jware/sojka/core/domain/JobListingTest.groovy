package co.jware.sojka.core.domain

import co.jware.sojka.core.BaseDataJpaTest
import co.jware.sojka.core.domain.party.Hirer
import co.jware.sojka.entities.core.HirerBo
import co.jware.sojka.entities.core.JobListingBo
import co.jware.sojka.utils.Creator
import com.fasterxml.jackson.core.type.TypeReference
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.jsonSchema.JsonSchemaGenerator
import com.github.vincentrussell.json.datagenerator.impl.JsonDataGeneratorImpl
import mockit.Injectable
import mockit.integration.junit4.JMockit
import org.junit.ClassRule
import org.junit.Ignore
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.core.io.Resource
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit4.rules.SpringClassRule
import org.springframework.test.context.junit4.rules.SpringMethodRule
import org.springframework.transaction.annotation.Transactional

import java.beans.Introspector
import java.time.OffsetDateTime

import static org.junit.Assert.*

@RunWith(JMockit.class)
@Transactional
class JobListingTest extends BaseDataJpaTest {


    @ClassRule
    public static final SpringClassRule springClassRule = new SpringClassRule();
    @Rule
    public SpringMethodRule springMethodRule = new SpringMethodRule();

    @Injectable
    private Hirer owner;
    @Autowired
    ObjectMapper objectMapper

    @Autowired
    Creator creator

    @Test
    public void defaultCapacity() throws Exception {
        JobListing jobListing = JobListing.builder()
                .owner(owner)
                .name('')
                .build();
        assertEquals(1, jobListing.capacity());
    }

    @Test
    public void invalidCapacity() throws Exception {
        JobListing jobListing = JobListing.builder()
                .name('')
                .owner(owner).build();
        try {
            jobListing.withCapacity(-1);
            fail("Should throw exception");
        } catch (IllegalStateException e) {
            assertEquals("Capacity must be equal or greater than 1", e.getMessage());
        }
    }

    @Test
    public void invalidFreeCapacity() throws Exception {
        JobListing jobListing = JobListing.builder()
                .owner(owner)
                .name('')
                .build();
        try {
            jobListing.withFreeCapacity(2);
            fail("Should throw exception");
        } catch (IllegalStateException e) {
            assertEquals("Free capacity can't exceed capacity", e.getMessage());
        }
    }

    @Test
    public void hasFreeCapacity() throws Exception {
        JobListing jobListing = JobListing.builder().owner(owner)
                .name('')
                .build();
        JobListing build = jobListing
                .withCapacity(5)
                .withFreeCapacity(2);
        assertTrue(build.hasFreeCapacity());
    }

    @Test
    public void decreaseFreeCapacity() throws Exception {
        JobListing jobListing = JobListing.NULL.withCapacity(4).withFreeCapacity(3);
        jobListing = jobListing.withFreeCapacity(2);
        assertEquals(2, jobListing.freeCapacity());
    }

    @Value("classpath:/json/job-listings.json")
    Resource jsonResource

    @Test
    @Ignore
    void deserialize_list() throws Exception {
        def parser = new JsonDataGeneratorImpl()
        def outputStream = new ByteArrayOutputStream()
        parser.generateTestDataJson(jsonResource.file, outputStream)
        def bytes = outputStream.toByteArray()
//        List<JobListingEntity>
        def result = objectMapper.readValue(bytes, new TypeReference<List<JobListingBo>>() {})
        assert result
        def jobListings = result.collect(
                { j -> ImmutableHelper.fromEntity(j)
                }
        )
        assert jobListings
    }

    @Test
    public void convertFromEntity() throws Exception {
        def owner = new HirerBo(email: 'xxx', firstName: 'xxx', lastName: 'xxx')
        def entity = new JobListingBo(
                [name     : 'amazon',
                 capacity : 1, freeCapacity: 1,
                 owner    : owner,
                 locations: [],
                 version  : 0,
                 validFrom: OffsetDateTime.now(),
                 validTo  : OffsetDateTime.now().plusDays(30)]
        )
        def jobListing = objectMapper.convertValue(entity, JobListing)
        assert jobListing
    }

    @Test
    public void jsonSchema() throws Exception {
        def schemaGenerator = new JsonSchemaGenerator(objectMapper)
        def schema = schemaGenerator.generateSchema(JobListing.class)
        assert schema;
        def asString = objectMapper.writeValueAsString(schema)
        assert asString
    }

    @Test
    public void introspect() throws Exception {
        def info = Introspector.getBeanInfo(JobListing)
        assert info
    }

    @Test
    @Ignore
    void fromEntity() throws Exception {
        def now = OffsetDateTime.now()
        def entity = creator.save(new JobListingBo(validFrom: now, validTo: now.plusDays(30), locations: []))
        assert !entity.isNew()
        def jobListing = objectMapper.convertValue(entity, JobListing)
        assert jobListing
    }

    @ContextConfiguration
    static class TestConfig {
        @Bean
        Creator creator() {
            new Creator()
        }
    }
}
