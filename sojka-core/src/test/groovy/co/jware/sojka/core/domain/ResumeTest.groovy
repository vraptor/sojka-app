package co.jware.sojka.core.domain

import co.jware.sojka.core.CoreConfig
import co.jware.sojka.core.domain.party.Candidate
import co.jware.sojka.core.domain.party.Hirer
import co.jware.sojka.core.domain.wrapped.Keyword
import co.jware.sojka.entities.core.ResumeBo
import com.fasterxml.jackson.databind.ObjectMapper
import org.junit.Ignore
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit4.SpringRunner

@RunWith(SpringRunner)
@ContextConfiguration(classes = CoreConfig)
public class ResumeTest {
    @Autowired
    ObjectMapper mapper

    private owner = Candidate.builder()
            .email('')
            .firstName('a')
            .lastName('b')
            .build()

    @Test
    @Ignore
    void submitter_not_set() throws Exception {
        def owner = Candidate.builder()
                .email('')
                .firstName('a')
                .lastName('b')
                .build()
        Resume resume = Resume.builder()
                .owner(owner)
                .build()
        def submitter = resume.submitter()
        assert submitter == owner
    }

    @Test
    public void submitter_set() throws Exception {
        def owner = Candidate.builder()
                .email('')
                .firstName('a')
                .lastName('b')
                .build()
        def hirer = Hirer.builder().email('x')
                .firstName('d')
                .lastName('e').build()
        def resume = Resume.builder()
                .owner(owner)
                .submitter(hirer)
                .build()
        def candidate = resume.owner()
        def orElse = resume.submitter()
        assert orElse != candidate
    }

    @Test
    public void submitter_no_owner() throws Exception {
        def hirer = Hirer.builder().email('x')
                .firstName('d')
                .lastName('e').build()
        def resume = Resume.builder()
                .submitter(hirer)
                .build()
        assert !resume.owner()
        assert resume.submitter()
    }

    @Test
    public void serialize() throws Exception {
        def asString = mapper.writeValueAsString(Resume.builder()
                .owner(this.owner)
                .addKeyword(Keyword.of("Software Development"), Keyword.of("Agile"))
                .build())
        assert asString

    }

    @Test
    public void deserialize() throws Exception {
        def json = '''
{
    "owner": {
        "@type": "CANDIDATE",
        "registered": false,
        "firstName": "a",
        "lastName": "b",
        "email":"xxx"
    },
    "submitter": {
        "@type": "CANDIDATE",
        "registered": false,
        "firstName": "a",
        "lastName": "b",
        "email":"xxx"
    }
}
'''
        def resume = mapper.readValue(json, Resume)
        assert resume
    }

    @Test
    public void convertToEntity() throws Exception {
        def resume = Resume.builder().owner(this.owner).build()
//        def entity = immutableHelper.convertToEntity(resume)
//        entity
        def entity = mapper.convertValue(resume, ResumeBo)
        assert entity
    }
}