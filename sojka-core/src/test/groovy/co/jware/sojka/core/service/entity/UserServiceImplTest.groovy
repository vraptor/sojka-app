package co.jware.sojka.core.service.entity

import co.jware.sojka.core.TestConfig
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.dao.InvalidDataAccessApiUsageException
import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.transaction.annotation.Transactional

@SpringBootTest(classes = TestConfig.class)
@RunWith(SpringRunner.class)
@ActiveProfiles('test')
@Transactional
public class UserServiceImplTest {
    @Autowired
    UserService userService;
    @Autowired
    JdbcTemplate jdbcTemplate

    @Test(expected = InvalidDataAccessApiUsageException)
    public void findByUuid() throws Exception {
        userService.findByUuid(null)
    }

    @Test
    public void findByUsername() throws Exception {
        def user = userService.findByUsername('demo@sojka.co')
        assert user
    }
}