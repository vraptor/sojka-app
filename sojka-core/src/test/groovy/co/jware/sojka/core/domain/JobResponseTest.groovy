package co.jware.sojka.core.domain

import co.jware.sojka.core.CoreConfig
import co.jware.sojka.core.domain.party.Candidate
import co.jware.sojka.core.domain.party.Hirer
import co.jware.sojka.core.domain.party.Party
import co.jware.sojka.core.enums.JobListingOrigin
import co.jware.sojka.utils.Creator
import co.jware.sojka.utils.CreatorConfig
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.SerializationFeature
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module
import groovy.json.JsonOutput
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit4.SpringRunner

@RunWith(SpringRunner)
@DataJpaTest
@ContextConfiguration(classes = [CoreConfig, CreatorConfig])
public class JobResponseTest {
    @Autowired
    ObjectMapper mapper
    @Autowired
    Creator creator

    @Test
    public void serialize() throws Exception {

        def jobListing = JobListing.builder().name('').origin(JobListingOrigin.EXTERNAL).build()
        def jobResponse = JobResponse.builder()
                .jobListing(jobListing)
                .respondent(Candidate.builder().email('a').firstName('b').lastName('c').build())
                .build()
        this.mapper.registerModule(new Jdk8Module())
        this.mapper.setSerializationInclusion(JsonInclude.Include.NON_EMPTY)
        this.mapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS)
        def json = this.mapper.writeValueAsString(jobResponse)
        println JsonOutput.prettyPrint(json)
    }

    @Test
    public void cast_null() throws Exception {
        JobResponse.getClass().cast(null);
    }

    @Test(expected = IllegalStateException)
    void invalidRespondent() throws Exception {
        JobResponse.builder()
                .jobListing(JobListing.NULL)
                .respondent((Party) Hirer.NULL)
                .build();
    }
}