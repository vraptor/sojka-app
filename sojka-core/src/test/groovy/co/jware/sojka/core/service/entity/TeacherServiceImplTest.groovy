package co.jware.sojka.core.service.entity

import co.jware.sojka.core.domain.party.Teacher
import co.jware.sojka.repository.TeacherRepository
import org.junit.Test

public class TeacherServiceImplTest {

    @Test
    public void createTeacher() throws Exception {
        def teacherService = new TeacherServiceImpl()
        teacherService.repository = [save: {}] as TeacherRepository
        def teacher = Teacher.builder()
                .email('')
                .firstName('')
                .lastName('')
                .build()
        teacherService.createTeacher(teacher)
    }

}