package co.jware.sojka.core.domain

import co.jware.sojka.entities.core.CompanyBo
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonTypeInfo
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module
import org.junit.Before
import org.junit.Test

public class CompanyTest {
    ObjectMapper mapper

    @Before
    public void setUp() throws Exception {
        mapper = new ObjectMapper()
        mapper.registerModule(new Jdk8Module())
        mapper.addMixIn(CompanyBo, TypeIdMixin)
        mapper.addMixIn(Company, CompanyMixin)
    }

    @Test
    public void toEntity() throws Exception {

        def company = Company.builder().name('Damage Inc.')
                .build()
        def entity = mapper.readValue(mapper.writeValueAsBytes(company), CompanyBo)
        assert entity
    }

    @Test
    public void fromEntity() throws Exception {
        def entity = new CompanyBo([uuid: UUID.randomUUID(), name: 'Vector'])
        Company company = mapper.readValue(mapper.writeValueAsBytes(entity), Company.class)
        assert company

    }

    @JsonIgnoreProperties(['@type', '@id'])
    public class TypeIdMixin {}

    @JsonTypeInfo(use = JsonTypeInfo.Id.NAME, defaultImpl = Company.class)
    public class CompanyMixin {

    }
}
