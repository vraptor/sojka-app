package co.jware.sojka.core.domain.mapping

import co.jware.sojka.core.domain.Company
import co.jware.sojka.core.domain.JobListing
import co.jware.sojka.core.domain.Salary
import co.jware.sojka.core.domain.SalaryRange
import co.jware.sojka.core.domain.party.Candidate
import co.jware.sojka.entities.core.CompanyBo
import co.jware.sojka.entities.core.JobListingBo
import org.junit.Test

import static java.math.BigDecimal.ONE
import static java.math.BigDecimal.ZERO

public class DomainMapperTest {
    @Test
    public void toEntity_Candidate() throws Exception {
        def candidate = Candidate.builder().email('1').firstName('2').lastName('3').build()
        def mapper = candidate as DomainMapper
        def entity = mapper.toEntity()
        assert entity

    }

    @Test
    public void toEntity_Company() throws Exception {
        def uuid = UUID.randomUUID()
        def name = 'Damage'
        def company = Company.builder()
                .name(name)
                .uuid(uuid)
                .build()
        def mapper = company as DomainMapper
        CompanyBo entity = mapper.toEntity()
        assert [entity.name, entity.uuid] == [name, uuid]
    }

    @Test
    public void toEntity_JobListing() throws Exception {
        def jobListing = JobListing.builder()
                .name('__invalid__')
                .salary(Salary.builder().range(SalaryRange.of(ZERO, ONE)).build())
                .build()
        def mapper = jobListing as DomainMapper
        JobListingBo entity = mapper.toEntity()
        assert entity
    }

    @Test
    public void toEntity_cast() throws Exception {
        def company = Company.builder()
                .name('ABC')
                .uuid(UUID.randomUUID())
                .build()
        def mapper = company as DomainMapper
        def entity = mapper.toEntity()
        assert entity.getClass() == CompanyBo

    }
}