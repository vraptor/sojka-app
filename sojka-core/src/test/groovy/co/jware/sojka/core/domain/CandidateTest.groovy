package co.jware.sojka.core.domain

import co.jware.sojka.core.CoreConfig
import co.jware.sojka.core.domain.party.Candidate
import com.fasterxml.jackson.databind.ObjectMapper
import groovy.json.JsonOutput
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner

import static org.junit.Assert.assertFalse

@RunWith(SpringJUnit4ClassRunner)
@ContextConfiguration(classes = CoreConfig)
public class CandidateTest {
    @Autowired
    ObjectMapper objectMapper

    @Test
    public void hasUuid() throws Exception {
        Candidate john = Candidate.builder()
                .firstName("John")
                .lastName("Doe")
                .email("john.doe@jware.co")
                .build();
        assert !john.uuid()
    }

    @Test
    public void deserialize() throws Exception {
        def candidate = ["@type"    : "CANDIDATE",
                         "uuid"     : "19145f12-707e-4a1a-a9df-739e6d0de625",
                         "email"    : "a",
                         "firstName": "b",
                         "lastName" : "c"]
        def result = objectMapper.readValue(JsonOutput.toJson(candidate), Candidate)
        assert result
        assert result.class == Candidate
    }

    @Test
    public void serialize() throws Exception {

        def owner = Candidate.builder()
                .email('a')
                .firstName('b')
                .lastName('c')
                .build()

        def resumeOne = Resume.builder()
                .owner(owner)
//                .submitter(owner)
                .build()
        def resumeTwo = Resume.builder()
                .owner(owner)
//                .submitter(owner)
                .build()
        assert owner

        def json = objectMapper.writeValueAsString(owner)
        println JsonOutput.prettyPrint(json)
    }


}