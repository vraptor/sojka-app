package co.jware.sojka.core.domain

import co.jware.sojka.core.domain.party.Hirer
import co.jware.sojka.entities.core.HirerBo
import org.junit.Before
import org.junit.Test

class HirerTest {
    def user, account
    Hirer hirer

    @Before
    public void setUp() throws Exception {

        user = User.builder()
                .username('vraptor')
                .build()


        hirer = Hirer.builder()
                .email('joe@jware.co')
                .firstName('joe')
                .lastName('doe')
                .registered(false)
                .version(1L)
                .build()

        account = Account.builder()
                .owner(hirer)
                .user(user)
                .build()
    }


    @Test
    public void toEntity() throws Exception {
        assert hirer
        HirerBo entity = ImmutableHelper.toEntity(hirer)
        assert entity.version == 1
    }

    @Test
    public void toEntity_zero_version() throws Exception {
        HirerBo entity = ImmutableHelper.toEntity(Hirer.copyOf(hirer).withVersion(0L))
        assert entity.version == 0
    }

}
