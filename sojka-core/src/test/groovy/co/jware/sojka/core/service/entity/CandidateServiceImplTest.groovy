package co.jware.sojka.core.service.entity

import co.jware.sojka.core.TestConfig
import co.jware.sojka.core.domain.party.Candidate
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.data.jpa.repository.JpaContext
import org.springframework.test.annotation.Rollback
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner
import org.springframework.transaction.annotation.Transactional

@SpringBootTest(classes = TestConfig.class)
@RunWith(SpringJUnit4ClassRunner.class)
@ActiveProfiles('test')
@Transactional
@Rollback()
public class CandidateServiceImplTest {
    @Autowired
    UserService userService
    @Autowired
    CandidateService candidateService
    @Autowired
    JpaContext jpaContext

    @Test
    public void createCandidate() throws Exception {
//        def demoUser = userService.findByUsername('demo')
//        def account = Account.builder().user(demoUser)
//                .build()
        def candidate = Candidate.builder()
                .email('demo@jware.co')
                .firstName('_1')
                .lastName('_2')
//                .account(account)
                .build()
//        account = account.withOwner(candidate)
        Candidate result = candidateService.createCandidate(candidate)
        assert result.version() == 0
    }

//    @Test
//    public void createTwoCandidates() throws Exception {
//
//        def demoUser = userService.findByUsername('candidate')
//        def candidateOne = Candidate.builder()
//                .email('demo@jware.co')
//                .firstName('_1.1')
//                .lastName('_2.1')
//                .account(Account.builder().user(demoUser).build())
//                .build()
//
//        Candidate resultOne = candidateService.createCandidate(candidateOne)
//        def candidateTwo = Candidate.builder()
//                .email('candidate')
//                .firstName('_1.2')
//                .lastName('_2.2')
//                .build()
//        Candidate resultTwo = candidateService.createCandidate(candidateTwo)
//        jpaContext.getEntityManagerByManagedType(AccountEntity).flush()
//        assert !resultOne.equals(resultTwo)
//    }
}