package co.jware.sojka.core.service.entity

import co.jware.sojka.core.TestConfig
import co.jware.sojka.core.domain.JobListing
import co.jware.sojka.core.domain.Recommendation
import co.jware.sojka.core.service.PersonService
import com.fasterxml.jackson.databind.ObjectMapper
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.transaction.annotation.Transactional

@SpringBootTest(classes = TestConfig.class)
@RunWith(SpringRunner.class)
@Transactional
@ActiveProfiles("test")
public class RecommendationServiceImplTest {
    @Autowired
    ObjectMapper mapper
    @Autowired
    RecommendationService recommendationService;
    @Autowired
    PersonService personService
    @Autowired
    JobListingService jobListingService
    @Autowired
    JdbcTemplate jdbcTemplate
//
//    @Testii
//    public void findByReceiverAndSubject() throws Exception {
//        recommendationService.findByReceiverAndSubject(
//                Candidate.builder()
//                        .email('xxx')
//                        .firstName('xxx')
//                        .lastName('xxx')
//                        .uuid(UUID.randomUUID())
//                        .build(),
//                JobListing.builder().name('NONAME').build())
//    }

    @Test
    public void createPersonRecommendation() throws Exception {
        def receiver = personService.findByEmail('candidate@sojka.co')
                .orElseThrow({ new IllegalStateException() })
        def sender = personService.findByEmail('demo@sojka.co')
                .orElseThrow({ new IllegalStateException() })
        def uuids = jdbcTemplate.queryForList('select uuid from job_listing where link is not null', String)
        JobListing jobListing = jobListingService.findByUuid(UUID.fromString(uuids[0]))
                .orElseThrow({ new IllegalArgumentException() })
        def recommendation = Recommendation.<JobListing> builder()
                .sender(sender)
                .receiver(receiver)
                .subject(jobListing)
                .build()
        recommendationService.createRecommendation(recommendation)
    }

}