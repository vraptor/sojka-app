package co.jware.sojka.core.service.impl

import co.jware.sojka.core.TestConfig
import co.jware.sojka.core.domain.party.Candidate
import co.jware.sojka.core.domain.party.Party
import co.jware.sojka.core.service.PersonService
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner
import org.springframework.transaction.annotation.Transactional

@SpringBootTest(classes = TestConfig.class)
@RunWith(SpringJUnit4ClassRunner.class)
@ActiveProfiles('test')
@Transactional
public class PersonServiceImplTest {
    @Autowired
    PersonService personService

    @Test
    public void findByEmail() throws Exception {
        Candidate demo = personService.findByEmail('demo@sojka.co')
                .orElseThrow({ new IllegalStateException() }) as Candidate;
        assert demo.registered()
    }

    @Test
    public void findByEmailContaining() throws Exception {
        def persons = personService.findByEmailContaining('sojka')
        assert persons
        persons.each { p ->
            assert p.email().contains('sojka')
        }
    }
}