package co.jware.sojka.core.processors

import co.jware.sojka.core.TestConfig
import co.jware.sojka.core.domain.JobListing
import co.jware.sojka.core.enums.JobListingOrigin
import co.jware.sojka.core.enums.JobListingStatus
import co.jware.sojka.core.exceptions.OwnerNotEligibleException
import co.jware.sojka.core.service.PersonService
import co.jware.sojka.core.service.entity.JobListingService
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.junit4.SpringRunner

import static org.mockito.BDDMockito.given
import static org.mockito.Matchers.any

@SpringBootTest(classes = TestConfig)
@RunWith(SpringRunner)
@ActiveProfiles('test')
public class DefaultJobProcessorTest {
    @Autowired
    JobProcessor jobProcessor
    JobListing jobListing
    @Autowired
    PersonService personService
    @MockBean
    JobListingService jobListingService

    @Before
    public void setUp() throws Exception {
        def hirer = personService.findByEmail('hr@sojka.co')
                .orElseThrow({ new IllegalStateException() })
        jobListing = JobListing.builder()
                .link('__FAKE__')
                .name('NONAME')
                .owner(hirer)
                .origin(JobListingOrigin.INTERNAL)

                .build()
    }

    @Test
    void publishJobListing() throws Exception {
        assert this.jobListing.status() == JobListingStatus.PENDING
        def result = jobProcessor.publishJobListing(this.jobListing)
        assert result.status() == JobListingStatus.ACTIVE
    }

    @Test(expected = OwnerNotEligibleException)
    void publishJobListing_owner_not_eligible() throws Exception {
        given(jobListingService.checkOwnerEligibility(any(JobListing)))
                .willThrow(OwnerNotEligibleException)
        jobProcessor.publishJobListing(jobListing)

    }

    @Test(expected = IllegalArgumentException)
    void publishJobListing_external() throws Exception {
        jobProcessor.publishJobListing(jobListing.withOrigin(JobListingOrigin.EXTERNAL))
    }
}