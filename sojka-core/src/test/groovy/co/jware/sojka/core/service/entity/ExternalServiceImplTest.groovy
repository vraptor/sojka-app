package co.jware.sojka.core.service.entity

import co.jware.sojka.core.BaseDataJpaTest
import co.jware.sojka.core.domain.party.External
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.transaction.annotation.Transactional

@RunWith(SpringRunner)
@Transactional
@ContextConfiguration
public class ExternalServiceImplTest extends BaseDataJpaTest {


    @Autowired
    ExternalServiceImpl externalService

//    @Before
//    public void setUp() throws Exception {
//        externalService = new ExternalServiceImpl()
//        externalService.repository = [
//                save       : { ExternalEntity e -> assert e },
//                findByEmail: {}
//        ] as ExternalRepository
//    }

    @Test
    public void createExternal() throws Exception {
        def external = External.builder().email('jack@e.co').build()
        externalService.createExternal(external)
    }

    @Test
    void findOrCreate() throws Exception {
        def external = External.builder()
                .email('jack@e.co')
                .build()
        def result = externalService.findOrCreate(external)
        assert result;
    }
}
