package co.jware.sojka.core.domain

import co.jware.sojka.entities.core.EmbeddedSalaryRange
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module
import org.junit.Test

public class SalaryRangeTest {
    @Test
    public void toEmbeddable() throws Exception {
        def mapper = new ObjectMapper()
        mapper.registerModule(new Jdk8Module())
        def salaryRange = SalaryRange.of(15000 as BigDecimal, 25000 as BigDecimal)
        def range = mapper.readValue(mapper.writeValueAsBytes(salaryRange),
                EmbeddedSalaryRange)
        assert range
    }

    @Test
    public void isFull() throws Exception {
        def salaryRange = SalaryRange.of(BigDecimal.ZERO, BigDecimal.ZERO)
        assert !salaryRange.isFull()
    }
}