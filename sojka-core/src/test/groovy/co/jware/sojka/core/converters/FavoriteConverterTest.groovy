package co.jware.sojka.core.converters

import co.jware.sojka.core.domain.favorite.Favorite
import co.jware.sojka.entities.core.FavoriteBo
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.core.convert.ConversionService
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit4.SpringRunner

@RunWith(SpringRunner)
@ContextConfiguration(classes = [ConversionConfig])
public class FavoriteConverterTest {
    @Autowired
    ConversionService conversionService

    @Test
    public void convert() throws Exception {
        assert conversionService.canConvert(Favorite, FavoriteBo)
//        assert conversionService.canConvert(FavoriteEntity, Favorite)
    }
}