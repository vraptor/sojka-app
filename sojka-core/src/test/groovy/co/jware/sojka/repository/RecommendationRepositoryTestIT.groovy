package co.jware.sojka.repository

import co.jware.sojka.core.TestConfig
import co.jware.sojka.entities.core.*
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner
import org.springframework.transaction.annotation.Transactional

@SpringBootTest(classes = TestConfig.class)
@RunWith(SpringJUnit4ClassRunner.class)
@ActiveProfiles('test')
@Transactional
public class RecommendationRepositoryTestIT {

    @Autowired
    RecommendationRepository repository
    @Autowired
    ExternalRepository externalRepository
    @Autowired
    CandidateRepository candidateRepository
    @Autowired
    JobListingRepository jobListingRepository


    private RecommendationBo createJobRecommendation() {
        def entity = new RecommendationBo()
        def jobListingEntity = new JobListingBo()
        jobListingEntity = jobListingRepository.save(jobListingEntity)
//        def subjectEntity = new JobListingSubjectEntity(target: jobListingEntity)
//        entity.setSubject(subjectEntity)
        entity.setSubject(jobListingEntity);
        def externalEntity = new ExternalBo(email: 'joe@jware.co')
        externalRepository.save(externalEntity)
        entity.setReceiver(externalEntity)
        def candidateEntity = new CandidateBo(email: 'jack@jware.co')
        candidateRepository.save(candidateEntity)
        entity.setSender(candidateEntity)
        def recommendationEntity = repository.save(entity)
        return recommendationEntity
    }

    @Test
    public void create_company() throws Exception {
        RecommendationBo recommendationEntity = createCompanyRecommendation()
        assert recommendationEntity
    }

    private RecommendationBo createCompanyRecommendation() {
        def entity = new RecommendationBo()
        def companyEntity = new CompanyBo()
        companyEntity = repository.save(companyEntity)
        entity.setSubject()
        def externalEntity = new ExternalBo(email: 'josh@cnn.com')
        externalRepository.save(externalEntity)
        entity.setReceiver(externalEntity)
        def candidateEntity = new CandidateBo(email: 'janet@jware.co')
        candidateRepository.save(candidateEntity)
        entity.setSender(candidateEntity)
        def recommendationEntity = repository.save(entity)
        return recommendationEntity
    }

    @Test
    public void testCreateJobListingRecommndation() throws Exception {
        createJobRecommendation();
    }
}