package co.jware.sojka.repository

import co.jware.sojka.core.TestConfig
import co.jware.sojka.entities.core.AccountBo
import co.jware.sojka.entities.core.CandidateBo
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.data.jpa.repository.JpaContext
import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.test.jdbc.JdbcTestUtils
import org.springframework.transaction.annotation.Transactional

@SpringBootTest(classes = TestConfig.class)
@RunWith(SpringRunner.class)
@ActiveProfiles('test')
@Transactional
public class CandidateRepositoryTest {
    @Autowired
    CandidateRepository repository
    @Autowired
    AccountRepository accountRepository
    @Autowired
    UserRepository userRepository
    @Autowired
    JdbcTemplate jdbcTemplate;
    @Autowired
    JpaContext jpaContext

    @Test
    public void save() throws Exception {
        def rows = JdbcTestUtils.countRowsInTable(jdbcTemplate, "person")

        CandidateBo candidateEntity = new CandidateBo(email: 'vraptor@jware.co')


        def userEntity = userRepository.findByUsername('demo')
        def accountEntity = new AccountBo(owner: candidateEntity, user: userEntity)
        def entity = repository.save(candidateEntity)

        jpaContext.getEntityManagerByManagedType(CandidateBo).flush()
        assert entity
        assert rows + 1 == JdbcTestUtils.countRowsInTable(jdbcTemplate, "person")
    }
}