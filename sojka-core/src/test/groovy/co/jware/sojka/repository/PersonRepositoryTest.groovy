package co.jware.sojka.repository

import co.jware.sojka.core.TestConfig
import co.jware.sojka.entities.core.PersonBo
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner
import org.springframework.transaction.annotation.Transactional

@SpringBootTest(classes = TestConfig.class)
@RunWith(SpringJUnit4ClassRunner.class)
@ActiveProfiles('test')
@Transactional
public class PersonRepositoryTest {
    @Autowired
    PersonRepository repository
    @Test
    public void findByEmail() throws Exception {
        PersonBo demo = repository.findByEmail('demo@sojka.co')
        assert demo.getRegistered()
    }
}