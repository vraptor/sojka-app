package co.jware.sojka.entities.core

import co.jware.sojka.core.CoreConfig
import co.jware.sojka.core.domain.party.Candidate
import com.fasterxml.jackson.databind.ObjectMapper
import com.jayway.jsonpath.JsonPath
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit4.SpringRunner

@RunWith(SpringRunner)
@ContextConfiguration(classes = CoreConfig)
public class CandidateEntityTest {
    @Autowired
    ObjectMapper mapper

    @Test
    public void serializationCycle() throws Exception {
        def uuid = UUID.randomUUID()
        def entity = new CandidateBo([uuid: uuid])
        def asString = mapper.writeValueAsString(entity);
        def document = JsonPath.parse(asString)
        def type = document.read('$.@type')
        def resultUuid = document.read('$.uuid')
        assert type == 'CANDIDATE'
        assert resultUuid == uuid.toString()
        def result = mapper.readValue(asString, CandidateBo)
        assert result.uuid == uuid
    }

    @Test
    public void fromEntity() throws Exception {
        def entity = new CandidateBo([email: 'xxx', firstName: 'a', lastName: 'b'])
        def result = Optional.ofNullable(entity)
                .map({ e -> mapper.writeValueAsBytes(e) })
                .map({ b -> mapper.readValue(b, Candidate) })
                .orElseThrow({ -> 'Error mapping from CandidateEntity' })
//        def asString = objectMapper.writeValueAsString(entity)
//        def result = objectMapper.readValue(asString, Candidate)
        assert result
    }
}
