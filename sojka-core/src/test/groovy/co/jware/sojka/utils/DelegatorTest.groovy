package co.jware.sojka.utils

import co.jware.sojka.core.domain.User
import org.junit.Test

public class DelegatorTest {

    private class OneMapper {
        def toEntity() {

        }
    }

    @Test
    public void delegateAll() throws Exception {

        def user = User.builder()
                .username('doe')
                .build()
        assert !user.metaClass.respondsTo('toEntity', {})
        def userDelegator = new Delegator(user, new OneMapper())
        userDelegator.delegateAll()
        user.toEntity()
    }

    @Test
    public void delegateAll_array() throws Exception {
        def user = User.builder()
                .username('doe')
                .build()
        assert !user.metaClass.respondsTo('toEntity', {})
        def userDelegator = new Delegator(user, new OneMapper())
        userDelegator.delegateAll('toEntity')
        user.toEntity()
    }
}