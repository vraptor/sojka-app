package co.jware.sojka.repository.pipelines;


import java.util.List;

import co.jware.sojka.core.domain.party.Hirer;
import co.jware.sojka.entities.core.pipelines.OwnerPipelineBo;

public interface OwnerPipelineRepository
        extends PipelineRepository<OwnerPipelineBo> {

    List<OwnerPipelineBo> findAllByOwner(Hirer hirer);
}
