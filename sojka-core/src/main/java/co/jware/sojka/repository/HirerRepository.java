package co.jware.sojka.repository;


import co.jware.sojka.entities.core.HirerBo;

public interface HirerRepository extends CrudQueryDslRepository<HirerBo> {
}
