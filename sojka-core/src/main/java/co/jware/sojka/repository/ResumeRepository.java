package co.jware.sojka.repository;


import co.jware.sojka.entities.core.ResumeBo;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.UUID;

public interface ResumeRepository extends CrudQueryDslRepository<ResumeBo> {

    @Query("select r from ResumeBo r where r.submitter.id=:uuid")
    Page<ResumeBo> findAllBySubmitterId(@Param("uuid") UUID uuid, Pageable pageable);

    @Query("select r from ResumeBo r where r.owner.id=:uuid")
    Page<ResumeBo> findAllByOwnerId(@Param("uuid") UUID uuid, Pageable Pageable);
}
