package co.jware.sojka.repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.querydsl.jpa.impl.JPAQueryFactory;

import co.jware.sojka.entities.core.AccountBo;
import co.jware.sojka.entities.core.PersonBo;
import co.jware.sojka.entities.core.QAccountBo;

public class AccountRepositoryImpl implements AccountRepositoryCustom {
    @PersistenceContext
    private EntityManager em;

    @Override
    public AccountBo findByOwner(PersonBo owner) {
        JPAQueryFactory queryFactory = new JPAQueryFactory(em);
        QAccountBo $ = QAccountBo.accountBo;
        AccountBo result = queryFactory.selectFrom($).where($.owner.eq(owner)).fetchOne();
        return result;
    }
}
