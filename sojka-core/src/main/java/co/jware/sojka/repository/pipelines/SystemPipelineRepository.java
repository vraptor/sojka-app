package co.jware.sojka.repository.pipelines;


import co.jware.sojka.entities.core.pipelines.SystemPipelineBo;

public interface SystemPipelineRepository
        extends PipelineRepository<SystemPipelineBo> {
}
