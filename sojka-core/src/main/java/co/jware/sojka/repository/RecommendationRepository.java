package co.jware.sojka.repository;


import co.jware.sojka.core.enums.SubjectType;
import co.jware.sojka.entities.core.RecommendationBo;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.UUID;

@SuppressWarnings("JpaQlInspection")
public interface RecommendationRepository extends CrudQueryDslRepository<RecommendationBo> {

    @Query("select r from RecommendationBo r where r.receiver.id=:uuid")
    Page<RecommendationBo> findAllByReceiverId(@Param("uuid") UUID uuid, Pageable pageable);

    @Query("select r from RecommendationBo r where r.sender.id=:uuid")
    Page<RecommendationBo> findAllBySenderId(@Param("uuid") UUID uuid, Pageable pageable);

    Page<RecommendationBo> findByReceiverIdAndSubjectId(UUID receiverUuid, UUID subjectUuid, Pageable pageable);

    Page<RecommendationBo> findAllBySenderIdAndSubjectId(UUID senderUuid, UUID subjectUuid, Pageable pageable);


    @Query("select r from RecommendationBo r where r.subject.id=:uuid and r.subjectType=:subjectType")
    Page<RecommendationBo> findAllBySubjectIdAndSubjectType(@Param("uuid") UUID uuid, @Param("subjectType") SubjectType subjectType,
                                                            Pageable pageable);

    //    @Query("select r from RecommendationEntity r where r.subject.id=:uuid and r.subjectType='COMPANY'")
    //    Page<RecommendationEntity> findByCompanySubject(@Param("uuid") UUID uuid, Pageable pageable);

    @Query("select r from RecommendationBo r where r.subject.id=:uuid")
    Page<RecommendationBo> findAllBySubjectId(@Param("uuid") UUID uuid, Pageable pageable);
}
