package co.jware.sojka.repository.search;


import co.jware.sojka.entities.core.metasearch.PageBo;
import co.jware.sojka.repository.CrudQueryDslRepository;

public interface PageRepository extends CrudQueryDslRepository<PageBo> {

    PageBo findByUrl(String url);
}
