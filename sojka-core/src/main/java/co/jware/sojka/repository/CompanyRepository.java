package co.jware.sojka.repository;


import co.jware.sojka.entities.core.CompanyBo;

public interface CompanyRepository extends CrudQueryDslRepository<CompanyBo> {

    CompanyBo findByName(String name);
}
