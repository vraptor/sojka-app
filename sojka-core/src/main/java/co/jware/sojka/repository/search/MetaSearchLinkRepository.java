package co.jware.sojka.repository.search;


import co.jware.sojka.entities.core.metasearch.MetaSearchLinkBo;
import co.jware.sojka.entities.core.metasearch.MetaSearchLinkId;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.UUID;

public interface MetaSearchLinkRepository extends JpaRepository<MetaSearchLinkBo, MetaSearchLinkId> {

    List<MetaSearchLinkBo> findByMetaSearchUuid(UUID uuid);

    List<MetaSearchLinkBo> findByMetaLinkUuid(UUID uuid);

    MetaSearchLinkBo findByMetaSearchUuidAndMetaLinkUuid(UUID metaSearchUuid, UUID metaLinkUuid);

    Page<MetaSearchLinkBo> findByMetaSearchUuid(UUID uuid, Pageable pageable);

    long countByMetaSearchUuid(UUID uuid);
}
