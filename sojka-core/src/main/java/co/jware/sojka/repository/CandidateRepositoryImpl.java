package co.jware.sojka.repository;


import java.util.stream.Stream;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaContext;

import com.querydsl.jpa.impl.JPAQueryFactory;

import co.jware.sojka.entities.core.CandidateBo;
import co.jware.sojka.entities.core.QResumeBo;
import co.jware.sojka.entities.core.ResumeBo;

public class CandidateRepositoryImpl implements CandidateRepositoryCustom {

    EntityManager em;
    @Autowired
    JpaContext jpaContext;

    @PostConstruct
    private void init() {
        this.em = jpaContext.getEntityManagerByManagedType(CandidateBo.class);
    }

    @Override
    public Stream<ResumeBo> getResumesByOwner(CandidateBo owner) {
        init();
        QResumeBo $ = QResumeBo.resumeBo;
        JPAQueryFactory queryFactory = new JPAQueryFactory(em);
        return queryFactory.selectFrom($)
                .where($.owner.eq(owner)).select($).fetch().stream();
    }
}
