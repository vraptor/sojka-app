package co.jware.sojka.repository;


import co.jware.sojka.entities.core.ExternalBo;


public interface ExternalRepository extends CrudQueryDslRepository<ExternalBo> {

    ExternalBo findByEmail(String email);
}
