package co.jware.sojka.repository;


import co.jware.sojka.entities.core.CandidateBo;
import co.jware.sojka.entities.core.ResumeBo;

import java.util.stream.Stream;

public interface CandidateRepositoryCustom {
    Stream<ResumeBo> getResumesByOwner(CandidateBo owner);
}
