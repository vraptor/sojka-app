package co.jware.sojka.repository.search;


import co.jware.sojka.entities.core.metasearch.MetaLinkBo;
import co.jware.sojka.repository.CrudQueryDslRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;

import java.time.OffsetDateTime;
import java.util.List;

public interface MetaLinkRepository extends CrudQueryDslRepository<MetaLinkBo> {

    List<MetaLinkBo> findByHref(String href);

    Page<MetaLinkBo> findAllByCreatedBefore(OffsetDateTime date, Pageable pageable);

    @Query("select m from MetaLinkBo m where m.href not in(select p.url from PageDetailBo p)")
    Page<MetaLinkBo> findAllByMissingPageDetail(Pageable pageable);


}
