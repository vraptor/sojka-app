package co.jware.sojka.repository;


import co.jware.sojka.entities.core.pipelines.PipelineBo;
import co.jware.sojka.entities.core.pipelines.StageBo;

import java.util.List;

public interface StageRepository extends CrudQueryDslRepository<StageBo> {

    List<StageBo> findAllByPipeline(PipelineBo entity);
}
