package co.jware.sojka.repository;


import co.jware.sojka.entities.core.CandidateBo;

import org.springframework.data.jpa.repository.Query;

import java.util.stream.Stream;

public interface CandidateRepository extends CrudQueryDslRepository<CandidateBo>,
        CandidateRepositoryCustom {

    @Query("select c from CandidateBo c")
    Stream<CandidateBo> findAllAndStream();
}
