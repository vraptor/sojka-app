package co.jware.sojka.repository;


import co.jware.sojka.entities.core.JobListingBo;

import org.springframework.transaction.annotation.Transactional;



@Transactional
public interface JobListingRepository extends CrudQueryDslRepository<JobListingBo> {

    JobListingBo findByLink(String link);
}