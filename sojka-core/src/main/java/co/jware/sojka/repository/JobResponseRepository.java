package co.jware.sojka.repository;


import co.jware.sojka.core.enums.RespondentType;
import co.jware.sojka.entities.core.JobResponseBo;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.UUID;

public interface JobResponseRepository extends CrudQueryDslRepository<JobResponseBo> {

    @Query("select r from JobResponseBo r where r.respondent.id = :uuid")
    Page<JobResponseBo> findAllByRespondentId(@Param("uuid") UUID uuid, Pageable pageable);

    @Query("select  r from JobResponseBo r where r.jobListing.id=:uuid")
    Page<JobResponseBo> findAllByJobListingId(@Param("uuid") UUID uuid, Pageable pageable);

    Page<JobResponseBo> findAllByRespondentType(RespondentType type, Pageable pageable);
}
