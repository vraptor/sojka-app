package co.jware.sojka.repository;


import java.time.OffsetDateTime;
import java.util.UUID;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import co.jware.sojka.entities.core.CourseClassBo;

public interface CourseClassRepository extends CrudQueryDslRepository<CourseClassBo> {

    Page<CourseClassBo> findAllByCourseId(UUID uuid, Pageable pageable);

    Page<CourseClassBo> findAllByOwnerId(UUID uuid, Pageable pageable);

    Page<CourseClassBo> findAllByCourseIdAndStartDateBetween(UUID uuid, OffsetDateTime from, OffsetDateTime to,
                                                             Pageable pageable);
}
