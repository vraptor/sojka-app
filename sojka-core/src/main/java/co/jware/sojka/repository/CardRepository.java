package co.jware.sojka.repository;


import co.jware.sojka.core.enums.CardType;
import co.jware.sojka.entities.core.pipelines.CardBo;
import co.jware.sojka.entities.core.pipelines.StageBo;

import java.util.List;

public interface CardRepository extends CrudQueryDslRepository<CardBo> {

    List<CardBo> findAllByCardType(CardType cardType);

    List<CardBo> findAllByStageAndCardType(StageBo entity, CardType cardType);

    List<CardBo> findAllByStage(StageBo entity);

}
