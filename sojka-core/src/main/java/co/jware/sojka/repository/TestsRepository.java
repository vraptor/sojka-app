package co.jware.sojka.repository;


import co.jware.sojka.entities.core.TestBo;

public interface TestsRepository extends CrudQueryDslRepository<TestBo> {
}
