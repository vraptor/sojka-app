package co.jware.sojka.repository;


import co.jware.sojka.entities.core.PersonBo;

import java.util.List;

public interface PersonRepository extends CrudQueryDslRepository<PersonBo> {

    PersonBo findByEmail(String email);

    List<PersonBo> findByEmailContaining(String email);
}
