package co.jware.sojka.repository;


import co.jware.sojka.entities.core.TeacherBo;

public interface TeacherRepository extends CrudQueryDslRepository<TeacherBo> {
}
