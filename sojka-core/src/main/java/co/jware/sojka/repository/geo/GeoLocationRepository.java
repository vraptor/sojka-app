package co.jware.sojka.repository.geo;


import co.jware.sojka.entities.core.geo.GeoLocationBo;
import co.jware.sojka.repository.CrudQueryDslRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Set;

public interface GeoLocationRepository extends CrudQueryDslRepository<GeoLocationBo> {

    List<GeoLocationBo> findByName(String address);

    @Query("select g from GeoLocationBo g where g.name in (:locations)")
    List<GeoLocationBo> findByNames(@Param("locations") Set<String> locations);

    GeoLocationBo findByPlaceId(String placeId);

    Page<GeoLocationBo> findAllByLatitudeIsNullAndLongitudeIsNull(Pageable pageable);

}
