package co.jware.sojka.repository;


import co.jware.sojka.entities.core.AccountBo;
import co.jware.sojka.entities.core.PersonBo;

public interface AccountRepositoryCustom {

    AccountBo findByOwner(PersonBo owner);
}
