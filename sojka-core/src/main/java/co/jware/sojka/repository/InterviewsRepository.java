package co.jware.sojka.repository;


import co.jware.sojka.entities.core.InterviewBo;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.time.OffsetDateTime;
import java.util.Optional;
import java.util.UUID;

public interface InterviewsRepository extends CrudQueryDslRepository<InterviewBo> {

    Page<InterviewBo> findAllByJobListingIdAndCandidateId(UUID jobListingUuid, UUID candidateUuid, Pageable pageable);

    Page<InterviewBo> findAllByCandidateId(UUID uuid, Pageable pageable);

    Page<InterviewBo> findAllByJobListingId(UUID uuid, Pageable pageable);

    Page<InterviewBo> findAllByInterviewDateBetween(OffsetDateTime from, OffsetDateTime to, Pageable pageable);

    Optional<InterviewBo> findByUuid(UUID uuid);


}
