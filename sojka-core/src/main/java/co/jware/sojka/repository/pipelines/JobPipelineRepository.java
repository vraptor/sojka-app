package co.jware.sojka.repository.pipelines;

import co.jware.sojka.entities.core.JobListingBo;
import co.jware.sojka.entities.core.pipelines.JobPipelineBo;

public interface JobPipelineRepository extends
        PipelineRepository<JobPipelineBo> {

    JobPipelineBo findByJobListing(JobListingBo joblisting);

}
