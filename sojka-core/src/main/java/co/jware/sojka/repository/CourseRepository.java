package co.jware.sojka.repository;


import java.time.OffsetDateTime;
import java.util.UUID;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import co.jware.sojka.entities.core.CourseBo;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface CourseRepository extends CrudQueryDslRepository<CourseBo> {
    @Query("select c from CourseBo c where c.owner.id=:uuid")
    Page<CourseBo> findAllByOwnerId(@Param("uuid") UUID uuid, Pageable pageable);

    Page<CourseBo> findAllByCourseDateBetween(OffsetDateTime from, OffsetDateTime to, Pageable pageable);
}


