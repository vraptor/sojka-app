package co.jware.sojka.repository.pipelines;

import co.jware.sojka.entities.core.pipelines.PersonPipelineBo;


public interface PersonPipelineRepository extends PipelineRepository<PersonPipelineBo> {
}
