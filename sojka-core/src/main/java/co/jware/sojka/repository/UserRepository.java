package co.jware.sojka.repository;


import co.jware.sojka.entities.core.UserBo;

public interface UserRepository extends CrudQueryDslRepository<UserBo> {

    UserBo findByUsername(String username);
}
