package co.jware.sojka.repository.pipelines;


import co.jware.sojka.entities.core.pipelines.PipelineBo;
import co.jware.sojka.repository.CrudQueryDslRepository;
import org.springframework.data.repository.NoRepositoryBean;

@NoRepositoryBean
public interface PipelineRepository<T extends PipelineBo>
        extends CrudQueryDslRepository<T> {
}
