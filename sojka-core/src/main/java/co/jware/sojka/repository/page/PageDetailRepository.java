package co.jware.sojka.repository.page;


import co.jware.sojka.entities.core.metasearch.page.PageDetailBo;
import co.jware.sojka.repository.CrudQueryDslRepository;

public interface PageDetailRepository extends CrudQueryDslRepository<PageDetailBo> {

    PageDetailBo findByUrl(String url);

    boolean existsByUrl(String url);
}