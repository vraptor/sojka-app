package co.jware.sojka.repository.geo;

import co.jware.sojka.entities.core.geo.OsmResponseBo;
import co.jware.sojka.repository.CrudQueryDslRepository;

public interface OsmResponseRepository extends CrudQueryDslRepository<OsmResponseBo> {
}
