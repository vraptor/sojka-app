package co.jware.sojka.repository;


import co.jware.sojka.entities.core.AccountBo;
import co.jware.sojka.entities.core.PersonBo;
import co.jware.sojka.entities.core.UserBo;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Optional;
import java.util.UUID;

public interface AccountRepository extends CrudQueryDslRepository<AccountBo>, AccountRepositoryCustom {

    AccountBo findByOwner(PersonBo owner);

    AccountBo findByUser(UserBo user);

    Optional<AccountBo> findByUserUuid(UUID uuid);

    @Query("select a from AccountBo a where a.owner.id = :uuid")
    Optional<AccountBo> findByOwnerUuid(@Param("uuid") UUID uuid);

    AccountBo findOneByUser_Username(String userName);

}
