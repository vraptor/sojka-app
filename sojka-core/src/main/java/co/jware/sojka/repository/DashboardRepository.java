package co.jware.sojka.repository;


import co.jware.sojka.entities.core.dashboard.DashboardBo;

public interface DashboardRepository extends CrudQueryDslRepository<DashboardBo> {
}
