package co.jware.sojka.repository.search;


import co.jware.sojka.entities.core.metasearch.MetaSearchBo;
import co.jware.sojka.repository.CrudQueryDslRepository;

public interface MetaSearchRepository extends CrudQueryDslRepository<MetaSearchBo> {
}
