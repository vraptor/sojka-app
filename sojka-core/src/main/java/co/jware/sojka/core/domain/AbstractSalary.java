package co.jware.sojka.core.domain;

import co.jware.sojka.core.enums.RecurrencePeriod;
import org.immutables.value.Value;

import javax.annotation.Nullable;
import java.util.Currency;

import static co.jware.sojka.core.domain.SalaryRangeDef.EMPTY;

@Value.Immutable
public abstract class AbstractSalary {

    @Value.Default
    public SalaryRange range() {
        return EMPTY;
    }

    @Nullable
    public abstract Currency currency();

    @Value.Default
    public RecurrencePeriod period() {
        return RecurrencePeriod.MONTHLY;
    }
}
