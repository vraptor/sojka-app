package co.jware.sojka.core.service.entity;

import co.jware.sojka.core.domain.Evaluation;
import co.jware.sojka.core.domain.Interview;
import co.jware.sojka.core.domain.JobListing;
import co.jware.sojka.core.domain.JobResponse;
import co.jware.sojka.core.domain.party.Candidate;
import co.jware.sojka.core.domain.party.Party;
import co.jware.sojka.entities.core.JobResponseBo;
import co.jware.sojka.entities.core.QJobResponseBo;
import co.jware.sojka.repository.JobResponseRepository;
import com.querydsl.core.types.dsl.BooleanExpression;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static java.util.stream.Collectors.toList;

@Service("jobResponseService")
public class JobResponseServiceImpl extends AbstractEntityService implements JobResponseService {
    @Autowired
    JobResponseRepository repository;

    QJobResponseBo $ = QJobResponseBo.jobResponseBo;

    @Override
    public Page<JobResponse> findByJobListing(JobListing jobListing, Pageable pageable) {
        return repository.findAllByJobListingId(resolveUuid(jobListing), pageable)
                .map((entity) -> objectMapper.convertValue(entity, JobResponse.class));
    }

    @Override
    public Page<JobResponse> findByCandidate(Candidate candidate, Pageable pageable) {
        return repository.findAll($.respondent.uuid.eq(resolveUuid(candidate)), pageable)
                .map(entity -> objectMapper.convertValue(entity, JobResponse.class));
    }

    @Override
    public Optional<JobResponse> createJobResponse(JobResponse jobResponse) {
        UUID jobListingUuid = jobResponse.jobListing().uuid();
        UUID respondentUuid = jobResponse.respondent().uuid();
//        Optional<JobResponseEntity> entity = repository.findByRespondentAndJobListing(respondentUuid, jobListingUuid);
//        if (!entity.isPresent()) {
        return Optional.of(jobResponse)
                .map(j -> objectMapper.convertValue(j, JobResponseBo.class))
                .map(repository::save)
                .map(e -> objectMapper.convertValue(e, JobResponse.class));
//        }
//        throw new IllegalArgumentException("JobResponse already submitted");
    }

    @Override
    public List<Interview> getInterviews(JobResponse jobResponse) {
        return repository.findById(resolveUuid(jobResponse))
                .orElseThrow(IllegalArgumentException::new)
                .getInterviews().stream()
                .map(c -> objectMapper.convertValue(c, Interview.class))
                .collect(toList());
    }

    @Override
    public List<Evaluation> getTests(JobResponse jobResponse) {
        return repository.findById(resolveUuid(jobResponse))
                .orElseThrow(IllegalArgumentException::new)
                .getTests().stream()
                .sorted((t0, t1) -> t1.getTestDate().compareTo(t0.getTestDate()))
                .map(c -> objectMapper.convertValue(c, Evaluation.class))
                .collect(toList());
    }

    @Override
    public Optional<JobResponse> findJobResponse(JobResponse response) {
        UUID jobListingUuid = resolveUuid(response.jobListing());
        UUID candidateUuid = resolveUuid(response.respondent());
        QJobResponseBo $ = QJobResponseBo.jobResponseBo;
        BooleanExpression predicate = $.jobListing.uuid.eq(jobListingUuid)
                .and($.respondent.uuid.eq(candidateUuid));
        return Optional.of(repository.findOne(predicate))
                .map(c -> objectMapper.convertValue(c, JobResponse.class))
                .map(JobResponse.class::cast);
    }

    @Override
    public Page<JobResponse> findByRespondent(Party respondent, Pageable pageable) {
        return repository.findAllByRespondentId(resolveUuid(respondent), pageable)
                .map(e -> objectMapper.convertValue(e, JobResponse.class));
    }

}
