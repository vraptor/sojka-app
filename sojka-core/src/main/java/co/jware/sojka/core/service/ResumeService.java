package co.jware.sojka.core.service;


import java.util.List;
import java.util.Optional;
import java.util.UUID;

import co.jware.sojka.core.domain.Resume;
import co.jware.sojka.core.domain.party.Candidate;
import co.jware.sojka.core.domain.party.Submitter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface ResumeService {

    Resume createResume(Resume resume);

    Optional<Resume> submitResume(Resume resume);

    Page<Resume> findByOwner(Candidate candidate, Pageable pageable);

    Page<Resume> findBySubmitter(Submitter submitter, Pageable pageable);

    Resume findResume(UUID uuid);
}
