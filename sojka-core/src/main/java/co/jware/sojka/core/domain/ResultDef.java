package co.jware.sojka.core.domain;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.immutables.value.Value;

import javax.annotation.Nullable;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import java.util.Optional;

@Tupled
@Value.Immutable
@JsonSerialize
@JsonDeserialize
public interface ResultDef {

    boolean result();

    @Max(value = 100)
    @Min(value = 0)
    @Nullable
    Integer percentage();
}
