package co.jware.sojka.core.domain.wrapped;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.immutables.value.Value;

import java.io.IOException;

@Value.Immutable
@Wrapped
@JsonSerialize(as = RecommendationWeight.class, converter = WrapperConverter.class)
@JsonDeserialize(using = _RecommendationWeight.Deserializer.class)
public abstract class _RecommendationWeight extends Wrapper<Double> {

    static class Deserializer extends JsonDeserializer<RecommendationWeight> {
        @Override
        public RecommendationWeight deserialize(JsonParser jp, DeserializationContext ctxt)
                throws IOException {
            JsonNode node = jp.getCodec().readTree(jp);
            double weight = node.asDouble();
            return RecommendationWeight.of(weight);
        }
    }
}
