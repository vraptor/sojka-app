package co.jware.sojka.core.domain.search.page;

import co.jware.sojka.core.domain.BaseImmutable;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.immutables.value.Value;

import javax.annotation.Nullable;

@Value.Immutable
@JsonDeserialize(builder = PageDetail.Builder.class)
public abstract class AbstractPageDetail extends BaseImmutable {

    public static PageDetail empty() {
        return PageDetail.builder()
                .url("n/a")
                .title("n/a")
                .richText("n/a")
                .pageMetaData(PageMetaData.empty())
                .build();
    }

    public abstract String url();

    public abstract String title();

    public abstract PageMetaData pageMetaData();

    public abstract String richText();

    @Nullable
    public abstract String stemmedText();
}
