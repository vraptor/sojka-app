package co.jware.sojka.core.domain.pipelines;

import com.fasterxml.jackson.annotation.JsonTypeName;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.immutables.value.Value;

@Value.Immutable
@JsonDeserialize(builder = SystemPipeline.Builder.class)
@JsonTypeName("SYSTEM_PIPELINE")
public abstract class AbstractSystemPipeline implements Pipeline {

}
