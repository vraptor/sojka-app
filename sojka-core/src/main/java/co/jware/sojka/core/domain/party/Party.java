package co.jware.sojka.core.domain.party;

import java.util.UUID;

import javax.annotation.Nullable;

import co.jware.sojka.core.domain.party.Candidate;
import co.jware.sojka.core.domain.party.External;
import co.jware.sojka.core.domain.party.Hirer;
import co.jware.sojka.core.domain.party.Teacher;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

import co.jware.sojka.core.domain.HasEmail;
import co.jware.sojka.core.domain.Identifiable;


@JsonTypeInfo(use = JsonTypeInfo.Id.NAME)
@JsonSubTypes({
        @JsonSubTypes.Type(name = "EXTERNAL", value = External.class),
        @JsonSubTypes.Type(name = "CANDIDATE", value = Candidate.class),
        @JsonSubTypes.Type(name = "HIRER", value = Hirer.class),
        @JsonSubTypes.Type(name = "TEACHER", value = Teacher.class),
        @JsonSubTypes.Type(name = "BOT", value = Bot.class)
})
public interface Party extends Identifiable, HasEmail {
    Party NULL = new Party() {
        @Override
        public String email() {
            return null;
        }

        @Nullable
        @Override
        public UUID uuid() {
            return null;
        }

        @Nullable
        @Override
        public Long version() {
            return null;
        }
    };

}
