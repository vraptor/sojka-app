package co.jware.sojka.core.domain.search.page;


import co.jware.sojka.core.enums.HirerType;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.immutables.value.Value;

import javax.annotation.Nullable;
import java.util.Locale;

@Value.Immutable
@JsonDeserialize(builder = PageMetaData.Builder.class)
public abstract class AbstractPageMetaData {

    public static PageMetaData empty() {
        return PageMetaData.builder()
                .hirerType(HirerType.UNKNOWN)
                .company("n/a")
                .rawData("n/a")
                .locale(Locale.getDefault())
                .build();
    }

    public abstract String company();

    public abstract HirerType hirerType();

    @Nullable
    public abstract String rawData();

    public abstract Locale locale();
}
