package co.jware.sojka.core.domain;

import co.jware.sojka.core.enums.InvitationState;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.immutables.value.Value;

//@Value.Immutable
//@JsonSerialize(as = InvitationHistory.class)
//@JsonDeserialize(as = InvitationHistory.class)
public interface InvitationHistory<T> {

    abstract Invitation<T> invitation();

    abstract InvitationState state();
}
