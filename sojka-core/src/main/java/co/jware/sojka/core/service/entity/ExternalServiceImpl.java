package co.jware.sojka.core.service.entity;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import co.jware.sojka.core.domain.party.External;
import co.jware.sojka.entities.core.ExternalBo;
import co.jware.sojka.repository.ExternalRepository;
import co.jware.sojka.utils.CastUtils;

@Service("externalService")
public class ExternalServiceImpl extends AbstractEntityService implements ExternalService {

    @Autowired
    ExternalRepository repository;

    @Override
    public External createExternal(External external) {
        return Optional.ofNullable(external)
                .map(ext -> objectMapper.convertValue(ext, ExternalBo.class))
                .map(repository::save)
                .map(e -> objectMapper.convertValue(e, External.class))
                .orElseThrow(() -> new RuntimeException("Error creating external"));
    }

    @Override
    public Optional<External> findByEmail(String email) {
        return Optional.ofNullable(email)
                .map(repository::findByEmail)
                .map(c -> objectMapper.convertValue(c, External.class));
    }

    @Override
    public External findOrCreate(External external) {
        return findByEmail(external.email())
                .orElseGet(() -> createExternal(external));
    }
}
