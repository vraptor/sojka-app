package co.jware.sojka.core.domain;

import java.util.Date;
import java.util.Optional;

import org.immutables.value.Value;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import co.jware.sojka.core.domain.party.Candidate;
import co.jware.sojka.core.domain.wrapped.Description;
import co.jware.sojka.core.enums.EvaluationStatus;

import javax.annotation.Nullable;

import static co.jware.sojka.core.enums.EvaluationStatus.PENDING;

@Value.Immutable
@JsonDeserialize(builder = Evaluation.Builder.class)
public interface AbstractEvaluation extends Identifiable, Ordered {

    String title();

    @Nullable
    Description description();

    @Nullable
    JobListing jobListing();

    Candidate candidate();

    Date testDate();

    default EvaluationStatus status() {
        return PENDING;
    }

    @Nullable
    Result result();

}
