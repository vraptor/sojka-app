package co.jware.sojka.core.service.impl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import co.jware.sojka.core.domain.Account;
import co.jware.sojka.core.domain.HasEmail;
import co.jware.sojka.core.domain.User;
import co.jware.sojka.core.domain.party.Candidate;
import co.jware.sojka.core.domain.party.Hirer;
import co.jware.sojka.core.domain.party.Person;
import co.jware.sojka.core.domain.party.Teacher;
import co.jware.sojka.core.enums.RegistrationType;
import co.jware.sojka.core.service.PersonService;
import co.jware.sojka.core.service.RegisterService;
import co.jware.sojka.core.service.entity.AccountService;

@Service("registerService")
@Transactional
public class RegisterServiceImpl implements RegisterService {
    @Autowired
    private AccountService accountService;
    @Autowired
    private PersonService personService;

    @Override
    public Account register(String firstName, String lastName, String email, RegistrationType type) {
        return Optional.ofNullable(type).map(t -> {
            Person owner = null;
            switch (type) {
                case CANDIDATE:
                    owner = Candidate.builder()
                            .firstName(firstName)
                            .lastName(lastName)
                            .email(email)
                            .build();
                    break;
                case HIRER:
                    owner = Hirer.builder()
                            .firstName(firstName)
                            .lastName(lastName)
                            .email(email)
                            .build();
                    break;
                case TEACHER:
                    owner = Teacher.builder()
                            .firstName(firstName)
                            .lastName(lastName)
                            .email(email)
                            .build();
                    break;
                default:
                    throw new IllegalArgumentException("Registration type is required");
            }
            return owner;
        }).map(p -> {
            Person owner = personService.createPerson(p);
            Account account = Account.builder()
                    .owner(owner)
                    .build();
            return accountService.createAccount(account);
        }).orElseThrow(RuntimeException::new);
    }

    @Override
    public Account attachUser(Account account) {
        String username = Optional.ofNullable(account.owner())
                .map(HasEmail::email)
                .orElseThrow(RuntimeException::new);
        User user = User.builder().username(username).build();
        Account result = Account.copyOf(account);
        result = result.withUser(user);
        result = accountService.save(result);
        return result;
    }

}
