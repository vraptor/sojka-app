package co.jware.sojka.core.service.impl;

import java.util.Optional;
import java.util.UUID;

import co.jware.sojka.core.exceptions.InterviewAlreadyScheduledException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.querydsl.core.types.Predicate;

import co.jware.sojka.core.domain.Interview;
import co.jware.sojka.core.domain.JobListing;
import co.jware.sojka.core.domain.party.Candidate;
import co.jware.sojka.core.service.InterviewService;
import co.jware.sojka.core.service.entity.AbstractEntityService;
import co.jware.sojka.entities.core.InterviewBo;
import co.jware.sojka.entities.core.QInterviewBo;
import co.jware.sojka.repository.InterviewsRepository;
import co.jware.sojka.utils.CastUtils;

@Service("interviewService")
public class InterviewServiceImpl extends AbstractEntityService implements InterviewService {

    private final InterviewsRepository repository;

    public InterviewServiceImpl(InterviewsRepository repository) {
        this.repository = repository;
    }

    @Override
    public Optional<Interview> scheduleInterview(Interview interview) {
        checkIfExists(interview);
        return createInterview(interview);
    }

    @Override
    public Optional<Interview> createInterview(Interview interview) {
        return Optional.ofNullable(interview)
                .map(c -> objectMapper.convertValue(c, InterviewBo.class))
                .map(repository::save)
                .map(c -> objectMapper.convertValue(c, Interview.class));
    }


    @Override
    public Optional<Interview> setResult(Interview interview) {
        return loadInterview(interview)
                .filter(interview::equals);
    }

    @Override
    public Optional<Interview> loadInterview(Interview interview) {
        return Optional.ofNullable(interview)
                .map(this::resolveUuid)
                .map(repository::findById)
                .map(c -> objectMapper.convertValue(c, Interview.class));
    }

    @Override
    public Optional<Interview> changeInterviewStatus(Interview interview) {
        Optional<Interview> loaded;
        loaded = loadInterview(interview)
                .map(it -> it.withStatus(interview.status()))
                .map(c -> objectMapper.convertValue(c, InterviewBo.class))
                .map(repository::save)
                .map(c -> objectMapper.convertValue(c, Interview.class));
        return loaded;
    }

    public void checkIfExists(Interview interview) {
        QInterviewBo ie = QInterviewBo.interviewBo;
        Predicate predicate = ie.candidate.uuid.eq(checkCandidateUuid(interview))
                .and(ie.jobListing.uuid.eq(checkJobListingUuid(interview)))
                .and(ie.order.eq(interview.order()));
        if (repository.count(predicate) > 0)
            throw new InterviewAlreadyScheduledException();
    }

    private UUID checkJobListingUuid(Interview interview) {
        return resolveUuid(interview.jobListing());
    }

    private UUID checkCandidateUuid(Interview interview) {
        return resolveUuid(interview.candidate());
    }

    @Override
    public Page<Interview> findInterviewsLike(Interview interview, Pageable pageable) {
        UUID jobListingUuid = resolveUuid(interview.jobListing());
        UUID candidateUuid = resolveUuid(interview.candidate());
        return repository.findAllByJobListingIdAndCandidateId(jobListingUuid, candidateUuid, pageable)
                .map(e -> objectMapper.convertValue(e, Interview.class));
    }

    @Override
    public Optional<Interview> findByUuid(UUID uuid) {
        return repository.findByUuid(uuid)
                .map(e -> objectMapper.convertValue(e, Interview.class));
    }

    @Override
    public Page<Interview> findByCandidate(Candidate candidate, Pageable pageable) {
        return repository.findAllByCandidateId(candidate.uuid(), pageable)
                .map(e -> objectMapper.convertValue(e, Interview.class));
    }

    @Override
    public Page<Interview> findByJbListing(JobListing jobListing, Pageable pageable) {
        return repository.findAllByJobListingId(resolveUuid(jobListing), pageable)
                .map(e -> objectMapper.convertValue(e, Interview.class));
    }

}
