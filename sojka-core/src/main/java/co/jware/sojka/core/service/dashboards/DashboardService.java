package co.jware.sojka.core.service.dashboards;

import co.jware.sojka.core.domain.dashboards.Dashboard;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;
import java.util.UUID;

public interface DashboardService {


    Dashboard createDashboard(Dashboard dashboard);

    Dashboard saveDashboard(Dashboard newDashboard);

    Dashboard updateDashboard(Dashboard dashboard);

    Optional<Dashboard> getDashboard(UUID uuid);

    Page<Dashboard> getByOwner(UUID uuid, Pageable pageable);

    void deleteDashboard(UUID uuid);
}
