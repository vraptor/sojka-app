package co.jware.sojka.core.domain.mapping;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

public class ObjectMapperHolder {

    private static ObjectMapper OBJECTMAPPER;

    @Autowired
    void setMapper(ObjectMapper mapper) {
        OBJECTMAPPER = mapper;
    }

    public static ObjectMapper mapper() {
        return OBJECTMAPPER;
    }
}
