package co.jware.sojka.core.service.geo;


import co.jware.sojka.core.domain.geo.GeoLocation;

import java.util.Set;

public interface GoogleMapsService {

    Set<GeoLocation> geocode(String address);
}
