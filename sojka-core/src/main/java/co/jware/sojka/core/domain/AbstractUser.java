package co.jware.sojka.core.domain;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.immutables.value.Value;

import javax.annotation.Nullable;
import java.util.Optional;

@Value.Immutable
@JsonSerialize(as = User.class)
@JsonDeserialize(as = User.class)
public interface AbstractUser extends Identifiable {
    User NULL = User.builder()
            .username("_UNKNOWN_")
            .build();

    String username();

    @Nullable
    Boolean enabled();
}
