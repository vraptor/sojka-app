package co.jware.sojka.core.service.impl;

import co.jware.sojka.core.domain.search.MetaLink;
import co.jware.sojka.core.domain.search.MetaSearch;
import co.jware.sojka.core.domain.search.MetaSearchLink;
import co.jware.sojka.core.service.MetaLinkService;
import co.jware.sojka.core.service.MetaSearchLinkService;
import co.jware.sojka.core.service.entity.MetaSearchService;
import co.jware.sojka.entities.core.metasearch.MetaSearchLinkBo;
import co.jware.sojka.repository.search.MetaSearchLinkRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.ILock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static java.util.stream.Collectors.toList;

@Service("metaSearchLinkService")
@Transactional
public class MetaSearchLinkServiceImpl implements MetaSearchLinkService {
    @Autowired
    private HazelcastInstance hazelcastInstance;

    @Autowired
    private ObjectMapper mapper;
    @Autowired
    private MetaLinkService metaLinkService;
    @Autowired
    private MetaSearchService metaSearchService;
    @Autowired
    private MetaSearchLinkRepository repository;

    @Override
    public MetaSearchLink storeMetaSearchLink(MetaSearchLink metaSearchLink) {
        ILock metaSearchLinkLock = hazelcastInstance.getLock("metaSearchLink");
        metaSearchLinkLock.lock();
        try {
            MetaLink metaLink = metaLinkService.findOrCreate(metaSearchLink.metaLink());
            MetaSearch metaSearch = metaSearchService.findOrCreate(metaSearchLink.metaSearch());
            metaLink = metaLink
                    .withLocations(metaSearch.locations())
                    .withKeywords(metaSearch.keywords());
            Optional<MetaSearchLink> linkOptional = findMetaServiceLink(metaSearch.uuid(), metaLink.uuid());
            MetaLink finalMetaLink = metaLink;
            metaLinkService.saveLink(metaLink);
            MetaSearchLink newMetaSearchLink = linkOptional
                    .map(m -> m.withMetaLink(finalMetaLink).withMetaSearch(metaSearch))
                    .orElse(MetaSearchLink.of(metaLink, metaSearch));

            return save(newMetaSearchLink);
        } finally {
            metaSearchLinkLock.unlock();
        }
    }

    private Optional<MetaSearchLink> findMetaServiceLink(UUID metaSearchUuid, UUID metaLinkUuid) {
        MetaSearchLinkBo entity = repository.findByMetaSearchUuidAndMetaLinkUuid(metaSearchUuid, metaLinkUuid);
        return Optional.ofNullable(entity).map(l -> mapper.convertValue(l, MetaSearchLink.class));
    }

    @Override
    public List<MetaSearch> getMetaSearches(MetaLink metaLink) {
        return Optional.ofNullable(metaLink.uuid())
                .map(repository::findByMetaLinkUuid)
                .orElseGet(Collections::emptyList).stream()
                .map(MetaSearchLinkBo::getMetaSearch)
                .map(l -> mapper.convertValue(l, MetaSearch.class))
                .collect(toList());
    }

    @Override
    public MetaSearchLink save(MetaSearchLink metaSearchLink) {
        MetaSearchLinkBo entity = mapper.convertValue(metaSearchLink, MetaSearchLinkBo.class);
        MetaSearchLinkBo linkEntity = repository.save(entity);
        return mapper.convertValue(linkEntity, MetaSearchLink.class);
    }

    @Override
    public Page<MetaLink> getLinksByMetaSearch(UUID uuid, Pageable pageable) {
        Page<MetaSearchLinkBo> page = repository.findByMetaSearchUuid(uuid, pageable);
        return page.map(e -> mapper.convertValue(e.getMetaLink(), MetaLink.class));
    }

    @Override
    public Page<MetaLink> getLinksByMetaSearch(MetaSearch search, Pageable pageable) {
        return metaSearchService.getSearch(search)
                .map(s -> getLinksByMetaSearch(s.uuid(), pageable))
                .orElseGet(() -> getLinksByMetaSearch(search.uuid(), pageable));
    }

    @Override
    public long countByMetaSearch(MetaSearch search) {
        return metaSearchService.getSearch(search)
                .map(s -> countLinksByMetaSearch(s.uuid()))
                .orElse(0L);
    }

    private long countLinksByMetaSearch(UUID uuid) {
        return repository.countByMetaSearchUuid(uuid);
    }
}
