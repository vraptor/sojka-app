package co.jware.sojka.core.domain;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.google.common.base.Preconditions;
import org.immutables.value.Value;

import java.time.LocalDate;
import java.util.UUID;

@Value.Immutable
@JsonSerialize
@JsonDeserialize
public interface AbstractCourseClass extends Identifiable {

    UUID course();

    LocalDate startDate();

    LocalDate endDate();

    int seats();

    @Value.Check
    default void check() {
        Preconditions.checkState(startDate().isBefore(endDate()));
    }
}
