@Value.Style(typeImmutable = "*", typeModifiable = "*VO",
        defaultAsDefault = true, jdkOnly = true,
        deepImmutablesDetection = true, depluralize = true,
        additionalJsonAnnotations = JsonUnwrapped.class, beanFriendlyModifiables = true)
package co.jware.sojka.core.domain;

import com.fasterxml.jackson.annotation.JsonUnwrapped;
import org.immutables.value.Value;