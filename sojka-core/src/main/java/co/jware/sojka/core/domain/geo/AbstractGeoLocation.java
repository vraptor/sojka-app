package co.jware.sojka.core.domain.geo;

import co.jware.sojka.core.enums.GeoLocationStatus;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.immutables.value.Value;

import javax.annotation.Nullable;
import java.io.Serializable;
import java.util.UUID;

@Value.Immutable
@JsonDeserialize(builder = GeoLocation.Builder.class)
public abstract class AbstractGeoLocation implements Serializable {

    @Nullable
    abstract public UUID uuid();

    abstract public String name();

    @Nullable
    @JsonProperty("place_id")
    abstract public String placeId();

    @Nullable
    abstract public String fullResult();

    @Value.Auxiliary
    @Nullable
    abstract public Long version();

    @Nullable
    @JsonProperty("lat")
    abstract public Double latitude();

    @Nullable
    @JsonProperty("lon")
    abstract public Double longitude();

    @Nullable
    abstract public GeoLocationAddress address();

    @Value.Default
    public GeoLocationStatus status() {
        return GeoLocationStatus.PENDING;
    }
}
