package co.jware.sojka.core.domain.wrapped;

import org.immutables.value.Value;

@Value.Immutable
@Wrapped
public abstract class _LocationEntry extends Wrapper<String> {
}
