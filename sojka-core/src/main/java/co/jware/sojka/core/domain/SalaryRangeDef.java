package co.jware.sojka.core.domain;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.immutables.value.Value;

import java.math.BigDecimal;

import static java.math.BigDecimal.ZERO;

@Tupled
@Value.Immutable
@JsonSerialize(as = SalaryRange.class)
@JsonDeserialize(as = SalaryRange.class)
public interface SalaryRangeDef {


    SalaryRange EMPTY = SalaryRange.of(ZERO, ZERO);

    BigDecimal salaryFrom();

    BigDecimal salaryTo();

    @JsonIgnore
    default boolean isFull() {
        return salaryFrom().doubleValue() > 0 ||
                salaryTo().doubleValue() > 0;
    }
}
