package co.jware.sojka.core.service.entity;


import java.util.List;
import java.util.Optional;

import co.jware.sojka.core.domain.Company;
import co.jware.sojka.core.domain.party.Person;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface CompanyService {
    Optional<Company> getOrCreate(Company company);

    List<Person> personnel(Company company);

    Page<Person> personnel(Company company, Pageable pageable);

    Optional<Company> findByName(String company);
}
