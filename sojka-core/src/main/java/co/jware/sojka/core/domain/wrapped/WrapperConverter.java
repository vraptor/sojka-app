package co.jware.sojka.core.domain.wrapped;

import com.fasterxml.jackson.databind.util.StdConverter;

class WrapperConverter<T extends Wrapper> extends StdConverter<T, String> {
    @Override
    public String convert(T value) {
        return String.valueOf(((T) value).value());
    }
}
