package co.jware.sojka.core.service.entity;

import java.util.Optional;

import co.jware.sojka.core.domain.party.External;


public interface ExternalService {

    External createExternal(External external);

    Optional<External> findByEmail(String email);

    External findOrCreate(External external);
}
