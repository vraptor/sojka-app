package co.jware.sojka.core.service.pipelines;


import co.jware.sojka.core.domain.JobListing;
import co.jware.sojka.core.domain.party.Candidate;
import co.jware.sojka.core.domain.party.Party;
import co.jware.sojka.core.domain.party.Person;
import co.jware.sojka.core.domain.pipelines.*;

import java.util.UUID;

public interface PipelineService {

    OwnerPipeline generateOwnerPipeline(SystemPipeline pipeline, Party owner);

    OwnerPipeline generatePipelineByOwner(Party owner);

    JobPipeline generatePipelineByJobListing(JobListing jobListing);

    JobPipeline generateJobPipeline(OwnerPipeline ownerPipeline, JobListing jobListing);

    PersonPipeline generatePersonPipeline(JobPipeline jobPipeline, Candidate candidate);

    Pipeline createPipeline(Pipeline pipeline, String userName);

    SystemPipeline getOrCreateSystemPipeline();

    OwnerPipeline getOrCreateOwnerPipeline(OwnerPipeline pipeline);

    JobPipeline getOrCreateJobPipeline(JobPipeline jobPipeline);

    OwnerPipeline findOwnerPipeline(UUID uuid);

    JobPipeline getJobPipeline(UUID uuid);
}
