package co.jware.sojka.core.service.impl;

import co.jware.sojka.core.domain.Resume;
import co.jware.sojka.core.domain.party.Candidate;
import co.jware.sojka.core.domain.party.Submitter;
import co.jware.sojka.core.service.ResumeService;
import co.jware.sojka.core.service.TransactionHelper;
import co.jware.sojka.core.service.entity.AbstractEntityService;
import co.jware.sojka.entities.core.ResumeBo;
import co.jware.sojka.repository.ResumeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.UUID;

@Service("resumeService")
public class ResumeServiceImpl extends AbstractEntityService implements ResumeService {

    private final ResumeRepository resumeRepository;

    @Autowired
    private TransactionHelper helper;

    public ResumeServiceImpl(ResumeRepository resumeRepository) {
        this.resumeRepository = resumeRepository;
    }

    @Override
    public Resume createResume(Resume resume) {
        return Optional.ofNullable(resume)
                .map(r -> objectMapper.convertValue(r, ResumeBo.class))
                .map(entity -> helper.withTransaction(() -> resumeRepository.save(entity)))
                .map(e -> objectMapper.convertValue(e, Resume.class))
                .orElseThrow(IllegalArgumentException::new);
    }

    private Submitter hasSupportedSubmitter(Resume resume) {
        return Optional.ofNullable(resume.submitter())
                .filter(Submitter::isSupportedSubmitter)
                .orElseThrow(IllegalArgumentException::new);
    }

    @Override
    public Optional<Resume> submitResume(Resume resume) {
        Submitter submitter = hasSupportedSubmitter(resume);
        return Optional.ofNullable(resume)
                .map(e -> objectMapper.convertValue(e, ResumeBo.class))
                .map(entity -> helper.withTransaction(() -> resumeRepository.save(entity)))
                .map(r -> objectMapper.convertValue(r, Resume.class));
    }

    @Override
    public Page<Resume> findByOwner(Candidate candidate, Pageable pageable) {
        UUID uuid = resolveUuid(candidate);
        return resumeRepository.findAllByOwnerId(uuid, pageable)
                .map(e -> objectMapper.convertValue(e, Resume.class));
    }

    @Override
    public Page<Resume> findBySubmitter(Submitter submitter, Pageable pageable) {
        UUID uuid = resolveUuid(submitter);
        return resumeRepository.findAllBySubmitterId(uuid, pageable)
                .map(e -> objectMapper.convertValue(e, Resume.class));
    }

    @Override
    public Resume findResume(UUID uuid) {
        return resumeRepository.findById(uuid)
                .map(e -> objectMapper.convertValue(e, Resume.class))
                .orElseThrow(RuntimeException::new);
    }
}
