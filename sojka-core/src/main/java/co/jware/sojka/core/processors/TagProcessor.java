package co.jware.sojka.core.processors;


import co.jware.sojka.core.domain.Tag;

import java.math.BigDecimal;
import java.util.Set;

public interface TagProcessor {
    BigDecimal evaluateTags(Set<Tag> tags, Set<Tag> etalonTags);
}
