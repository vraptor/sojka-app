package co.jware.sojka.core.exceptions;


public class MappingException extends RuntimeException {
    public MappingException(String message) {
        super(message);
    }
}
