package co.jware.sojka.core.domain;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.immutables.value.Value;

@Value.Immutable
@JsonSerialize
@JsonDeserialize(builder = Location.Builder.class)
public interface AbstractLocation {
    @Value.Default
    default boolean primary() {
        return false;
    }

    String city();

    String region();

    String country();

}
