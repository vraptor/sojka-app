package co.jware.sojka.core.service.entity;


import co.jware.sojka.core.domain.search.MetaSearch;
import org.springframework.data.domain.Pageable;

import java.util.Optional;
import java.util.UUID;

public interface MetaSearchService {

    MetaSearch storeSearch(MetaSearch search);

    Optional<MetaSearch> getSearch(UUID uuid);

    MetaSearch findOrCreate(MetaSearch search);

    Optional<MetaSearch> getSearch(MetaSearch search);

    void getSearchLinks(UUID uuid, Pageable page);

    MetaSearch save(MetaSearch metaSearch);


}
