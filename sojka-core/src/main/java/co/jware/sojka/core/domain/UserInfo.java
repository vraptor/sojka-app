package co.jware.sojka.core.domain;

import co.jware.sojka.entities.core.UserBo;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.HashSet;

import static java.util.Optional.ofNullable;
import static java.util.stream.Collectors.toList;

public class UserInfo implements UserDetails {
    private UserBo userEntity;

    public UserInfo(UserBo userEntity) {

        this.userEntity = userEntity;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return ofNullable(userEntity.getRoles()).orElse(new HashSet<>())
                .stream()
                .map(r -> new SimpleGrantedAuthority(r.name()))
                .collect(toList());
    }

    @Override
    public String getPassword() {
        return userEntity.getPassword();
    }

    @Override
    public String getUsername() {
        return userEntity.getUsername();
    }

    @Override
    public boolean isAccountNonExpired() {

        return getEnabled();
    }

    @Override
    public boolean isAccountNonLocked() {
        return getEnabled();
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return userEntity.getEnabled();
    }

    @Override
    public boolean isEnabled() {
        return getEnabled();
    }

    private Boolean getEnabled() {
        return ofNullable(userEntity.getEnabled()).orElse(false);
    }
}
