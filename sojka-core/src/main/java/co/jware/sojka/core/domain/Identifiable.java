package co.jware.sojka.core.domain;


import co.jware.sojka.core.domain.party.Candidate;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import org.immutables.value.Value;

import javax.annotation.Nullable;
import java.io.Serializable;
import java.util.Optional;
import java.util.UUID;

//@JsonTypeInfo(use = JsonTypeInfo.Id.NAME)
//@JsonSubTypes({
//        @JsonSubTypes.Type(value = JobListing.class, name = "JOB_LISTING"),
//        @JsonSubTypes.Type(value = Candidate.class, name = "CANDIDATE"),
//        @JsonSubTypes.Type(value = Evaluation.class, name = "TEST"),
//        @JsonSubTypes.Type(value = Interview.class, name = "INTERVIEW"),
//        @JsonSubTypes.Type(value = JobResponse.class, name = "JOB_RESPONSE")
//})
public interface Identifiable extends Serializable {

    @Value.Auxiliary
    @Nullable
    UUID uuid();

    @Value.Auxiliary
    @Nullable
    Long version();

    @JsonIgnore
    @Value.Auxiliary
    @Value.Derived
    default boolean isNew() {
        return uuid() == null;
    }
}
