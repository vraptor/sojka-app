package co.jware.sojka.core.domain;

import co.jware.sojka.core.domain.party.Hirer;
import co.jware.sojka.core.domain.party.Party;
import co.jware.sojka.core.domain.wrapped.Benefit;
import co.jware.sojka.core.domain.wrapped.JobSector;
import co.jware.sojka.core.domain.wrapped.Requirement;
import co.jware.sojka.core.enums.EducationLevel;
import co.jware.sojka.core.enums.JobListingOrigin;
import co.jware.sojka.core.enums.JobListingStatus;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonTypeName;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.immutables.serial.Serial;
import org.immutables.value.Value;

import javax.annotation.Nullable;
import java.time.OffsetDateTime;
import java.util.List;
import java.util.Set;

import static co.jware.sojka.core.enums.JobListingStatus.EXPIRED;
import static co.jware.sojka.core.enums.JobListingStatus.PENDING;
import static com.google.common.base.Preconditions.checkState;

@Value.Immutable
@Serial.Structural
@Serial.Version(100)
@JsonSerialize(as = JobListing.class)
@JsonDeserialize(builder = JobListing.Builder.class)
//@JsonTypeInfo(use = JsonTypeInfo.Id.NAME,property = "XXX")
//@JsonSubTypes({
//        @JsonSubTypes.Type(value = JobListing.class, name = "JOB_LISTING")
//})
@JsonTypeName("JOB_LISTING")
public interface AbstractJobListing extends Identifiable {


    JobListing NULL = JobListing.builder()
            .owner(Hirer.NULL)
            .name("__UNDEFINED__")
            .build();


    @Nullable
    Party owner();

    @Nullable
    String title();

    @Nullable
    String link();

    @Nullable
    List<Location> locations();

    String name();

    @Nullable
    String function();

    @Nullable
    String industry();

    @Nullable
    String description();

    Set<JobSector> jobSectors();

    @Nullable
    String summary();

    @Nullable
    List<Requirement> requirements();

    default int capacity() {
        return 1;
    }

    default int freeCapacity() {
        return 1;
    }

    @JsonFormat(shape = JsonFormat.Shape.STRING,
            pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSZZ")
    default OffsetDateTime validFrom() {
        return OffsetDateTime.now();
    }

    @JsonFormat(shape = JsonFormat.Shape.STRING,
            pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSZZ")
    default OffsetDateTime validTo() {
        return OffsetDateTime.now().plusDays(30);
    }

    @Nullable
    Set<Tag> tags();

    @Nullable
    Set<Benefit> benefits();

    @Nullable
    Salary salary();

    @Value.Default
    default JobListingOrigin origin() {
        return JobListingOrigin.UNKNOWN;
    }

    @Value.Default
    default JobListingStatus status() {
        if (isExpired()) {
            return EXPIRED;
        }
        return PENDING;
    }

    @Nullable
    EducationLevel educationLevel();

    @Nullable
    String externalJobId();

    @Nullable
    String source();

    @Value.Check
    default void check() {
        checkState(validFrom().isBefore(validTo()), "Valid from  must be before valid to");
        checkState(capacity() > 0, "Capacity must be equal or greater than 1");
        checkState(capacity() >= freeCapacity(), "Free capacity can't exceed capacity");
    }

    @Value.Derived
    default boolean hasFreeCapacity() {
        return freeCapacity() > 0;
    }

    @JsonIgnore
    default boolean isExpired() {
        return this.validTo().isBefore(OffsetDateTime.now());
    }

    default boolean active() {
        return true;
    }
}
