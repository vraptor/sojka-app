package co.jware.sojka.core.domain.pipelines;

import co.jware.sojka.core.domain.JobListing;

import com.fasterxml.jackson.annotation.JsonTypeName;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.immutables.value.Value;

@Value.Immutable
@JsonDeserialize(builder = JobPipeline.Builder.class)
@JsonTypeName("JOB_PIPELINE")
public abstract class AbstractJobPipeline implements Pipeline {
    abstract public JobListing jobListing();
}
