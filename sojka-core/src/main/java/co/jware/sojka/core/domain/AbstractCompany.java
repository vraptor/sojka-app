package co.jware.sojka.core.domain;

import com.fasterxml.jackson.annotation.JsonTypeName;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.immutables.value.Value;

import javax.annotation.Nullable;
import java.util.Optional;

@Value.Immutable
@JsonSerialize(as = Company.class)
@JsonDeserialize(builder = Company.Builder.class)
@JsonTypeName("COMPANY")
public interface AbstractCompany extends Identifiable {
    @Value.Parameter
    String name();

    @Nullable
    String summary();
}
