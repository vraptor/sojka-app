package co.jware.sojka.core.exceptions;

public class InvalidStatusRequestedException extends RuntimeException {

    public InvalidStatusRequestedException(String msg) {
        super(msg);
    }
}
