package co.jware.sojka.core.service.entity;

import co.jware.sojka.core.domain.CourseClass;
import co.jware.sojka.core.domain.party.Person;
import co.jware.sojka.core.service.PersonService;
import co.jware.sojka.entities.core.CourseClassBo;
import co.jware.sojka.repository.CourseClassRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service("courseClassService")
@Transactional
public class CourseClassServiceImpl extends AbstractEntityService<CourseClassBo>
        implements CourseClassService {

    private final CourseClassRepository repository;
    @Autowired
    private PersonService personService;

    public CourseClassServiceImpl(CourseClassRepository repository) {
        this.repository = repository;
    }

    @Override
    public List<Person> loadStudents(CourseClass courseClass) {
        return repository.findById(resolveUuid(courseClass))
                .orElseThrow(IllegalArgumentException::new)
                .getStudents().stream()
                .map(personService::findByUuid)
                .map(Person.class::cast)
                .collect(Collectors.toList());
    }
}
