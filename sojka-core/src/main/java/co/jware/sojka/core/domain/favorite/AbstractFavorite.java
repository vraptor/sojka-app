package co.jware.sojka.core.domain.favorite;


import co.jware.sojka.core.domain.Identifiable;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import org.immutables.value.Value;
import org.immutables.value.Value.Check;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.google.common.base.Preconditions;

import co.jware.sojka.core.domain.party.Person;

@Value.Immutable
@JsonSerialize
@JsonDeserialize(builder = Favorite.Builder.class)
public interface AbstractFavorite<T extends Identifiable> extends Identifiable {

    @JsonTypeInfo(use = JsonTypeInfo.Id.NAME)
    Person owner();

    @JsonTypeInfo(use = JsonTypeInfo.Id.NAME)
    T subject();

    @Check
    default void check() {
        Preconditions.checkState(!owner().equals(subject()));
    }
}
