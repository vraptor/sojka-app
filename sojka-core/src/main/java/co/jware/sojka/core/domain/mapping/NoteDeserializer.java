
package co.jware.sojka.core.domain.mapping;


import co.jware.sojka.core.domain.wrapped.Note;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;

import java.io.IOException;

import static java.util.Optional.ofNullable;

public class NoteDeserializer extends JsonDeserializer<Note> {
    @Override
    public Note deserialize(JsonParser jp, DeserializationContext ctx) throws IOException {
        JsonNode node = jp.getCodec().readTree(jp);
        String text = ofNullable(node.get("text")).map(JsonNode::asText)
                .orElse("");
        return Note.of(text);
    }
}
