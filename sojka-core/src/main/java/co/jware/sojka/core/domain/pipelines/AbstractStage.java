package co.jware.sojka.core.domain.pipelines;

import co.jware.sojka.core.domain.Identifiable;
import co.jware.sojka.core.domain.wrapped.Description;
import co.jware.sojka.core.enums.StageType;

import com.fasterxml.jackson.annotation.JsonTypeName;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.immutables.value.Value;

import javax.annotation.Nullable;
import java.util.Optional;
import java.util.Set;

@Value.Immutable
@JsonDeserialize(builder = Stage.Builder.class)
@JsonTypeName("PIPELINE_STAGE")
public interface AbstractStage extends Identifiable {

    String name();

    int position();

    Set<Label> label();

    Set<Card<?>> cards();

    @Nullable
    Description description();

    default StageType stageType() {
        return StageType.GENERIC;
    }
}
