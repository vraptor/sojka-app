package co.jware.sojka.core.service.impl;


import co.jware.sojka.core.domain.UserInfo;
import co.jware.sojka.entities.core.UserBo;
import co.jware.sojka.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import javax.annotation.PostConstruct;
import java.sql.ResultSet;
import java.sql.SQLException;

import static java.lang.String.format;

public class DefaultUserDetailsService implements UserDetailsService {
    @Autowired
    UserRepository userRepository;
    @Autowired
    PasswordEncoder passwordEncoder;
    @Autowired
    JdbcTemplate jdbcTemplate;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Assert.hasText(username, "Username should not be empty");
        UserBo userEntity = userRepository.findByUsername(username);
        if (null != userEntity) {
            return new UserInfo(userEntity);
        }
        throw new UsernameNotFoundException(format("username %s not found", username));
    }

    //    @Scheduled(fixedRate = 180000L)
//    @Async()
    @PostConstruct
    @Transactional()
    public void init() {
        String sql = "select uuid,passwd from users where enabled is null";
        jdbcTemplate.query(sql, new RowCallbackHandler() {
            @Override
            public void processRow(ResultSet rs) throws SQLException {
                String uuid = rs.getString(1);
                String password = rs.getString(2);
                String encoded = passwordEncoder.encode(password);
                jdbcTemplate.update("update users set passwd=?, enabled= 'Y' where uuid = ? ", encoded, uuid);
            }
        });
    }
}
