package co.jware.sojka.core.domain;

import org.immutables.builder.Builder;
import org.immutables.value.Value;

import co.jware.sojka.core.domain.party.Person;
import co.jware.sojka.core.enums.RegistrationType;

@Value.Immutable
public interface AbstractRegistration {
    @Builder.Switch(defaultName = "CANDIDATE")
    RegistrationType type();

    @Builder.Parameter
    Person registrant();

//    @Builder.Factory
//    static Registration factory(@Builder.Switch RegistrationType type) {
//        return Registration.builder()
//                .type(type)
//                .build();
//    }

    @Value.Check
    default void doCheck() {
    }
}
