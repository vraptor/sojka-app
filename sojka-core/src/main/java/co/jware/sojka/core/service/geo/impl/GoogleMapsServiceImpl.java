package co.jware.sojka.core.service.geo.impl;

import co.jware.sojka.core.domain.geo.GeoLocation;
import co.jware.sojka.core.service.geo.GoogleMapsService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.maps.GeoApiContext;
import com.google.maps.GeocodingApi;
import com.google.maps.model.AddressType;
import com.google.maps.model.GeocodingResult;
import com.google.maps.model.LatLng;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.Arrays;
import java.util.Set;

import static java.util.stream.Collectors.toSet;

@Service("googleMapsService")

public class GoogleMapsServiceImpl implements GoogleMapsService {

    private GeoApiContext context;

    @Value("${system.google.maps.apiKey:AIzaSyDBL0DKQJjcgWiFIZ801qOfHeNFolxD6gc}")
    private String apiKey;

    @Autowired
    private ObjectMapper mapper;

    @Override
    public Set<GeoLocation> geocode(String address) {
        try {
            GeocodingResult[] results = GeocodingApi.newRequest(context)
                    .address(address)
                    .language("cs")
                    .await();
            return Arrays.stream(results).map(r -> {
                try {
                    LatLng location = r.geometry.location;
                    AddressType[] types = r.types;
                    AddressType type = types[0];
                    String name = Arrays.stream(r.addressComponents)
                            .filter(a -> a.types[0].name().equals(type.name()))
                            .findAny()
                            .map(c -> c.shortName)
                            .orElse(r.formattedAddress);
                    return GeoLocation.builder()
                            .name(name)
                            .placeId(r.placeId)
                            .latitude(location.lat)
                            .longitude(location.lng)
                            .fullResult(mapper.writeValueAsString(r))
                            .build();
                } catch (JsonProcessingException e) {
                    throw new RuntimeException("Error converting geocoding result", e);
                }
            }).collect(toSet());
        } catch (Exception e) {
            // FIXME: 3/31/2017 - Declare specific exception
            throw new RuntimeException("Error geocoding", e);
        }
    }

    @PostConstruct
    void init() {
        this.context = new GeoApiContext()
                .setApiKey(apiKey)
                .setQueryRateLimit(1, 15_000);
    }
}
