package co.jware.sojka.core.service.entity;


import co.jware.sojka.core.domain.party.Teacher;

public interface TeacherService {

    Teacher createTeacher(Teacher teacher);
}
