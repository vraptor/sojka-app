package co.jware.sojka.core.service;


import co.jware.sojka.core.domain.search.MetaSearchResult;
import co.jware.sojka.entities.core.metasearch.PageBo;

public interface PageService {

    PageBo createPage(MetaSearchResult metaSearchResult);
}
