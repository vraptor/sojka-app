package co.jware.sojka.core;

import co.jware.sojka.core.service.HibernateSearchService;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.Module;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.hibernate5.Hibernate5Module;
import org.springframework.boot.autoconfigure.jackson.Jackson2ObjectMapperBuilderCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.persistence.EntityManagerFactory;


@Configuration
public class SojkaCoreConfig {
    @Bean
    public Jackson2ObjectMapperBuilderCustomizer objectMapperBuilderCustomizer() {
        return builder -> {
//            builder.modulesToInstall(Jdk8Module.class, JavaTimeModule.class);
            builder.failOnUnknownProperties(false);
            builder.serializationInclusion(JsonInclude.Include.NON_NULL);
            builder.featuresToDisable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
        };
    }

    @Bean
    public Module hibernate5Module() {
        return new Hibernate5Module()
                .disable(Hibernate5Module.Feature.FORCE_LAZY_LOADING)
                .enable(Hibernate5Module.Feature.REPLACE_PERSISTENT_COLLECTIONS)
                .enable(Hibernate5Module.Feature.SERIALIZE_IDENTIFIER_FOR_LAZY_NOT_LOADED_OBJECTS);
    }

    @Bean
    public HibernateSearchService hibernateSearchService(EntityManagerFactory emf) {
        HibernateSearchService hibernateSearchService = new HibernateSearchService(emf);
        hibernateSearchService.initHibernateService();
        return hibernateSearchService;
    }
}
