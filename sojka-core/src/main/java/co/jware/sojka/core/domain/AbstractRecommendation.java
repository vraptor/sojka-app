package co.jware.sojka.core.domain;

import co.jware.sojka.core.domain.party.Party;
import co.jware.sojka.core.domain.wrapped.RecommendationWeight;
import co.jware.sojka.core.enums.RecommendationStatus;
import co.jware.sojka.core.enums.RecommendationType;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.google.common.base.Preconditions;
import org.immutables.value.Value;

import static co.jware.sojka.core.enums.RecommendationStatus.PENDING;
import static co.jware.sojka.core.enums.RecommendationType.PERSON;


@JsonSerialize(as = Recommendation.class)
@JsonDeserialize(builder = Recommendation.Builder.class)
@Value.Immutable
public interface AbstractRecommendation<S> extends Identifiable {

    @JsonTypeInfo(use = JsonTypeInfo.Id.NAME)
    @JsonSubTypes({
            @JsonSubTypes.Type(name = "JOB_LISTING", value = JobListing.class),
            @JsonSubTypes.Type(name = "COMPANY", value = Company.class)
    })
    S subject();

    default RecommendationType type() {
        return PERSON;
    }

    @Value.Check
    default void check() {
        Preconditions.checkState(!receiver().equals(sender()));
    }

    @JsonTypeInfo(use = JsonTypeInfo.Id.NAME)
//    @JsonSubTypes({
//            @JsonSubTypes.Type(value = Person.class, name = "PERSON"),
//            @JsonSubTypes.Type(value = External.class, name = "EXTERNAL"),
//            @JsonSubTypes.Type(value = Bot.class, name = "BOT")
//    })
    Party sender();

    @JsonTypeInfo(use = JsonTypeInfo.Id.NAME)
//    @JsonSubTypes({
//            @JsonSubTypes.Type(value = Person.class, name = "PERSON"),
//            @JsonSubTypes.Type(value = External.class, name = "EXTERNAL")
//    })
    Party receiver();

    @Value.Default
    default RecommendationStatus status() {
        return PENDING;
    }

    @Value.Default
    default RecommendationWeight weight() {
        return RecommendationWeight.of(1d);
    }
}
