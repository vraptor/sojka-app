package co.jware.sojka.core.service.entity;

import java.util.UUID;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import co.jware.sojka.core.domain.Course;
import co.jware.sojka.core.domain.party.Teacher;
import co.jware.sojka.entities.core.CourseBo;
import co.jware.sojka.repository.CourseRepository;

@Service("courseService")
@Transactional
public class CourseServiceImpl extends AbstractEntityService<CourseBo> implements CourseService {

    private final CourseRepository repository;

    public CourseServiceImpl(CourseRepository repository) {this.repository = repository;}

    @Override
    public Page<Course> getCoursesByOwner(Teacher owner, Pageable pageable) {
        UUID uuid = resolveUuid(owner);
        return repository.findAllByOwnerId(uuid, pageable)
                .map(e -> objectMapper.convertValue(e, Course.class));
    }
}
