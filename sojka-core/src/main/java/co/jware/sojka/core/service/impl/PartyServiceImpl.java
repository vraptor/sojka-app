package co.jware.sojka.core.service.impl;

import co.jware.sojka.core.domain.party.External;
import co.jware.sojka.core.domain.party.Party;
import co.jware.sojka.core.domain.party.Person;
import co.jware.sojka.core.service.PartyService;
import co.jware.sojka.core.service.entity.AbstractEntityService;
import co.jware.sojka.entities.core.ExternalBo;
import co.jware.sojka.entities.core.PersonBo;
import co.jware.sojka.repository.ExternalRepository;
import co.jware.sojka.repository.PersonRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service("partyService")
public class PartyServiceImpl extends AbstractEntityService implements PartyService {
    @Autowired
    private PersonRepository personRepository;
    @Autowired
    private ExternalRepository externalRepository;

    @Override
    public Optional<Party> findByEmail(String email) {
        Optional<Party> external = Optional.ofNullable(externalRepository.findByEmail(email))
                .map(e -> objectMapper.convertValue(e, External.class));
        if (external.isPresent()) {
            return external;
        }
        return Optional.ofNullable(personRepository.findByEmail(email))
                .map(e -> objectMapper.convertValue(e, Person.class));
    }


    @Override
    public Party createParty(Party party) {
        if (party instanceof External) {
            return createExternal((External) party);
        }
        if (party instanceof Person) {
            return createPerson((Person) party);
        }
        throw new IllegalArgumentException("Unsupported party class");
    }

    private Person createPerson(Person person) {
        PersonBo entity = objectMapper.convertValue(person, PersonBo.class);
        entity = personRepository.save(entity);
        return objectMapper.convertValue(entity, Person.class);
    }

    private External createExternal(External external) {
        ExternalBo entity = objectMapper.convertValue(external, ExternalBo.class);
        entity = externalRepository.save(entity);
        return objectMapper.convertValue(entity, External.class);
    }
}

