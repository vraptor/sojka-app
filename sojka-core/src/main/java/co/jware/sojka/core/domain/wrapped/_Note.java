package co.jware.sojka.core.domain.wrapped;

import com.fasterxml.jackson.annotation.JsonUnwrapped;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.immutables.value.Value;

@Wrapped
@Value.Immutable
@JsonSerialize(converter = WrapperConverter.class)
@JsonDeserialize()
public abstract class _Note extends Wrapper<String> {
}
