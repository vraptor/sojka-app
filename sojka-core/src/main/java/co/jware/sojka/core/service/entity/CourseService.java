package co.jware.sojka.core.service.entity;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import co.jware.sojka.core.domain.Course;
import co.jware.sojka.core.domain.party.Teacher;

public interface CourseService {

    Page<Course> getCoursesByOwner(Teacher owner, Pageable pageable);
}
