package co.jware.sojka.core.service;


import co.jware.sojka.core.domain.Account;
import co.jware.sojka.core.enums.RegistrationType;

public interface RegisterService {

    Account register(String firstName, String lastName, String email, RegistrationType type);

    Account attachUser(Account account);
}
