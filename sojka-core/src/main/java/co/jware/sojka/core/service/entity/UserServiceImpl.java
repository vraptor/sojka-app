package co.jware.sojka.core.service.entity;

import co.jware.sojka.core.domain.User;
import co.jware.sojka.entities.core.UserBo;
import co.jware.sojka.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.Optional;
import java.util.UUID;


@Service("userService")
public class UserServiceImpl extends AbstractEntityService implements UserService {
    @Autowired
    UserRepository repository;

    @Override
    public User findByUuid(UUID uuid) {
        return fromEntity(repository.findById(uuid)
                .orElseThrow(IllegalArgumentException::new));
    }

    @Override
    public User findByUsername(String username) {
        return fromEntity(repository.findByUsername(username));
    }

    private User fromEntity(UserBo userBo) {
        return objectMapper.convertValue(userBo, User.class);
    }

    @Override
    public User createUser(User user) {
        return Optional.ofNullable(user)
                .map(this::toEntity)
                .map(u -> {
                    if (StringUtils.isEmpty(u.getPassword())) {
                        u.setPassword("changeit");
                    }
                    return u;
                })
                .map(repository::save)
                .map(this::fromEntity)
                .orElseThrow(RuntimeException::new);
    }

    private UserBo toEntity(User user) {
        return objectMapper.convertValue(user, UserBo.class);
    }
}
