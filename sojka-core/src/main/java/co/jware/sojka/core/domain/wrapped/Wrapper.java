package co.jware.sojka.core.domain.wrapped;


import com.fasterxml.jackson.annotation.JsonUnwrapped;
import org.immutables.value.Value;

abstract class Wrapper<T> {
    @Value.Parameter
    @JsonUnwrapped
    public abstract T value();
}
