package co.jware.sojka.core.service.entity;

import co.jware.sojka.core.domain.party.Candidate;

public interface CandidateService {

    Candidate createCandidate(Candidate candidate);
}
