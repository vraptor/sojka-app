package co.jware.sojka.core.domain;

import co.jware.sojka.core.domain.wrapped.Description;
import co.jware.sojka.core.domain.wrapped.TagWeight;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.immutables.value.Value;

import javax.annotation.Nullable;
import java.util.Optional;

import static java.math.BigDecimal.ONE;

@Value.Immutable
@JsonSerialize
public interface AbstractTag {
    String name();

    @Nullable
    Description description();

    @Value.Default
    default TagWeight weight() {
        return TagWeight.of(ONE);
    }
}
