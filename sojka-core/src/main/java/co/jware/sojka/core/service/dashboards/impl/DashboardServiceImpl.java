package co.jware.sojka.core.service.dashboards.impl;

import co.jware.sojka.core.domain.dashboards.Dashboard;
import co.jware.sojka.core.domain.party.Party;
import co.jware.sojka.core.exceptions.AccountNotPresentException;
import co.jware.sojka.core.service.dashboards.DashboardService;
import co.jware.sojka.core.service.entity.AccountService;
import co.jware.sojka.entities.core.dashboard.DashboardBo;
import co.jware.sojka.entities.core.dashboard.QDashboardBo;
import co.jware.sojka.repository.DashboardRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.UUID;

@Service("dashboardService")
public class DashboardServiceImpl implements DashboardService {

    @Autowired
    private AccountService accountService;
    @Autowired
    private ObjectMapper mapper;
    @Autowired
    private DashboardRepository dashboardRepository;

    @Override
    public Dashboard createDashboard(Dashboard dashboard) {
        Party owner = dashboard.owner();
        accountService.findByOwner(owner)
                .orElseThrow(AccountNotPresentException::new);
        return saveDashboard(dashboard);
    }

    @Override
    public Dashboard saveDashboard(Dashboard newDashboard) {
        DashboardBo entity = mapper.convertValue(newDashboard, DashboardBo.class);
        entity = dashboardRepository.save(entity);
        newDashboard = mapper.convertValue(entity, Dashboard.class);
        return newDashboard;
    }

    @Override
    public Dashboard updateDashboard(Dashboard dashboard) {
        Dashboard saved = getDashboard(dashboard.uuid())
                .orElse(saveDashboard(dashboard));
        if (saved.equals(dashboard)) {
            return saved;
        } else {
            return saveDashboard(dashboard.withUuid(saved.uuid()).withVersion(saved.version()));
        }
    }

    @Override
    public Optional<Dashboard> getDashboard(UUID uuid) {
        return Optional.ofNullable(dashboardRepository.findById(uuid))
                .map(e -> mapper.convertValue(e, Dashboard.class));
    }

    @Override
    public Page<Dashboard> getByOwner(UUID uuid, Pageable pageable) {
        QDashboardBo $ = QDashboardBo.dashboardBo;
        return dashboardRepository.findAll($.owner.uuid.eq(uuid), pageable)
                .map(e -> mapper.convertValue(e, Dashboard.class));
    }

    @Override
    public void deleteDashboard(UUID uuid) {
        dashboardRepository.deleteById(uuid);
    }
}
