package co.jware.sojka.core.domain;


public interface HasEmail {
    String email();
}
