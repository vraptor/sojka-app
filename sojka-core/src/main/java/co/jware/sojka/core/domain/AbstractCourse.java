package co.jware.sojka.core.domain;

import java.time.Duration;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import org.immutables.value.Value;

import co.jware.sojka.core.domain.party.Teacher;
import co.jware.sojka.core.domain.wrapped.Description;

import javax.annotation.Nullable;

@Value.Immutable

public interface AbstractCourse extends Identifiable {
    Course NULL = Course.builder()
            .title("_useless_")
            .owner(Teacher.NULL)
            .courseDate(LocalDate.now())
            .seats(9)
            .build();

    Teacher owner();

    String title();

    @Nullable
    Description description();

    LocalDate courseDate();

    int seats();

    List<CourseTopic> courseTopics();

    @Value.Derived
    default Duration totalDuration() {
        return courseTopics().stream()
                .map(CourseTopic::duration)
                .reduce(Duration.ZERO, Duration::plus);
    }
}
