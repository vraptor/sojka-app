package co.jware.sojka.core.domain.wrapped;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.immutables.value.Value;

import java.math.BigDecimal;

@Value.Immutable

@Wrapped
@JsonSerialize(converter = WrapperConverter.class)
public abstract class _TagWeight extends Wrapper<BigDecimal> {
}
