package co.jware.sojka.core.domain.party;

import java.util.List;
import java.util.Optional;

import org.immutables.value.Value;

import com.fasterxml.jackson.annotation.JsonTypeName;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import co.jware.sojka.core.domain.Company;
import co.jware.sojka.core.domain.JobListing;
import co.jware.sojka.core.enums.HirerType;

import javax.annotation.Nullable;

@Value.Immutable
@JsonSerialize(as = Hirer.class)
@JsonDeserialize(builder = Hirer.Builder.class)
@JsonTypeName("HIRER")
public interface AbstractHirer extends Person, Submitter {

    Hirer NULL = Hirer.builder()
            .firstName("h")
            .lastName("r")
            .email("h.r@jobs.net")
            .registered(false)
            .build();

    @Nullable
    List<JobListing> jobs();

    @Nullable
    Company company();

    default HirerType hirerType() {
        return HirerType.EMPLOYER;
    }
}
