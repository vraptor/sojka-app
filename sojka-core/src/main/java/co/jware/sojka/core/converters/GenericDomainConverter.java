package co.jware.sojka.core.converters;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ResolvableType;
import org.springframework.core.convert.TypeDescriptor;

import java.util.Collections;
import java.util.Set;


public class GenericDomainConverter<S, T> implements DomainConverter<S, T> {

    protected Class<S> sourceType;
    protected Class<T> targetType;

    private ObjectMapper objectMapper;

    @Autowired
    public GenericDomainConverter(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
        Class<?>[] generics = ResolvableType.forClass(GenericDomainConverter.class, getClass())
                .resolveGenerics();
        sourceType = (Class<S>) generics[0];
        targetType = (Class<T>) generics[1];
    }

    @Override
    public Set<ConvertiblePair> getConvertibleTypes() {
        return Collections.singleton(new ConvertiblePair(sourceType, targetType));
    }


    @Override
    public Object convert(Object source, TypeDescriptor sourceType, TypeDescriptor targetType) {
        if (null == source) {
            return null;
        }
        return objectMapper.convertValue(source, this.targetType);
    }
}
