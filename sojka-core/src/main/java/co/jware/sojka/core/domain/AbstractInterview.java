package co.jware.sojka.core.domain;

import java.util.Date;
import java.util.Optional;
import java.util.Set;

import org.immutables.value.Value;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import co.jware.sojka.SkipNulls;
import co.jware.sojka.core.domain.party.Candidate;
import co.jware.sojka.core.domain.wrapped.Note;
import co.jware.sojka.core.enums.InterviewStatus;

import javax.annotation.Nullable;

import static co.jware.sojka.core.enums.InterviewStatus.PENDING;

@Value.Immutable
@JsonSerialize(as = Interview.class)
@JsonDeserialize(builder = Interview.Builder.class)
public interface AbstractInterview extends Identifiable, Ordered {

    JobListing jobListing();

    Candidate candidate();

    Date interviewDate();

    @Value.Default
    default InterviewStatus status() {
        return PENDING;
    }

    @SkipNulls
    @Nullable
    Set<Note> notes();

    @Nullable
    Result result();
}
