package co.jware.sojka.core.service.page.impl;

import co.jware.sojka.core.domain.search.page.PageDetail;
import co.jware.sojka.core.service.page.PageDetailService;
import co.jware.sojka.entities.core.metasearch.page.PageDetailBo;
import co.jware.sojka.repository.page.PageDetailRepository;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.flipkart.zjsonpatch.JsonDiff;
import com.flipkart.zjsonpatch.JsonPatch;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Optional;
import java.util.UUID;

@Service("pageDetailService")
@Transactional
public class PageDetailServiceImpl implements PageDetailService {
    private static final Logger logger = LoggerFactory.getLogger(PageDetailServiceImpl.class);
    @Autowired
    private PageDetailRepository repository;
    @Autowired
    private ObjectMapper mapper;
    @PersistenceContext
    private EntityManager em;

    @Override
    @Transactional
    public PageDetail save(PageDetail detail) {
        PageDetailBo entity = mapper.convertValue(detail, PageDetailBo.class);
        // FIXME: 7/15/2017  Extract correct metadata
        String rawData = entity.getPageMetaData().getRawData();
        Optional.ofNullable(rawData).ifPresent(data -> {
            logger.info("Metadata length: {}", data.length());
            data = StringUtils.abbreviate(data, 2040);
            entity.getPageMetaData().setRawData(data);
        });

        PageDetailBo result = repository.save(entity);
        return mapper.convertValue(result, PageDetail.class);
    }

    @Override
    @Transactional
    public PageDetail saveOrUpdate(PageDetail detail) {
        return findByUrl(detail.url())
                .map(found -> {
                    JsonNode sourceJsonNode = mapper.valueToTree(found);
                    JsonNode targetJsonNode = mapper.valueToTree(detail);
                    JsonNode diffJsonNode = JsonDiff.asJson(sourceJsonNode, targetJsonNode);
                    JsonNode resultJsonNode = JsonPatch.apply(diffJsonNode, sourceJsonNode);
                    PageDetail pageDetail;
                    try {
                        pageDetail = mapper.treeToValue(resultJsonNode, PageDetail.class
                        ).withVersion(found.version());
                        if (!found.equals(pageDetail)) {
                            return save(pageDetail);
                        }
                    } catch (JsonProcessingException e) {
                        throw new RuntimeException(e);
                    }
                    return pageDetail;
                })
                .orElse(save(detail));
    }

    @Override
    public Optional<PageDetail> findByUrl(String url) {
        return Optional.ofNullable(repository.findByUrl(url))
                .map(e -> mapper.convertValue(e, PageDetail.class));
    }

    @Override
    @Transactional
    public boolean exists(UUID uuid) {
        return repository.existsById(uuid);
    }

    @Override
    public boolean existsByUrl(String url) {
        return repository.existsByUrl(url);
    }
}
