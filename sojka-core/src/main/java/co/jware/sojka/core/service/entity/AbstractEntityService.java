package co.jware.sojka.core.service.entity;

import co.jware.sojka.core.domain.Identifiable;
import co.jware.sojka.entities.Persistent;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Optional;
import java.util.UUID;

public class AbstractEntityService<T extends Persistent> {
    @Autowired
    protected ObjectMapper objectMapper;

    protected UUID resolveUuid(Identifiable hasUuid) {
        return Optional.ofNullable(hasUuid.uuid())
                .orElseThrow(() -> new IllegalArgumentException("Invalid UUID"));
    }
}
