package co.jware.sojka.core.service.pipelines.impl;

import co.jware.sojka.core.domain.Account;
import co.jware.sojka.core.domain.JobListing;
import co.jware.sojka.core.domain.party.Candidate;
import co.jware.sojka.core.domain.party.Hirer;
import co.jware.sojka.core.domain.party.Party;
import co.jware.sojka.core.domain.pipelines.*;
import co.jware.sojka.core.enums.StageType;
import co.jware.sojka.core.exceptions.AccountNotPresentException;
import co.jware.sojka.core.exceptions.OwnerNotpresentException;
import co.jware.sojka.core.service.entity.AccountService;
import co.jware.sojka.core.service.pipelines.PipelineService;
import co.jware.sojka.entities.core.pipelines.JobPipelineBo;
import co.jware.sojka.entities.core.pipelines.OwnerPipelineBo;
import co.jware.sojka.entities.core.pipelines.SystemPipelineBo;
import co.jware.sojka.repository.pipelines.JobPipelineRepository;
import co.jware.sojka.repository.pipelines.OwnerPipelineRepository;
import co.jware.sojka.repository.pipelines.SystemPipelineRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.WordUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static java.util.stream.Collectors.toList;

@Service("pipelineService")
@Transactional
public class PipelineServiceImpl implements PipelineService {
    @Autowired
    private AccountService accountService;
    @Autowired
    private ObjectMapper mapper;
    @Autowired
    private SystemPipelineRepository systemPipelineRepository;
    @Autowired
    private OwnerPipelineRepository ownerPipelineRepository;
    @Autowired
    private JobPipelineRepository jobPipelineRepository;

    @Override
    public OwnerPipeline generateOwnerPipeline(SystemPipeline pipeline, Party owner) {
        return OwnerPipeline.builder()
                .uuid(UUID.randomUUID())
                .name(pipeline.name())
                .owner(owner)
                .addAllStages(copyStages(pipeline))
                .parent(pipeline)
                .build();
    }

    @Override
    public OwnerPipeline generatePipelineByOwner(Party owner) {
        Party person = Optional.ofNullable(owner)
                .orElseThrow(IllegalArgumentException::new);
        return OwnerPipeline.builder()
                .owner(person)
                .name("OWNER_PIPELINE")
                .parent(getOrCreateSystemPipeline())
                .build();
    }

    @Override
    public JobPipeline generatePipelineByJobListing(JobListing jobListing) {
        return JobPipeline.builder()
                .jobListing(jobListing)
                .parent(generatePipelineByOwner(Optional.ofNullable(jobListing.owner())
                        .orElse(Hirer.NULL)))
                .build();
    }

    @Override
    public JobPipeline generateJobPipeline(OwnerPipeline ownerPipeline, JobListing jobListing) {
        return JobPipeline.builder()
                .name(ownerPipeline.name())
                .jobListing(jobListing)
                .addAllStages(copyStages(ownerPipeline))
                .parent(ownerPipeline)
                .build();
    }

    @Override
    public PersonPipeline generatePersonPipeline(JobPipeline jobPipeline, Candidate candidate) {
        return PersonPipeline.builder()
                .uuid(UUID.randomUUID())
                .name(jobPipeline.name())
                .person(candidate)
                .addAllStages(copyStages(jobPipeline))
                .parent(jobPipeline)
                .build();
    }

    @Override
    public SystemPipeline getOrCreateSystemPipeline() {
        Page<SystemPipelineBo> entities = systemPipelineRepository.findAll(new PageRequest(0, 1));
        if (entities.hasContent()) {
            SystemPipelineBo systemPipelineEntity = entities.getContent().get(0);
            return mapper.convertValue(systemPipelineEntity, SystemPipeline.class);
        } else
            return createSystemPipeline();
    }

    @Override
    public OwnerPipeline getOrCreateOwnerPipeline(OwnerPipeline pipeline) {
        OwnerPipelineBo entity = mapper.convertValue(pipeline, OwnerPipelineBo.class);
        if (entity.isNew()) {
            entity = ownerPipelineRepository.save(entity);
        } else {
            entity = Optional.of(ownerPipelineRepository.findById(entity.getId())
                    .orElseThrow(IllegalArgumentException::new))
                    .orElse(ownerPipelineRepository.save(entity));
        }
        return mapper.convertValue(entity, OwnerPipeline.class);
    }

    @Override
    public JobPipeline getOrCreateJobPipeline(JobPipeline jobPipeline) {
        JobPipelineBo entity = mapper.convertValue(jobPipeline, JobPipelineBo.class);
        if (entity.isNew()) {
            entity = jobPipelineRepository.save(entity);
        } else {
            assert entity.getId() != null;
            entity = jobPipelineRepository.findById(entity.getId())
                    .orElseThrow(IllegalArgumentException::new);
        }
        return mapper.convertValue(entity, JobPipeline.class);
    }

    @Override
    public OwnerPipeline findOwnerPipeline(UUID uuid) {
        return Optional.ofNullable(ownerPipelineRepository.findById(uuid))
                .map(e -> mapper.convertValue(e, OwnerPipeline.class))
                .orElseThrow(IllegalArgumentException::new);
    }

    @Override
    public JobPipeline getJobPipeline(UUID uuid) {
        return Optional.ofNullable(jobPipelineRepository.findById(uuid))
                .map(e -> mapper.convertValue(e, JobPipeline.class))
                .orElseThrow(IllegalArgumentException::new);
    }


    private SystemPipeline createSystemPipeline() {
        final int[] pos = {0};
        List<Stage> stages = Arrays.stream(StageType.values())
                .map(type -> Stage.builder()
                        .stageType(type)
                        .position(pos[0]++)
                        .name(type.name())
                        .build())
                .map(stage -> {
                    String name = WordUtils.capitalizeFully(stage.name(),
                            new char[]{'_'});
                    name = StringUtils.replaceChars(name, '_', ' ');
                    return stage.withName(name);
                })
                .collect(toList());
        SystemPipeline systemPipeline = SystemPipeline.builder()
                .name("_SYSTEM_")
                .addAllStages(stages)
                .build();
        return saveSystemPipeline(systemPipeline);
    }

    @Override
    public Pipeline createPipeline(Pipeline pipeline, String userName) {
        Account account = accountService.findByUsername(userName)
                .orElseThrow(AccountNotPresentException::new);
        Party party = Optional.ofNullable(account.owner())
                .orElseThrow(OwnerNotpresentException::new);
        if (party instanceof Hirer) {
            Hirer hirer = (Hirer) party;
            return generateOwnerPipeline(getOrCreateSystemPipeline(), hirer)
                    .withName(pipeline.name());
        }
        throw new IllegalArgumentException("Error creating pipeline");
    }

    public SystemPipeline saveSystemPipeline(SystemPipeline systemPipeline) {
        SystemPipelineBo entity = mapper.convertValue(systemPipeline, SystemPipelineBo.class);
        entity = systemPipelineRepository.save(entity);
        SystemPipeline result = mapper.convertValue(entity, SystemPipeline.class);
        return result;
    }

    private List<Stage> copyStages(Pipeline pipeline) {
        List<Stage> stages = pipeline.stages();
        return stages.stream().map(s -> Stage.copyOf(s).withUuid(UUID.randomUUID())).collect(toList());
    }
}
