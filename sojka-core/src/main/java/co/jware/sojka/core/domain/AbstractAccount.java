package co.jware.sojka.core.domain;


import java.util.Optional;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeName;
import org.immutables.value.Value;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import co.jware.sojka.core.domain.party.Party;

import javax.annotation.Nullable;


@Value.Immutable
@JsonSerialize(as = Account.class)
@JsonDeserialize(builder = Account.Builder.class)
@JsonTypeName("ACCOUNT")
public interface AbstractAccount extends Identifiable {

    @Nullable
    User user();

    @JsonTypeInfo(use = JsonTypeInfo.Id.NAME)
    @Nullable
    Party owner();


    @JsonIgnore
    @Nullable
    BillingInfo billingInfo();
}
