package co.jware.sojka.core.domain;

import org.immutables.value.Value;

import javax.annotation.Nullable;
import java.util.UUID;

/**
 * Created by vraptor on 5/8/2017.
 */
public abstract class BaseImmutable {
    @Nullable
    @Value.Auxiliary
    public abstract UUID uuid();

    @Nullable
    @Value.Auxiliary
    public abstract Long version();
}
