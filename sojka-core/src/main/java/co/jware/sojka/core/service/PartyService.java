package co.jware.sojka.core.service;

import co.jware.sojka.core.domain.party.Party;

import java.util.Optional;

public interface PartyService {

    Optional<Party> findByEmail(String email);

    Party createParty(Party party);
}
