package co.jware.sojka.core;

import co.jware.sojka.core.domain.Resume;
import co.jware.sojka.core.domain.mapping.ObjectMapperHolder;
import co.jware.sojka.entities.core.ResumeBo;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.hibernate5.Hibernate5Module;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.context.annotation.Primary;
import org.springframework.core.convert.converter.Converter;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import static com.fasterxml.jackson.databind.DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES;
import static com.fasterxml.jackson.databind.SerializationFeature.WRITE_DATES_AS_TIMESTAMPS;

@Configuration
@EnableTransactionManagement(proxyTargetClass = true)
@EnableScheduling
@EnableAsync
@EnableAspectJAutoProxy
public class CoreConfig {
    @Bean
    public ObjectMapperHolder objectMapperHolder() {
        return new ObjectMapperHolder();
    }

    @Bean
    @Primary
    public ObjectMapper objectMapper() {
        ObjectMapper mapper = new Jackson2ObjectMapperBuilder().json()
                .modulesToInstall(Jdk8Module.class, JavaTimeModule.class, Hibernate5Module.class)
                .serializationInclusion(JsonInclude.Include.NON_EMPTY)
                .featuresToDisable(WRITE_DATES_AS_TIMESTAMPS, FAIL_ON_UNKNOWN_PROPERTIES).build();
        return ((ObjectMapper) (mapper));
    }

    @Bean
    public Converter<Resume, ResumeBo> ResumeConverter(final ObjectMapper mapper) {
        return new Converter<Resume, ResumeBo>() {
            @Override
            public ResumeBo convert(Resume source) {
                return mapper.convertValue(source, ResumeBo.class);
            }

        };
    }

}
