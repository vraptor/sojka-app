package co.jware.sojka.core.domain.search;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.immutables.value.Value;

import javax.annotation.Nullable;
import java.util.List;
import java.util.Set;

@Value.Immutable
@JsonDeserialize(builder = MetaLinksFilter.Builder.class)
public interface AbstractMetaLinksFilter {

    Set<String> keywords();

    Set<String> locations();
}
