package co.jware.sojka.core.domain;

import java.util.Optional;
import java.util.Set;

import org.immutables.value.Value;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.google.common.base.Preconditions;

import co.jware.sojka.SkipNulls;
import co.jware.sojka.core.domain.party.Candidate;
import co.jware.sojka.core.domain.party.Submitter;
import co.jware.sojka.core.domain.wrapped.Keyword;

import javax.annotation.Nullable;

@Value.Immutable

@JsonSerialize(as = Resume.class)
@JsonDeserialize(builder = Resume.Builder.class)
public interface AbstractResume extends Identifiable {

    @Nullable
    Candidate owner();

    @JsonTypeInfo(use = JsonTypeInfo.Id.NAME)
    @Nullable
    Submitter submitter();


    @SkipNulls
    Set<Skill> skills();

    @SkipNulls
    Set<Employment> employments();

    @SkipNulls
    Set<Tag> tags();

    @SkipNulls
    Set<Keyword> keywords();

    @Value.Check
    default void check() {
        Preconditions.checkState(submitter() != null || owner() != null);
    }
}