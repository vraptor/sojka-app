package co.jware.sojka.core.service.entity;


import java.util.Optional;
import java.util.UUID;

import co.jware.sojka.core.domain.search.MetaLink;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import co.jware.sojka.core.domain.JobListing;
import co.jware.sojka.core.domain.party.Hirer;

public interface JobListingService {

    default int offset() {
        return 0;
    }

    default int limit() {
        return 20;
    }

    Optional<JobListing> findByUuid(UUID uuid);

    Page<JobListing> findJobListings();

    Page<JobListing> findJobListings(Pageable pageable);

    Optional<JobListing> createJobListing(JobListing jobListing);

    JobListing getOrCreate(JobListing jobListing);

    Page<JobListing> findByOwner(Hirer hirer, Pageable pageable);

    void checkOwnerEligibility(JobListing jobListing);

    Optional<JobListing> findByLink(String link);

    JobListing update(JobListing jobListing);

    JobListing fromMetaLink(MetaLink metaLink);
}
