package co.jware.sojka.core.service.impl;

import java.util.Optional;
import java.util.concurrent.CompletableFuture;

import org.springframework.beans.factory.annotation.Autowired;

import co.jware.sojka.core.domain.JobListing;
import co.jware.sojka.core.domain.Recommendation;
import co.jware.sojka.core.domain.Recommendation;
import co.jware.sojka.core.domain.party.External;
import co.jware.sojka.core.domain.party.Party;
import co.jware.sojka.core.domain.party.Person;
import co.jware.sojka.core.processors.RecommendProcessor;
import co.jware.sojka.core.service.PersonService;
import co.jware.sojka.core.service.RecommendService;


public class RecommendServiceImpl implements RecommendService {
    @Autowired
    RecommendProcessor recommendProcessor;
    @Autowired
    PersonService personService;

    @Override
    public CompletableFuture<Recommendation> recommendViaReceiverEmail(String receiverEmail, Person sender, JobListing jobListing) {

        CompletableFuture<Party> from = CompletableFuture.supplyAsync(() -> Optional.ofNullable(sender.uuid())
                .map(personService::findByUuid)
                .orElseThrow(IllegalArgumentException::new));

        CompletableFuture<Party> to = CompletableFuture.supplyAsync(() ->
                Optional.ofNullable(receiverEmail)
                        .flatMap(personService::findByEmail)
                        .orElseThrow(IllegalArgumentException::new));

        to = to.thenApplyAsync(party -> Optional.ofNullable(party).orElse(External.builder().email(receiverEmail).build()));

        CompletableFuture<JobListing> listing = CompletableFuture.completedFuture(jobListing);

        return to.thenCombineAsync(from, (toParty, fromParty) -> Recommendation.<JobListing>builder()
                .receiver(toParty)
                .sender((Person) fromParty))
                .thenCombineAsync(listing, (b, j) -> b.subject(j).build());
    }

    @Override
    public CompletableFuture<Recommendation> getRecommendation(String uuid) {
        return null;
    }
}
