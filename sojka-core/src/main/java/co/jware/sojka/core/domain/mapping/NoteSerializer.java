package co.jware.sojka.core.domain.mapping;

import co.jware.sojka.core.domain.wrapped.Note;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.IOException;

public class NoteSerializer extends JsonSerializer<Note> {
    @Override
    public void serialize(Note note, JsonGenerator jGen, SerializerProvider provider) throws IOException {
        jGen.writeStartObject();
        jGen.writeStringField("text", note.value());
        jGen.writeEndObject();
    }
}
