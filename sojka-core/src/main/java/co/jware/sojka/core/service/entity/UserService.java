package co.jware.sojka.core.service.entity;


import co.jware.sojka.core.domain.User;

import java.util.UUID;

public interface UserService {

    User findByUuid(UUID uuid);

    User findByUsername(String username);

    User createUser(User user);
}
