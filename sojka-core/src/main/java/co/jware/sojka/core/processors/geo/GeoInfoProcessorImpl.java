package co.jware.sojka.core.processors.geo;

import co.jware.sojka.core.Events;
import co.jware.sojka.core.domain.geo.GeoLocation;
import co.jware.sojka.core.domain.search.MetaLink;
import co.jware.sojka.core.service.MetaLinkService;
import co.jware.sojka.core.service.geo.GeoLocationService;
import com.google.common.collect.Sets;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IQueue;
import com.hazelcast.core.ItemEvent;
import com.hazelcast.core.ItemListener;
import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.schedulers.Schedulers;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.eventbus.MessageConsumer;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import java.lang.invoke.MethodHandles;
import java.util.Collections;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.TimeUnit;

@Component
@Transactional
public class GeoInfoProcessorImpl implements GeoInfoProcessor {
    @Autowired
    private EventBus eventBus;
    @Autowired
    private MetaLinkService metaLinkService;
    @Autowired
    private GeoLocationService geoLocationService;
    @Autowired
    private HazelcastInstance hazelcastInstance;

    private IQueue<MetaLink> metaLinkGeo;

    private boolean geoServiceEnabled = false;

    private static final Logger LOGGER = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    @PostConstruct
    void init() {
        if (geoServiceEnabled) {
            metaLinkGeo = hazelcastInstance.getQueue("meta_link_geo");
            MessageConsumer<MetaLink> consumer = eventBus.consumer(Events.META_LINK_SUBMIT);
            consumer.handler(metaLinkMessage -> metaLinkGeo.add(metaLinkMessage.body()));
            metaLinkGeo.addItemListener(getItemListener(), false);
        }
    }

    private ItemListener<MetaLink> getItemListener() {
        return new ItemListener<MetaLink>() {
            @Override
            @Transactional
            public void itemAdded(ItemEvent<MetaLink> itemEvent) {
                Single.fromCallable(() -> {
                    MetaLink metaLink = metaLinkGeo.take();
                    String address = metaLink.address();
                    Set<GeoLocation> locations = Optional.ofNullable(metaLink.geoLocations())
                            .orElse(Collections.emptySet());
                    if (StringUtils.isNoneBlank(address) && locations.isEmpty()) {
                        Page<MetaLink> page = metaLinkService.findByAddress(address);
                        if (page.hasContent()) {
                            Iterable<GeoLocation> geoLocations = Observable.fromIterable(page.getContent())
                                    .filter(m -> m.geoLocations() != null)
                                    .flatMap(m -> Observable.fromIterable(m.geoLocations()))
                                    .distinct(GeoLocation::placeId).blockingIterable();
                            return saveWithGeoLocations(metaLink, Sets.newHashSet(geoLocations));
                        } else {
                            Set<GeoLocation> geoLocations = geoLocationService.findOrCreate(address);
                            if (!geoLocations.isEmpty()) {
                                return saveWithGeoLocations(metaLink, geoLocations);
                            }
                        }
                    }
                    return metaLink;
                }).subscribeOn(Schedulers.io())
                        .delay(15, TimeUnit.SECONDS)
                        .subscribe(m -> {

                        });

            }

            @Override
            public void itemRemoved(ItemEvent<MetaLink> itemEvent) {

            }
        };
    }

    private MetaLink saveWithGeoLocations(MetaLink metaLink, Set<GeoLocation> geoLocations) {
        return metaLinkService.loadMetaLink(metaLink.uuid())
                .map(metalink -> {
                    MetaLink withGeoLocations = metalink.withGeoLocations(geoLocations);
                    return metaLinkService.saveLink(withGeoLocations);
                })
                .orElseThrow(() -> new RuntimeException("Error saving geo info"));
    }
}
