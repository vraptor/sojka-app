package co.jware.sojka.core.domain.party;

import java.util.List;
import java.util.Optional;

import com.fasterxml.jackson.annotation.JsonTypeName;
import org.immutables.value.Value;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import co.jware.sojka.core.domain.Recommendation;

import javax.annotation.Nullable;

@Value.Immutable

@JsonSerialize(as = External.class)
@JsonDeserialize(as = External.class)
@JsonIgnoreProperties(value = "registered")
@JsonTypeName("EXTERNAL")
public interface AbstractExternal extends Party {

    External NULL = External.builder()
            .email("_EXTERNAL_")
            .build();

    @Nullable
    List<Recommendation> recommendations();
}
