package co.jware.sojka.core.domain.geo;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.immutables.value.Value;

import javax.annotation.Nullable;

@Value.Immutable
@JsonDeserialize(builder = GeoLocationAddress.Builder.class)
public interface AbstractGeoLocationAddress {
    @Nullable
    String county();

    @Nullable
    String state();

    @Nullable
    String city();

    @Nullable
    String town();

    @Nullable
    String village();

    @Nullable
    String suburb();

    @Nullable
    String hamlet();

    @Nullable
    String country();

    @Nullable
    @JsonProperty("country_code")
    String countryCode();
}
