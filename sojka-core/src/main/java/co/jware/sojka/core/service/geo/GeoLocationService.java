package co.jware.sojka.core.service.geo;


import co.jware.sojka.core.domain.geo.GeoLocation;

import java.util.Optional;
import java.util.Set;

public interface GeoLocationService {

    Set<GeoLocation> resolveAddress(String address);

    Set<GeoLocation> findOrCreate(String address);

    Set<GeoLocation> findNearBy(String location);

    Optional<GeoLocation> findByName(String address);
}
