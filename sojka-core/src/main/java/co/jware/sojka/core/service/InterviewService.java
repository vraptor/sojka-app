package co.jware.sojka.core.service;


import co.jware.sojka.core.domain.Interview;
import co.jware.sojka.core.domain.JobListing;
import co.jware.sojka.core.domain.party.Candidate;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;
import java.util.UUID;

public interface InterviewService {

    Optional<Interview> scheduleInterview(Interview interview);

    Optional<Interview> createInterview(Interview interview);

    Optional<Interview> setResult(Interview interview);

    Optional<Interview> loadInterview(Interview interview);

    Optional<Interview> changeInterviewStatus(Interview interview);

    Page<Interview> findInterviewsLike(Interview interview, Pageable pageable);

    Optional<Interview> findByUuid(UUID uuid);

    Page<Interview> findByCandidate(Candidate candidate, Pageable pageable);

    Page<Interview> findByJbListing(JobListing jobListing, Pageable pageable);
}
