package co.jware.sojka.core.domain.party;

import com.fasterxml.jackson.annotation.JsonTypeName;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.immutables.value.Value;

@Value.Immutable
@JsonSerialize(as = Candidate.class)
@JsonDeserialize(builder = Candidate.Builder.class)
@JsonTypeName("CANDIDATE")
public interface AbstractCandidate extends Person, Submitter {

    Candidate NULL = Candidate.builder()
            .email("a.b@c.net")
            .firstName("a")
            .lastName("b")
            .registered(false)
            .build();
}
