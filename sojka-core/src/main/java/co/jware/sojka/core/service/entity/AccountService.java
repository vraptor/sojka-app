package co.jware.sojka.core.service.entity;

import java.util.Optional;
import java.util.UUID;

import co.jware.sojka.core.domain.Account;
import co.jware.sojka.core.domain.User;
import co.jware.sojka.core.domain.party.Party;

public interface AccountService {

    Account createAccount(Account account);

    Optional<Account> findByOwner(Party party);

    Optional<Account> findByOwnerUuid(UUID uuid);

    Optional<Account> findByUser(User user);

    Optional<Account> findByUserUuid(UUID uuid);

    Optional<Account> findByUsername(String username);

    Account save(Account account);
}
