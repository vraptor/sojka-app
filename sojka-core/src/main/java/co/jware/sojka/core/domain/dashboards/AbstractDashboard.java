package co.jware.sojka.core.domain.dashboards;


import co.jware.sojka.core.domain.Identifiable;
import co.jware.sojka.core.domain.party.Party;
import co.jware.sojka.core.domain.party.*;
import co.jware.sojka.core.domain.pipelines.Label;
import co.jware.sojka.core.domain.pipelines.Pipeline;
import co.jware.sojka.core.domain.wrapped.Description;
import co.jware.sojka.core.enums.Visibility;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeName;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import io.swagger.annotations.ApiModel;
import org.immutables.value.Value;

import javax.annotation.Nullable;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import static com.google.common.base.Preconditions.checkState;

@Value.Immutable
@JsonDeserialize(builder = Dashboard.Builder.class)
@JsonTypeName("DASHBOARD")
public interface AbstractDashboard extends Identifiable {


    @JsonTypeInfo(use = JsonTypeInfo.Id.NAME)
    Party owner();

    @Nullable
    String name();

    @Nullable
    Description description();

    Set<Label> labels();

    List<Pipeline> pipelines();

    default boolean starred() {
        return false;
    }

    default Visibility visibility() {
        return Visibility.PRIVATE;
    }

    @Value.Check
    default void checkOwner() {
        checkState(owner() instanceof Hirer, "Invalid owner type");
    }
}
