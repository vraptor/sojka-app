package co.jware.sojka.core.domain.geo;

import co.jware.sojka.core.domain.Tupled;
import org.immutables.value.Value;

@Value.Immutable
@Tupled
public abstract class GeoBoxDef {

    public abstract GeoPoint northEast();

    public abstract GeoPoint southWest();
}
