package co.jware.sojka.core.domain.wrapped;

        import org.immutables.value.Value;

        import static org.immutables.value.Value.Style.ImplementationVisibility.PUBLIC;

@Value.Style(
        typeAbstract = "_*",
        typeImmutable = "*",
        visibility = PUBLIC,
        defaults = @Value.Immutable(builder = false, copy = false)
)
public @interface Wrapped {
}
