package co.jware.sojka.core.domain.party;

import java.util.List;
import java.util.Optional;

import com.fasterxml.jackson.annotation.JsonTypeName;
import org.immutables.value.Value;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import co.jware.sojka.core.domain.Recommendation;

import javax.annotation.Nullable;

@Value.Immutable
@JsonDeserialize(builder = Bot.Builder.class)
@JsonTypeName("BOT")
public interface AbstractBot extends Party {

    @Nullable
    List<Recommendation> recommendations();
}
