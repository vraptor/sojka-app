package co.jware.sojka.core.service.impl;

import co.jware.sojka.core.domain.Account;
import co.jware.sojka.core.domain.User;
import co.jware.sojka.core.domain.party.*;
import co.jware.sojka.core.service.PersonService;
import co.jware.sojka.core.service.entity.AbstractEntityService;
import co.jware.sojka.core.service.entity.AccountService;
import co.jware.sojka.entities.core.*;
import co.jware.sojka.repository.ExternalRepository;
import co.jware.sojka.repository.PersonRepository;
import co.jware.sojka.utils.CastUtils;
import com.querydsl.jpa.impl.JPAQueryFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static java.util.stream.Collectors.toList;

@Service("personService")
@Transactional
public class PersonServiceImpl extends AbstractEntityService implements PersonService {

    @Autowired
    PersonRepository personRepository;
    @Autowired
    ExternalRepository externalRepository;
    @Autowired
    AccountService accountService;

    @PersistenceContext
    private EntityManager em;

    @Override
    public Optional<Party> findByEmail(String email) {
        return Optional.ofNullable(toParty(personRepository.findByEmail(email)));
    }

    @Override
    public Party findByUuid(UUID uuid) {
        return toParty(personRepository.findById(uuid).orElseThrow(IllegalArgumentException::new));
    }

    private Party toParty(PersonBo entity) {
        if (ExternalBo.class.isInstance(entity)) {
            return (objectMapper.convertValue(entity, External.class));
        } else
            return toPerson(entity);
    }

    @Override
    public Person createPerson(Party person) {
        return toPerson(personRepository.save(objectMapper.convertValue(person, PersonBo.class)));
    }

    @Override
    public List<Person> findByEmailContaining(String email) {
        List<PersonBo> persons = personRepository.findByEmailContaining(email);
        return persons.stream()
                .map(p -> objectMapper.convertValue(p, Person.class))
                .collect(toList());
    }

//    @Override
//    public Party createParty(Party party) {
//        PartyEntity entity = (PersonEntity) toEntity(party);
//        if (entity instanceof ExternalEntity) {
//            entity = externalRepository.save((ExternalEntity) entity);
//            ExternalEntity externalEntity = (ExternalEntity) entity;
//            return (Party) fromEntity(externalEntity);
//        } else
//            return createPerson(party);
//    }

    @Override
    public Optional<Person> findByUser(User user) {
        return accountService.findByUser(user)
                .map(Account::owner)
                .map(p -> CastUtils.as(Person.class, p));
    }

    @Override
    public Optional<Candidate> findCandidate(UUID uuid) {
        QCandidateBo candidateEntity = QCandidateBo.candidateBo;
        CandidateBo entity = new JPAQueryFactory(em)
                .selectFrom(candidateEntity)
                .where(candidateEntity.uuid.eq(uuid))
                .fetchOne();
        return Optional.ofNullable(entity)
                .map(e -> objectMapper.convertValue(e, Candidate.class));
    }


    private Person toPerson(PersonBo personBo) {
        if (personBo instanceof CandidateBo) {
            return objectMapper.convertValue(personBo, Candidate.class)
                    .withRegistered(personBo.getRegistered());
        } else if (personBo instanceof HirerBo) {
            return objectMapper.convertValue(personBo, Hirer.class)
                    .withRegistered(personBo.getRegistered());
        } else if (personBo instanceof TeacherBo) {
            objectMapper.convertValue(personBo, Teacher.class)
                    .withRegistered(getRegistered(personBo.getRegistered()));
        }
        throw new IllegalArgumentException("Unsupported person entity");
    }

    private Boolean getRegistered(Boolean registered) {
        return Optional.ofNullable(registered).orElse(false);
    }

}
