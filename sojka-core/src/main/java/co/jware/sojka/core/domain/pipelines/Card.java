package co.jware.sojka.core.domain.pipelines;

import co.jware.sojka.core.domain.Identifiable;
import co.jware.sojka.core.domain.party.AbstractCandidate;
import co.jware.sojka.core.domain.party.Candidate;
import co.jware.sojka.core.domain.wrapped.Description;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.immutables.value.Value;

import javax.annotation.Nullable;
import java.util.Optional;
import java.util.Set;

//@Value.Immutable()
//@JsonDeserialize(builder = Card.Builder.class)
public interface Card<T extends Identifiable> extends Identifiable {

    @JsonTypeInfo(use = JsonTypeInfo.Id.NAME)
    @Nullable
    T content();

    Set<Label> labels();

    @Nullable
    Description description();

    default int position() {
        return 0;
    }

}

