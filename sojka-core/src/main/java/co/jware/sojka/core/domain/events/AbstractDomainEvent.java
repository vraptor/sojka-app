package co.jware.sojka.core.domain.events;

import org.immutables.serial.Serial;
import org.immutables.value.Value;

import java.time.OffsetDateTime;
import java.util.UUID;

@Value.Immutable
@Serial.Structural
public abstract class AbstractDomainEvent {

    abstract UUID eventId();

    abstract OffsetDateTime createdOn();
}
