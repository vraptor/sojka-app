package co.jware.sojka.core.service.impl;

import co.jware.sojka.core.domain.search.MetaSearch;
import co.jware.sojka.core.service.entity.MetaSearchService;
import co.jware.sojka.entities.core.metasearch.MetaSearchBo;
import co.jware.sojka.entities.core.metasearch.QMetaSearchBo;
import co.jware.sojka.repository.search.MetaSearchRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.hazelcast.core.HazelcastInstance;
import com.querydsl.core.types.dsl.BooleanExpression;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.concurrent.locks.Lock;

import static java.util.Optional.ofNullable;
import static java.util.stream.Collectors.toSet;
import static org.springframework.transaction.annotation.Propagation.REQUIRES_NEW;

@Service("metaSearchService")
@Transactional
public class MetaSearchServiceImpl implements MetaSearchService {


    public static final Logger log = LoggerFactory.getLogger(MetaSearchServiceImpl.class);

    @Autowired
    private MetaSearchRepository repository;
    @Autowired

    private ObjectMapper mapper;
    @Autowired
    private HazelcastInstance hazelcastInstance;

    @Override
    public MetaSearch storeSearch(MetaSearch search) {
        return getSearch(search)
                .orElseGet(() -> {
                    log.info("Not found:" + search.toString());
                    return save(search.withUuid(null).withVersion(null));
                });
    }

    @Override
    public Optional<MetaSearch> getSearch(UUID uuid) {
        return repository.findById(uuid)
                .map(metaSearchBo -> mapper.convertValue(metaSearchBo, MetaSearch.class));
    }

    @Override
    @Transactional(propagation = REQUIRES_NEW)
    public MetaSearch findOrCreate(MetaSearch search) {
        Lock metaSearchLock = hazelcastInstance.getLock("metaSearch");
        metaSearchLock.lock();
        try {
            return getSearch(search)
                    .orElse(save(search));
        } finally {
            metaSearchLock.unlock();
        }
    }


    @Override
    public Optional<MetaSearch> getSearch(MetaSearch search) {
        search = processKeywords(search);
        QMetaSearchBo $ = QMetaSearchBo.metaSearchBo;
        BooleanExpression predicate = $.page.eq(search.page());
        if (Objects.requireNonNull(search.keywords()).isEmpty()) {
            predicate = predicate.and($.keywords.size().eq(0));
        } else {
            predicate = search.keywords().stream()
                    .map(k -> $.keywords.any().eq(k))
                    .reduce(predicate, BooleanExpression::and);
        }
        Set<String> locations = Optional.ofNullable(search.locations())
                .orElse(Collections.emptySet())
                .stream()
                .filter(StringUtils::isNotBlank)
                .collect(toSet());
        if (locations.isEmpty()) {
            predicate = predicate.and($.locations.size().eq(0));
        } else {
            predicate = locations.stream()
                    .map(loc -> $.locations.any().eq(loc))
                    .reduce(predicate, BooleanExpression::and);
        }

        Page<MetaSearchBo> page = repository.findAll(predicate, PageRequest.of(0, 10,
                Sort.Direction.DESC, "updated"));
        if (page.hasContent()) {
            List<MetaSearchBo> searches = page.getContent();
            return Optional.ofNullable(mapper.convertValue(searches.get(0), MetaSearch.class));
        }
        return Optional.empty();
    }

    private MetaSearch processKeywords(MetaSearch search) {
        Set<String> keywords = ofNullable(search.keywords())
                .orElse(Collections.emptySet())
                .stream()
                .filter(StringUtils::isNotBlank)
                .collect(toSet());
        return search.withKeywords(keywords);
    }

    @Override
    public void getSearchLinks(UUID uuid, Pageable page) {

    }


    @Override
    @Transactional
    public MetaSearch save(MetaSearch metaSearch) {
        MetaSearch search = processKeywords(metaSearch);
        MetaSearchBo entity = repository.save(mapper.convertValue(search, MetaSearchBo.class));
        return ofNullable(entity)
                .map(e -> mapper.convertValue(e, MetaSearch.class))
                .orElse(metaSearch);
    }
}
