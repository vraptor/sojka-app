package co.jware.sojka.core.domain.wrapped;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.immutables.value.Value;

@Value.Immutable
@Wrapped
@JsonSerialize(converter = WrapperConverter.class)
@JsonDeserialize
public abstract class _SkillName extends Wrapper<String> {

}
