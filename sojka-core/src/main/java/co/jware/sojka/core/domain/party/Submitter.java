package co.jware.sojka.core.domain.party;

import org.immutables.value.Value;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

import co.jware.sojka.core.domain.Identifiable;

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME)
@JsonSubTypes({
        @JsonSubTypes.Type(name = "CANDIDATE", value = Candidate.class),
        @JsonSubTypes.Type(name = "HIRER", value = Hirer.class)})
public interface Submitter extends Identifiable {

    static boolean isSupportedSubmitter(Submitter submitter) {
        return Candidate.class.isInstance(submitter) ||
                Hirer.class.isInstance(submitter);
    }

    @Value.Auxiliary
    static boolean isSupportedOwner(Submitter submitter) {
        return Candidate.class.isInstance(submitter);
    }
}
