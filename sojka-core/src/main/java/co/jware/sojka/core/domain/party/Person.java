package co.jware.sojka.core.domain.party;


import co.jware.sojka.core.domain.Named;
import co.jware.sojka.core.domain.Recommendation;
import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;

import java.util.List;

@ApiModel(subTypes = {Candidate.class, Hirer.class, Teacher.class, External.class})
public interface Person extends Party, Named {

    List<Recommendation> recommendations();

    @JsonIgnore
    default boolean registered() {
        return false;
    }
}
