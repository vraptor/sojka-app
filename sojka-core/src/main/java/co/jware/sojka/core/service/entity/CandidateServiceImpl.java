package co.jware.sojka.core.service.entity;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import co.jware.sojka.core.domain.party.Candidate;
import co.jware.sojka.entities.core.CandidateBo;
import co.jware.sojka.repository.CrudQueryDslRepository;

@Service("candidateService")
public class CandidateServiceImpl extends AbstractEntityService implements CandidateService {

    @Autowired
    CrudQueryDslRepository<CandidateBo> repository;

    @Override
    public Candidate createCandidate(Candidate candidate) {
        return Optional.ofNullable(candidate)
                .map(c -> objectMapper.convertValue(c, CandidateBo.class))
                .map(repository::save)
                .map(c -> objectMapper.convertValue(c, Candidate.class))
                .orElseThrow(IllegalArgumentException::new);
    }
}
