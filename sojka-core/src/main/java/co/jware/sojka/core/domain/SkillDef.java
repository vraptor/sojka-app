package co.jware.sojka.core.domain;

import co.jware.sojka.core.domain.wrapped.SkillName;
import co.jware.sojka.core.enums.SkillLevel;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.immutables.value.Value;

@Tupled
@Value.Immutable
@JsonSerialize(as = Skill.class)
@JsonDeserialize(as = Skill.class)
public interface SkillDef {

    SkillName name();

    SkillLevel level();

    int experienceYears();


}
