package co.jware.sojka.core.service.entity;

import co.jware.sojka.core.domain.Account;
import co.jware.sojka.core.domain.User;
import co.jware.sojka.core.domain.party.External;
import co.jware.sojka.core.domain.party.Party;
import co.jware.sojka.entities.core.AccountBo;
import co.jware.sojka.entities.core.PersonBo;
import co.jware.sojka.entities.core.QAccountBo;
import co.jware.sojka.repository.AccountRepository;
import co.jware.sojka.repository.PersonRepository;
import co.jware.sojka.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import java.util.Optional;
import java.util.UUID;

@Service("accountService")
@Transactional
public class AccountServiceImpl extends AbstractEntityService implements AccountService {

    private final QAccountBo $ = QAccountBo.accountBo;
    @Autowired
    AccountRepository repository;
    @Autowired
    UserRepository userRepository;
    @Autowired
    PersonRepository personRepository;

    @Override
    public Account createAccount(Account account) {
        Party owner = Optional.ofNullable(account.owner())
                .orElseThrow(() -> new IllegalArgumentException("Owner is required"));
        Assert.isTrue(!External.class.isInstance(owner), "Can't create account for external");
        PersonBo personEntity = personRepository.getOne(resolveUuid(owner));
        AccountBo entity = (AccountBo) toEntity(account);
        entity.setOwner(personEntity);
        AccountBo finalEntity = entity;
        entity = Optional.ofNullable(account.user())
                .map(this::resolveUuid)
                .map(userRepository::getOne).map(u -> {
                    finalEntity.setUser(u);
                    return finalEntity;
                }).orElse(entity);
        AccountBo saved = repository.save(entity);
        return Account.class.cast(fromEntity(saved));
    }

    private Account fromEntity(AccountBo accountBo) {
        return objectMapper.convertValue(accountBo, Account.class);
    }

    private AccountBo toEntity(Account account) {
        return objectMapper.convertValue(account, AccountBo.class);
    }

    @Override
    public Optional<Account> findByOwner(Party party) {
        return Optional.of(party)
                .filter(p -> !External.class.isInstance(p))
                .map(this::resolveUuid)
                .map(personRepository::getOne)
                .map(repository::findByOwner)
                .map(this::fromEntity)
                .map(Account.class::cast);
    }

    @Override
    public Optional<Account> findByOwnerUuid(UUID uuid) {
        return repository.findByOwnerUuid(uuid)
                .map(e -> objectMapper.convertValue(e, Account.class));
    }

    @Override
    public Optional<Account> findByUser(User user) {
        return Optional.ofNullable(user)
                .map(this::resolveUuid)
                .map((id) -> userRepository.getOne(id))
                .map((user1) -> repository.findByUser(user1))
                .map(this::fromEntity)
                .map(Account.class::cast);
    }

    @Override
    public Optional<Account> findByUserUuid(UUID uuid) {
        return repository.findByUserUuid(uuid)
                .map(e -> objectMapper.convertValue(e, Account.class));
    }

    @Override
    public Optional<Account> findByUsername(String username) {
        return repository.findOne($.user.username.eq(username))
                .map(this::fromEntity);
    }

    @Override
    public Account save(Account account) {
        return Optional.ofNullable(account)
                .map(this::toEntity)
                .map(AccountBo.class::cast)
                .map(repository::save)
                .map(this::fromEntity)
                .map(Account.class::cast)
                .orElseThrow(RuntimeException::new);
    }
}
