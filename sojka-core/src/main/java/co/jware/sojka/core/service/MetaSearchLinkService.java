package co.jware.sojka.core.service;


import co.jware.sojka.core.domain.search.MetaLink;
import co.jware.sojka.core.domain.search.MetaSearch;
import co.jware.sojka.core.domain.search.MetaSearchLink;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.UUID;

public interface MetaSearchLinkService {

    MetaSearchLink storeMetaSearchLink(MetaSearchLink metaSearchLink);

    List<MetaSearch> getMetaSearches(MetaLink metaLink);

    MetaSearchLink save(MetaSearchLink metaSearchLink);

    Page<MetaLink> getLinksByMetaSearch(UUID uuid, Pageable pageable);

    Page<MetaLink> getLinksByMetaSearch(MetaSearch search, Pageable pageable);

    long countByMetaSearch(MetaSearch search);
}
