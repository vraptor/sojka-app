package co.jware.sojka.core.processors;


import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.CompletableFuture;

import org.springframework.core.task.AsyncListenableTaskExecutor;
import org.springframework.stereotype.Component;

import co.jware.sojka.core.domain.JobListing;
import co.jware.sojka.core.domain.Recommendation;
import co.jware.sojka.core.domain.Recommendation;
import co.jware.sojka.core.domain.party.Party;
import co.jware.sojka.core.domain.party.Person;
import co.jware.sojka.core.enums.RecommendationStatus;
import co.jware.sojka.core.exceptions.InvalidStatusRequestedException;

@Component
public class RecommendProcessor {


    AsyncListenableTaskExecutor taskExecutor;
    private Set<Recommendation> recommendations = new HashSet<>();

    public CompletableFuture<Recommendation> createRecommendation(Person sender, Party receiver, JobListing jobListing) {
        return CompletableFuture.supplyAsync(() -> Recommendation.<JobListing>builder()
                .sender(sender)
                .receiver(receiver)
                .subject(jobListing)
                .build());
    }


    public boolean addRecommendation(Recommendation recommendation) {
        return recommendations.add(recommendation);
    }


    public Recommendation updateStatus(Recommendation recommendation, RecommendationStatus status) {
        RecommendationStatus prevStatus = recommendation.status();
        if (status == null || prevStatus == status) {
            return recommendation;
        }
        isValidStatusTransition(prevStatus, status);
        if (recommendations.contains(recommendation)) {
            recommendations.remove(recommendation);
        }
        Recommendation result = recommendation.withStatus(status);
        addRecommendation(result);
        return result;
    }

    void isValidStatusTransition(RecommendationStatus prevStatus, RecommendationStatus status) {
        int statusOrder = status.order();
        int prevStatusOrder = prevStatus.order();
        if (statusOrder < prevStatusOrder || statusOrder - prevStatusOrder > 1) {
            throw new InvalidStatusRequestedException(status.toString());
        }
    }
}
