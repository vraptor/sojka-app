package co.jware.sojka.core.service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import co.jware.sojka.core.domain.User;
import co.jware.sojka.core.domain.party.Candidate;
import co.jware.sojka.core.domain.party.Party;
import co.jware.sojka.core.domain.party.Person;

public interface PersonService {

    Optional<Party> findByEmail(String email);

    Party findByUuid(UUID uuid);

    Person createPerson(Party person);

    List<Person> findByEmailContaining(String email);

    Optional<Person> findByUser(User user);


    Optional<Candidate> findCandidate(UUID uuid);
}