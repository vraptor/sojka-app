package co.jware.sojka.core.service.impl;

import co.jware.sojka.core.domain.Identifiable;
import co.jware.sojka.core.domain.JobListing;
import co.jware.sojka.core.domain.Recommendation;
import co.jware.sojka.core.domain.party.External;
import co.jware.sojka.core.domain.party.Party;
import co.jware.sojka.core.enums.SubjectType;
import co.jware.sojka.core.service.entity.AbstractEntityService;
import co.jware.sojka.core.service.entity.ExternalService;
import co.jware.sojka.core.service.entity.JobListingService;
import co.jware.sojka.core.service.entity.RecommendationService;
import co.jware.sojka.entities.core.RecommendationBo;
import co.jware.sojka.repository.RecommendationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.UUID;

@Service("recommendationService")
public class RecommendationServiceImpl extends AbstractEntityService implements RecommendationService {

    private final RecommendationRepository repository;
    @Autowired
    private ExternalService externalService;
    @Autowired
    private JobListingService jobListingService;

    public RecommendationServiceImpl(RecommendationRepository repository) {
        this.repository = repository;
    }

    @Override
    public Page<Recommendation> findPersonRecommendations(Pageable pageable) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Page<Recommendation> findByReceiver(Party person, Pageable pageable) {
        UUID uuid = resolveUuid(person);
        return repository.findAllByReceiverId(uuid, pageable)
                .map(e -> objectMapper.convertValue(e, Recommendation.class));
    }

    @Override
    public Page<Recommendation> findBySender(Party sender, Pageable pageable) {
        return repository.findAllBySenderId(resolveUuid(sender), pageable)
                .map(e -> objectMapper.convertValue(e, Recommendation.class));
    }

    @Override
    public Page<Recommendation> findByJobListing(JobListing jobListing, Pageable pageable) {
        return repository.findAllBySubjectIdAndSubjectType(resolveUuid(jobListing), SubjectType.JOB_LISTING, pageable)
                .map(e -> objectMapper.convertValue(e, Recommendation.class));
    }

    @Override
    public Optional<Recommendation> findByUuid(UUID uuid) {
        return Optional.ofNullable(uuid)
                .map(repository::findById)
                .map(e -> objectMapper.convertValue(e, Recommendation.class));
    }

    @Override
    public Recommendation createRecommendation(Recommendation recommendation) {
        if (recommendation != null) {
            return createPersonRecommendation(recommendation);
        }
        throw new UnsupportedOperationException("Unknown recommendation type");
    }

    private Recommendation createPersonRecommendation(Recommendation recommendation) {
        return Optional.ofNullable(recommendation)
                .map(r -> objectMapper.convertValue(recommendation, RecommendationBo.class))
                .map(repository::save)
                .map(e -> objectMapper.convertValue(e, Recommendation.class))
                .orElseThrow(() -> new RuntimeException("Error creating recommendation"));
    }

    @Override
    public Optional<Recommendation> findRecommendation(Recommendation recommendation) {
        return Optional.of(repository.findById(resolveUuid(recommendation)))
                .map(c -> objectMapper.convertValue(c, Recommendation.class));
    }

    @Override
    public Recommendation recommendSubject(Party sender, Party receiver, UUID uuid) {
        if (sender instanceof External) {
            External external = (External) sender;
            sender = externalService.findOrCreate(external);
        }
        if (receiver instanceof External) {
            External external = (External) receiver;
            receiver = externalService.findOrCreate(external);
        }
        Identifiable subject = findSubjectByUuid(uuid);
        Recommendation<JobListing> personRecommendation = Recommendation.<JobListing>builder()
                .receiver(receiver)
                .sender(sender)
                .subject((JobListing) subject).build();
        return createPersonRecommendation(personRecommendation);
    }

    @Override
    public void delete(Recommendation recommendation) {
        repository.deleteById(resolveUuid(recommendation));
    }

    @Override
    public Page<Recommendation> findBySubject(UUID uuid, Pageable pageable) {
        return repository.findAllBySubjectId(uuid, pageable)
                .map(e -> objectMapper.convertValue(e, Recommendation.class));
    }

    private Identifiable findSubjectByUuid(UUID uuid) {
        return jobListingService.findByUuid(uuid)
                .orElseThrow(() -> new IllegalArgumentException("Subject not found"));
    }

}
