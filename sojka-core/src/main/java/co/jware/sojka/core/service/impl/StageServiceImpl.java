package co.jware.sojka.core.service.impl;

import co.jware.sojka.core.domain.pipelines.Stage;
import co.jware.sojka.core.service.StageService;
import co.jware.sojka.core.service.entity.AbstractEntityService;
import co.jware.sojka.repository.StageRepository;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.UUID;

@Service("stageService")
public class StageServiceImpl extends AbstractEntityService implements StageService {
    private final StageRepository stageRepository;

    public StageServiceImpl(StageRepository stageRepository) {
        this.stageRepository = stageRepository;
    }

    @Override
    public Stage findStage(UUID uuid) {
        return Optional.ofNullable(stageRepository.findById(uuid))
                .map(e -> objectMapper.convertValue(e, Stage.class))
                .orElseThrow(RuntimeException::new);
    }
}
