package co.jware.sojka.core.domain.search;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.immutables.serial.Serial;
import org.immutables.value.Value;

import javax.annotation.Nullable;
import java.util.UUID;

@Value.Immutable
@Serial.Version(1L)
@JsonDeserialize(builder = MetaSearchLink.Builder.class)
public abstract class AbstractMetaSearchLink {

    @Nullable
    public abstract UUID uuid();

    @org.immutables.value.Value.Parameter
    public abstract MetaLink metaLink();

    @Value.Parameter
    public abstract MetaSearch metaSearch();

    @Nullable
    public abstract Long version();

    @Value.Default
    public int position() {
        return 0;
    }

    ;

    @Nullable
    public abstract Boolean relevant();
}

