package co.jware.sojka.core.service.entity;


import java.util.List;

import co.jware.sojka.core.domain.CourseClass;
import co.jware.sojka.core.domain.party.Person;

public interface CourseClassService {
    List<Person> loadStudents(CourseClass courseClass);
}
