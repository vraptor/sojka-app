package co.jware.sojka.core.service.impl;

import co.jware.sojka.core.Events;
import co.jware.sojka.core.domain.search.Commons;
import co.jware.sojka.core.domain.search.MetaLink;
import co.jware.sojka.core.domain.search.MetaLinksFilter;
import co.jware.sojka.core.domain.search.MetaSearch;
import co.jware.sojka.core.service.MetaLinkService;
import co.jware.sojka.core.service.MetaSearchLinkService;
import co.jware.sojka.entities.core.metasearch.MetaLinkBo;
import co.jware.sojka.entities.core.metasearch.QMetaLinkBo;
import co.jware.sojka.repository.search.MetaLinkRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Sets;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.ILock;
import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.dsl.BooleanExpression;
import io.reactivex.Flowable;
import io.reactivex.Observable;
import io.vertx.core.Handler;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.eventbus.Message;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.*;

import static java.util.Optional.ofNullable;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toSet;

@Service("metalinkService")
@Transactional
public class MetaLinkServiceImpl implements MetaLinkService {

    private final QMetaLinkBo $ = QMetaLinkBo.metaLinkBo;
    private final Logger logger = LoggerFactory.getLogger(MetaLinkServiceImpl.class);
    @Autowired
    private EventBus eventBus;
    @Autowired
    private MetaLinkRepository metaLinkRepository;
    @Autowired
    private HazelcastInstance hazelcast;
    @Autowired
    private ObjectMapper mapper;
    @Autowired
    private MetaSearchLinkService metaSearchLinkService;
    @PersistenceContext
    private EntityManager em;

    @Override
    @Transactional
    public MetaLink findOrCreate(final MetaLink metaLink) {
        ILock metaLinkLock = hazelcast.getLock("metaLink");
        metaLinkLock.lock();
        try {
            String href = metaLink.href();
            MetaLink result = metaLinkRepository.findByHref(href).stream()
                    .findFirst()
                    .map(e -> mapper.convertValue(e, MetaLink.class))
                    .orElseGet(() -> saveLink(metaLink));
            if (result.equals(metaLink)) {
                return result;
            } else {
                return saveLink(MetaLink.copyOf(result)
                        .withKeywords(combineKeywords(metaLink, result))
                        .withLocations(combineLocations(metaLink, result)));
            }
        } finally {
            metaLinkLock.unlock();
        }
    }

    private Set<String> combineLocations(MetaLink metaLink, MetaLink result) {
        Set<String> metaLinkLocations = getLocations(metaLink);
        Set<String> resultLocations = getLocations(result);
        resultLocations.addAll(metaLinkLocations);
        return resultLocations;
    }

    private Set<String> combineKeywords(MetaLink metaLink, MetaLink result) {
        Set<String> metaLinkLocations = getKeywords(metaLink);
        Set<String> resultKeywords = getKeywords(result);
        resultKeywords.addAll(metaLinkLocations);
        return resultKeywords;
    }

    private Set<String> getLocations(MetaLink metaLink) {
        return ofNullable(metaLink.locations())
                .map(Sets::newHashSet)
                .orElse(Sets.newHashSet());
    }

    private Set<String> getKeywords(MetaLink metaLink) {
        return ofNullable(metaLink.keywords())
                .map(Sets::newHashSet)
                .orElse(Sets.newHashSet());
    }

    @Override
    @Transactional
    public MetaLink saveLink(MetaLink metaLink) {

        String abbreviated = StringUtils.abbreviate(metaLink.summary(), 1_000);
        MetaLinkBo linkEntity = mapper.convertValue(metaLink.withSummary(abbreviated), MetaLinkBo.class);
        linkEntity = metaLinkRepository.saveAndFlush(linkEntity);
        if (linkEntity.isNew()) {
            em.persist(linkEntity);
        } else {
            linkEntity = em.merge(linkEntity);
        }
        return mapper.convertValue(linkEntity, MetaLink.class);
    }

    @Override
    public MetaLink getMetaLink(UUID uuid) {
        return ofNullable(metaLinkRepository.findById(uuid))
                .map(e -> {
                    MetaLink metaLink = mapper.convertValue(e, MetaLink.class);
                    eventBus.publish(Events.META_LINK_ACCESSED, metaLink);
                    return metaLink;
                }).orElse(null);
    }

    @Override
    public Page<MetaLink> findByExample(MetaLinkBo entity, Pageable pageable) {
        Set<String> keywords = entity.getKeywords();
        Set<String> locations = entity.getLocations();
        BooleanBuilder builder = createBuilder(keywords, locations);
        Page<MetaLinkBo> page = metaLinkRepository.findAll(builder, pageable);
        return page.map(m -> mapper.convertValue(m, MetaLink.class));
    }

    private BooleanBuilder createBuilder(Set<String> keywords, Set<String> locations) {
        BooleanBuilder builder = new BooleanBuilder();
        ofNullable(keywords)
                .map(words -> words.stream()
                        .filter(StringUtils::isNotBlank)
                        .filter(w -> !Commons.NOT_AVAILABLE.equals(w))
                        .collect(toSet()))
                .orElse(Collections.emptySet())
                .forEach(k -> builder.and($.keywords.any().equalsIgnoreCase(k)));
        locations = ofNullable(locations)
                .map(loc -> {
                    loc.removeIf(StringUtils::isEmpty);
                    return loc;
                })
                .orElse(Collections.emptySet());
        if (!locations.isEmpty()) {
            BooleanBuilder locationBuilder = new BooleanBuilder();
            locations.forEach(loc -> {
                locationBuilder.or($.geoLocations.any().name.containsIgnoreCase(loc));
            });
            builder.and(locationBuilder);
        }
        return builder;
    }

    @Override
    public Flowable<MetaLink> metaLinks(MetaLink example, Pageable pageable) {
        MetaLinkBo entity = mapper.convertValue(example, MetaLinkBo.class);
        Page<MetaLink> links = findByExample(entity, pageable);
        if (links.hasNext()) {
            return Flowable.fromIterable(links)
                    .concatWith(Flowable.defer(() -> metaLinks(example, links.nextPageable())));
        } else {
            return Flowable.fromIterable(links);
        }
    }

    @Override
    public Page<MetaLink> getLatest(Pageable pageable) {
        //        QMetaLinkEntity $ = QMetaLinkEntity.metaLinkEntity;
        //        BooleanExpression after = $.created.after(ZonedDateTime.now().minusDays(1));
        Page<MetaLinkBo> entities = metaLinkRepository.findAll(pageable);
        return entities.map(e -> mapper.convertValue(e, MetaLink.class));
    }

    @Override
    public Page<MetaLink> findUnresolvedGeoInfo(Pageable pageable) {
        Page<MetaLinkBo> page = metaLinkRepository.findAll($.geoLocations.isEmpty()
                .and($.address.isNotEmpty()), pageable);
        return page.map(e -> mapper.convertValue(e, MetaLink.class));
    }

    @Override
    public Page<MetaLink> findByAddress(String address) {
        return findByAddress(address, new PageRequest(0, 10));
    }

    @Override
    public Page<MetaLink> findByAddress(String address, Pageable pageable) {
        BooleanExpression predicate = $.address.equalsIgnoreCase(address)
                .and($.geoLocations.isNotEmpty());
        return metaLinkRepository.findAll(predicate, pageable)
                .map(e -> mapper.convertValue(e, MetaLink.class));
    }

    @Override
    public Page<MetaLink> findResolvedGeoInfo(String location, Pageable pageable) {
        QMetaLinkBo $ = QMetaLinkBo.metaLinkBo;
        String[] locations = location.split("-");
        List<String> locationList = Arrays.stream(locations)
                .filter(StringUtils::isNotBlank)
                .map(String::trim)
                .map(String::toLowerCase)
                .collect(toList());
        BooleanExpression predicate =
                $.geoLocations.any().name.toLowerCase().in(locationList);
        Page<MetaLinkBo> page = metaLinkRepository.findAll(predicate, pageable);
        return page.map(e -> mapper.convertValue(e, MetaLink.class));
    }

    @Override
    public Observable<MetaLink> allMetaLinks(int initialPage) {
        return Observable.defer(() -> {
            Page<MetaLinkBo> entities = metaLinkRepository.findAll(new PageRequest(initialPage, 20));
            if (entities.hasContent()) {
                return Observable.fromIterable(entities.getContent())
                        .map(e -> mapper.convertValue(e, MetaLink.class))
                        .concatWith(Observable.defer(() -> allMetaLinks(initialPage + 1)));
            }
            return Observable.empty();
        });
    }

    @Override
    public void saveLinks(List<MetaLink> metaLinks) {
        if (metaLinks != null) {
            Set<MetaLinkBo> entities = metaLinks.stream()
                    .map(m -> mapper.convertValue(m, MetaLinkBo.class))
                    .collect(toSet());
            metaLinkRepository.saveAll(entities);
        }
    }

    @Override
    public Flowable<MetaLink> allMetaLinks(Pageable page) {
        Page<MetaLinkBo> entitiesPage = metaLinkRepository.findAll(page);
        List<MetaLink> content = entitiesPage
                .map(e -> mapper.convertValue(e, MetaLink.class))
                .getContent();
        if (entitiesPage.hasNext()) {
            return Flowable.fromIterable(content)
                    .concatWith(Flowable.defer(() -> allMetaLinks(entitiesPage.nextPageable())));
        }
        return Flowable.fromIterable(content);
    }


    @Override
    public Flowable<MetaLink> allMissingPageDetail(Pageable page) {
        Page<MetaLinkBo> entitiesPage = metaLinkRepository.findAllByMissingPageDetail(page);
        List<MetaLink> content = entitiesPage
                .map(e -> mapper.convertValue(e, MetaLink.class))
                .getContent();
        if (entitiesPage.hasNext()) {
            return Flowable.fromIterable(content)
                    .concatWith(Flowable.defer(() -> allMissingPageDetail(entitiesPage.nextPageable())));
        }
        return Flowable.fromIterable(content);
    }

    @Override
    public void delete(MetaLink metaLink) {
        metaLinkRepository.deleteById(metaLink.uuid());
    }

    @Override
    public Page<MetaLink> filterLinks(MetaLinksFilter filter, Pageable pageable) {
        QMetaLinkBo q = QMetaLinkBo.metaLinkBo;

        BooleanBuilder keywordsBuilder = Optional.ofNullable(filter.keywords())
                .map(keywords -> keywords.stream()
                        .filter(StringUtils::isNotBlank)
                        .reduce(new BooleanBuilder(),
                                (booleanBuilder, s) -> booleanBuilder.and(q.keywords.any().equalsIgnoreCase(s)),
                                BooleanBuilder::and))
                .orElseGet(BooleanBuilder::new);

        BooleanBuilder locationsBuilder = Optional.ofNullable(filter.locations())
                .map(locations -> locations.stream()
                        .filter(StringUtils::isNotBlank)
                        .reduce(new BooleanBuilder(),
                                (booleanBuilder, s) -> booleanBuilder.or(q.locations.any().equalsIgnoreCase(s)),
                                BooleanBuilder::or))
                .orElseGet(BooleanBuilder::new);

        BooleanBuilder result = keywordsBuilder.and(locationsBuilder);
        submitMetaSearchEvent(filter);
        return metaLinkRepository.findAll(result, pageable)
                .map(m -> mapper.convertValue(m, MetaLink.class));
    }

    @Override
    public long countLinks(MetaLink example) {
        Set<String> keywords = example.keywords();
        keywords = ofNullable(keywords)
                .map(Sets::newHashSet)
                .orElse(Sets.newHashSet());
        Set<String> locations = example.locations();
        locations = ofNullable(locations)
                .map(Sets::newHashSet)
                .orElse(Sets.newHashSet());
        return metaLinkRepository.count(createBuilder(keywords, locations));
    }

    @Override
    public Optional<MetaLink> loadMetaLink(UUID uuid) {
        return Optional.ofNullable(metaLinkRepository.findById(uuid))
                .map(metaLinkBo -> mapper.convertValue(metaLinkBo, MetaLink.class));
    }

    private void submitMetaSearchEvent(MetaLinksFilter filter) {
        MetaSearch metaSearch = mapper.convertValue(filter, MetaSearch.class);
        eventBus.publish(Events.META_SEARCH_SUBMIT, metaSearch);
    }

    @Override
    public Page<MetaLinkBo> linksWithoutGeoLocations(Pageable pageable) {
        QMetaLinkBo $ = QMetaLinkBo.metaLinkBo;
        BooleanExpression predicate = $.geoLocations.isEmpty();
        Page<MetaLinkBo> page = metaLinkRepository.findAll(predicate, pageable);
        return page;
    }

    @Override
    public void findById(UUID uuid) {

    }

    @PostConstruct
    void init() {
        eventBus.consumer(Events.META_LINK_ACCESSED, (Handler<Message<MetaLink>>) event -> {
            ofNullable(event.body()).ifPresent(m -> {
                List<MetaSearch> metaSearches = metaSearchLinkService.getMetaSearches(m);
                metaSearches.forEach(ms -> {
                    eventBus.publish(Events.META_SEARCH_SUBMIT, ms);
                });
            });
        });
    }
}
