package co.jware.sojka.core.domain;


import javax.validation.constraints.Min;

public interface Ordered {
    @Min(value = 1)
    int order();
}
