package co.jware.sojka.core.domain.search;

import org.immutables.value.Value;

import java.util.Set;

@Value.Immutable
public abstract class AbstractMetaLocationResult {

    public abstract MetaLocation location();

    public abstract Set<String> resolvedLocations();
}
