package co.jware.sojka.core.service;

import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.function.Supplier;

@Service
public class TransactionHelper {

    @Transactional
    public <T> T withTransaction(Supplier<T> supplier) {
        return supplier.get();
    }

    @Transactional
    public void withTransAction(Runnable runnable) {
        runnable.run();
    }
}
