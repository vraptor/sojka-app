package co.jware.sojka.core.domain.pipelines;

import co.jware.sojka.core.domain.party.Party;

import com.fasterxml.jackson.annotation.JsonTypeName;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.immutables.value.Value;

@Value.Immutable
@JsonDeserialize(builder = OwnerPipeline.Builder.class)
@JsonTypeName("OWNER_PIPELINE")
public abstract class AbstractOwnerPipeline implements Pipeline {

    public abstract Party owner();

}
