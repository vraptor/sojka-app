package co.jware.sojka.core.processors;


import java.time.OffsetDateTime;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.springframework.util.Assert;

import co.jware.sojka.core.domain.JobListing;
import co.jware.sojka.core.domain.JobResponse;
import co.jware.sojka.core.domain.Recommendation;
import co.jware.sojka.core.domain.party.Party;
import co.jware.sojka.core.enums.JobResponseStatus;
import co.jware.sojka.core.enums.RecommendationStatus;
import co.jware.sojka.core.exceptions.DuplicateJobResponseException;
import co.jware.sojka.core.exceptions.JobListingExpiredException;


public class ResponseProcessor {
    Set<JobResponse> responses = new HashSet<>();

    public JobResponse responseToJob(JobListing jobListing, Party respondent) {
        Assert.isTrue(jobListing.hasFreeCapacity(), "Must have free capacity");
        JobListing listing = jobListing.withFreeCapacity(jobListing.freeCapacity() - 1);
        isNotExpired(listing);
        JobResponse response = JobResponse.builder()
                .jobListing(listing)
                .respondent(respondent)
                .build();
        return addResponse(response);
    }

    private void isNotExpired(JobListing jobListing) {
        Date now = new Date();
        if (jobListing.validTo().isBefore(OffsetDateTime.now())) {
            throw new JobListingExpiredException();
        }
    }

    public JobResponse respondToRecommendation(Recommendation recommendation) {
        Assert.isTrue(recommendation.status() == RecommendationStatus.PENDING);
        JobResponse response = JobResponse.builder()
                .respondent(recommendation.receiver())
                .build();
        return addResponse(response);
    }

    private JobResponse addResponse(JobResponse response) {
        if (responses.add(response)) {
            return response;
        } else {
            throw new DuplicateJobResponseException();
        }
    }

    public JobResponse processJobResponse(JobResponse jobResponse) {
        JobResponseStatus status = jobResponse.status();
        JobResponseStatus next = status.next();
        return jobResponse.withStatus(next);
    }
}
