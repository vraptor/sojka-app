package co.jware.sojka.core.domain.pipelines;

import co.jware.sojka.core.domain.JobListing;
import org.immutables.value.Value;

@Value.Immutable
public interface AbstractJobListingCard extends Card<JobListing> {
}
