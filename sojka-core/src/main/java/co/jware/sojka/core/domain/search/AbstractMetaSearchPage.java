package co.jware.sojka.core.domain.search;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.immutables.value.Value;

import java.util.List;

@Value.Immutable
@JsonDeserialize(builder = MetaSearchPage.Builder.class)
public abstract class AbstractMetaSearchPage {

    public abstract MetaSearch metaSearch();

    public abstract List<MetaLink> links();
}
