package co.jware.sojka.core.service.entity;

import co.jware.sojka.core.domain.JobListing;
import co.jware.sojka.core.domain.party.Hirer;
import co.jware.sojka.core.domain.party.Party;
import co.jware.sojka.core.domain.search.MetaLink;
import co.jware.sojka.entities.core.JobListingBo;
import co.jware.sojka.entities.core.QJobListingBo;
import co.jware.sojka.repository.JobListingRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;
import java.util.UUID;

@Service("jobListingService")
@Transactional
public class JobListingServiceImpl extends AbstractEntityService implements JobListingService {
    @Autowired
    JobListingRepository repository;
    @Autowired
    AccountService accountService;
    @Autowired
    private CompanyService companyService;
    @Autowired
    private HirerService hirerService;
    QJobListingBo $ = QJobListingBo.jobListingBo;
    @Autowired
    ObjectMapper mapper;

    @Override
    public Optional<JobListing> findByUuid(UUID uuid) {
        return Optional.ofNullable(repository.findById(uuid))
                .map(e -> mapper.convertValue(e, JobListing.class));
    }

    @Override
    public Page<JobListing> findJobListings() {
        return findJobListings(new PageRequest(offset(), limit()));
    }

    @Override
    @Transactional
    public Page<JobListing> findJobListings(Pageable pageable) {
        final Page<JobListingBo> page = repository.findAll(pageable);
        return page.map(entity -> {
                    Hibernate.initialize(entity.getOwner());
                    Hirer owner = mapper.convertValue(entity.getOwner(), Hirer.class);
                    return mapper.convertValue(entity, JobListing.class);
                }
        );
    }

    @Override
    public Optional<JobListing> createJobListing(JobListing jobListing) {
        return Optional.ofNullable(jobListing)
                .map(j -> mapper.convertValue(j, JobListingBo.class))
                .map(repository::save)
                .map(e -> mapper.convertValue(e, JobListing.class));
    }

    @Override
    public JobListing getOrCreate(JobListing jobListing) {
        String link = Optional.ofNullable(jobListing.link())
                .orElseThrow(IllegalArgumentException::new);
        return findByLink(link)
                .orElseGet(() -> createJobListing(jobListing)
                        .orElseThrow(IllegalArgumentException::new));
    }

    @Override
    public Page<JobListing> findByOwner(Hirer hirer, Pageable pageable) {
        return repository.findAll($.owner.uuid.eq(resolveUuid(hirer)), pageable)
                .map(e -> mapper.convertValue(e, JobListing.class));
    }

    @Override
    public void checkOwnerEligibility(JobListing jobListing) {
        Party owner = Optional.ofNullable(jobListing.owner())
                .orElseThrow(IllegalArgumentException::new);
    }

    @Override
    public Optional<JobListing> findByLink(String link) {
        return Optional.ofNullable(repository.findByLink(link))
                .map(e -> objectMapper.convertValue(e, JobListing.class));
    }

    @Override
    public JobListing update(JobListing jobListing) {
        return Optional.ofNullable(jobListing)
                .map(j -> mapper.convertValue(j, JobListingBo.class))
                .map(repository::save)
                .map(e -> mapper.convertValue(e, JobListing.class))
                .orElseThrow(IllegalArgumentException::new);
    }

    @Override
    public JobListing fromMetaLink(MetaLink metaLink) {
        JobListing jobListing = objectMapper.convertValue(metaLink, JobListing.Builder.class)
                .name(metaLink.title())
                .build();
        Optional.ofNullable(jobListing.owner()).orElseGet(() -> hirerService.systemHirer());
        return jobListing;
    }
}
