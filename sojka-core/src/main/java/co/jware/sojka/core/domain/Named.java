package co.jware.sojka.core.domain;


import javax.annotation.Nullable;
import java.util.Optional;

public interface Named {
    String firstName();

    @Nullable
    String surname();

    String lastName();
}
