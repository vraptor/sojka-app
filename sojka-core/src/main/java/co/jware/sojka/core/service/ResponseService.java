package co.jware.sojka.core.service;


import co.jware.sojka.core.domain.JobListing;
import co.jware.sojka.core.domain.JobResponse;
import co.jware.sojka.core.domain.Recommendation;
import co.jware.sojka.core.domain.party.Candidate;

public interface ResponseService
{
    JobResponse respondToRecommendation(Recommendation recommendation);

    JobResponse respondToJobListing(JobListing jobListing, Candidate candidate);
}
