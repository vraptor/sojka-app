package co.jware.sojka.core.processors;


import co.jware.sojka.core.domain.Tag;
import co.jware.sojka.core.domain.wrapped.TagWeight;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

import static com.google.common.collect.Sets.intersection;
import static java.util.Optional.ofNullable;
import static java.util.stream.Collectors.averagingDouble;

public class DefaultTagProcessor implements TagProcessor {

    @Override
    public BigDecimal evaluateTags(Set<Tag> tags, Set<Tag> etalonTags) {
        Set<Tag> toTags = ofNullable(tags).orElse(new HashSet<>());
        Set<Tag> fromTags = ofNullable(etalonTags).orElse(new HashSet<>());
        return new BigDecimal(intersection(fromTags, toTags)
                .stream()
                .map(Tag::weight)
                .map(TagWeight::value)
                .collect(averagingDouble(BigDecimal::doubleValue)));
    }


    private double round(double value) {
        return Math.round(value * 100) / 100.0;
    }
}
