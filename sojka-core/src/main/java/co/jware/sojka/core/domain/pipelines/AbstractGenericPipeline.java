package co.jware.sojka.core.domain.pipelines;

import org.immutables.value.Value;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeName;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

@Value.Immutable
@JsonDeserialize(builder = PersonPipeline.Builder.class)
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME)
@JsonTypeName("GENERIC_PIPELINE")
public abstract class AbstractGenericPipeline implements Pipeline {
}
