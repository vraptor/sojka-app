package co.jware.sojka.core.domain.search;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.immutables.value.Value;
import org.springframework.web.util.UriComponentsBuilder;

import javax.annotation.Nullable;
import java.net.URI;

@Value.Immutable
@JsonDeserialize(builder = MetaSearchResult.Builder.class)
public abstract class AbstractMetaSearchResult {

    public abstract URI uri();

    @Value.Derived
    public URI baseUri() {
        return UriComponentsBuilder.fromUri(uri())
                .replacePath("")
                .replaceQueryParams(null)
                .build()
                .encode()
                .toUri();
    }

    @Nullable
    public abstract String htmlPayload();

    public abstract int statusCode();
}
