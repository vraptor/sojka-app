package co.jware.sojka.core.service;


import co.jware.sojka.core.domain.search.MetaLink;
import co.jware.sojka.core.domain.search.MetaLinksFilter;
import co.jware.sojka.entities.core.metasearch.MetaLinkBo;
import io.reactivex.Flowable;
import io.reactivex.Observable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface MetaLinkService {
    Page<MetaLink> findByExample(MetaLinkBo entity, Pageable pageable);

    MetaLink findOrCreate(MetaLink metaLink);

    MetaLink saveLink(MetaLink metaLink);

    MetaLink getMetaLink(UUID uuid);

    Flowable<MetaLink> metaLinks(MetaLink example, Pageable pageable);

    Page<MetaLink> getLatest(Pageable pageable);

    Page<MetaLink> findUnresolvedGeoInfo(Pageable pageable);

    Page<MetaLink> findByAddress(String address);

    Page<MetaLink> findByAddress(String address, Pageable pageable);

    Page<MetaLink> findResolvedGeoInfo(String location, Pageable pageable);

    Observable<MetaLink> allMetaLinks(int initialPage);

    void saveLinks(List<MetaLink> metaLinks);

    long countLinks(MetaLink example);

    Optional<MetaLink> loadMetaLink(UUID uuid);

    Flowable<MetaLink> allMetaLinks(Pageable page);

    Flowable<MetaLink> allMissingPageDetail(Pageable page);

    void delete(MetaLink metaLink);

    Page<MetaLink> filterLinks(MetaLinksFilter filter, Pageable pageable);

    Page<MetaLinkBo> linksWithoutGeoLocations(Pageable pageable);

    void findById(UUID fromString);
}
