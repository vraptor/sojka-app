package co.jware.sojka.core.domain;

import com.fasterxml.jackson.annotation.JsonTypeName;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.immutables.value.Value;

import javax.annotation.Nullable;
import java.time.LocalDate;

@Value.Immutable
@JsonDeserialize(builder = Employment.Builder.class)
@JsonTypeName("EMPLOYMENT")
public interface AbstractEmployment {

    String position();

    @Nullable
    String description();

    @Nullable
    Company company();

    @Nullable
    LocalDate fromDate();

    @Nullable
    LocalDate toDate();
}
