package co.jware.sojka.core.service;



import co.jware.sojka.core.domain.JobListing;
import co.jware.sojka.entities.core.JobListingBo;
import org.springframework.cache.annotation.Cacheable;

public interface JobService {
    @Cacheable("owners")
    Iterable<JobListingBo> findByOwner(String ownerUuid);

    JobListing findByUuid(String uuidString);

    JobListing postJobListing(JobListing jobListing);
}
