package co.jware.sojka.core.domain.pipelines;

import co.jware.sojka.core.domain.Identifiable;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

import javax.annotation.Nullable;
import java.util.List;
import java.util.Set;

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME)
public interface Pipeline extends Identifiable {
    @Nullable
    @JsonTypeInfo(use = JsonTypeInfo.Id.NAME)
    Pipeline parent();

    String name();

    List<Stage> stages();

    @Nullable
    Set<Label> labels();
}
