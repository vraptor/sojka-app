package co.jware.sojka.core.service.entity;

import co.jware.sojka.core.domain.JobListing;
import co.jware.sojka.core.domain.Recommendation;
import co.jware.sojka.core.domain.party.Party;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;
import java.util.UUID;

public interface RecommendationService {

    Page<Recommendation> findPersonRecommendations(Pageable pageable);

    Page<Recommendation> findByReceiver(Party person, Pageable pageable);

    Page<Recommendation> findBySender(Party person, Pageable pageable);

    Page<Recommendation> findByJobListing(JobListing jobListing, Pageable pageable);

    Optional<Recommendation> findByUuid(UUID uuid);

    Recommendation createRecommendation(Recommendation recommendation);

    Optional<Recommendation> findRecommendation(Recommendation recommendation);

    Recommendation recommendSubject(Party sender, Party receiver, UUID uuid);

    void delete(Recommendation recommendation);

    Page<Recommendation> findBySubject(UUID uuid, Pageable pageable);
}
