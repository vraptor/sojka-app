package co.jware.sojka.core.service.impl;

import co.jware.sojka.core.domain.JobListing;
import co.jware.sojka.core.domain.JobResponse;
import co.jware.sojka.core.domain.Recommendation;
import co.jware.sojka.core.domain.party.Candidate;
import co.jware.sojka.core.enums.RecommendationStatus;
import co.jware.sojka.core.processors.RecommendProcessor;
import co.jware.sojka.core.processors.ResponseProcessor;
import co.jware.sojka.core.service.ResponseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;

import static co.jware.sojka.core.enums.RecommendationStatus.PENDING;


public class ResponseServiceImpl implements ResponseService {

    @Autowired
    private ResponseProcessor responseProcessor;
    @Autowired
    private RecommendProcessor recommendProcessor;

    @Override
    public JobResponse respondToRecommendation(Recommendation recommendation) {
       Assert.isTrue(PENDING.equals(recommendation.status()));
        JobResponse response = responseProcessor.respondToRecommendation(recommendation);
        recommendProcessor.updateStatus(recommendation, RecommendationStatus.RESPONSE_SENT);
        return response;
    }

    @Override
    public JobResponse respondToJobListing(JobListing jobListing, Candidate candidate) {
        Assert.isTrue(!jobListing.isExpired());
        Assert.isTrue(jobListing.hasFreeCapacity());
        return responseProcessor.responseToJob(jobListing, candidate);
    }
}
