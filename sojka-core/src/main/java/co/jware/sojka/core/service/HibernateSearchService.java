package co.jware.sojka.core.service;

import javax.persistence.EntityManagerFactory;

public class HibernateSearchService {


    private final EntityManagerFactory emf;

    public HibernateSearchService(EntityManagerFactory emf) {
        this.emf = emf;
    }

    public void initHibernateService() {
        // TODO: 11/18/2018 Externalize configuration
//        EntityManager entityManager = emf.createEntityManager();
//        FullTextEntityManager fullTextEntityManager = Search.getFullTextEntityManager(entityManager);
//        try {
//            fullTextEntityManager.createIndexer().startAndWait();
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
    }
}
