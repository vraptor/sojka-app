package co.jware.sojka.core.service.impl;

import co.jware.sojka.core.domain.Company;
import co.jware.sojka.core.domain.party.Person;
import co.jware.sojka.core.service.entity.AbstractEntityService;
import co.jware.sojka.core.service.entity.CompanyService;
import co.jware.sojka.entities.core.CompanyBo;
import co.jware.sojka.entities.core.PersonBo;
import co.jware.sojka.entities.core.QHirerBo;
import co.jware.sojka.repository.CompanyRepository;
import co.jware.sojka.repository.HirerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static java.util.stream.Collectors.toList;

@Service("companyService")
@Transactional
public class CompanyServiceImpl extends AbstractEntityService implements CompanyService {
    private final static ExampleMatcher matcher = ExampleMatcher.matching().withIgnorePaths("uuid");
    @Autowired
    CompanyRepository repository;
    @Autowired
    HirerRepository hirerRepository;

    @Override
    public Optional<Company> getOrCreate(Company company) {
        Optional<Company> loaded = loadCompany(company);
        return (loaded.isPresent()) ? loaded : saveCompany(company);
    }

    private Optional<Company> saveCompany(Company company) {
        return Optional.ofNullable(company)
                .map(c -> objectMapper.convertValue(c, CompanyBo.class))
                .map(repository::save)
                .map(c -> objectMapper.convertValue(c, Company.class));
    }

    private Optional<Company> loadCompany(Company company) {
        return Optional.ofNullable(company)
                .map(Company::name)
                .map(repository::findByName)
                .map(c -> objectMapper.convertValue(c, Company.class));
    }

    Optional<Company> loadByExample(Company company) {
        return Optional.of(repository.findOne(Example.of(objectMapper.convertValue(company, CompanyBo.class))))
                .map(c -> objectMapper.convertValue(c, Company.class));
    }

    @Override
    public List<Person> personnel(Company company) {
        UUID uuid = resolveUuid(company);
        CompanyBo entity = repository.findById(uuid)
                .orElseThrow(IllegalArgumentException::new);
        List<PersonBo> personnel = entity.getPersonnel();
        return Optional.ofNullable(personnel)
                .orElse(Collections.emptyList())
                .stream()
                .map(c -> objectMapper.convertValue(c, Person.class))
                .collect(toList());
    }

    @Override
    public Page<Person> personnel(Company company, Pageable pageable) {
        QHirerBo h = QHirerBo.hirerBo;
        return hirerRepository.findAll(h.company.uuid.eq(resolveUuid(company)), pageable)
                .map(e -> objectMapper.convertValue(e, Person.class));
    }

    @Override
    public Optional<Company> findByName(String name) {
        return Optional.ofNullable(repository.findByName(name))
                .map(e -> objectMapper.convertValue(e, Company.class));
    }
}
