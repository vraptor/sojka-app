package co.jware.sojka.core.processors;

import co.jware.sojka.core.domain.JobListing;


public interface JobProcessor {

    JobListing postJobListing(JobListing jobListing);

    JobListing publishJobListing(JobListing jobListing);
}
