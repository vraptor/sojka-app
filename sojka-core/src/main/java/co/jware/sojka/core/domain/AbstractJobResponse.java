package co.jware.sojka.core.domain;

import co.jware.sojka.core.domain.party.Candidate;
import co.jware.sojka.core.domain.party.External;
import co.jware.sojka.core.domain.party.Party;
import co.jware.sojka.core.enums.JobResponseStatus;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.google.common.base.Preconditions;
import org.immutables.serial.Serial;
import org.immutables.value.Value;

import java.time.OffsetDateTime;

import static co.jware.sojka.core.enums.JobResponseStatus.PENDING;

@Value.Immutable
@Serial.Structural
@JsonSerialize(as = JobResponse.class)
@JsonDeserialize(builder = JobResponse.Builder.class)
public interface AbstractJobResponse extends Identifiable {

    JobListing jobListing();

    @Value.Default
    @Value.Auxiliary
    @JsonFormat(shape = JsonFormat.Shape.STRING,
            pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSZZ")
    default OffsetDateTime createDate() {
        return OffsetDateTime.now();
    }

    @JsonTypeInfo(use = JsonTypeInfo.Id.NAME)
    Party respondent();

    @Value.Default
    default JobResponseStatus status() {
        return PENDING;
    }

    @Value.Check
    default void check() {
        Preconditions.checkState(Candidate.class.isInstance(respondent()) ||
                External.class.isInstance(respondent()), "Illegal respondent type");
    }


}
