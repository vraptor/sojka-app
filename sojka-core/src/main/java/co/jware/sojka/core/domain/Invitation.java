package co.jware.sojka.core.domain;

import java.time.OffsetDateTime;
import java.util.List;

import javax.annotation.Nullable;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.immutables.value.Value;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import co.jware.sojka.core.domain.party.Party;
import co.jware.sojka.core.domain.party.Person;

import static com.fasterxml.jackson.annotation.JsonSubTypes.Type;

//@Value.Immutable()
//@JsonSerialize(as = Invitation.class)
//@JsonDeserialize(builder = Invitation.Builder.class)
public interface Invitation<T> {
//    @JsonTypeInfo(use = JsonTypeInfo.Id.NAME)
//    @JsonSubTypes({
//            @Type(name = "EVALUATION", value = Evaluation.class),
//            @Type(name = "INTERVIEW", value = Interview.class)
//    })
    T event();

    @JsonFormat(shape = JsonFormat.Shape.STRING,
            pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSZZ")
    OffsetDateTime eventDateTime();

    Person receiver();

    Party sender();

    @Nullable
    List<InvitationHistory<T>> history();
}
