package co.jware.sojka.core.domain.search;

import co.jware.sojka.SkipNulls;
import co.jware.sojka.core.domain.BaseImmutable;
import co.jware.sojka.core.domain.geo.GeoLocation;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.immutables.serial.Serial;
import org.immutables.value.Value;

import javax.annotation.Nullable;
import java.util.Set;

@Value.Immutable
@JsonDeserialize(builder = MetaLink.Builder.class)
@Serial.Version(2L)
@Serial.Structural()
public abstract class AbstractMetaLink extends BaseImmutable {

    @Nullable
    public abstract String baseUri();

    public abstract String href();

    public abstract String title();

    @Nullable
    public abstract String company();

    @Nullable
    public abstract String address();

    @Nullable
    public abstract String summary();

    @SkipNulls
    @Nullable
    public abstract Set<String> keywords();

    @SkipNulls
    @Nullable
    public abstract Set<String> locations();

    @Nullable
    public abstract Set<GeoLocation> geoLocations();

    @Value.Default
    @SkipNulls
    public String[] sources() {
        return new String[0];
    }

    @Value.Default
    public int httpStatus() {
        return 0;
    }
}
