package co.jware.sojka.core.service;

import co.jware.sojka.core.domain.pipelines.Stage;

import java.util.UUID;

public interface StageService {
    Stage findStage(UUID uuid);
}
