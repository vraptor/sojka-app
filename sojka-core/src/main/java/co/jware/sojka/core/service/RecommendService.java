package co.jware.sojka.core.service;


import java.util.concurrent.CompletableFuture;

import co.jware.sojka.core.domain.JobListing;
import co.jware.sojka.core.domain.Recommendation;
import co.jware.sojka.core.domain.party.Person;

public interface RecommendService {
    CompletableFuture<Recommendation> recommendViaReceiverEmail(String receiverEmail, Person sender, JobListing jobListing);

    CompletableFuture<Recommendation> getRecommendation(String uuid);
}