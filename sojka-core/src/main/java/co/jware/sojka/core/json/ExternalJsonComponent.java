package co.jware.sojka.core.json;

import java.io.IOException;

import org.springframework.boot.jackson.JsonComponent;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import co.jware.sojka.core.domain.party.External;

@JsonComponent()
public class ExternalJsonComponent {
    public static class Serializer extends JsonSerializer<External> {
        @Override
        public void serialize(External external, JsonGenerator jgen, SerializerProvider sp)
                throws IOException {
            jgen.writeStartObject();
//            external.uuid().map(uuid -> {
//                try {
//                    return jgen.writeStringField("uuid", uuid.toString());
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//            });
            jgen.writeEndObject();
        }

    }
}

