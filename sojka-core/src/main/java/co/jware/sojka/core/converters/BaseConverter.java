package co.jware.sojka.core.converters;

import com.fasterxml.classmate.ResolvedType;
import com.fasterxml.classmate.TypeResolver;
import com.fasterxml.jackson.databind.ObjectMapper;
import groovy.lang.Lazy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.converter.Converter;

import java.util.List;


public class BaseConverter<S, T> implements Converter<S, T> {

    private final Class targetType;

    public BaseConverter() {
        super();
        TypeResolver typeResolver = new TypeResolver();
        ResolvedType resolve = typeResolver.resolve(this.getClass());
        List<ResolvedType> types = resolve.typeParametersFor(BaseConverter.class);
        targetType = types.get(1).getErasedType();
    }

    @Autowired
    private ObjectMapper mapper;

    @Lazy
    protected ConversionService conversionService;

    @Override
    public T convert(S source) {
        return (T) mapper.convertValue(source, targetType);
    }

    public void setConversionService(ConversionService conversionService) {
        this.conversionService = conversionService;
    }
}
