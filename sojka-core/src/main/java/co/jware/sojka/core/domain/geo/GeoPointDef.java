package co.jware.sojka.core.domain.geo;


import co.jware.sojka.core.domain.Tupled;
import org.immutables.value.Value;

import java.math.BigDecimal;

@Value.Immutable
@Tupled
public abstract class GeoPointDef {

    public abstract BigDecimal lon();

    public abstract BigDecimal lat();
}
