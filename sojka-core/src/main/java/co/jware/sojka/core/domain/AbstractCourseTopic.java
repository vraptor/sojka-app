package co.jware.sojka.core.domain;

import org.immutables.value.Value;

import javax.annotation.Nullable;
import java.time.Duration;
import java.util.Optional;

@Value.Immutable
public interface AbstractCourseTopic {

    String name();

    @Nullable
    String description();

    default Duration duration() {
        return Duration.ZERO;
    }
}
