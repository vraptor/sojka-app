package co.jware.sojka.core.domain.search;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.immutables.value.Value;

@Value.Immutable
@JsonDeserialize(builder = MetaLocation.Builder.class)
public abstract class AbstractMetaLocation {

    public abstract String name();
}
