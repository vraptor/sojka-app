package co.jware.sojka.core.service.entity;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import co.jware.sojka.core.domain.party.Teacher;
import co.jware.sojka.entities.core.TeacherBo;
import co.jware.sojka.repository.TeacherRepository;

import static java.util.Optional.ofNullable;

@Service("teacherService")
public class TeacherServiceImpl extends AbstractEntityService implements TeacherService {
    @Autowired
    TeacherRepository repository;

    @Override
    public Teacher createTeacher(Teacher teacher) {
        return ofNullable(teacher)
                .map(c -> objectMapper.convertValue(c, TeacherBo.class))
                .map(repository::save)
                .map(c -> objectMapper.convertValue(c, Teacher.class))
                .orElseThrow(IllegalArgumentException::new);
    }
}
