package co.jware.sojka.core.service.page;


import co.jware.sojka.core.domain.search.page.PageDetail;
import org.hibernate.StaleObjectStateException;
import org.springframework.orm.ObjectOptimisticLockingFailureException;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Retryable;

import java.util.Optional;
import java.util.UUID;

public interface PageDetailService {

    PageDetail save(PageDetail detail);

    @Retryable(maxAttempts = 3, backoff = @Backoff(delay = 5000L))
    PageDetail saveOrUpdate(PageDetail detail);

    Optional<PageDetail> findByUrl(String url);

    boolean exists(UUID uuid);

    boolean existsByUrl(String url);
}
