package co.jware.sojka.core.service.entity;

import co.jware.sojka.core.domain.Company;
import co.jware.sojka.core.domain.party.Hirer;
import co.jware.sojka.entities.core.HirerBo;
import co.jware.sojka.entities.core.QHirerBo;
import co.jware.sojka.repository.HirerRepository;
import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;

@Service("hirerService")
@Transactional
public class HirerServiceImpl extends AbstractEntityService implements HirerService {
    @Autowired
    HirerRepository repository;
    @Autowired
    CompanyService companyService;

    @Override
    public Hirer createHirer(Hirer hirer) {
        return Optional.ofNullable(hirer)
                .map(h -> objectMapper.convertValue(h, HirerBo.class))
                .map(repository::save)
                .map(e -> objectMapper.convertValue(e, Hirer.class))
                .orElseThrow(() -> new RuntimeException("Error creating hirer"));
    }

    @Override
    public Hirer getOrCreate(Hirer hirer) {
        HirerBo example = objectMapper.convertValue(hirer, HirerBo.class);
        ExampleMatcher matcher = ExampleMatcher.matching().withIgnorePaths("uuid", "company");
        Example<HirerBo> entityExample = Example.of(example, matcher);
        return repository.findOne(entityExample)
                .map(c -> objectMapper.convertValue(c, Hirer.class))
                .orElseGet(() -> this.createHirer(hirer));
    }

    @Override
    public Optional<Hirer> findByCompanyName(Company company) {
        Comparator<HirerBo> comp = Comparator.comparing(a -> a.getId().toString());
        QHirerBo $ = QHirerBo.hirerBo;
        List list = Optional.ofNullable(company)
                .map(Company::name)
                .map(name -> (List) repository.findAll($.company.name.eq(name)))
                .map(l -> {
                    l.sort(comp);
                    return l;
                })
                .orElse(Lists.newArrayList());
        if (list.isEmpty()) {
            return Optional.empty();
        } else {
            HirerBo entity = (HirerBo) list.get(0);
            return Optional.ofNullable(entity)
                    .map(c -> objectMapper.convertValue(c, Hirer.class));
        }
    }

    @Override
    public Hirer systemHirer() {
        return Hirer.builder()
                .firstName("Sojka")
                .lastName("Hirer")
                .email("system.sojka@jware.co")
                .build();
    }
}
