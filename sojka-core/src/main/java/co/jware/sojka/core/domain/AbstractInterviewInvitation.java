package co.jware.sojka.core.domain;

import org.immutables.value.Value;

@Value.Immutable
public interface AbstractInterviewInvitation extends Invitation<Interview> {
}
