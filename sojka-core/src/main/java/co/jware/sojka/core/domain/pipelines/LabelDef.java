package co.jware.sojka.core.domain.pipelines;

import co.jware.sojka.core.domain.Tupled;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.immutables.value.Value;

@Tupled
@Value.Immutable
@JsonDeserialize()
public interface LabelDef {
    String name();

    String color();
}
