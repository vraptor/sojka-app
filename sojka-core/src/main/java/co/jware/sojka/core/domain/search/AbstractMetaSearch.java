package co.jware.sojka.core.domain.search;

import co.jware.sojka.SkipNulls;
import co.jware.sojka.core.domain.BaseImmutable;
import co.jware.sojka.core.enums.search.MetaSearchSource;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.immutables.serial.Serial;
import org.immutables.value.Value;

import javax.annotation.Nullable;
import java.net.URL;
import java.time.OffsetDateTime;
import java.util.Collections;
import java.util.EnumSet;
import java.util.Set;

import static co.jware.sojka.core.enums.search.MetaSearchSource.ANY;

@Value.Immutable
@JsonDeserialize(builder = MetaSearch.Builder.class)
@Serial.Version(1L)
public abstract class AbstractMetaSearch extends BaseImmutable {

    @Value.Default
    @SkipNulls
    @Nullable
    public Set<String> locations() {
        return Collections.emptySet();
    }

    @Value.Default
    @SkipNulls
    @Nullable
    public Set<String> keywords() {
        return Collections.emptySet();
    }

    @Value.Default
    @Value.Auxiliary
    public OffsetDateTime created() {
        return OffsetDateTime.now();
    }

    @Nullable
    @Value.Auxiliary
    public abstract OffsetDateTime updated();

    @Value.Default
    public int page() {
        return 1;
    }

    @Nullable
    @Value.Auxiliary
    @SkipNulls
    public abstract Set<URL> resolved();

    @Value.Default
    public EnumSet<MetaSearchSource> sources() {
        return EnumSet.of(ANY);
    }
}
