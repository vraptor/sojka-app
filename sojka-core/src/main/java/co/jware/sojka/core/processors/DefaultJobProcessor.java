package co.jware.sojka.core.processors;


import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

import co.jware.sojka.core.domain.party.Party;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import co.jware.sojka.core.domain.JobListing;
import co.jware.sojka.core.domain.party.Hirer;
import co.jware.sojka.core.enums.JobListingOrigin;
import co.jware.sojka.core.enums.JobListingStatus;
import co.jware.sojka.core.exceptions.AccountNotPresentException;
import co.jware.sojka.core.exceptions.DuplicateJobPostException;
import co.jware.sojka.core.service.entity.JobListingService;

import javax.annotation.Nullable;

@Service("jobProcessor")
public class DefaultJobProcessor implements JobProcessor {

    @Autowired
    private JobListingService jobListingService;
    private Map<UUID, JobListing> jobs = new HashMap<>();

    @Override
    public JobListing postJobListing(JobListing jobListing) {

        Party owner = Optional.ofNullable(jobListing.owner())
                .orElseThrow(() -> new IllegalArgumentException("Owner not present"));
        if (owner instanceof Hirer) {
            Hirer hirer = (Hirer) owner;
            if (hirer.registered()) {
//        Account account = owner.account()
//                .orElseThrow(() -> new AccountNotPresentException());
//        account.billingInfo()
//                .orElseThrow(() -> new BillingInfoNotPresentException());
                JobListing listing = JobListing.copyOf(jobListing);
                addJobListing(listing);
                return listing;
            }
        }
        throw new AccountNotPresentException();
    }

    @Override
    public JobListing publishJobListing(JobListing jobListing) {
        if (jobListing.origin() != JobListingOrigin.INTERNAL)
            throw new IllegalArgumentException("Can't publish external job listing");
        Party owner = Optional.ofNullable(jobListing.owner())
                .orElseThrow(() -> new IllegalArgumentException("Owner is required"));
        jobListingService.checkOwnerEligibility(jobListing);
        return JobListing.copyOf(jobListing)
                .withStatus(JobListingStatus.ACTIVE);
    }

    private boolean addJobListing(JobListing listing) {
        if (this.jobs.containsValue(listing)) {
            throw new DuplicateJobPostException();
        }
        UUID uuid = Optional.ofNullable(listing.uuid())
                .orElse(UUID.randomUUID());
        JobListing jobListing = this.jobs.putIfAbsent(uuid, listing.withUuid(uuid));
        return null != jobListing;
    }

    public JobListing findByUuid(String uuidString) {
        UUID uuid = UUID.fromString(uuidString);
        JobListing jobListing = this.jobs.get(uuid);
        return jobListing;
    }
}
