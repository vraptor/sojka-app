package co.jware.sojka.core.service.entity;

import java.util.Optional;

import co.jware.sojka.core.domain.Company;
import co.jware.sojka.core.domain.party.Hirer;

public interface HirerService {

    Hirer createHirer(Hirer hirer);

    //    List<Candidate> getFavoriteCandidates(Hirer hirer);

    Hirer getOrCreate(Hirer hirer);

    Optional<Hirer> findByCompanyName(Company company);

    Hirer systemHirer();
}
