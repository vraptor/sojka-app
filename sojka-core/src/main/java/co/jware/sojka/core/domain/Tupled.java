package co.jware.sojka.core.domain;

import org.immutables.value.Value;

@Value.Style(
        typeAbstract = "*Def",
        typeImmutable = "*",
        allParameters = true,
        visibility = Value.Style.ImplementationVisibility.PUBLIC,
        defaults = @Value.Immutable(builder = false, copy = false)
)
public @interface Tupled {
}
