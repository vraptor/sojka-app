package co.jware.sojka.core.domain.pipelines;

import org.immutables.value.Value;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeName;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import co.jware.sojka.core.domain.party.Candidate;

@Value.Immutable
@JsonDeserialize(builder = PersonPipeline.Builder.class)
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME)
@JsonTypeName("PERSON_PIPELINE")
public abstract class AbstractPersonPipeline implements Pipeline {
    public abstract Candidate person();
}
