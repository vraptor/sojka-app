package co.jware.sojka.core.domain.party;

import com.fasterxml.jackson.annotation.JsonTypeName;
import org.immutables.value.Value;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@Value.Immutable
@JsonSerialize(as = Teacher.class)
@JsonDeserialize(builder = Teacher.Builder.class)
@JsonTypeName("TEACHER")
public interface AbstractTeacher extends Person {
    Teacher NULL = Teacher.builder()
            .email("t")
            .firstName("f")
            .lastName("l")
            .registered(false)
            .build();
}
