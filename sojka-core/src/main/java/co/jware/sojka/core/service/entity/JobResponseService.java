package co.jware.sojka.core.service.entity;


import java.util.List;
import java.util.Optional;

import co.jware.sojka.core.domain.party.Party;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import co.jware.sojka.core.domain.Evaluation;
import co.jware.sojka.core.domain.Interview;
import co.jware.sojka.core.domain.JobListing;
import co.jware.sojka.core.domain.JobResponse;
import co.jware.sojka.core.domain.party.Candidate;

public interface JobResponseService {

    Page<JobResponse> findByJobListing(JobListing jobListing, Pageable pageable);

    Page<JobResponse> findByCandidate(Candidate candidate, Pageable pageable);

    Optional<JobResponse> createJobResponse(JobResponse jobResponse);

    List<Interview> getInterviews(JobResponse jobResponse);

    List<Evaluation> getTests(JobResponse jobResponse);

    Optional<JobResponse> findJobResponse(JobResponse response);

    Page<JobResponse> findByRespondent(Party respondent, Pageable pageable);

//    List<Recommendation> listRecommedationsFor(JobResponse response);
}
