package co.jware.sojka.core.service.geo.impl;

import co.jware.sojka.core.domain.geo.GeoLocation;
import co.jware.sojka.core.domain.search.Commons;
import co.jware.sojka.core.service.geo.GeoLocationService;
import co.jware.sojka.core.service.geo.GoogleMapsService;
import co.jware.sojka.entities.core.geo.GeoLocationBo;
import co.jware.sojka.repository.geo.GeoLocationRepository;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Sets;
import com.google.maps.model.GeocodingResult;
import io.reactivex.Observable;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.io.IOException;
import java.util.*;

import static java.util.stream.Collectors.toSet;

@Service("geoLocationService")
@Transactional
public class GeoLocationServiceImpl implements GeoLocationService {

    @Autowired
    private GeoLocationRepository geoLocationRepository;
    @Autowired
    private ObjectMapper objectMapper;
    @Autowired
    private GoogleMapsService googleMapsService;
    @PersistenceContext
    private EntityManager em;

    @Override
    public Set<GeoLocation> resolveAddress(String address) {
        Set<String> locations = preProcessAddress(address);
        if (locations.isEmpty()) {
            return Collections.emptySet();
        }
        List<GeoLocationBo> entities = geoLocationRepository.findByNames(locations);
        return Sets.newHashSet(Observable.fromIterable(entities)
                .distinct(GeoLocationBo::getPlaceId)
                .map(e -> objectMapper.convertValue(e, GeoLocation.class))
                .blockingIterable());
    }

    Set<String> preProcessAddress(String address) {
        return Optional.ofNullable(address).map(a -> {
            if (address.toUpperCase().contains(Commons.NOT_AVAILABLE)) {
                return Collections.<String>emptySet();
            }
            String location = StringUtils.substringBefore(address, "+");
            return Arrays.stream(location.split("-"))
                    .map((String::trim))
                    .filter(StringUtils::isNotBlank)
                    .collect(toSet());
        }).orElse(Collections.emptySet());
    }

    @Override
    public Set<GeoLocation> findOrCreate(String address) {
        Set<GeoLocation> geoLocations = resolveAddress(address);
        if (geoLocations.isEmpty()) {
            geoLocations = preProcessAddress(address).stream()
                    .map(a -> googleMapsService.geocode(a))
                    .flatMap(Set::stream)
                    .filter(g -> geoLocationRepository.findByPlaceId(g.placeId()) == null)
                    .map(g -> objectMapper.convertValue(g, GeoLocationBo.class))
                    .map(e -> geoLocationRepository.saveAndFlush(e))
                    .map(e -> objectMapper.convertValue(e, GeoLocation.class))
                    .collect(toSet());
        }
        return geoLocations;
    }

    @Override
    public Set<GeoLocation> findNearBy(String location) {
        Set<GeoLocation> geoLocations = findOrCreate(location);
        GeoLocation geoLocation = geoLocations.stream().findFirst()
                .orElseThrow(() -> new RuntimeException("Location not found"));
        Double latitude = geoLocation.latitude();
        Double longitude = geoLocation.longitude();
        if (latitude == null || longitude == null) {
            String fullResult = geoLocation.fullResult();
            try {
                GeocodingResult[] results = objectMapper.readValue(fullResult, new TypeReference<GeocodingResult[]>() {
                });
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
        // TODO: 10/28/2018 Fix fulltext query
//        FullTextEntityManager fullTextEntityManager = Search.getFullTextEntityManager(em);
//        SearchFactory searchFactory = fullTextEntityManager.getSearchFactory();
//        QueryBuilder qb = searchFactory
//                .buildQueryBuilder()
//                .forEntity(GeoLocationBo.class).get();
//        double radius = 20;
//        Query query = qb.spatial()
//                .within(radius, Unit.KM)
//                .ofLatitude(latitude)
//                .andLongitude(longitude)
//                .createQuery();
//        FullTextQuery fullTextQuery = fullTextEntityManager.createFullTextQuery(query, GeoLocationBo.class);
//        List<GeoLocationBo> resultList = fullTextQuery.getResultList();
//        return resultList.stream()
//                .map(e -> objectMapper.convertValue(e, GeoLocation.class))
//                .collect(Collectors.toSet());
        return null;
    }

    @Override
    public Optional<GeoLocation> findByName(String address) {
        // TODO: 12/16/2018 Find a way to have unique geolocations
        List<GeoLocationBo> byName = geoLocationRepository.findByName(address);
        if (byName.isEmpty())
            return Optional.empty();
        return Optional.of(objectMapper.convertValue(byName.get(0), GeoLocation.class));
    }
}

