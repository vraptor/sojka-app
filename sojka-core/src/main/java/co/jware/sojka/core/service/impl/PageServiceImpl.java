package co.jware.sojka.core.service.impl;

import co.jware.sojka.core.domain.search.MetaSearchResult;
import co.jware.sojka.core.service.PageService;
import co.jware.sojka.entities.core.metasearch.PageBo;
import co.jware.sojka.repository.search.PageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service("pageService")
@Transactional
public class PageServiceImpl implements PageService {

    @Autowired
    private PageRepository repository;

    @Override
    public PageBo createPage(MetaSearchResult metaSearchResult) {
        String url = metaSearchResult.uri().toString();
        PageBo found = repository.findByUrl(url);
        PageBo entity =
                Optional.ofNullable(found)
                        .orElseGet(() -> {
                            PageBo newEntity = new PageBo();
                            newEntity.setContent(metaSearchResult.htmlPayload());
                            newEntity.setUrl(url);
                            repository.save(newEntity);
                            return newEntity;
                        });
        return entity;
    }
}
