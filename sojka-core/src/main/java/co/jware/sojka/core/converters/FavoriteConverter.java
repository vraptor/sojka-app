package co.jware.sojka.core.converters;


import co.jware.sojka.core.domain.favorite.Favorite;
import co.jware.sojka.entities.core.FavoriteBo;

import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.core.convert.converter.ConditionalGenericConverter;

import java.util.Collections;
import java.util.Set;

public class FavoriteConverter implements ConditionalGenericConverter {

    public static final Class<FavoriteBo> FAVORITE_ENTITY_CLASS = FavoriteBo.class;

    private final ConversionService conversionService;

    public static final Class<Favorite> FAVORITE_CLASS = Favorite.class;

    public FavoriteConverter(ConversionService conversionService) {
        this.conversionService = conversionService;
    }

    @Override
    public boolean matches(TypeDescriptor sourceType, TypeDescriptor targetType) {
        return (sourceType.getType().isAssignableFrom(FAVORITE_CLASS) &&
                targetType.getType().isAssignableFrom(FAVORITE_ENTITY_CLASS)) ||
                (targetType.getType().isAssignableFrom(FAVORITE_CLASS) &&
                        sourceType.getType().isAssignableFrom(FAVORITE_ENTITY_CLASS));
    }

    @Override
    public Set<ConvertiblePair> getConvertibleTypes() {
        return Collections.singleton(new ConvertiblePair(FAVORITE_CLASS, FAVORITE_ENTITY_CLASS));
    }

    @Override
    public Object convert(Object source, TypeDescriptor sourceType, TypeDescriptor targetType) {
        return null;
    }
}
