package co.jware.sojka.core.domain.pipelines;

import co.jware.sojka.core.domain.JobListing;
import co.jware.sojka.core.domain.party.Candidate;
import org.immutables.value.Value;

@Value.Immutable
public interface AbstractCandidateCard extends Card<Candidate> {
}
