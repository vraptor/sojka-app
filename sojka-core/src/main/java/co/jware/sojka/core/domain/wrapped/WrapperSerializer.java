package co.jware.sojka.core.domain.wrapped;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.IOException;

public class WrapperSerializer<T> extends JsonSerializer<Wrapper<T>> {

    @Override
    public void serialize(Wrapper<T> value, JsonGenerator jg, SerializerProvider serializers) throws IOException {
        T val = value.value();
        jg.writeString(String.valueOf(val));
    }
}
