package co.jware.sojka.core.domain.wrapped;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.immutables.value.Value;

@Value.Immutable
@Wrapped
@JsonSerialize(converter = WrapperConverter.class)
public abstract class _JobSector extends Wrapper<String> {
}
