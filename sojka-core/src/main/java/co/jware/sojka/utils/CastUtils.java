package co.jware.sojka.utils;


import java.util.Optional;

public class CastUtils {
    public static <T> T as(Class<T> clazz, Object obj) {
        return Optional.ofNullable(clazz)
                .map(c -> c.isInstance(obj) ? c.cast(obj) : null)
                .orElse(null);
    }

}
