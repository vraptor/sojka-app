package co.jware.sojka.utils


class Delegator {
    private targetClass
    private delegate

    Delegator(targetClass, delegate) {
        this.targetClass = targetClass
        this.delegate = delegate
    }

    def delegate(methodName) {
        delegate(methodName, methodName)
    }

    def delegate(String methodName, String asMethodName) {
        targetClass.class.metaClass."$asMethodName" = delegate.&"$methodName"
    }

    def delegateAll(String[] names) {
        names.each { name -> delegate(name) }
    }

    def delegateAll(Map names) {
        names.each { k, v -> delegate(k, v) }
    }

    def delegateAll() {
        delegate.class.methods*.name.each {
            name -> delegate(name)
        }
    }
}
