package co.jware.sojka.core.service.entity

import co.jware.sojka.core.domain.*
import co.jware.sojka.core.domain.mapping.DomainMapper
import co.jware.sojka.entities.core.RecommendationBo

class ImmutableHelper {

    static toEntity(def immutable) {
        if (immutable) {
            def mapper = immutable as DomainMapper
            mapper.toEntity()
        }
    }

    static fromEntity(def entity) {
        if (entity) {
            def mapper = entity as DomainMapper
            mapper.fromEntity()
        }
    }

    static toEntity(Recommendation recommendation) {
        def subject = recommendation.subject()
        def subjectEntity = toEntity(subject)
        RecommendationBo recommendationEntity = toEntity(recommendation as Identifiable)
        recommendationEntity.setSubject(subjectEntity);
        recommendationEntity
    }
}
