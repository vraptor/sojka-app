package co.jware.sojka.core.domain.mapping

import co.jware.sojka.core.domain.*
import co.jware.sojka.core.domain.SalaryRange as DomainSalaryRange
import co.jware.sojka.core.domain.wrapped.SkillName
import co.jware.sojka.entities.Persistent
import co.jware.sojka.entities.core.*
import org.apache.commons.lang3.text.WordUtils

import static co.jware.sojka.core.enums.RecommendationType.BOT
import static co.jware.sojka.core.enums.RecommendationType.PERSON

trait ImmutableMapper {

    def mappings = [
            Hirer               : HirerBo,
            Teacher             : TeacherBo,
            Candidate           : CandidateBo,
            Account             : AccountBo,
            User                : UserBo,
            Person              : PersonBo,
            External            : ExternalBo,
            JobListing          : JobListingBo,
            JobResponse         : JobResponseBo,
            BillingInfo         : PaymentInfo,
            PersonRecommendation: RecommendationBo,
            Interview           : InterviewBo,
            SalaryRange         : SalaryRange,
            Company             : CompanyBo
    ]


    static def props = [
            'version': [
                    'optional': true
            ]
    ]
    static def entityMappings = [
            'ResumeEntity'      : [
                    properties: [
                            'skills': [
                                    element: EmbeddedSkill
                            ],
                            'tags'  : [
                                    optional: true
                            ]
                    ],
                    immutable : Resume
            ],
            'EmbeddedEmployment': [
                    immutable: Employment
            ],
            'EmbeddedSkill'     : [
                    immutable: Skill
            ],
            'EmbeddedTag'       : [
                    immutable: Tag
            ],
            'Resume'            : [
                    'skills': [
                            element: Skill
                    ]
            ],
            'HirerEntity'       : [
                    'properties': props,
                    excludes    : ['registered']
            ],
            'JobListingEntity'  : [
                    properties: props
            ],
            'Hirer'             : [
                    'properties': props,
                    excludes    : ['account']
            ],
            'JobListing'        : [
                    properties: props
            ],
            'User'              : [
                    properties: props,
            ],
            'UserEntity'        : [
                    properties: props,
                    excludes  : ['password']
            ],
            'Account'           : [
                    'properties': props
            ],
            'AccountEntity'     : [
                    'properties': props
            ],
            'Candidate'         : [
                    'properties': props
            ],
            'CandidateEntity'   : [
                    'properties': props,
                    excludes    : []//'registered']
            ],
            'Teacher'           : [
                    'properties': props
            ],
            'TeacherEntity'     : [
                    'properties': props,
                    excludes    : ['registered']
            ],
            'JobResponse'       : [
                    'properties': props
            ],
            'JobResponseEntity' : [
                    'properties': props
            ]
    ]
//    static def propMappings = ['Candidate', 'User'].each { s -> [:] << [s: ['properties': props]] }

    def fromEntity() {
        def delegate = this.'$delegate'
        if (delegate instanceof Persistent) {
            def builder = getBuilder(delegate)
            def excludes = entityMappings[delegate.class.simpleName]?.excludes ?: []
            def names = delegate.metaClass.properties*.name - ['class', 'type', 'new', 'id'] - excludes
            names.forEach({ name ->
                if (builder.respondsTo(name)) {
                    def var = delegate."$name"
                    if (var != null) {
                        switch (var.class) {
                            case Persistent:
                                def mapper = var as ImmutableMapper
                                builder."$name"(mapper.fromEntity())

                                break
                            case UUID:
                                builder."$name"(Optional.ofNullable(var))
                                break
                            case Collection:
                                def result = [] as Set
                                var.forEach({ e ->
                                    def targetElementClass = entityMappings[e.class.simpleName]?.immutable as Class
                                    if (targetElementClass)
                                        result << map(e, targetElementClass)
                                })
                                if (result.size()) {
                                    if (isOptional(delegate, name))
                                        result = Optional.ofNullable(result)
                                    builder."$name"(result)
                                }
                                break
                            default:
                                if (isOptional(delegate, name))
                                    var = Optional.ofNullable(var)
                                builder."$name"(var)
                        }
                    }
                }
            })
            return builder.build();
        } else throw new IllegalArgumentException('Unsupported class')
    }

    def toEntity() {
        def delegate = this.'$delegate';
        def entity = createEntity(delegate);
        if (entity) {
            def excludes = entityMappings[entity.class.simpleName]?.excludes ?: []
            def methods = delegate.metaClass.methods.grep({ it.name.startsWith('with') })
            def names = (methods*.name.collect({ it -> WordUtils.uncapitalize(it - 'with') }) as Set) - excludes
            names.each { name ->
                def val = delegate."$name"()
                if (val != null && entity.hasProperty(name)) {
                    def result = null
                    if (val instanceof Identifiable) {
                        def mapper = val as ImmutableMapper
                        result = mapper.toEntity()
                    } else if (val instanceof Optional) {
                        val = val.orElse(null)
                        if (val != null) {
                            if (val instanceof Identifiable) {
                                def mapper = val as ImmutableMapper
                                result = mapper.toEntity()
                            } else if (mappings[val.class.simpleName]) {
                                def mapper = val as ImmutableMapper
                                result = mapper.toEntity()
                            } else {
                                result = val
                            }
                        }
                    } else {
                        result = val
                    }
                    try {
                        entity.setProperty(name, result)
                    } catch (ReadOnlyPropertyException e) {
                    }
                }
            }
            entity
        }
    }

    def createEntity(Identifiable delegate) {
        def clazz = mappings[delegate?.class.simpleName]
        clazz?.newInstance()
    }

    def createEntity(DomainSalaryRange delegate) {
        new SalaryRange()
    }

    boolean isOptional(def delegate, String name) {
        name && entityMappings[delegate?.class.simpleName]?.properties?."${name}"?.optional
    }


    def getBuilder(RecommendationBo entity) {
        switch (entity.getType()) {
            case PERSON:
                return PersonRecommendation.builder()
                break
            case BOT:
                return BotRecommendation.builder()
                break
            default:
                throw new IllegalArgumentException('Unsupported recommendation type')
        }

    }

    def getBuilder(JobListingBo entity) {
        return JobListing.builder()
    }

    def getBuilder(CandidateBo entity) {
        return Candidate.builder()
    }

    def getBuilder(ExternalBo entity) {
        return External.builder()
    }

    def getBuilder(ResumeBo entity) {
        return Resume.builder()
    }

    def getBuilder(HirerBo entity) {
        return Hirer.builder()
    }

    def getBuilder(AccountBo entity) {
        return Account.builder()
    }


    def getBuilder(Persistent persistent) {
        def immutable = 'co.jware.sojka.core.domain' + '.' +
                mappings.find { k, v -> v == persistent.class }?.getKey() as Class
        def builder = immutable?.builder()
        builder
    }

    def map(EmbeddedEmployment embeddedEmployment, Class<Employment> employment) {
        employment.builder()
                .position(embeddedEmployment.position)
                .build()
    }

    def map(EmbeddedSkill embeddedSkill, Class<Skill> skill) {
        return Skill.of(SkillName.of(embeddedSkill.name),
                embeddedSkill.level,
                embeddedSkill.experienceYears)
    }

    def map(EmbeddedTag embeddedTag, Class<Tag> tag) {
        def builder = Tag.builder()
                .name(embeddedTag.name)
                .weight(TagWeight.of(embeddedTag.weight.doubleValue()))
        if (embeddedTag.description?.length())
            builder.description(Optional.ofNullable(embeddedTag.description))
        return builder.build()
    }

    def map(BillingInfo info, Class<PaymentInfo> clazz) {
        def paymentInfo = clazz.newInstance()
    }
}