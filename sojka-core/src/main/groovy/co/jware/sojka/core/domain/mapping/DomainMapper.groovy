package co.jware.sojka.core.domain.mapping

import co.jware.sojka.core.domain.*
import co.jware.sojka.core.domain.party.Candidate
import co.jware.sojka.core.domain.party.External
import co.jware.sojka.core.domain.party.Hirer
import co.jware.sojka.core.domain.party.Teacher
import co.jware.sojka.entities.Persistent
import co.jware.sojka.entities.core.*
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder

import static com.fasterxml.jackson.databind.DeserializationFeature.*
import static com.fasterxml.jackson.databind.SerializationFeature.*

trait DomainMapper {
    def toEntity() {
        def delegate = this.'$delegate';
        toEntity(delegate)
    }

    def fromEntity() {
        def delegate = this.'$delegate';
        fromEntity(delegate)
    }

    def fromEntity(def entity) {
        Class<? extends Identifiable> clazz = immutableClass(entity)
        ObjectMapper mapper = ObjectMapperHolder.mapper() ?: mapper()
        clazz.cast(mapper.convertValue(entity, clazz))
    }

    def toEntity(def object) {
        Class<? extends Persistent> clazz = entityClass(object)
        ObjectMapper mapper = ObjectMapperHolder.mapper() ?: mapper()
        clazz.cast(mapper.convertValue(object, clazz));
    }

    private Class<? extends Identifiable> immutableClass(entity) {
        Class clazz
        def entityClass = entity.class
        switch (entityClass) {
            case UserBo: clazz = User
                break
            case AccountBo: clazz = Account
                break
            case CandidateBo: clazz = Candidate
                break
            case HirerBo: clazz = Hirer
                break
            case TeacherBo: clazz = Teacher
                break
            case ExternalBo: clazz = External
                break
            case InterviewBo: clazz = Interview
                break
            case CompanyBo: clazz = Company
                break
            case JobListingBo: clazz = JobListing
                break
            case JobResponseBo: clazz = JobResponse
                break
            case PersonRecommendationEntity: clazz = PersonRecommendation
                break
            case ResumeBo: clazz = Resume
                break

            default:
                throw new IllegalArgumentException("${entityClass} is not supported")
        }
        return clazz
    }

    private ObjectMapper mapper() {
        return Jackson2ObjectMapperBuilder.json()
                .modulesToInstall(Jdk8Module, JavaTimeModule)
                .serializationInclusion(JsonInclude.Include.NON_EMPTY)
                .featuresToDisable(FAIL_ON_UNKNOWN_PROPERTIES, WRITE_DATES_AS_TIMESTAMPS)
                .build()
    }

    private Class<? extends Persistent> entityClass(object) {
        Class clazz
        def objectClass = object.class
        switch (objectClass) {
            case User: clazz = UserBo
                break
            case Account: clazz = AccountBo
                break
            case Candidate: clazz = CandidateBo
                break
            case Hirer: clazz = HirerBo
                break
            case Teacher: clazz = TeacherBo
                break
            case External: clazz = ExternalBo
                break
            case Interview: clazz = InterviewBo
                break
            case Company: clazz = CompanyBo
                break
            case JobListing: clazz = JobListingBo
                break
            case JobResponse: clazz = JobResponseBo
                break
            case Recommendation: clazz = RecommendationBo
                break
            case Resume: clazz = ResumeBo
                break
            default:
                throw new IllegalArgumentException("${objectClass} is not supported")
        }
        return clazz
    }
}
