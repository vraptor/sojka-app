package co.jware.sojka.core.domain

import co.jware.sojka.core.enums.BankAccountType
import groovy.transform.Immutable

@Immutable(copyWith = true)
//@TupleConstructor
class BillingInfo {

    String accountNo

    String bankName

    BankAccountType type


}
