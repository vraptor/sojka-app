package co.jware.sojka.core.service.fulltext;

import java.util.Arrays;

import com.ibm.icu.text.Transliterator;

import static java.util.stream.Collectors.joining;

public class Transliteration {
    public String kebabCase(String text) {
        if (null != text) {
            String result = text.toLowerCase();
            String[] parts = result.split("\\s+");
            result = Arrays.stream(parts).collect(joining("-"));
            return result;
        }
        return text;
    }

}
