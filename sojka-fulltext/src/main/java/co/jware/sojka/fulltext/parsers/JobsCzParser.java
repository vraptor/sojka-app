package co.jware.sojka.fulltext.parsers;


import org.apache.tika.config.TikaConfig;
import org.apache.tika.exception.TikaException;
import org.apache.tika.io.TikaInputStream;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.parser.AutoDetectParser;
import org.apache.tika.parser.ParseContext;
import org.apache.tika.parser.Parser;
import org.apache.tika.sax.ToXMLContentHandler;
import org.apache.tika.sax.XHTMLContentHandler;
import org.apache.tika.sax.xpath.Matcher;
import org.apache.tika.sax.xpath.MatchingContentHandler;
import org.apache.tika.sax.xpath.XPathParser;
import org.xml.sax.ContentHandler;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

class JobsCzParser {
    public void parseAd(URL url) {
        XPathParser xPathParser = new XPathParser("xhtml", XHTMLContentHandler.XHTML);
        Matcher h3Matcher = xPathParser.parse("/xhtml:html/xhtml:body/descendant::node()");
        try (InputStream is = url.openStream();) {
            TikaConfig tikaConfig = TikaConfig.getDefaultConfig();
            Parser parser = new AutoDetectParser(tikaConfig);
            ToXMLContentHandler toXMLContentHandler = new ToXMLContentHandler();
            ContentHandler handler = new MatchingContentHandler(toXMLContentHandler, h3Matcher);
//                    new BodyContentHandler();
            Metadata metadata = new Metadata();
            parser.parse(TikaInputStream.get(is), handler, metadata, new ParseContext());
            String xml = handler.toString();
//            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
//            Document doc = factory.newDocumentBuilder().parse(is);
            System.out.println(xml);
        } catch (IOException | SAXException | TikaException e) {
            e.printStackTrace();
        }
    }
}