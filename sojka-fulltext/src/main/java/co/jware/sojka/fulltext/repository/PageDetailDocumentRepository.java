package co.jware.sojka.fulltext.repository;

import co.jware.sojka.fulltext.domain.PageDetailDocument;
import org.springframework.data.elasticsearch.repository.ElasticsearchCrudRepository;

import java.util.UUID;

public interface PageDetailDocumentRepository extends ElasticsearchCrudRepository<PageDetailDocument, UUID> {
}
