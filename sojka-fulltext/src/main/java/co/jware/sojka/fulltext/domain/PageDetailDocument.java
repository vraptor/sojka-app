package co.jware.sojka.fulltext.domain;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.Mapping;

import java.util.UUID;

@Getter
@Setter
@Document(indexName = "sojka_page_detail", replicas = 0, type = "page_detail",refreshInterval = "45s")
@Mapping(mappingPath = "mappings/page-detail.json")
public class PageDetailDocument {
    @Id
    private UUID uuid;
    @Field()
    private String lang;
}
