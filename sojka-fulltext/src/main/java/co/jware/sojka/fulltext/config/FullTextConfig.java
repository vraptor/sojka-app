package co.jware.sojka.fulltext.config;

import org.elasticsearch.client.Client;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.elasticsearch.core.ElasticsearchOperations;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.data.elasticsearch.repository.config.EnableElasticsearchRepositories;

@Configuration
@EnableElasticsearchRepositories(basePackages = "co.jware.sojka.fulltext.repository")
public class FullTextConfig {
    @Bean
    public ElasticsearchOperations elasticsearchOperations(Client client) {
        ElasticsearchTemplate template = new ElasticsearchTemplate(client);
        return template;
    }
}
