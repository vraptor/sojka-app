package co.jware.sojka.fulltext.repository;

import co.jware.sojka.fulltext.domain.MetaLinkDocument;
import org.springframework.data.elasticsearch.repository.ElasticsearchCrudRepository;

import java.util.UUID;

public interface MetaLinkDocumentRepository extends ElasticsearchCrudRepository<MetaLinkDocument, UUID> {
}
