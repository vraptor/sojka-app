package co.jware.sojka.fulltext.domain;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;

import java.util.UUID;

@Getter
@Setter
@Document(indexName = "sojka_meta_link", replicas = 0, type = "meta_link")
public class MetaLinkDocument {
    @Id
    private UUID uuid;
}
