package co.jware.sojka.core.service.fulltext;

import com.ibm.icu.text.Transliterator;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.text.WordUtils;
import org.junit.Test;

import java.util.Enumeration;

import static org.assertj.core.api.Assertions.assertThat;

public class TransliterationTest {

    Transliteration transliteration = new Transliteration();

    @Test
    public void transliterateIcu() throws Exception {
        Transliterator transliterator = Transliterator.getInstance("Latin-ASCII");
        String transformed = transliterator.transliterate("Kakaová buchta z tuku na pečení");
        assertThat(transformed).isEqualToIgnoringCase("Kakaova buchta z tuku na peceni");
        transformed = transliterator.transliterate("Křehké sýrové krekry");
        assertThat(transformed).isEqualToIgnoringCase("Krehke syrove krekry");
    }

    @Test
    public void transliterateStringUtils() throws Exception {
        String transformed = StringUtils.stripAccents("Kakaová buchta z tuku na pečení");
        assertThat(transformed).isEqualToIgnoringCase("Kakaova buchta z tuku na peceni");
        transformed = StringUtils.stripAccents("Křehké sýrové krekry");
        assertThat(transformed).isEqualToIgnoringCase("Krehke syrove krekry");
    }

    @Test
    public void kebabCase() throws Exception {
        String transformed = transliteration.kebabCase("Kakaova buchta z tuku na peceni");
        assertThat(transformed).isEqualTo("kakaova-buchta-z-tuku-na-peceni");
    }

    @Test
    public void kebabCase_null() throws Exception {
        String transformed = transliteration.kebabCase(null);
        assertThat(transformed).isNull();
    }

    @Test
    public void kebabCase_simple() throws Exception {
        String transformed = transliteration.kebabCase("single");
        assertThat(transformed).isEqualTo("single");
    }
}