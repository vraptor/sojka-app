package co.jware.sojka.core.service.fulltext;

import com.google.common.base.Optional;
import com.optimaize.langdetect.i18n.LdLocale;
import org.junit.Test;

import static org.assertj.core.api.Java6Assertions.assertThat;

public class LangDetectTest {
    @Test
    public void detect() throws Exception {
        Optional<LdLocale> optional = LangDetect.detect("Kakaová buchta z tuku na pečení");
        assertThat(optional.isPresent()).isTrue();
        String language = optional.get().getLanguage();
        assertThat(language).isEqualTo("cs");
    }


}