package co.jware.sojka.bot

import io.vertx.core.Vertx
import io.vertx.ext.web.Router

class ResumeHandler {
    static void main(def args) {
        def vertx = Vertx.vertx()
        def server = vertx.createHttpServer()
        def router = Router.router(vertx)
        router.route().handler({ routerContext ->
            def response = routerContext.response()
            response.putHeader('Content-Type', 'text/plain')
            response.end("Router: Sojka Flies")
        })
        server.requestHandler(router.&accept).listen(8008)
    }
}
