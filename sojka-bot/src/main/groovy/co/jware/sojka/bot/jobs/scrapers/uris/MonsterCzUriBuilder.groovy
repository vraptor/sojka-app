package co.jware.sojka.bot.jobs.scrapers.uris

import co.jware.sojka.core.domain.search.MetaSearch
import org.springframework.stereotype.Component
import org.springframework.web.util.UriComponentsBuilder

import static co.jware.sojka.bot.jobs.scrapers.SiteInfo.MONSTER_CZ

@Component
class MonsterCzUriBuilder extends BaseUriBuilder {
    private UriComponentsBuilder builder = UriComponentsBuilder
            .fromUri(baseUri())
            .path('prace/hledat/')
            .queryParam('cy', 'cz')

    @Override
    List<URI> buildUris(MetaSearch search) {
        def cloneBuilder = builder.cloneBuilder()
        filterKeyWords(search).forEach({ k -> cloneBuilder.queryParam('q', k) })
        def page = search.page()
        if (page > 1) {
            cloneBuilder.queryParam("page", page)
        }
        return [cloneBuilder.build().encode().toUri()];
    }

    @Override
    def URI baseUri() {
        return MONSTER_CZ.baseUri()
    }
}
