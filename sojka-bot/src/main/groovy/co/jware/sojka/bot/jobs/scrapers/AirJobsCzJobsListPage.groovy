package co.jware.sojka.bot.jobs.scrapers

import geb.Page


class AirJobsCzJobsListPage extends Page {
    static url = '/web/candidate/nalezene-nabidky'
    static content = {
        panels(wait: true) { $('div.announcementLayout') }
    }
}
