package co.jware.sojka.bot.jobs.scrapers

import co.jware.sojka.core.domain.JobListing
import org.springframework.stereotype.Component

//@Component
class StartupJobsCzScraper extends BaseScraper {

    def baseUrl = 'https://www.startupjobs.cz'
    def listUrl = baseUrl + '/nabidky'

    @Override
    List<JobListing> collectJobs() {
        []
    }
}
