package co.jware.sojka.bot.jobs.scrapers

import co.jware.sojka.core.domain.Company
import co.jware.sojka.core.domain.Hirer
import co.jware.sojka.core.domain.Salary
import co.jware.sojka.core.domain.SalaryRange
import co.jware.sojka.core.domain.party.Hirer


class JobScraperHelper {

    Hirer buildCompanyOwner(companyName) {
        Company company = Company.builder().name(companyName).build("", "", "", null)
        Hirer owner = Hirer.builder()
                .email('dummy@void.net')
                .firstName('john')
                .lastName('doe')
                .company(company)
                .build()
        return owner
    }

    Salary parseSalary(String salary) {
        def toParse = salary ?: ''
        def amounts = (toParse).findAll(/\d+\s*\d+/)
        amounts = amounts*.replaceAll(/\D+/, '')

        BigDecimal from = BigDecimal.ZERO
        BigDecimal to = BigDecimal.ZERO
        if (amounts.size() == 2) {
            from = amounts[0] as BigDecimal
            to = amounts[1] as BigDecimal
        } else if (amounts.size == 1) {
            from = to = amounts[0] as BigDecimal
        }
        Currency czk = Currency.getInstance('CZK')
        def salaryRange = SalaryRange.of(from, to)
        Salary.builder().range(salaryRange).currency(czk).build()
    }
}
