package co.jware.sojka.bot.jobs.scrapers.links

import co.jware.sojka.bot.jobs.scrapers.SiteInfo
import co.jware.sojka.core.domain.search.MetaLink
import org.jsoup.nodes.Document
import org.springframework.stereotype.Component
import org.springframework.web.util.UriComponentsBuilder

import java.util.stream.Collectors

@Component
class MonsterCzLinksExtractor implements Extractor {
    @Override
    List<MetaLink> extractLinks(Document doc, String baseUri) {
        return doc.select('article.js_result_row').stream()
                .map({ d ->
            def titleDiv = d.select('div.jobTitle')
            def link = titleDiv?.select('a')
            def title = link?.text()
            def href = link?.attr('href')
            href = UriComponentsBuilder.fromHttpUrl(href)
                    .replaceQueryParam("jobPosition")
                    .replaceQueryParam("mescoid")
                    .build(true).toUriString()
            def divCompany = d.select('div.company')
            def company = divCompany?.text()
            company = company.replaceFirst('Zdroj:', '')
                    .trim()
            def addressDiv = d.select('div.location')
            def address = addressDiv?.text()
            emptyBuilder()
                    .baseUri(baseUri)
                    .title(title)
                    .href(href)
                    .company(company)
                    .address(address)
                    .build()
        }).collect(Collectors.toList())
    }

    @Override
    String extractorKey() {
        return SiteInfo.MONSTER_CZ.baseUri().toString();
    }
}
