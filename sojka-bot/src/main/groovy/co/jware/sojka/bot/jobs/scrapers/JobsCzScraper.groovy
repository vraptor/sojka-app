package co.jware.sojka.bot.jobs.scrapers

import co.jware.sojka.core.domain.JobListing
import co.jware.sojka.core.domain.Location
import co.jware.sojka.core.domain.SalaryRange
import org.jsoup.Jsoup
import org.jsoup.nodes.Element

import java.time.OffsetDateTime
import java.time.format.DateTimeFormatter

import static co.jware.sojka.core.enums.JobListingOrigin.EXTERNAL
import static co.jware.sojka.core.enums.JobListingStatus.ACTIVE

//@Component
class JobsCzScraper extends BaseScraper {
    private JobScraperHelper helper = new JobScraperHelper()
    private dateTimeFormatter = DateTimeFormatter.ISO_OFFSET_DATE_TIME

    def baseUrl = 'http://jobs.cz'
    def listUrl = baseUrl + '/prace'

    List<JobListing> collectJobs() {
        def jobs = []

        def doc = Jsoup.connect(listUrl).get()
        def items = doc.select('.standalone.search-list__item')
        items.each { item ->
            def link = item.select('.search-list__main-info__title__link')
            if (link.size()) {
                def title = link.text()
                def href = baseUrl + link.attr('href')
                def salary = processSalary(item)
                def company = item.select('.search-list__main-info__company').text()
                def location = processLocation(item)
                def status = item.select('.search-list__status__data').select('span')
                if (status.size() && status.get(0).childNodeSize() > 1) {
                    status = status.get(0).child(1)
                }
                def validFrom = status?.attr('data-label-added-valid-from')
                def validTo = status?.attr('data-label-added-valid-to')
                def isUpdated = status?.attr('data-label-added-is-updated')

                def jobMap = [
                        title  : title,
                        name   : title,
                        link   : href,
                        company: company,
                        updated: isUpdated == 1]
                if (salary)
                    jobMap.salary = salary
                if (location)
                    jobMap.location = location
                if (validFrom)
                    jobMap.validFrom = validFrom
                if (validTo)
                    jobMap.validTo = validTo
                jobs << jobMap
            }
            link
        }
        jobs = jobs.collect { job ->
            def builder = JobListing.builder()
            builder.origin(EXTERNAL)
                    .status(ACTIVE)
                    .title(job.title)
                    .link(job.link)
                    .name(job.name)
                    .owner(helper.buildCompanyOwner(job.company))
            if (job.validFrom) {
                builder.validFrom(parseDate(job.validFrom))
            }
            if (job.validTo) {
                builder.validTo(parseDate(job.validTo))
            }
            if (job.salary) {
                SalaryRange salaryRange = helper.parseSalaryRange(job.salary)
                if (salaryRange.isFull()) {
                    builder.salaryRange(Optional.of(salaryRange))
                }
            }
            if (job.location) {
                def jobLocation = buildLocation(job)
                builder.location(jobLocation);
            }
            builder.build()
        }
        jobs
    }

    def buildLocation(job) {
        def loc = job.location
        loc = loc.split(/\+/)[0]
        def parts = loc.split(/\u2013/)
        parts = parts*.trim()
        def location = Location.builder()
                .country('CZ')
                .region(parts.size() == 2 ? parts[1] : '-')
                .city(parts[0])
                .build("", "", "", null)
        location
    }


    private OffsetDateTime parseDate(String from) {
        return OffsetDateTime.parse(from, dateTimeFormatter)
    }

    static def processLocation(Element item) {
        def location = item.select('.search-list__main-info__address')
                .select('span')
        (location.size() == 2) ? location.get(1).text() : ''
    }

    static def processSalary(Element item) {
        def salary = item.select('.search-list__salary')?.select('span')
        (salary.size() == 2) ? salary = salary.get(1).text() : ''
    }
}
