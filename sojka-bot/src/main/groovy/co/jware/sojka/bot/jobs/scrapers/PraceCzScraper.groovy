package co.jware.sojka.bot.jobs.scrapers

import co.jware.sojka.core.domain.JobListing
import co.jware.sojka.core.domain.Location
import org.jsoup.Jsoup
import org.jsoup.select.Elements

import static co.jware.sojka.core.enums.JobListingOrigin.EXTERNAL
import static co.jware.sojka.core.enums.JobListingStatus.ACTIVE

//@Component
class PraceCzScraper extends BaseScraper {

    private JobScraperHelper helper = new JobScraperHelper()

    def baseUrl = 'https://www.prace.cz'

    @Override
    List<JobListing> collectJobs() {
        def jobs = []
        def doc = Jsoup.connect(baseUrl + '/nabidky')
                .execute().parse()

        Elements items = doc.select('li.search-result__advert')
        items.each { item ->
            def link = item.select('a.link')

            if (link) {
                def locationDiv = item.select('.search-result__advert__box__item--location')
                def companyDiv = item.select('.search-result__advert__box__item--company')
                def salaryDiv = item.select('.search-result__advert__box__item--salary')
                def href = link.attr('href')
                def title = link.text()
                def location = locationDiv.text()
                def company = (companyDiv?.text() ?: '')
                def m = company =~ /\b(.*)$/
                company = m.find() ? m.group() : company
                def jobMap = [
                        link    : href,
                        title   : title,
                        name    : title,
                        company : company,
                        location: location,

                ]
                if (salaryDiv) {
                    jobMap += [salaryRange: salaryDiv.text()]
                }
                jobs << jobMap
            }
        }
        jobs.collect {
            job ->
                def builder = JobListing.builder()
                builder.origin(EXTERNAL)
                        .status(ACTIVE)
                        .link(job.link)
                        .title(job.title)
                        .name(job.name)
                        .owner(helper.buildCompanyOwner(job.company))
                        .location(buildLocation(job))
                def salaryRange = helper.parseSalaryRange(job.salaryRange)
                if (salaryRange.isFull()) {
                    builder.salaryRange(salaryRange)
                }
                builder.build()
        }
    }

    def buildLocation(job) {
        def loc = job.location
        loc = loc.split(/\+/)[0]
        def parts = loc.split(/\-/)
        parts = parts*.trim()
        def location = Location.builder()
                .country('CZ')
                .region(parts.size() == 2 ? parts[1] : '-')
                .city(parts[0])
                .build("", "", "", null)
        location
    }
}
