package co.jware.sojka.bot.jobs.scrapers

import geb.Page


class PraceCzJobsListPage extends Page {
    static url = '/nabidky'
    static content = {
        items(wait: true) { $($('li.search-result__advert')) }
    }
}
