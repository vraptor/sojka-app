package co.jware.sojka.bot.jobs.scrapers

import co.jware.sojka.core.domain.JobListing
import co.jware.sojka.core.domain.Location
import geb.Browser
import geb.Configuration
import org.jsoup.Jsoup
import org.jsoup.nodes.Document
import org.openqa.selenium.Dimension
import org.openqa.selenium.remote.DesiredCapabilities
import org.openqa.selenium.remote.RemoteWebDriver
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Component

import static co.jware.sojka.core.enums.JobListingOrigin.EXTERNAL
import static co.jware.sojka.core.enums.JobListingStatus.ACTIVE

//@Component
class AirJobsCzScraper extends BaseScraper {
    private JobScraperHelper helper = new JobScraperHelper()
    @Value('${phantomjs.url}')
    def phantomJsUrl
    @Value('${scrapers.airjobs_cz.enabled:false}')
    def enabled
    def baseUrl = 'https://www.airjobs.cz'
    def url
    def driver = {
        try {

            url = new URL(phantomJsUrl)
        } catch (MalformedURLException e) {
            enabled = false;
        }
        def driver = new RemoteWebDriver(url, DesiredCapabilities.phantomjs())
        driver.manage().window().setSize(new Dimension(1920, 1080))
        driver
    }
    def config = new Configuration([
            driver : driver,
            baseUrl: baseUrl
    ])

    @Override
    List<JobListing> collectJobs() {

        def jobs = []
        if (enabled) {
            def browser = Browser.drive(config) {
                to AirJobsCzJobsListPage
                assert title == 'Nalezené nabídky podle zadaných parametrů | AirJobs .cz'
                panels.contextElements.each { panel ->
                    def html = panel.getAttribute('innerHTML')
                    Document doc = Jsoup.parse(html, baseUrl)
                    def centerPanel = doc.select('.center-panel')
                    def centerLink = centerPanel.select('a')
                    def href = centerLink.attr('href')
                    def title = centerLink.text()
                    def companyLink = centerPanel.select('div.subHorizontalLayout a span').first()
                    assert companyLink
                    def right = doc.select('.right-panel')
                    assert right
                    def city = right.select('.city').first()
                    assert city
                    def wageRange = right.select('.wageRange').first()
                    assert wageRange
                    href = (href =~ /\#\w+$/).replaceFirst('')
                    def jobMap = [
                            link       : baseUrl + href,
                            title      : title,
                            name       : title,
                            company    : companyLink.text(),
                            location   : city.text(),
                            salaryRange: wageRange.text()
                    ]
                    jobs << jobMap
                }
            }
            jobs.collect { job ->
                def builder = JobListing.builder()
                builder.origin(EXTERNAL)
                        .status(ACTIVE)
                        .link(job.link)
                        .title(job.title)
                        .name(job.name)
                        .owner(helper.buildCompanyOwner(job.company))
                        .location(buildLocation(job))
                def salaryRange = helper.parseSalaryRange(job.salaryRange)
                if (salaryRange.isFull()) {
                    builder.salaryRange(salaryRange)
                }
                builder.build()
            }
        }

    }

    def buildLocation(job) {
        Location.builder()
                .city(job.location ?: '-')
                .country('CZ')
                .region('-')
                .build()
    }
}
