package co.jware.sojka.resources.search;


import co.jware.sojka.core.domain.search.MetaLink;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.Resource;

public class MetaLinkResource extends Resource<MetaLink> {


    public MetaLinkResource(MetaLink content, Link... links) {
        super(content, links);
    }

    public MetaLinkResource(MetaLink content, Iterable<Link> links) {
        super(content, links);
    }
}
