package co.jware.sojka.resources.assemblers;


import co.jware.sojka.bot.jobs.rest.api.controllers.MetaLinkController;
import co.jware.sojka.core.domain.search.MetaLink;
import co.jware.sojka.resources.search.MetaLinkResource;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;

public class MetaLinkResourceAssembler extends ResourceAssemblerSupport<MetaLink, MetaLinkResource> {

    public MetaLinkResourceAssembler() {
        super(MetaLinkController.class, MetaLinkResource.class);
    }

    @Override
    public MetaLinkResource toResource(MetaLink entity) {
        return createResourceWithId(entity.uuid(), entity);
    }
}
