package co.jware.sojka.jobs;

import co.jware.sojka.bot.service.NominatimService;
import co.jware.sojka.core.domain.search.MetaLink;
import co.jware.sojka.core.service.MetaLinkService;
import lombok.extern.slf4j.Slf4j;
import org.quartz.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.scheduling.quartz.QuartzJobBean;

import java.sql.Date;
import java.time.OffsetDateTime;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;

@Slf4j
public class GeoLocationJob extends QuartzJobBean {

    private final MetaLinkService metaLinkService;
    private final NominatimService nominatimService;
    private final Scheduler scheduler;

    public GeoLocationJob(MetaLinkService metaLinkService, NominatimService nominatimService, Scheduler scheduler) {
        this.metaLinkService = metaLinkService;
        this.nominatimService = nominatimService;
        this.scheduler = scheduler;
    }

    @Override
    public void executeInternal(JobExecutionContext context) throws JobExecutionException {
        AtomicInteger i = new AtomicInteger();
        Page<MetaLink> page = metaLinkService.findUnresolvedGeoInfo(PageRequest.of(0, 50,
                Sort.Direction.ASC, "created"));
        page.forEach(metaLink -> {
            UUID uuid = metaLink.uuid();
            JobDataMap jobDataMap = new JobDataMap();
            jobDataMap.put("metaLinkUuid", uuid);
            try {
                JobDetail jobDetail = JobBuilder.newJob(NominatimQueryJob.class)
                        .withIdentity(UUID.randomUUID().toString(), "Nominatim job")
                        .usingJobData(jobDataMap)
                        .storeDurably()
                        .build();

                Trigger trigger = TriggerBuilder.newTrigger()
                        .forJob(jobDetail)
                        .withIdentity(jobDetail.getKey().getName())
                        .startAt(Date.from(OffsetDateTime.now().plusSeconds(2).toInstant()))
                        .withSchedule(SimpleScheduleBuilder.simpleSchedule()
                                .withMisfireHandlingInstructionNextWithExistingCount())
                        .build();
                scheduler.scheduleJob(jobDetail, trigger);
            } catch (SchedulerException e) {
                log.error("Error in nominatim service", e);
            }
        });
    }
}
