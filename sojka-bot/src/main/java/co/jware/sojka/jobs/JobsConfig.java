package co.jware.sojka.jobs;

import org.quartz.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class JobsConfig {
    @Bean
    public JobDetail geoLocationJobDetail() {
        return JobBuilder.newJob(GeoLocationJob.class)
                .withIdentity("geolocations-job")
                .storeDurably()
                .build();
    }

    @Bean
    public Trigger geolocationJobTrigger() {
        return TriggerBuilder.newTrigger()
                .forJob("geolocations-job")
                .withSchedule(SimpleScheduleBuilder
                        .repeatSecondlyForever(15)
                        .withMisfireHandlingInstructionNextWithExistingCount())
                .build();
    }
}
