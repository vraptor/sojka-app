package co.jware.sojka.jobs;

import co.jware.sojka.bot.core.OsmResponse;
import co.jware.sojka.bot.service.NominatimService;
import co.jware.sojka.core.domain.geo.GeoLocation;
import co.jware.sojka.core.domain.geo.GeoLocationAddress;
import co.jware.sojka.core.domain.search.MetaLink;
import co.jware.sojka.core.service.MetaLinkService;
import co.jware.sojka.core.service.geo.GeoLocationService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang3.StringUtils;
import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Comparator;
import java.util.Optional;
import java.util.UUID;

public class NominatimQueryJob implements Job {

    @Autowired
    private MetaLinkService metaLinkService;
    @Autowired
    private NominatimService nominatimService;
    private final GeoLocationService geoLocationService;
    private final ObjectMapper objectMapper;

    public NominatimQueryJob(GeoLocationService geoLocationService, ObjectMapper objectMapper) {
        this.geoLocationService = geoLocationService;
        this.objectMapper = objectMapper;
    }

    public void execute(JobExecutionContext context) {
        JobDataMap jobDataMap = context.getJobDetail().getJobDataMap();
        UUID uuid = (UUID) jobDataMap.get("metaLinkUuid");
        MetaLink metaLink = metaLinkService.getMetaLink(uuid);
        String address = metaLink.address();
        if (StringUtils.isBlank(address)) {
            return;
        }
        String[] addresses = address.split(",");
        address = Optional.ofNullable(addresses[0]).orElse(address);
        String finalAddress = address;
        GeoLocation result = geoLocationService.findByName(address)
                .orElseGet(() -> {
                    try {
                        return this.nominatimService.search(finalAddress).stream()
                                .max(Comparator.comparing(OsmResponse::importance))
                                .map(osmResponse -> extractGeoLocation(finalAddress, osmResponse))
                                .orElseGet(() -> GeoLocation.builder().name(finalAddress).build());

                    } catch (Exception e) {
                        throw new RuntimeException(e);
                    }
                });
        MetaLink link = metaLink.withGeoLocations(result);
        metaLinkService.saveLink(link);
    }

    private GeoLocation extractGeoLocation(String address, OsmResponse osmResponse) {
        String asString = null;
        try {
            asString = objectMapper.writeValueAsString(osmResponse);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
        GeoLocation geoLocation = objectMapper.convertValue(osmResponse, GeoLocation.Builder.class)
                .name(address)
                .fullResult(asString)
//                .longitude(osmResponse.longitude())
//                .latitude(osmResponse.latitude())
                // TODO: 12/6/2018 Introduce osmId
                .placeId(osmResponse.osmId().toString())
                .build();
        GeoLocationAddress geoLocationAddress = objectMapper.convertValue(osmResponse.address(), GeoLocationAddress.class);
        return geoLocation.withAddress(geoLocationAddress);
    }
}
