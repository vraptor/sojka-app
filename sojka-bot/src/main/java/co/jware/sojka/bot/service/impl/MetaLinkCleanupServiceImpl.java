package co.jware.sojka.bot.service.impl;


import co.jware.sojka.core.service.MetaLinkService;
import co.jware.sojka.entities.core.metasearch.MetaLinkBo;
import co.jware.sojka.repository.search.MetaLinkRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.reactivex.Flowable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.OffsetDateTime;
import java.util.concurrent.TimeUnit;

@Service("metaLinkCleanupService")
@Transactional
public class MetaLinkCleanupServiceImpl implements ApplicationListener<ContextRefreshedEvent> {
    private final static Logger logger = LoggerFactory.getLogger(MetaLinkCleanupServiceImpl.class);

    @Value("${system.batch.delete.initial_page:0}")
    private int initialPage;
    @Value("${system.batch.delete.count:20}")
    private int deleteCount;
    @Value("${system.batch.delete.period:120}")
    private int deletePeriod;
    @Value("${system.batch.delete.page_size:20}")
    private int pageSize;
    @Value("${system.batch.delete.days_to_keep:14}")
    private int daysToKeep;
    @Autowired
    private MetaLinkRepository metaLinkRepository;
    @Autowired
    private MetaLinkService metaLinkService;
    @Autowired
    private ObjectMapper mapper;

    @Override
    @Transactional
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {
        PageRequest page = new PageRequest(initialPage, pageSize,
                Sort.Direction.ASC, "created");
        Flowable.interval(deletePeriod, TimeUnit.SECONDS)
                .forEach(aLong -> {
                    OffsetDateTime cuttoffDateTime = OffsetDateTime.now().minusDays(daysToKeep);
                    Page<MetaLinkBo> entities = metaLinkRepository.findAllByCreatedBefore(cuttoffDateTime, page);
                    do {
                        metaLinkRepository.deleteAll(entities);
                        logger.info("Deleted {} meta links", entities.getNumberOfElements());
                        entities = metaLinkRepository.findAllByCreatedBefore(cuttoffDateTime, entities.nextPageable());
                    }
                    while (entities.hasNext());
                });
    }
}
