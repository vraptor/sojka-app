package co.jware.sojka.bot.jobs.rest.api.service;


import co.jware.sojka.core.domain.search.MetaLink;
import co.jware.sojka.core.domain.search.MetaLinksFilter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.UUID;

public interface MetaLinkApiSvc {

    MetaLink getMetaLink(UUID uuid);

    Page<MetaLink> getLatest(Pageable pageable);

    Page<MetaLink> filterLinks(MetaLinksFilter filter, Pageable pageable);
}
