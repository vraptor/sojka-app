package co.jware.sojka.bot.jobs.scrapers.pages.impl;

import co.jware.sojka.bot.jobs.scrapers.SiteInfo;
import co.jware.sojka.core.domain.search.page.PageMetaData;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;
import org.jsoup.safety.Whitelist;
import org.jsoup.select.Elements;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

import static co.jware.sojka.core.domain.search.Commons.NOT_IMPLEMENTED;

@Component
public class JobsCzPageDetailExtractor extends BasePageDetailExtractor {

    public JobsCzPageDetailExtractor() {
        super();
        siteInfo = SiteInfo.JOBS_CZ;
    }

    @Override
    public String extractRichText(Document doc) {
        String richText = NOT_IMPLEMENTED;
        Elements jobadBody = doc.select("div.standalone.jobad__body");
        if (jobadBody.hasText()) {
            richText = jobadBody.text();
        }
        Elements g2Container = doc.select("div#g2-cont");
        if (g2Container.hasText()) {
            richText = g2Container.text();
        }
        Elements cseContainer = doc.select(".cse-detail-wrap");
        if (cseContainer.hasText()) {
            String clean = Jsoup.clean(cseContainer.html(), Whitelist.relaxed());
            richText = Jsoup.parse(clean).text();
        }

        if (richText.isEmpty()) {
            richText = NOT_IMPLEMENTED;
        }

        return richText;
    }

    @Override
    public PageMetaData extractMetaData(Document doc) {
        Elements info = doc.select("div:has(dl).standalone");
        PageMetaData result = this.emptyPageMetaData;
        if (info.size() > 0) {
            List<Pair> pairs = new ArrayList<>();
            Element dl = info.select("dl").get(0);
            Elements dtElements = dl.select("dt");
            dtElements.forEach(e -> {
                Node node = e.nextSibling();
                if (node instanceof Element) {
                    Element element = (Element) node;
                    if (element.is("dd")) {
                        String dt = e.text();
                        String dd = element.text();
                        if (StringUtils.isBlank(dt)) {
                            if (dd.indexOf(':') != -1) {
                                String[] pair = dd.split(":");
                                dt = pair[0];
                                dd = pair[1];
                            }
                        }
                        Pair<String, String> pair = Pair.of(dt, dd);
                        pairs.add(pair);
                    }
                }
            });
            String rawData = null;
            try {
                rawData = mapper.writeValueAsString(pairs);
                result = result.withRawData(rawData);
            } catch (JsonProcessingException ignored) {
            }
        }
        return result.withLocale(pageLocale(doc));
    }
}
