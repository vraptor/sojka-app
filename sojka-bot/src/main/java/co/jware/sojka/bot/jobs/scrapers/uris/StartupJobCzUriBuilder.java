package co.jware.sojka.bot.jobs.scrapers.uris;

import co.jware.sojka.bot.jobs.scrapers.SiteInfo;
import co.jware.sojka.core.domain.search.MetaSearch;
import org.springframework.stereotype.Component;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toSet;

//@Component
public class StartupJobCzUriBuilder extends BaseUriBuilder {
    @Override
    public List<URI> buildUris(MetaSearch search) {
        UriComponentsBuilder builder = UriComponentsBuilder
                .fromUri(baseUri())
                .path("nabidky");
        Set<String> keywords = filterKeyWords(search).collect(toSet());
        if (!keywords.isEmpty()) {
            builder.queryParam("superinput", filterKeyWords(search).collect(joining("+")));
        }
        int page = search.page();
        if (page > 1) {
            builder.path("/strana-" + page);
        }
        URI uri = builder
                .build().encode()
                .toUri();
        return Collections.singletonList(uri);
    }

    @Override
    URI baseUri() {
        throw new UnsupportedOperationException("Not implemented");
    }

//    @Override
//    public boolean active() {
//        return false;
//    }

//    @Override
//    URI baseUri() {
//        return SiteInfo.STARTUPJOBS_CZ.baseUri();
//    }
}
