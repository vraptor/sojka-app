package co.jware.sojka.bot.jobs.rest.api.service.impl;


import co.jware.sojka.bot.jobs.rest.api.service.MetaSearchApiSvc;
import co.jware.sojka.bot.jobs.scrapers.executors.MetaSearchExecutor;
import co.jware.sojka.core.Events;
import co.jware.sojka.core.domain.search.MetaLink;
import co.jware.sojka.core.domain.search.MetaSearch;
import co.jware.sojka.core.service.MetaLinkService;
import co.jware.sojka.core.service.entity.MetaSearchService;
import co.jware.sojka.rest.exceptions.HttpNotFoundException;
import com.google.common.collect.Lists;
import io.reactivex.Flowable;
import io.vertx.core.eventbus.EventBus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

@Service("metaSearchApiSvc")
public class MetaSearchApiSvcImpl implements MetaSearchApiSvc {
    @Autowired
    MetaLinkService metaLinkService;
    @Autowired
    private EventBus eventBus;
    @Autowired
    private MetaSearchService metaSearchService;
    @Autowired
    private MetaSearchExecutor metaSearchExecutor;

    @Override
    public Page<MetaLink> getMetaLinks(MetaSearch search, Pageable pageable) {
        MetaLink example = MetaLink.builder()
                .href("_BOGUS_")
                .title("_EXAMPLE_")
                .addAllLocations(search.locations())
                .addAllKeywords(search.keywords())
                .build();
        int pageSize = pageable.getPageSize();
        Iterable<MetaLink> links = metaLinkService.metaLinks(example, pageable)
//                .subscribeOn(Schedulers.io())
                .concatWith(metaSearchExecutor.metaLinks(search)
                        .flatMap(metaLink -> Flowable.fromCallable(() -> metaLinkService.findOrCreate(metaLink
                                .withLocations(search.locations())
                                .withKeywords(search.keywords())))
                                .doOnNext(m -> eventBus.publish(Events.META_LINK_SUBMIT, m))))
                .timeout(15, TimeUnit.SECONDS)
                .take(pageSize * 2)
                .blockingIterable();
        long count = metaLinkService.countLinks(example);
        PageImpl<MetaLink> metaLinks = new PageImpl<>(Lists.newArrayList(links), pageable, count);
        if (metaLinks.hasContent()) {
            return metaLinks;
        }
        throw new HttpNotFoundException("MetaSearch returned no data");
    }

    @Override
    public Optional<MetaSearch> getSearch(UUID uuid) {
        return metaSearchService.getSearch(uuid);
    }

    @Override
    public Optional<MetaSearch> getSearch(MetaSearch search) {
        return metaSearchService.getSearch(search);
    }

    @Override
    public MetaSearch findOrCreate(MetaSearch search) {
        return metaSearchService.findOrCreate(search);
    }

    @Override
    public MetaSearch defaultSearch() {
        return findOrCreate(MetaSearch.builder().build());
    }
}
