package co.jware.sojka.bot.jobs;

import co.jware.sojka.bot.SojkaBot;
import co.jware.sojka.bot.jobs.scrapers.Scraper;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Vertx;
import io.vertx.core.eventbus.EventBus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.List;


//@Component
public class JobsSojkaBot extends AbstractVerticle implements SojkaBot {
    @Autowired
    List<Scraper> scrapers;

    @Autowired
    Environment env;
    private EventBus eventBus;

    @PostConstruct
    void init() {
        vertx = Vertx.vertx();
        eventBus = vertx.eventBus();
        scrapers.forEach(s -> {
            s.setEventBus(this.eventBus);
            String name = s.getClass().getSimpleName();
            System.out.println(String.format("Scraper %s loaded and ready.", name));
        });
    }
}
