package co.jware.sojka.bot.service;

import co.jware.sojka.core.domain.Company;
import co.jware.sojka.core.domain.search.MetaLink;

import java.util.Optional;

public interface CompanyExtractorService {
    Optional<Company> extractCompany(MetaLink metaLink);
}
