package co.jware.sojka.bot.jobs.scrapers.links;

import co.jware.sojka.core.domain.search.MetaLink;
import io.reactivex.Flowable;
import org.apache.commons.lang3.StringUtils;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.springframework.stereotype.Component;

import java.util.List;

import static java.util.stream.Collectors.toList;

@Component
public class JobsCzLinksExtractor implements Extractor {

    @Override
    public List<MetaLink> extractLinks(Document doc, String baseUri) {
        return doc.select("div.search-list__main-info")
                .stream()
                .map(e -> {
                    Element link = e.select("a").first();
                    String href = link.attr("abs:href");
                    String title = link.text();
                    String company = e.select(".search-list__main-info__company")
                            .first()
                            .text();
                    String address = e.select(".search-list__main-info__address")
                            .select("span")
                            .last().text();
                    address = StringUtils.substringBefore(address, "+");
                    return MetaLink.builder()
                            .baseUri(baseUri)
                            .href(href)
                            .title(cleanTitle(title))
                            .company(company)
                            .address(address)
                            .build();
                }).collect(toList());
    }

    @Override
    public Flowable<MetaLink> extractLinksRx(Document doc, String baseUri) {
        return Flowable.fromIterable(extractLinks(doc, baseUri));
    }

    @Override
    public String extractorKey() {
        return EXTRACTOR_JOBS_CZ;
    }
}
