package co.jware.sojka.bot.cantidates;


import co.jware.sojka.bot.SojkaBot;
import io.vertx.core.AbstractVerticle;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.handler.StaticHandler;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class CandidateSojkaBot extends AbstractVerticle implements SojkaBot {
    @Value("${sojka.candidate.bot.port:8181}")
    private int port = 8181;

    @Override
    public void start() throws Exception {
        Router router = Router.router(vertx);
        router.route().handler(StaticHandler.create());
        vertx.createHttpServer()
                .requestHandler(router::accept)
                .listen(port);
    }
}
