package co.jware.sojka.bot.jobs.scrapers.links;

import co.jware.sojka.core.domain.search.MetaLink;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.TextNode;
import org.springframework.stereotype.Component;

import java.net.URI;
import java.util.List;

import static java.util.stream.Collectors.toList;

@Component
public class ProfesiaCzLinksExtractor implements Extractor {
    @Override
    public List<MetaLink> extractLinks(Document doc, String baseUri) {
        MetaLink.Builder builder = MetaLink.builder()
                .baseUri(EXTRACTOR_PROFESIA_CZ)
                .address("n/a")
                .company("n/a")
                .href("n/a")
                .title("n/a");
        return doc.select("li.list-row").stream().map(e -> {
            Element link = e.select("a.title").first();
            String title = link.text();
            String linkk = link.attr("href");
            String href = EXTRACTOR_PROFESIA_CZ + linkk;
            String company = e.select("span.employer").text();
            String location = e.select("span.job-location").text();
            builder
                    .title(title)
                    .address(location)
                    .company(company)
                    .href(href);
            return builder;
        })
                .map(MetaLink.Builder::build)
                .collect(toList());

    }

    @Override
    public URI getNextPageUrl(Document doc) {
        return null;
    }

    @Override
    public String extractorKey() {
        return EXTRACTOR_PROFESIA_CZ;
    }
}
