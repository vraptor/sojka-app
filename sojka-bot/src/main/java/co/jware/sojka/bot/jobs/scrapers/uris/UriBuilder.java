package co.jware.sojka.bot.jobs.scrapers.uris;

import co.jware.sojka.core.domain.search.Commons;
import co.jware.sojka.core.domain.search.MetaSearch;
import org.apache.commons.lang3.StringUtils;

import java.net.URI;
import java.util.List;
import java.util.stream.Stream;

public interface UriBuilder {

    List<URI> buildUris(MetaSearch search);

    boolean supportsAny(List<URI> baseUris);

    default Stream<String> filterKeyWords(MetaSearch search) {
        return search.keywords()    .stream()
                .filter(k -> !Commons.NOT_AVAILABLE.equalsIgnoreCase(k))
                .filter(StringUtils::isNotBlank);
    }

    default Stream<String> filterLocations(MetaSearch search) {
        return search.locations().stream()
                .filter(StringUtils::isNotBlank);
    }

    default boolean active() {
        return true;
    }
}
