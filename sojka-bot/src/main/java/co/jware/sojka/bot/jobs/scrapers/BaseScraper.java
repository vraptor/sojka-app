package co.jware.sojka.bot.jobs.scrapers;


import co.jware.sojka.core.domain.JobListing;
import co.jware.sojka.core.service.entity.CompanyService;
import co.jware.sojka.core.service.entity.HirerService;
import co.jware.sojka.core.service.entity.JobListingService;
import io.vertx.core.eventbus.EventBus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public abstract class BaseScraper implements Scraper {


    @Autowired
    JobListingService jobListingService;
    @Autowired
    HirerService hirerService;
    @Autowired
    CompanyService companyService;

    EventBus eventBus;

    public abstract List<JobListing> collectJobs();

    @Scheduled(cron = "0 * * * * *")
    @Transactional()
    public void doCollect() {
        List<JobListing> jobListings = this.collectJobs();
        if (jobListings != null) {
            jobListings.forEach(job -> {
//                Hirer hirer = job.owner().orElseThrow(IllegalArgumentException::new);
//                Company company = hirer.company()
//                        .flatMap(companyService::getOrCreate)
//                        .orElseThrow(RuntimeException::new);
//                hirer = hirer.withCompany(company);
//                Optional<Hirer> owner = hirerService.findByCompanyName(company);
//                if (!owner.isPresent()) {
//                    owner = Optional.ofNullable(hirerService.createHirer(hirer));
//                }
//                JobListing jobListing = job.withOwner(owner);
//                jobListingService.getOrCreate(jobListing)
//                        .map(jobListingFulltextService::index);
            });
        }
    }

    @Override
    public void setEventBus(EventBus eventBus) {
        this.eventBus = eventBus;
    }
}
