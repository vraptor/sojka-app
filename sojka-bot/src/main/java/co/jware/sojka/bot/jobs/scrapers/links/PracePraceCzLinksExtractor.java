package co.jware.sojka.bot.jobs.scrapers.links;

import co.jware.sojka.core.domain.search.MetaLink;
import org.apache.commons.lang3.StringUtils;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.springframework.stereotype.Component;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class PracePraceCzLinksExtractor implements Extractor {
    @Override
    public List<MetaLink> extractLinks(Document doc, String baseUri) {
        List<MetaLink> links = doc.select("table.inz_list tr.premium_0")
                .stream().map(e -> {
                    Elements link = e.select("td.title a");
                    String title = link.text();
                    String href = link.attr("href");
                    href = UriComponentsBuilder.fromHttpUrl(href)
                            .replaceQueryParam("param[seq]")
                            .build()
                            .encode()
                            .toUriString();
                    String summary = link.attr("onmouseover");
                    summary = StringUtils.abbreviate(summary, 1_000);
                    String company = e.select("td.company")
                            .text();
                    String address = e.select("td.region")
                            .text();
                    String sources = e.select("td.sources")
                            .text();
                    String[] sourceParts = sources.split(",");
                    String sourcePart = sourceParts[0];
                    MetaLink metaLink = MetaLink.builder()
                            .baseUri(baseUri).title(title)
                            .title(title)
                            .href(href)
                            .company(company)
                            .address(address)
                            .sources(sourcePart)
                            .summary(summary)
                            .build();
                    return metaLink;
                }).collect(Collectors.toList());
        return links;
    }

    @Override
    public String extractorKey() {
        return EXTRACTOR_PRACEPRACE_CZ;
    }

}
