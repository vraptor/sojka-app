package co.jware.sojka.bot.jobs.scrapers.pages;

import co.jware.sojka.core.domain.search.MetaLink;
import co.jware.sojka.core.domain.search.MetaSearchResult;
import co.jware.sojka.core.service.MetaLinkService;
import co.jware.sojka.core.service.PageService;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IQueue;
import com.hazelcast.core.ItemEvent;
import com.hazelcast.core.ItemListener;
import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.schedulers.Schedulers;
import io.vertx.core.Handler;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.eventbus.Message;
import org.asynchttpclient.AsyncHttpClient;
import org.asynchttpclient.uri.Uri;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.nio.charset.StandardCharsets;

@Component("pageDownloader")
public class PageDownloaderImpl implements PageDownloader {


    @Autowired
    private EventBus eventBus;
    @Autowired
    private AsyncHttpClient httpClient;
    @Autowired
    private PageService pageService;
    @Autowired
    private MetaLinkService metaLinkService;
    @Autowired
    private HazelcastInstance hazelcastInstance;

    @PostConstruct
    void init() {
//        MessageConsumer<MetaLink> consumer = eventBus.consumer(Events.META_LINK_SUBMIT);
//        consumer.handler(metaLinkHandler());
    }

    @NotNull
    private Handler<Message<MetaLink>> metaLinkHandler() {
        return message -> {
            MetaLink metaLink = message.body();
            Single.just(metaLink.href())
                    .map(httpClient::prepareGet)
                    .flatMap(b -> Single.fromFuture(b.execute())
                            .subscribeOn(Schedulers.io()))
                    .map(response -> {
                        String body = response.getResponseBody(StandardCharsets.UTF_8);
                        Uri uri = response.getUri();
                        int statusCode = response.getStatusCode();
                        return MetaSearchResult
                                .builder()
                                .htmlPayload(body)
                                .uri(uri.toJavaNetURI())
                                .statusCode(statusCode).build();
                    })
                    .subscribe(metaSearchResult -> {
                        int statusCode = metaSearchResult.statusCode();
                        metaLinkService.saveLink(metaLink.withHttpStatus(statusCode)
                                .withHref(metaSearchResult.uri().toString()));
                        if (statusCode == 200) {
                            pageService.createPage(metaSearchResult);
                        }
                    });
        };
    }

    public void sjAHSjas() {
        IQueue<MetaLink> queue = hazelcastInstance.getQueue("pageDownloadQueue");

        Observable.create(subscriber -> {
            queue.addItemListener(new ItemListener<MetaLink>() {
                @Override
                public void itemAdded(ItemEvent<MetaLink> item) {
                    try {
                        subscriber.onNext(queue.take());
                    } catch (InterruptedException e) {
                        subscriber.onError(e);
                    }
                }

                @Override
                public void itemRemoved(ItemEvent<MetaLink> item) {

                }
            }, false);
        }).publish();
    }
}
