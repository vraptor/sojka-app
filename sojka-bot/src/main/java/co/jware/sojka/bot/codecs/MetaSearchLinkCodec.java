package co.jware.sojka.bot.codecs;

import co.jware.sojka.core.domain.search.MetaSearchLink;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.eventbus.MessageCodec;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public class MetaSearchLinkCodec implements MessageCodec<MetaSearchLink, MetaSearchLink> {

    @Autowired
    private ObjectMapper mapper;

    @Override
    public void encodeToWire(Buffer buffer, MetaSearchLink metaSearchLink) {
        try {
            byte[] bytes = mapper.writeValueAsBytes(metaSearchLink);
            buffer.appendInt(bytes.length);
            buffer.appendBytes(bytes);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public MetaSearchLink decodeFromWire(int pos, Buffer buffer) {
        int len = buffer.getInt(pos);
        byte[] bytes = buffer.getBytes(pos + 4, pos + len);
        try {
            return mapper.readValue(bytes, MetaSearchLink.class);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public MetaSearchLink transform(MetaSearchLink metaSearchLink) {
        return metaSearchLink;
    }

    @Override
    public String name() {
        return this.getClass().getSimpleName();
    }

    @Override
    public byte systemCodecID() {
        return -1;
    }
}
