package co.jware.sojka.bot.jobs.scrapers.executors.impl;

import co.jware.sojka.bot.jobs.scrapers.executors.PageDetailExecutor;
import co.jware.sojka.bot.jobs.scrapers.pages.PageDetailExtractor;
import co.jware.sojka.core.Events;
import co.jware.sojka.core.domain.search.MetaLink;
import co.jware.sojka.core.domain.search.page.PageDetail;
import co.jware.sojka.core.service.MetaLinkService;
import co.jware.sojka.core.service.page.PageDetailService;
import io.reactivex.Flowable;
import io.reactivex.schedulers.Schedulers;
import io.vertx.core.Handler;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.eventbus.Message;
import org.apache.commons.lang3.tuple.Pair;
import org.asynchttpclient.AsyncHttpClient;
import org.asynchttpclient.Request;
import org.asynchttpclient.Response;
import org.asynchttpclient.extras.rxjava2.RxHttpClient;
import org.jdeferred.DeferredManager;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

import static java.util.stream.Collectors.toMap;

@Service("pageDetailExecutor")
public class PageDetailExecutorImpl implements PageDetailExecutor, ApplicationListener<ContextRefreshedEvent> {

    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(PageDetailExecutorImpl.class);
    @Autowired
    private MetaLinkService metaLinkService;
    @Autowired
    private PageDetailService pageDetailService;
    @Autowired
    private AsyncHttpClient asyncHttpClient;
    @Autowired
    private List<PageDetailExtractor> pageDetailExtractors;
    @Autowired
    private RxHttpClient rxHttpClient;
    @Autowired
    private DeferredManager deferredManager;
    @Autowired
    private EventBus eventBus;
    @Value("${system.batch.pages.initial_delay:15}")
    private long initialDelay;
    @Value("${system.batch.pages.period:120}")
    private long crawlPeriod;
    @Value("${system.batch.pages.initial_page:0}")
    private int initialPage;
    @Value("${system.batch.pages.page_size:20}")
    private int pageSize;
    @Value("${system.batch.pages.enable:true}")
    private boolean crawlEnabled;
    @Value("${system.batch.pages.count:500}")
    private int crawlCount;

    private Map<String, PageDetailExtractor> extractorMap;

    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {
        extractorMap = pageDetailExtractors.stream()
                .collect(toMap(PageDetailExtractor::baseUri, p -> p));
        eventBus.consumer(Events.META_LINK_SUBMIT,
                (Handler<Message<MetaLink>>) event -> {
                    MetaLink metaLink = event.body();
                    Optional.ofNullable(extractorMap.get(metaLink.baseUri()))
                            .ifPresent(extractor -> doExtractPageDetail(metaLink, extractor));
                });
        if (crawlEnabled) {
            Flowable.interval(initialDelay, crawlPeriod, TimeUnit.SECONDS)
                    .forEach(pageNum -> {
                        PageRequest page = PageRequest.of(initialPage, pageSize, Direction.DESC, "created");
                        metaLinkService.allMissingPageDetail(page)
                                .subscribeOn(Schedulers.io())
                                .filter(m -> !"n/a".equalsIgnoreCase(m.href()))
                                .filter(m -> !pageDetailService.existsByUrl(m.href()))
                                .filter(m -> extractorMap.containsKey(m.baseUri()))
                                .doOnNext(m -> logger.info("Page detail for {}", m.href()))
                                .flatMap(ml -> rxHttpClient.prepare(asyncHttpClient
                                        .prepareGet(ml.href())
                                        .setRequestTimeout(15_000).build()).onErrorComplete()
                                        .toFlowable()
                                        .filter(r -> r.getStatusCode() == 200)
                                        .flatMap((Response r) -> Flowable.just(Pair.of(ml, r)), true, 15))
                                .take(crawlCount)
                                .subscribe(pair -> {
                                    deferredManager.when(() -> {
                                        Response response = pair.getRight();
                                        return Jsoup.parse(response.getResponseBody());
                                    }).then((Document doc) -> {
                                        MetaLink metaLink = pair.getLeft();
                                        PageDetail detail = extractorMap.get(metaLink.baseUri())
                                                .extract(doc)
                                                .withUrl(metaLink.href())
                                                .withUuid(metaLink.uuid())
                                                .withTitle(metaLink.title());
                                        pageDetailService.saveOrUpdate(detail);
                                    }).fail(t -> logger.error("Error extracting page detail: {} ", t.getMessage()));
                                });
                    });
        }
    }

    private void doExtractPageDetail(MetaLink metaLink, PageDetailExtractor extractor) {
        if (!pageDetailService.existsByUrl(metaLink.href())) {
            Request request = asyncHttpClient.prepareGet(metaLink.href())
                    .setReadTimeout(15_000)
                    .build();
            rxHttpClient.prepare(request)
                    .onErrorComplete()
                    .subscribeOn(Schedulers.io())
                    .filter(r -> r.getStatusCode() == 200)
                    .map(Response::getResponseBody)
                    .map(Jsoup::parse)
                    .map(extractor::extract)
                    .map(p -> p.withTitle(metaLink.title())
                            .withUrl(metaLink.href())
                            .withUuid(metaLink.uuid()))
                    .subscribe(pageDetailService::saveOrUpdate,
                            err -> logger.error("Error extracting pagedetail", err),
                            () -> logger.debug("Pagedetail for {} extracted", metaLink.href()));
        }
    }
}
