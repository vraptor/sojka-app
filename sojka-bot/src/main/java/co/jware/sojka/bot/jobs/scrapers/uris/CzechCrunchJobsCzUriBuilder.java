package co.jware.sojka.bot.jobs.scrapers.uris;

import co.jware.sojka.bot.jobs.scrapers.SiteInfo;
import co.jware.sojka.core.domain.search.MetaSearch;
import org.springframework.stereotype.Component;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toSet;

@Component
public class CzechCrunchJobsCzUriBuilder extends BaseUriBuilder {
    private final UriComponentsBuilder uriComponentsBuilder = UriComponentsBuilder
            .fromUri(baseUri());

    @Override
    public List<URI> buildUris(MetaSearch search) {
        Set<String> keywords = filterKeyWords(search).collect(toSet());
        UriComponentsBuilder builder = uriComponentsBuilder.cloneBuilder();
        if (!keywords.isEmpty()) {
            builder.queryParam("term", filterKeyWords(search).collect(joining("+")));
        }
        int page = search.page();
        if (page > 1) {
            builder.queryParam("paginator-page", page);
        }
        URI uri = builder.build()
                .encode()
                .toUri();
        return Collections.singletonList(uri);
    }

    @Override
    URI baseUri() {
        return SiteInfo.CRUNCHJOBS_CZ.baseUri();
    }
}
