package co.jware.sojka.bot.jobs.scrapers.uris;

import co.jware.sojka.bot.jobs.scrapers.SiteInfo;
import co.jware.sojka.core.domain.search.MetaSearch;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.util.Collections;
import java.util.List;

import static java.util.stream.Collectors.joining;

@Component
public class ChciPraciCzUriBuilder extends BaseUriBuilder {

    private final UriComponentsBuilder uriComponentsBuilder = UriComponentsBuilder
            .fromUri(baseUri())
            .path("nabidka/");

    @Override
    public List<URI> buildUris(MetaSearch search) {
        UriComponentsBuilder builder = this.uriComponentsBuilder.cloneBuilder();
        String keywordsParam = filterKeyWords(search).collect(joining(","));
        if (StringUtils.isNotBlank(keywordsParam)) {
            builder.queryParam("fullText", keywordsParam);
        }
        // filter zamestnavatel
        builder.queryParam("f[]", 715);
        int page = search.page();
        if (page > 1) {
            builder.queryParam("page", page);
        }
        URI uri = builder.build()
                .encode()
                .toUri();
        return Collections.singletonList(uri);
    }

    @Override
    URI baseUri() {
        return SiteInfo.CHCI_PRACI_CZ.baseUri();
    }
}
