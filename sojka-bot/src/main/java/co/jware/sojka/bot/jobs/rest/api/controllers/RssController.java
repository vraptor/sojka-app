package co.jware.sojka.bot.jobs.rest.api.controllers;

import co.jware.sojka.bot.jobs.rest.api.service.MetaLinkApiSvc;
import co.jware.sojka.core.Events;
import co.jware.sojka.core.domain.search.MetaLink;
import co.jware.sojka.core.domain.search.MetaSearch;
import co.jware.sojka.repository.search.MetaLinkRepository;
import com.rometools.rome.feed.rss.Channel;
import com.rometools.rome.feed.rss.Description;
import com.rometools.rome.feed.rss.Item;
import com.rometools.rome.feed.synd.SyndContentImpl;
import com.rometools.rome.io.FeedException;
import com.rometools.rome.io.WireFeedOutput;
import io.vertx.core.eventbus.EventBus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.hateoas.mvc.ControllerLinkBuilder;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.util.Date;
import java.util.List;

import static java.util.stream.Collectors.toList;

@RestController
@RequestMapping(path = "/rss")
public class RssController {
    private final MetaSearch metaSearch = MetaSearch.builder().build();
    @Autowired
    private MetaLinkRepository repository;
    @Autowired
    private EventBus eventBus;
    private SyndContentImpl description;
    @Autowired
    private MetaLinkApiSvc metaLinkApiSvc;

    @GetMapping(produces = MediaType.APPLICATION_RSS_XML_VALUE)
    public String getRss() {
        try {
            eventBus.publish(Events.META_SEARCH_SUBMIT, MetaSearch.builder()
                    .build());
            return feedString();
        } catch (FeedException e) {
            throw new RuntimeException(e);
        }
    }

    private String feedString() throws FeedException {
        String rssLink = ServletUriComponentsBuilder.fromCurrentRequest()
                .replacePath("")
                .scheme("https")
                .build()
                .encode()
                .toUri()
                .toString();

        ControllerLinkBuilder linkBuilder = ControllerLinkBuilder
                .linkTo(MetaLinkController.class);
        Channel channel = new Channel("rss_2.0");
        channel.setTitle("Nabidky prace");
        channel.setTtl(10);
        channel.setLanguage("cs");
        channel.setLastBuildDate(new Date());
        channel.setLink(rssLink);
        channel.setDescription("Nabídky práce z mnoha pracovních portálů.");
        PageRequest page = new PageRequest(0, 250, Sort.Direction.DESC, "created");
        Page<MetaLink> latest = metaLinkApiSvc.getLatest(page);
        List<MetaLink> metaLinks = latest.getContent();
        channel.setItems(metaLinks.stream().map(m -> {
            Item item = new Item();
            String href = linkBuilder.slash(m.uuid()).withSelfRel().getHref();
            item.setLink(href);
            Description description = new Description();
            description.setType("text/html");
            description.setValue(m.summary());
            item.setDescription(description);
            item.setTitle(m.title());
            item.setComments(m.href());
            item.setAuthor(m.baseUri());
            return item;
        }).collect(toList()));
        return new WireFeedOutput().outputString(channel);
    }
}
