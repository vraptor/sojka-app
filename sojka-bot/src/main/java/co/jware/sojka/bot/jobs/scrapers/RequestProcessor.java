package co.jware.sojka.bot.jobs.scrapers;

import co.jware.sojka.core.Events;
import co.jware.sojka.core.domain.search.MetaSearchLink;
import co.jware.sojka.core.service.MetaSearchLinkService;
import com.hazelcast.core.IQueue;
import com.hazelcast.core.ItemEvent;
import com.hazelcast.core.ItemListener;
import io.vertx.core.eventbus.EventBus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Optional;

@Component
public class RequestProcessor {

    @Autowired
    private EventBus eventBus;

    private MetaSearchLinkService metaSearchLinkService;
    @Autowired
    private IQueue<MetaSearchLink> metaSearchLinkQueue;

    @PostConstruct
    void init() {
        metaSearchLinkQueue.addItemListener(new ItemListener<MetaSearchLink>() {
            @Override
            public void itemAdded(ItemEvent<MetaSearchLink> item) {
                try {
                    MetaSearchLink metaSearchLink = metaSearchLinkQueue.take();
                    Optional.ofNullable(metaSearchLink).ifPresent(m -> {
                        metaSearchLinkService.storeMetaSearchLink(m);
//                        eventBus.publish(Events.META_SEARCH_SUBMIT, m.metaSearch());
                        eventBus.publish(Events.META_LINK_SUBMIT, m.metaLink());
                    });

                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }

            @Override
            public void itemRemoved(ItemEvent<MetaSearchLink> item) {

            }
        }, true);
    }
}
