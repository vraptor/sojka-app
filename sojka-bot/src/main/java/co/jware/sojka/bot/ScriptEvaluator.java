package co.jware.sojka.bot;


import groovy.lang.GroovyShell;
import org.springframework.util.ResourceUtils;

import java.io.File;
import java.io.IOException;

public class ScriptEvaluator {


    Object evaluate(String script) throws IOException {
        File file = ResourceUtils.getFile(script);
        GroovyShell groovyShell = new GroovyShell();
        Object eval = groovyShell.evaluate(file);
        return eval;
    }
}
