package co.jware.sojka.bot.config;

import org.hsqldb.persist.HsqlProperties;
import org.hsqldb.server.Server;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.*;

import javax.sql.DataSource;

@Configuration
@Profile("hsql")
public class HsqlConfig {

    @Bean
    public HsqlProperties hsqlProperties() {
        HsqlProperties properties = new HsqlProperties();
        properties.setProperty("server.remote_open", true);
        properties.setProperty("server.no_system_exit", true);
        return properties;
    }

    @Bean(initMethod = "start", destroyMethod = "shutdown")
    public Server hsqlServer(HsqlProperties properties) throws Exception {
        Server server = new Server();
        server.setProperties(properties);
        return server;
    }

    @Bean
    @Primary
    @ConfigurationProperties(prefix = "spring.datasource")
    @DependsOn("hsqlServer")
    public DataSource dataSource() {
        return DataSourceBuilder.create().build();
    }
}
