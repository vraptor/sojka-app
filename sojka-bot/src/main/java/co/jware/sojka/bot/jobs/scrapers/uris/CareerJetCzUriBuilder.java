package co.jware.sojka.bot.jobs.scrapers.uris;

import co.jware.sojka.bot.jobs.scrapers.SiteInfo;
import co.jware.sojka.core.domain.search.MetaSearch;
import org.springframework.stereotype.Component;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.util.Collections;
import java.util.List;

import static java.util.stream.Collectors.joining;


@Component
public class CareerJetCzUriBuilder extends BaseUriBuilder {
    private final UriComponentsBuilder uriComponentsBuilder = UriComponentsBuilder.
            fromUri(baseUri())
            .path("/whledej/nabidky_prace");
    private final int pageSize = 20;

    @Override
    public List<URI> buildUris(MetaSearch search) {
        UriComponentsBuilder builder = uriComponentsBuilder.cloneBuilder();
        String query = filterKeyWords(search).collect(joining(","));
        builder.queryParam("s", query);
        int page = search.page();
        if (page > 1) {
            int offset = (page - 1) * pageSize + 1;
            builder.queryParam("b", offset);
        }
        URI uri = builder.build().encode().toUri();
        return Collections.singletonList(uri);
    }

    @Override
    URI baseUri() {
        return SiteInfo.CAREER_JET_CZ.baseUri();
    }
}
