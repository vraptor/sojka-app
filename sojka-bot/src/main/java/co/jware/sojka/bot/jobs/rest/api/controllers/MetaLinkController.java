package co.jware.sojka.bot.jobs.rest.api.controllers;

import co.jware.sojka.bot.jobs.rest.api.service.MetaLinkApiSvc;
import co.jware.sojka.core.domain.search.MetaLink;
import co.jware.sojka.core.domain.search.MetaLinksFilter;
import co.jware.sojka.resources.search.MetaLinkResource;
import co.jware.sojka.rest.exceptions.HttpPageGoneException;
import org.jetbrains.annotations.NotNull;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;
import org.jsoup.safety.Whitelist;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.PagedResources;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.ResourceAssembler;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import java.io.IOException;
import java.util.Optional;
import java.util.UUID;

import static org.springframework.data.domain.Sort.Direction.DESC;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;

@RestController
@RequestMapping(path = "/meta-link")
public class MetaLinkController {
    @Autowired
    private MetaLinkApiSvc metaLinkApiSvc;

    private ResourceAssembler<MetaLink, MetaLinkResource> resourceAssembler =
            entity -> new MetaLinkResource(entity);

    @GetMapping(path = "/{uuid}")
    public ModelAndView getMetaLink(@PathVariable UUID uuid) {
        MetaLink metaLink = metaLinkApiSvc.getMetaLink(uuid);
        if (metaLink != null) {
            return new ModelAndView(new RedirectView(metaLink.href()));
        }
        throw new HttpPageGoneException("MetaLink gone");
    }

    @GetMapping("/{uuid}/detail")
    public MetaLink detail(@PathVariable UUID uuid) {
        return Optional.ofNullable(metaLinkApiSvc.getMetaLink(uuid))
                .orElseThrow(() -> new HttpPageGoneException("MetaLink gone"));
    }

    @GetMapping(path = "/{uuid}/proxy")
    public ResponseEntity<String> proxyUrl(@PathVariable UUID uuid) {
        MetaLink metaLink = metaLinkApiSvc.getMetaLink(uuid);
        return Optional.ofNullable(metaLink)
                .map(m -> {
                    Element body = null;
                    try {
                        body = Jsoup.connect(metaLink.href()).execute().parse()
                                .body();
                        String html = body.outerHtml();
                        html = Jsoup.clean(html, Whitelist.relaxed());
                        return new ResponseEntity<String>(html, HttpStatus.OK);
                    } catch (IOException e) {
                        throw new RuntimeException(e);
                    }
                })
                .orElseThrow(() -> new HttpPageGoneException("MetaLink gone"));
    }

    @GetMapping(path = "/latest")
    public PagedResources<Resource<MetaLinkResource>> getLatest(@PageableDefault(size = 20, sort = "updated", direction = DESC) Pageable pageable,
                                                                PagedResourcesAssembler<MetaLinkResource> assembler) {
        return assembler.toResource(metaLinkApiSvc.getLatest(pageable)
                .map(resourceAssembler::toResource));
//                .map(addLinks()));
    }

    @PostMapping(path = "/filter")
    public ResponseEntity<PagedResources<Resource<MetaLinkResource>>> filterLinks(@RequestBody MetaLinksFilter filter, @PageableDefault(size = 20, sort = "updated", direction = DESC) Pageable pageable,
                                                                                  PagedResourcesAssembler<MetaLinkResource> assembler) {
        PagedResources<Resource<MetaLinkResource>> resources = assembler.toResource(metaLinkApiSvc.filterLinks(filter, pageable)
                .map(resourceAssembler::toResource));
//                .map(addLinks()));
        return ResponseEntity.ok(resources);
    }

    @NotNull
    private Converter<MetaLinkResource, MetaLinkResource> addLinks() {
        return r -> {
            r.add(linkTo(this.getClass()).slash(r.getContent().uuid()).withSelfRel());
            return r;
        };
    }
}
