package co.jware.sojka.bot.jobs.scrapers.links;

import co.jware.sojka.core.domain.search.MetaLink;
import com.beust.jcommander.internal.Lists;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class PraceCzLinksExtractor implements Extractor {

    private static final Logger logger = LoggerFactory.getLogger(PraceCzLinksExtractor.class);

    @Override
    public List<MetaLink> extractLinks(Document doc, String baseUri) {
        List<MetaLink> result = Lists.newArrayList();
        doc.select(".search-result__advert").forEach(e -> {
            try {
                MetaLink.Builder builder = Extractor.emptyBuilder()
                        .baseUri(EXTRACTOR_PRACE_CZ);
                Element link = e.select("a.link").first();
                if (null != link) {
                    String title = link.text();
                    String href = link.attr("href");
                    builder.title(title);
                    builder.href(href);
                }
                Element companyElement = e.select(".search-result__advert__box__item--company").first();
                if (null != companyElement) {
                    String company = cleanTitle(companyElement.text());
                    builder.company(company);
                }
                Element locationElement = e.select(".search-result__advert__box__item--location").first();
                if (null != locationElement) {
                    String address = locationElement.text();
                    builder.address(address);
                }
                MetaLink metaLink = builder
                        .baseUri(baseUri)
                        .build();
                result.add(metaLink);
                if (logger.isTraceEnabled()) {
                    logger.trace("MetaLink:" + metaLink.toString());
                }
            } catch (IllegalStateException ex) {
                logger.error("Element can not be parsed - " + ex.getMessage());
            }

        });
        return result;
    }

    @Override
    public String extractorKey() {
        return EXTRACTOR_PRACE_CZ;
    }
}
