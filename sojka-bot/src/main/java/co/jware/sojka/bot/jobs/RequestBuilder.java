package co.jware.sojka.bot.jobs;


import co.jware.sojka.bot.jobs.scrapers.uris.UriBuilder;
import co.jware.sojka.core.domain.search.MetaSearch;
import io.reactivex.Flowable;
import io.reactivex.schedulers.Schedulers;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.util.List;
import java.util.Objects;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;

public class RequestBuilder {

    @Autowired
    private List<UriBuilder> uriBuilders;

    public List<URL> buildUri(MetaSearch search) {
        return getUrlsStream(search, uriBuilders.stream());
    }

    public List<URL> buildUri(final MetaSearch search, List<URI> baseUris) {
        return getUrlsStream(search, uriBuilders.stream()
                .filter(u -> u.supportsAny(baseUris)));
    }

    private List<URL> getUrlsStream(MetaSearch search, Stream<UriBuilder> stream) {
        return stream
                .filter(UriBuilder::active)
                .map(u -> u.buildUris(search))
                .flatMap(List::stream)
                .filter(Objects::nonNull)
                .map(this::getUrl)
                .collect(toList());
    }

    @NotNull
    private URL getUrl(URI u) {
        try {
            return u.toURL();
        } catch (MalformedURLException e) {
            throw new RuntimeException(e);
        }
    }

    public Flowable<URI> buildUris(MetaSearch search) {
        return Flowable.fromIterable(uriBuilders)
                .filter(UriBuilder::active)
                .flatMap(u -> Flowable.fromCallable(() -> u.buildUris(search))
                        .subscribeOn(Schedulers.io()), true, 5)
                .flatMap(Flowable::fromIterable);
    }
}
