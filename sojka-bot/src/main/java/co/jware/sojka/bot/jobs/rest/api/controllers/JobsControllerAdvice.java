package co.jware.sojka.bot.jobs.rest.api.controllers;

import co.jware.sojka.rest.exceptions.ErrorResponse;
import co.jware.sojka.rest.exceptions.HttpNotFoundException;
import co.jware.sojka.rest.exceptions.HttpPageGoneException;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.servlet.http.HttpServletResponse;

@RestControllerAdvice(basePackageClasses = MetaSearchController.class)
public class JobsControllerAdvice {
    @ExceptionHandler(HttpNotFoundException.class)
    public ErrorResponse notFound(HttpNotFoundException exception, HttpServletResponse response) {
        int status = HttpStatus.NOT_FOUND.value();
        response.setStatus(status);
        return ErrorResponse.builder(status, exception.getMessage())
                .build();
    }

    @ExceptionHandler(HttpPageGoneException.class)
    public ErrorResponse pageGone(HttpPageGoneException exception, HttpServletResponse response) {
        int status = HttpStatus.GONE.value();
        response.setStatus(status);
        return ErrorResponse.builder(status, exception.getMessage())
                .build();
    }

    @ExceptionHandler({IllegalArgumentException.class, HttpMessageNotReadableException.class})
    public ErrorResponse illegalArgument(Exception exception, HttpServletResponse response) {
        int status = HttpStatus.BAD_REQUEST.value();
        response.setStatus(status);
        return ErrorResponse.builder(status, exception.getMessage())
                .build();
    }
}
