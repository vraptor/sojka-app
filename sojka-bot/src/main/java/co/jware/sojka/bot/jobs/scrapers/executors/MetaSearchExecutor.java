package co.jware.sojka.bot.jobs.scrapers.executors;


import co.jware.sojka.bot.jobs.scrapers.SiteInfo;
import co.jware.sojka.core.domain.search.MetaLink;
import co.jware.sojka.core.domain.search.MetaSearch;
import co.jware.sojka.core.domain.search.MetaSearchResult;
import io.reactivex.Flowable;
import io.reactivex.Observable;

import java.util.Collection;
import java.util.Set;

public interface MetaSearchExecutor {

    Observable<MetaSearchResult> metaSearchResult(String url);

    Flowable<MetaSearchResult> metaSearchResults(Collection<String> urls);

    Flowable<MetaLink> metaLinks(MetaSearch metaSearch);

    Flowable<MetaLink> metaLinks(MetaSearch search, Set<SiteInfo> siteInfos);

    Flowable<MetaSearchResult> metaSearchResults(MetaSearch search);
}
