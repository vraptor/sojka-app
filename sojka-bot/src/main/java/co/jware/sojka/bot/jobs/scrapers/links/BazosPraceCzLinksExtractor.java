package co.jware.sojka.bot.jobs.scrapers.links;

import co.jware.sojka.core.domain.search.MetaLink;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.springframework.stereotype.Component;

import java.net.URI;
import java.util.List;

import static java.util.stream.Collectors.toList;

@Component
public class BazosPraceCzLinksExtractor implements Extractor {
    @Override
    public List<MetaLink> extractLinks(Document doc, String baseUri) {
        return doc.select("table.inzeraty")
                .stream().map(e -> {
                    MetaLink.Builder builder = Extractor.emptyBuilder()
                            .baseUri(EXTRACTOR_BAZOSPRACE_CZ);
                    Element link = e.select("a").first();
                    String title = link.text();
                    String href = link.attr("abs:href");
                    builder.title(title)
                            .href(href);
                    String desc = e.select("div.popis").text();
                    String location = e.select("td").get(2).textNodes().get(0).text();
                    builder.address(location).summary(desc);
                    return builder;
                })
                .map(MetaLink.Builder::build)
                .collect(toList());
    }

    @Override
    public URI getNextPageUrl(Document doc) {
        return null;
    }

    @Override
    public String extractorKey() {
        return Extractor.EXTRACTOR_BAZOSPRACE_CZ;
    }
}
