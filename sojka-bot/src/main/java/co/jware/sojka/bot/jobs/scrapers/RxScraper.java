package co.jware.sojka.bot.jobs.scrapers;

import co.jware.sojka.core.domain.JobListing;
import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.util.Collections;
import java.util.List;

import static org.jsoup.Connection.Response;

public class RxScraper extends BaseScraper {


    @Override
    public List<JobListing> collectJobs() {

        String startPageUrl = "http://www.jobs.cz/prace/";
        parsePage(startPageUrl)
                .map(doc -> Observable.fromIterable(doc.select("a.search-list__main-info__title__link")))
                .flatMap(e -> e)
                .map(el -> "http://www.jobs.cz" + el.attr("href"))
                .map(href -> Observable.defer(() -> this.parsePage(href)))
                .subscribeOn(Schedulers.io())
                .flatMap(d -> d)
                .map(d -> d.select("#cont").first())
                .subscribe(
                        System.out::println,
                        Throwable::printStackTrace,
                        () -> System.out.println("Completed"));
        return Collections.emptyList();
    }

    private Observable<Document> parsePage(String startPageUrl) {
        return Observable.just(startPageUrl)
                .map(Jsoup::connect)
//                .map(c -> c.proxy("web-proxy.bbn.hpecorp.net", 8080))
                .map(Connection::execute)
                .map(Response::parse);
    }
}
