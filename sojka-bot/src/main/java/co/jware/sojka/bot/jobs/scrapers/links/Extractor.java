package co.jware.sojka.bot.jobs.scrapers.links;

import co.jware.sojka.bot.jobs.scrapers.SiteInfo;
import co.jware.sojka.core.domain.search.MetaLink;
import io.reactivex.Flowable;
import org.jsoup.nodes.Document;

import java.net.URI;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

public interface Extractor {

    String[] regexes = {"(\\p{Sm})", "([\\>|\\<]{2,})", "^(\\PL+\\s+)"};

    String EXTRACTOR_CAREER_JET_CZ = SiteInfo.CAREER_JET_CZ.baseUri().toString();
    String EXTRACTOR_CHCI_PRACI_CZ = SiteInfo.CHCI_PRACI_CZ.baseUri().toString();
    String EXTRACTOR_DOBRA_PRACE_CZ = SiteInfo.DOBRA_PRACE_CZ.baseUri().toString();
    String EXTRACTOR_JENPRACE_CZ = SiteInfo.JEN_PRACE_CZ.baseUri().toString();
    String EXTRACTOR_JOBS_CZ = SiteInfo.JOBS_CZ.baseUri().toString();
    String EXTRACTOR_PRACE_CZ = SiteInfo.PRACE_CZ.baseUri().toString();
    String EXTRACTOR_SUPERKARIERA_CZ = SiteInfo.SUPERKARIERA_CZ.baseUri().toString();
    String EXTRACTOR_PRACEPRACE_CZ = SiteInfo.PRACEPRACE_CZ.baseUri().toString();
    String EXTRACTOR_EXPATS_CZ = SiteInfo.EXPATS_CZ.baseUri().toString();
    String EXTRACTOR_CRUNCHJOBS_CZ = SiteInfo.CRUNCHJOBS_CZ.baseUri().toString();
    //    String EXTRACTOR_STARTUPJOBS_CZ = SiteInfo.STARTUPJOBS_CZ.baseUri().toString();
    String EXTRACTOR_BAZOSPRACE_CZ = SiteInfo.BAZOSPRACE_CZ.baseUri().toString();
    String EXTRACTOR_PROFESIA_CZ = SiteInfo.PROFESIA_CZ.baseUri().toString();

    String META_LINK = "search.meta_link";
    String META_SEARCH_LINK = "search.meta_search_link";

    static MetaLink.Builder emptyBuilder() {
        return MetaLink.builder()
                .address("n/a")
                .company("n/a")
                .title("n/a")
                .href("n/a");
    }

    List<MetaLink> extractLinks(Document doc, String baseUri);

    default Flowable<MetaLink> extractLinksRx(Document doc, String baseUri) {
        return Flowable.fromIterable(extractLinks(doc, baseUri));
    }

    default URI getNextPageUrl(Document doc) {
        return null;
    }

    String extractorKey();

    default String cleanTitle(final String t) {
        final String[] target = {t};
        Arrays.stream(regexes)
                .map(r -> Pattern.compile(r, Pattern.UNICODE_CHARACTER_CLASS))
                .map(p -> p.matcher(target[0]))
                .forEach(m -> {
                    String result = t;
                    while (m.find()) {
                        for (int i = 1; i <= m.groupCount(); i++) {
                            result = result.replace(m.group(i), "");
                        }
                        target[0] = result;
                    }
                });
        return target[0].trim();
    }
}
