package co.jware.sojka.bot.jobs.scrapers.uris;

import co.jware.sojka.bot.jobs.scrapers.SiteInfo;
import co.jware.sojka.core.domain.search.MetaSearch;
import org.springframework.stereotype.Component;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toSet;

/**
 * Created by vkorl on 26.02.2017.
 */
@Component
public class DobraPraceCzUriBuilder extends BaseUriBuilder {
    private final UriComponentsBuilder uriComponentsBuilder = UriComponentsBuilder
            .fromUri(baseUri())
            .path("nabidka-prace/")
            .queryParam("search", "1");

    @Override
    public List<URI> buildUris(MetaSearch search) {
        int page = search.page();
        Set<String> keywords = filterKeyWords(search).collect(toSet());
        if (keywords.isEmpty()) {
            UriComponentsBuilder cloneBuilder = this.uriComponentsBuilder.cloneBuilder();
            if (page > 1) {
                cloneBuilder.queryParam("list", page);
            }
            List<URI> uriList = Collections.singletonList(cloneBuilder.build().encode().encode().toUri());
            return uriList;
        }
        List<URI> uriList = filterKeyWords(search).map(k -> {
            UriComponentsBuilder builder = uriComponentsBuilder
                    .cloneBuilder();
            if (page > 1) {
                builder.queryParam("list", page);
            }
            return builder
                    .queryParam("text_what", k)
                    .build()
                    .encode()
                    .toUri();
        }).collect(Collectors.toList());
        return uriList;
    }

    @Override
    public boolean active() {
        return true;
    }

    @Override
    URI baseUri() {
        return SiteInfo.DOBRA_PRACE_CZ.baseUri();
    }
}
