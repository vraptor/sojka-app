package co.jware.sojka.bot.jobs.scrapers.links;

import co.jware.sojka.core.domain.search.MetaLink;
import org.apache.commons.lang.StringUtils;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.stereotype.Component;

import java.util.List;

import static java.util.Optional.ofNullable;
import static java.util.stream.Collectors.toList;

@Component
public class JenpraceCzLinksExtractor implements Extractor {
    @Override
    public List<MetaLink> extractLinks(Document doc, String baseUri) {
        return doc.select("article.offer").stream()
                .filter(e -> !(e.hasClass("rm-offer")))
                .map((Element article) -> {
                    MetaLink.Builder builder = Extractor.emptyBuilder();
                    Elements link = article.select("a");
                    String href = link.attr("abs:href");
                    String title = link.text();
                    String address = ofNullable(article.select("span.fa-map-marker"))
                            .map(span -> {
                                return span.parents().first().text();
                            })
                            .orElse("");
                    String company = ofNullable(article.select("span.fa-building-o"))
                            .map(span -> {
                                return span.parents().first().text();
                            })
                            .orElse("");
                    String summary = article.select("article")
                            .text();
                    if (StringUtils.isNotBlank(summary)) {
                        builder.summary(summary);
                    }
                    return builder.baseUri(baseUri)
                            .href(href)
                            .title(title)
                            .company(company)
                            .address(address)
                            .build();
                }).collect(toList());
    }

    @Override
    public String extractorKey() {
        return EXTRACTOR_JENPRACE_CZ;
    }
}
