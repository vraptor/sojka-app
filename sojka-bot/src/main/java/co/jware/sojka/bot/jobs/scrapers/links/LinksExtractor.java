package co.jware.sojka.bot.jobs.scrapers.links;


import co.jware.sojka.core.service.entity.JobListingService;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.concurrent.CompletableFuture;

import static java.util.stream.Collectors.toList;

public class LinksExtractor {


    private JobListingService service;

    public List<URL> extractLinks(String url) {
        try {
            Document doc = Jsoup.connect(url)
                    .userAgent("Mozilla/5.0 (jsoup)")
                    .get();
            return getUrls(doc);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    List<URL> getUrls(Document doc) {
        Elements links = doc.select("a.link[href]");
        return links.stream()
                .map(e -> e.attr("href"))
                .map(href -> {
                    try {
                        return new URL(href);
                    } catch (MalformedURLException e) {
                        throw new RuntimeException(e);
                    }
                })
                .collect(toList());
    }

    private static <T> CompletableFuture<List<T>> sequence(List<CompletableFuture<T>> futures) {
        CompletableFuture<Void> allDoneFuture = CompletableFuture.allOf(futures.toArray(new CompletableFuture[futures.size()]));
        return allDoneFuture.thenApply(v -> futures.stream()
                .map(CompletableFuture::join)
                .collect(toList()));


    }
}
