package co.jware.sojka.bot.config;


import co.jware.sojka.bot.jobs.scrapers.SiteInfo;
import co.jware.sojka.bot.jobs.scrapers.pages.PageDetailExtractor;
import co.jware.sojka.bot.jobs.scrapers.pages.impl.BasePageDetailExtractor;
import co.jware.sojka.core.domain.search.Commons;
import co.jware.sojka.core.domain.search.page.PageMetaData;
import org.jsoup.nodes.Document;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class PageExtractionConfig {

    @Bean
    public PageDetailExtractor bazosCzPageDetailExtractor() {
        return new GenericPageDetailExtractor(SiteInfo.BAZOSPRACE_CZ);
    }

    @Bean
    public PageDetailExtractor superKarieraCzPageDetailExtractor() {
        return new GenericPageDetailExtractor(SiteInfo.SUPERKARIERA_CZ);
    }

    @Bean
    public PageDetailExtractor profesiaCzPageDetailExtractor() {
        return new GenericPageDetailExtractor(SiteInfo.PROFESIA_CZ);
    }

//    @Bean
//    public PageDetailExtractor startupJobsCzPageDetailExtractor() {
//        return new GenericPageDetailExtractor(SiteInfo.STARTUPJOBS_CZ);
//    }

    @Bean
    public PageDetailExtractor chciPraciCzPageDetailExtractor() {
        return new GenericPageDetailExtractor(SiteInfo.CHCI_PRACI_CZ);
    }

    @Bean
    public PageDetailExtractor monsterCzPageDetailExtractor() {
        return new GenericPageDetailExtractor(SiteInfo.MONSTER_CZ);
    }

    @Bean
    public PageDetailExtractor crunchJobsCzPageDetailExtractor() {
        return new GenericPageDetailExtractor(SiteInfo.CRUNCHJOBS_CZ);
    }

    @Bean
    public PageDetailExtractor jenPraceCzPageDetailExtractor() {
        return new GenericPageDetailExtractor(SiteInfo.JEN_PRACE_CZ);
    }

    @Bean
    public PageDetailExtractor hyperpraceCzPageDetailExtractor() {
        return new GenericPageDetailExtractor(SiteInfo.HYPERPRACE_CZ);
    }

    @Bean
    public PageDetailExtractor joblistCz() {
        return new GenericPageDetailExtractor(SiteInfo.JOBLIST_CZ);
    }

    @Bean
    public PageDetailExtractor easyPraceCzPageDetailExtractor() {
        return new GenericPageDetailExtractor(SiteInfo.EASY_PRACE_CZ);
    }

    public PageDetailExtractor praceJobcityPageDetailExtractor() {
        return new GenericPageDetailExtractor(SiteInfo.PRACE_JOBCITY_CZ);
    }

    private static class GenericPageDetailExtractor extends BasePageDetailExtractor {

        GenericPageDetailExtractor(SiteInfo siteInfo) {
            super();
            this.siteInfo = siteInfo;
        }

        @Override
        public String extractRichText(Document doc) {
            return Commons.NOT_IMPLEMENTED;
        }

        @Override
        public PageMetaData extractMetaData(Document doc) {
            return PageMetaData.empty();
        }
    }
}
