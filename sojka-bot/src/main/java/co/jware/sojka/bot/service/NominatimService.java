package co.jware.sojka.bot.service;

import co.jware.sojka.bot.core.OsmResponse;

import java.io.IOException;
import java.util.List;

public interface NominatimService {
    List<OsmResponse> search(String searchQuery) throws IOException;

    List<OsmResponse> search(String searchQuery, int limit) throws IOException;
}
