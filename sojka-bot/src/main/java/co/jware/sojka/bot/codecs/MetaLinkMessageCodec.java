package co.jware.sojka.bot.codecs;

import co.jware.sojka.core.domain.search.MetaLink;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.eventbus.MessageCodec;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public class MetaLinkMessageCodec implements MessageCodec<MetaLink, MetaLink> {
    @Autowired
    ObjectMapper mapper;

    @Override
    public void encodeToWire(Buffer buffer, MetaLink metaLink) {
        try {
            String json = mapper.writeValueAsString(metaLink);
            buffer.appendInt(json.length());
            buffer.appendString(json);
        } catch (JsonProcessingException e) {
            throw new RuntimeException("Error encoding MetaLink");
        }
    }

    @Override
    public MetaLink decodeFromWire(int pos, Buffer buffer) {
        int len = buffer.getInt(pos);
        String json = buffer.getString(pos + 4, pos + len);
        try {
            return mapper.readValue(json, MetaLink.class);
        } catch (IOException e) {
            throw new RuntimeException("Error decoding MetaLink");
        }
    }

    @Override
    public MetaLink transform(MetaLink metaLink) {
        return metaLink;
    }

    @Override
    public String name() {
        return this.getClass().getSimpleName();
    }

    @Override
    public byte systemCodecID() {
        return -1;
    }
}
