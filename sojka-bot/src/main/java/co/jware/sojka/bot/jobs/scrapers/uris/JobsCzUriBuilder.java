package co.jware.sojka.bot.jobs.scrapers.uris;

import co.jware.sojka.bot.jobs.scrapers.SiteInfo;
import co.jware.sojka.core.domain.search.MetaSearch;
import com.beust.jcommander.internal.Lists;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toSet;
import static org.springframework.web.util.UriComponentsBuilder.fromUri;

@Component
public class JobsCzUriBuilder extends BaseUriBuilder {

    private final UriComponentsBuilder builder = fromUri(baseUri())
            .path("prace/");

    private boolean active = true;

    @Override
    public List<URI> buildUris(MetaSearch search) {
        List<URI> result = Lists.newArrayList();
        if (active()) {
            Stream<String> keywordsStream = filterKeyWords(search);
            Stream<String> locationsStream = filterLocations(search);
            Set<String> left = keywordsStream.collect(toSet());
            Set<String> right = locationsStream.collect(toSet());
            Set<UriComponentsBuilder> builders = generatePairs(left, right);
            if (builders.isEmpty()) {
                builders.add(builder);
            }
            int page = search.page();
            result = builders.stream()
                    .peek(b -> {
                        if (page > 1) {
                            b.replaceQueryParam("page", page);
                        }
                    })
                    .map(b -> b.build().encode().toUri())
                    .collect(toList());
        }
        return result;
    }


    private UriComponentsBuilder build(String keyword, String location) {
        UriComponentsBuilder b = this.builder.cloneBuilder();
        if (StringUtils.isNotBlank(keyword)) {
            b.queryParam("q[]", keyword);
        }
        if (StringUtils.isNotBlank(location)) {
            b.queryParam("locality[label]", location);
        }
        return b;
    }


    Set<UriComponentsBuilder> generatePairs(Set<String> left, Set<String> right) {
        HashSet<UriComponentsBuilder> result = new HashSet<>();
        if (left.size() > right.size()) {
            left.forEach(k -> {
                if (right.isEmpty()) {
                    result.add(build(k, ""));
                } else {
                    right.forEach(l -> {
                        result.add(build(k, l));
                    });
                }
            });
        } else {
            right.forEach(l -> {
                if (left.isEmpty()) {
                    result.add(build("", l));
                } else {
                    left.forEach(k -> {
                        result.add(build(k, l));
                    });
                }
            });
        }
        return result;
    }

    @Override
    public boolean active() {
        return this.active;
    }

    @Override
    URI baseUri() {
        return SiteInfo.JOBS_CZ.baseUri();
    }
}
