package co.jware.sojka.bot.jobs.scrapers;


import co.jware.sojka.core.domain.JobListing;
import co.jware.sojka.core.domain.Salary;
import co.jware.sojka.core.domain.SalaryRange;
import co.jware.sojka.core.domain.wrapped.Benefit;
import co.jware.sojka.core.enums.RecurrencePeriod;
import com.ibm.icu.text.Transliterator;
import org.apache.http.HttpStatus;
import org.asynchttpclient.AsyncHttpClient;
import org.asynchttpclient.Response;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;

import java.io.IOException;
import java.math.BigDecimal;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

import static co.jware.sojka.core.enums.JobListingOrigin.EXTERNAL;
import static java.util.regex.Pattern.UNICODE_CHARACTER_CLASS;
import static java.util.regex.Pattern.compile;
import static java.util.stream.Collectors.toList;


public class Spider {

    private final String rootUrl = "http://www.prace.cz";
    @Autowired
    private AsyncHttpClient asyncClient;

    private static <T> CompletableFuture<List<T>> sequence(List<CompletableFuture<T>> futures) {
        CompletableFuture<Void> allDoneFuture = CompletableFuture.allOf(futures.toArray(new CompletableFuture[futures.size()]));
        return allDoneFuture.thenApply(v -> futures.stream()
                .map(CompletableFuture::join)
                .collect(toList()));


    }

    public void collect(String url) {
        try {
            Document doc = Jsoup.connect(url).get();
            sequence(extractLinks(doc).stream().
                    map(u -> this.pageDetails(u)
                            .thenAccept(jobListing -> {
                                if (jobListing != null) {
                                    System.err.println(jobListing.toString());
                                }
                            }))
                    .collect(toList())).join();
            nextPageUrl(doc).ifPresent(this::collect);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private Optional<String> nextPageUrl(Document doc) {
        return Stream.of("span.page a", "span.pager__next a")
                .map(doc::select).filter(e -> !(e.isEmpty()))
                .map(Elements::first)
                .filter(e -> e.is("a"))
                .map(e -> e.attr("href"))
                .findFirst().map(e -> rootUrl + e);
    }

    CompletableFuture<JobListing> pageDetails(URL url) {
//        try {
        String url_ = url.toExternalForm();
        CompletableFuture<Response> future = asyncClient.prepareGet(url_)
                .execute()
                .toCompletableFuture();
        return future.thenApply(r -> {
            if (r.getStatusCode() == HttpStatus.SC_MOVED_TEMPORARILY) {
                throw new RuntimeException("Redirection");
            }
            return r;
        }).exceptionally(t -> null)
                .thenApply(r -> {
                    JobListing.Builder builder = JobListing.builder()
                            .name("_EMPTY_");
                    try {
                        if (r != null) {
                            Document doc = Jsoup.parse(r.getResponseBodyAsStream(), "UTF8", rootUrl);
                            Stream.of("script", "form")
                                    .map(doc::select)
                                    .forEach(e -> e.remove());
                            builder.link(url_);
                            String title = doc.select("h1").first().text();
                            builder.name(title)
                                    .origin(EXTERNAL);
                            // Benefits
                            Elements benefits = doc.select(".advert__list--benefit span.data");
                            if (!benefits.isEmpty()) {
                                Set<String> benefitSet = StringUtils.commaDelimitedListToSet(benefits.first().text());
                                benefitSet.stream()
                                        .map(Benefit::of)
                                        .forEach(builder::addBenefit);
                            }
                            // Salary
                            Elements salary = doc.select(".advert__list--salary div.data");
                            if (!salary.isEmpty()) {
                                String salaryTxt = salary.first().text();
                                Pattern salaryPattern = compile("(\\b\\d+\\W{0,}\\d+\\b)\\s+\\W\\s+(\\b\\d+\\W{0,}\\d+\\b)\\s+(\\w+)\\W+(\\w+)",
                                        UNICODE_CHARACTER_CLASS);
                                Matcher matcher = salaryPattern.matcher(salaryTxt);
                                if (matcher.matches()) {
                                    String from = matcher.group(1).replaceAll("\\D", "");
                                    String to = matcher.group(2).replaceAll("\\D", "");
                                    Transliterator transliterator = Transliterator.getInstance("Latin-ASCII");
                                    String currency = matcher.group(3);
                                    currency = transliterator.transform(currency)
                                            .toLowerCase();
                                    Currency instance = Currency.getInstance(Locale.getDefault());
                                    if ("kc".equalsIgnoreCase(currency)) {
                                        instance = Currency.getInstance("CZK");
                                    }
                                    builder.salary(Salary.builder()
                                            .range(SalaryRange.of(new BigDecimal(from), new BigDecimal(to)))
                                            .currency(instance)
                                            //TODO [Emk]: Parse recurrence
                                            .period(RecurrencePeriod.MONTHLY)
                                            .build());
                                }
                            }
                        }
                    } catch (IOException e) {
                        throw new RuntimeException("Error reading response,e");
                    }
                    return builder.build();
                });
    }

    private List<URL> extractLinks(Document doc) {
        return doc.select("a.link").stream()
                .map(e -> e.attr("href"))
                .map(s -> {
                    try {
                        return new URL(s);
                    } catch (MalformedURLException e) {
                        throw new RuntimeException(e);
                    }
                }).collect(toList());
    }
}
