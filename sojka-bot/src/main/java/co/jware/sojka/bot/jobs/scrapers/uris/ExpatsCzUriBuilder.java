package co.jware.sojka.bot.jobs.scrapers.uris;

import co.jware.sojka.bot.jobs.scrapers.SiteInfo;
import co.jware.sojka.core.domain.search.MetaSearch;
import org.springframework.stereotype.Component;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.util.Collections;
import java.util.List;

/**
 * Created by vkorl on 13.03.2017.
 */
@Component
public class ExpatsCzUriBuilder extends BaseUriBuilder {
    private final UriComponentsBuilder uriComponentsBuilder = UriComponentsBuilder
            .fromUri(baseUri())
            .path("jobs/");

    @Override
    public List<URI> buildUris(MetaSearch search) {
        UriComponentsBuilder builder = uriComponentsBuilder.cloneBuilder();
        filterKeyWords(search).forEach(k -> builder.pathSegment("search-" + k));
        builder.fragment("results");
        int page = search.page();
        if (page > 1) {
            builder.queryParam("page", page);
        }
        URI uri = builder.build()
                .encode()
                .toUri();
        return Collections.singletonList(uri);
    }

    @Override
    URI baseUri() {
        return SiteInfo.EXPATS_CZ.baseUri();
    }
}
