package co.jware.sojka.bot.jobs.rest.api.service;


import co.jware.sojka.core.domain.search.MetaLink;
import co.jware.sojka.core.domain.search.MetaSearch;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;
import java.util.UUID;

public interface MetaSearchApiSvc {

    Page<MetaLink> getMetaLinks(MetaSearch search, Pageable pageable);

    Optional<MetaSearch> getSearch(UUID uuid);

    Optional<MetaSearch> getSearch(MetaSearch search);

    MetaSearch findOrCreate(MetaSearch search);

    MetaSearch defaultSearch();

}
