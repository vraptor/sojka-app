package co.jware.sojka.bot.jobs.scrapers.executors;

import co.jware.sojka.core.domain.search.MetaLink;
import co.jware.sojka.core.domain.search.MetaSearch;
import co.jware.sojka.core.service.entity.MetaSearchService;
import com.google.common.collect.Sets;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.ILock;
import com.hazelcast.core.IMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.time.OffsetDateTime;
import java.time.temporal.TemporalUnit;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Component
@Transactional
public class MetaSearchExecutorHelper {
    @Autowired
    private IMap<MetaSearch, OffsetDateTime> metaSearchMap;
    @Autowired
    private MetaSearchService metaSearchService;
    @Autowired
    private HazelcastInstance hazelcastInstance;
    @Value("${system.batch.search.quiet:3600}")
    private int quietTime;
    @Value("${system.batch.search.max_pages:100}")
    private int maxPageNum;

    @Transactional
    public MetaSearch findNextSearch(MetaSearch search) {
        MetaSearch metaSearch = MetaSearch.copyOf(search);
        Optional<MetaSearch> searchOptional = metaSearchService.getSearch(metaSearch);
        if (searchOptional.isPresent()) {
            metaSearch = searchOptional.get();
            if (isOld(metaSearch)) {
                return metaSearchService.save(metaSearch.withUpdated(OffsetDateTime.now()));
            } else {
                return findNextSearch(metaSearch.withPage(metaSearch.page() + 1));
            }
        } else
            return metaSearchService.save(metaSearch.withUuid(null).withVersion(null));
    }

    private MetaSearch putMetaSearch(MetaSearch search, OffsetDateTime now) {
        ILock lock = hazelcastInstance.getLock("meta_search_lock");
        lock.lock();
        MetaSearch metaSearch;
        try {
            metaSearch = search.withUpdated(now);
            MetaSearch saved = metaSearchService.save(metaSearch);
            metaSearchMap.put(saved, now);
        } finally {
            lock.unlock();
        }
        return metaSearch;
    }

    public MetaLink enhanceMetaLink(MetaLink metaLink, MetaSearch metaSearch) {
        Set<String> searchKeywords = nullSafeSet(metaSearch.keywords());
        searchKeywords.addAll(nullSafeSet(metaLink.keywords()));
        Set<String> searchLocations = nullSafeSet(metaSearch.locations());
        searchLocations.addAll(nullSafeSet(metaLink.locations()));
        return MetaLink.copyOf(metaLink.withKeywords(searchKeywords)
                .withLocations(searchLocations));
    }


    private boolean isOld(MetaSearch search) {
        OffsetDateTime now = OffsetDateTime.now();
        OffsetDateTime updated = Optional.ofNullable(search.updated())
                .orElse(now);
        return updated.plusSeconds(quietTime).isBefore(now);
    }

    private <T> Set<T> nullSafeSet(Set<T> keywords) {
        return Optional.ofNullable(keywords)
                .map(Sets::<T>newHashSet)
                .orElseGet(Sets::<T>newHashSet);
    }
}
