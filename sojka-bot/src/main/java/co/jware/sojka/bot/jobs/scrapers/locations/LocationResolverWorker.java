package co.jware.sojka.bot.jobs.scrapers.locations;


import co.jware.sojka.core.domain.search.MetaLocation;
import co.jware.sojka.core.domain.search.MetaLocationResult;

public interface LocationResolverWorker {
    MetaLocationResult resolve(MetaLocation location);
}
