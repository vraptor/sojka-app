package co.jware.sojka.bot.jobs.scrapers.executors;


import co.jware.sojka.core.domain.search.Commons;
import co.jware.sojka.core.domain.search.MetaSearch;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.hazelcast.nio.serialization.ByteArraySerializer;

import java.io.IOException;

public class MetaSearchByteArraySerializer implements ByteArraySerializer<MetaSearch> {

    private final ObjectMapper mapper;

    public MetaSearchByteArraySerializer(ObjectMapper mapper) {
        super();
        this.mapper = mapper;
    }

    @Override
    public byte[] write(MetaSearch object) throws IOException {
        return mapper.writeValueAsBytes(object);
    }

    @Override
    public MetaSearch read(byte[] buffer) throws IOException {
        return mapper.readValue(buffer, MetaSearch.class);
    }

    @Override
    public int getTypeId() {
        return Commons.META_SEARCH_TYPE_ID;
    }

    @Override
    public void destroy() {

    }
}
