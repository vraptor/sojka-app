package co.jware.sojka.bot.jobs.scrapers.links;

import co.jware.sojka.core.domain.search.MetaLink;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.springframework.stereotype.Component;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.util.List;
import java.util.Optional;

import static java.util.stream.Collectors.toList;


@Component
public class ChciPraciCzLinksExtractor implements Extractor {

    @Override
    public List<MetaLink> extractLinks(Document doc, String baseUri) {
        List<MetaLink> metaLinks = doc.select("div.wrap")
                .stream().map(e -> {
                    MetaLink.Builder builder = Extractor.emptyBuilder()
                            .baseUri(EXTRACTOR_CHCI_PRACI_CZ);
                    Element link = e.select("a").first();
                    String title = link.text();
                    String href = link.attr("abs:href");
                    String fullUri = UriComponentsBuilder.fromUriString(href).fragment(null).build().toUriString();
                    builder.title(title)
                            .href(fullUri);
                    Element locElement = e.select("h3.location").first();
                    Optional.ofNullable(locElement).ifPresent(el -> {
                        if (el.hasText()) {
                            String location = el.text();
                            builder.address(location);
                        }
                    });

                    Element companyElement = e.select("h3.employer").first();

                    if (companyElement.hasText()) {
                        String company = companyElement.text();
                        builder.company(company);
                    }
                    Element p = e.select("p").first();
                    if (p.hasText()) {
                        String desc = p.text();
                        builder.summary(desc);
                    }
                    MetaLink metaLink = builder.build();
                    return metaLink;
                })
                .filter(m -> !m.href().contains("cmd=banner-click"))
                .peek(m -> m.company())
                .collect(toList());
        return metaLinks;
    }

    public URI getNextPageUrl(Document doc) {
        Element select = doc.select("btn.btn-next").first();
        return Optional.ofNullable(select).map(e -> {
            try {
                String href = e.attr("href");
                return new URI(EXTRACTOR_CHCI_PRACI_CZ + href);
            } catch (Exception ex) {
                return null;
            }
        }).orElse(null);
    }

    @Override
    public String extractorKey() {
        return Extractor.EXTRACTOR_CHCI_PRACI_CZ;
    }
}
