package co.jware.sojka.bot.jobs.rest.api.service.impl;

import co.jware.sojka.bot.jobs.rest.api.service.MetaLinkApiSvc;
import co.jware.sojka.core.domain.search.MetaLink;
import co.jware.sojka.core.domain.search.MetaLinksFilter;
import co.jware.sojka.core.service.MetaLinkService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service("metaLinkApiSvc")
class MetaLinkApiSvcImpl implements MetaLinkApiSvc {

    private final MetaLinkService metaLinkService;

    MetaLinkApiSvcImpl(MetaLinkService metaLinkService) {
        this.metaLinkService = metaLinkService;
    }

    @Override
    public MetaLink getMetaLink(UUID uuid) {
        return metaLinkService.getMetaLink(uuid);
    }

    @Override
    public Page<MetaLink> getLatest(Pageable pageable) {
        return metaLinkService.getLatest(pageable);
    }

    @Override
    public Page<MetaLink> filterLinks(MetaLinksFilter filter, Pageable pageable) {
        return metaLinkService.filterLinks(filter, pageable);
    }
}
