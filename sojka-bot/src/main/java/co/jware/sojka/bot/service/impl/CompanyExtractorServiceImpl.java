package co.jware.sojka.bot.service.impl;

import co.jware.sojka.bot.service.CompanyExtractorService;
import co.jware.sojka.core.domain.Company;
import co.jware.sojka.core.domain.search.MetaLink;
import co.jware.sojka.core.service.MetaLinkService;
import co.jware.sojka.core.service.entity.CompanyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service("companyExtractorService")
public class CompanyExtractorServiceImpl implements CompanyExtractorService {
    @Autowired
    private CompanyService companyService;
    @Autowired
    private MetaLinkService metaLinkService;

    @Override
    public Optional<Company> extractCompany(MetaLink metaLink) {
        String company = metaLink.company();
        return Optional.ofNullable(company).flatMap(c ->
                companyService.getOrCreate(Company.builder().name(c).build()));
    }

    public void aVoid() {
        metaLinkService.allMetaLinks(new PageRequest(0, 20, Sort.Direction.DESC, "created"));
    }
}
