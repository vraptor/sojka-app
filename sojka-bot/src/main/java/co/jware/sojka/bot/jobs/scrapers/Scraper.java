package co.jware.sojka.bot.jobs.scrapers;


import io.vertx.core.eventbus.EventBus;

public interface Scraper {
    void setEventBus(EventBus eventBus);
}
