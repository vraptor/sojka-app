package co.jware.sojka.bot.jobs.scrapers.uris;

import co.jware.sojka.bot.jobs.scrapers.SiteInfo;
import co.jware.sojka.core.domain.search.MetaSearch;

import org.springframework.stereotype.Component;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.stream.Collectors.*;
import static java.util.stream.Collectors.joining;

@Component
public class PraceCzUriBuilder extends BaseUriBuilder {


    @Override
    public List<URI> buildUris(MetaSearch search) {
        UriComponentsBuilder builder = UriComponentsBuilder
                .fromUri(baseUri());
        Set<String> keyWords = filterKeyWords(search).collect(toSet());
        if (keyWords.isEmpty()) {
            builder.path("nabidky/");
        } else {
            builder.queryParam("searchForm[other]", keyWords.stream().collect(joining(";")))
                    .path("hledat/");
        }
        int page = search.page();
        if (page > 1) {
            builder.queryParam("page", page);
        }
        URI uri = builder
                .build().encode()
                .toUri();
        return Collections.singletonList(uri);
    }

    @Override
    URI baseUri() {
        return SiteInfo.PRACE_CZ.baseUri();
    }
}

