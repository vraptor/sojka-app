package co.jware.sojka.bot.jobs.scrapers.uris;

import co.jware.sojka.bot.jobs.scrapers.SiteInfo;
import co.jware.sojka.core.domain.search.MetaSearch;
import org.springframework.stereotype.Component;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.util.Collections;
import java.util.List;

import static java.util.stream.Collectors.joining;

@Component
public class BazosCzUriBuilder extends BaseUriBuilder {
    private final UriComponentsBuilder uriComponentsBuilder = UriComponentsBuilder.fromUri(baseUri());

    @Override
    public List<URI> buildUris(MetaSearch search) {
        UriComponentsBuilder builder = uriComponentsBuilder.cloneBuilder();
        builder.queryParam("hledat", filterKeyWords(search).collect(joining("+")));
        if (search.page() > 1) {
            builder.pathSegment(Integer.toString((search.page() - 1) * 20))
                    .path("/");
        }
        URI uri = builder.build()
                .encode()
                .toUri();
        return Collections.singletonList(uri);
    }

    @Override
    URI baseUri() {
        return SiteInfo.BAZOSPRACE_CZ.baseUri();
    }
}
