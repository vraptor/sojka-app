package co.jware.sojka.bot.core;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.immutables.value.Value;

import javax.annotation.Nullable;

@Value.Immutable
@JsonDeserialize(builder = OsmResponse.Builder.class)
public interface AbstractOsmResponse {

    @JsonProperty("place_id")
    Long placeId();

    @JsonProperty("osm_id")
    Long osmId();

    String category();

    String type();

    @JsonProperty("display_name")
    String displayName();

    @JsonProperty("lat")
    Double latitude();

    @JsonProperty("lon")
    Double longitude();

    @JsonProperty("boundingbox")
    double[] boundingBox();

    double importance();

    @Nullable
    OsmAddress address();
}

