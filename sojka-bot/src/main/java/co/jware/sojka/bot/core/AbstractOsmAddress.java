package co.jware.sojka.bot.core;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.immutables.value.Value;

import javax.annotation.Nullable;

@Value.Immutable
@JsonDeserialize(builder = OsmAddress.Builder.class)
public interface AbstractOsmAddress {

    @Nullable
    String county();

    @Nullable
    String state();

    @Nullable
    String city();

    @Nullable
    String town();

    @Nullable
    String village();

    @Nullable
    String suburb();

    @Nullable
    String hamlet();

    @Nullable
    String country();

    @Nullable
    @JsonProperty("country_code")
    String countryCode();

    @Nullable
    String continent();
}
