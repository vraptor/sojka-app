package co.jware.sojka.bot.jobs.scrapers.links;

import co.jware.sojka.core.domain.search.MetaLink;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.net.URI;
import java.util.List;
import java.util.Optional;

import static java.util.stream.Collectors.toList;

/**
 * Created by vkorl on 24.02.2017.
 */
@Component
public class DobraPraceCzLinksExtractor implements Extractor {
    private final Logger logger = LoggerFactory.getLogger(DobraPraceCzLinksExtractor.class);

    @Override
    public List<MetaLink> extractLinks(Document doc, String baseUri) {

        return doc.select("div.inzerat").stream()
                .map(e -> {
                    MetaLink.Builder builder = Extractor.emptyBuilder()
                            .baseUri(baseUri);
                    try {
                        Element summaryElement = e.select("div.text").first();
                        String summary = summaryElement.textNodes().get(0).text();
                        builder.summary(summary);
                    } catch (Exception e1) {
                        logger.error("Error extracting summary");
                    }
                    Element titel = e.select("a.heading").first();
                    String title = titel.text();
                    Element urel = e.select("a.heading").first();
                    String url = urel.attr("abs:href");
                    Element locatino = e.select("div.region").first();
                    String location = locatino.text();
                    builder.address(location);
                    builder.title(title);
                    builder.href(url);
                    MetaLink metaLink = builder.build();
                    return metaLink;
                }).collect(toList());
    }

    @Override
    public URI getNextPageUrl(Document doc) {
        Elements select = doc.select("div.after a");
        return Optional.ofNullable(select).map(e -> {
            try {
                String href = e.first().attr("href");
                return new URI(href);
            } catch (Exception ex) {
                return null;
            }
        }).orElse(null);
    }

    @Override
    public String extractorKey() {
        return Extractor.EXTRACTOR_DOBRA_PRACE_CZ;
    }
}
