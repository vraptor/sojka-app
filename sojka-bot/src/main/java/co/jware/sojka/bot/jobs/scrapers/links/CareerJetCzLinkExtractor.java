package co.jware.sojka.bot.jobs.scrapers.links;

import co.jware.sojka.core.domain.search.MetaLink;
import io.reactivex.Flowable;
import io.reactivex.schedulers.Schedulers;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.springframework.stereotype.Component;

import java.net.URI;
import java.util.List;
import java.util.Optional;

import static java.util.stream.Collectors.toList;

@Component
public class CareerJetCzLinkExtractor implements Extractor {
    @Override
    public List<MetaLink> extractLinks(Document doc, String baseUri) {
        return doc.select("div.job").stream()
                .map(e -> doBuildLink(baseUri, e))
                .collect(toList());
    }

    @Override
    public Flowable<MetaLink> extractLinksRx(Document doc, String baseUri) {
        return Flowable.fromIterable(doc.select("div.job"))
                .flatMap(e -> Flowable.fromCallable(() -> doBuildLink(baseUri, e))
                        .subscribeOn(Schedulers.computation()));
    }

    @NotNull
    MetaLink doBuildLink(String baseUri, Element e) {
        MetaLink.Builder builder = Extractor.emptyBuilder()
                .baseUri(baseUri);
        Element link = e.select("a.title-company").first();
        String href = link.attr("abs:href");
        String title = link.text();
        Element data = e.select(".company_compact").first();
        String company = data.text();
        String location = e.select(".locations_compact").text();
        builder.address(location);
        if (StringUtils.isNoneBlank(company)) {
            builder.company(company);
        }
        String description = e.select(".advertise_compact").text();
        builder.summary(description);
        builder.title(title);
        builder.href(href);
        return builder.build();
    }

    public URI getNextPageUrl(Document doc) {
        Element childSelect = doc.select("span.square-class").first();
        Element select = childSelect.parent();
        return Optional.ofNullable(select).map(e -> {
            try {
                String href = e.attr("href");
                return new URI(EXTRACTOR_CAREER_JET_CZ + href);
            } catch (Exception ex) {
                return null;
            }
        }).orElse(null);
    }

    @Override
    public String extractorKey() {
        return EXTRACTOR_CAREER_JET_CZ;
    }
}
