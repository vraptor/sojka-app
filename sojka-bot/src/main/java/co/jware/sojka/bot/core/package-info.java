@Value.Style(typeImmutable = "*", jdkOnly = true)
package co.jware.sojka.bot.core;

import org.immutables.value.Value;