package co.jware.sojka.bot.jobs.scrapers.uris;

import java.net.URI;
import java.util.List;

import static java.util.Optional.ofNullable;

public abstract class BaseUriBuilder implements UriBuilder {

    abstract URI baseUri();

    @Override
    public boolean supportsAny(List<URI> baseUris) {
        return ofNullable(baseUris)
                .map(uris -> uris.contains(this.baseUri()))
                .orElse(false);
    }
}
