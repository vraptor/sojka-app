package co.jware.sojka.bot.jobs.scrapers.links;

import co.jware.sojka.core.domain.search.MetaLink;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.springframework.stereotype.Component;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import static java.util.stream.Collectors.toList;

@Component
public class CzechCrunchJobsCzLinksExtractor implements Extractor {
    @Override
    public List<MetaLink> extractLinks(Document doc, String baseUri) {
        return doc.select("#search-result div.panel-title").stream().map(e -> {
            MetaLink.Builder builder = Extractor.emptyBuilder()
                    .baseUri(baseUri);
            Element name = e.select("a.panel-link.text-primary").first();
            String link = name.attr("abs:href");
            String title = name.text();
            Element comp = e.select("a.text-small.text-light-gray").first();
            String company = comp.text();
            title = title.replaceFirst("TOP", "");
            Element locate = e.select("span.text-small.text-gray").first();
            if (locate.hasText()) {
                String location = locate.text();
                location = location.replaceAll("^\\W+\\b", "");
                builder.address(location);
            }
            builder.company(company)
                    .title(title)
                    .href(link);
            return builder;
        })
                .map(MetaLink.Builder::build)
                .collect(toList());
    }

    @Override
    public URI getNextPageUrl(Document doc) {
        Element child = doc.select("span.sr-only").first();
        Element parent = child.parent();
        String url = EXTRACTOR_CRUNCHJOBS_CZ + parent.attr("href");
        return Optional.ofNullable(parent)
                .map(e -> {
                    try {
                        return new URI(url);
                    } catch (URISyntaxException ex) {
                        return null;
                    }
                })
                .orElse(null);
    }

    @Override
    public String extractorKey() {
        return Extractor.EXTRACTOR_CRUNCHJOBS_CZ;
    }
}
