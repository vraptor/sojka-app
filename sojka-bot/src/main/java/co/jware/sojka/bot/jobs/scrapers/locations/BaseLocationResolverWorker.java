package co.jware.sojka.bot.jobs.scrapers.locations;


import co.jware.sojka.core.domain.search.MetaLocation;
import co.jware.sojka.core.domain.search.MetaLocationResult;
import com.beust.jcommander.internal.Sets;

import java.util.Set;

public abstract class BaseLocationResolverWorker implements LocationResolverWorker {

    private LocationResolverWorker next;

    public BaseLocationResolverWorker() {
    }

    protected BaseLocationResolverWorker(LocationResolverWorker next) {
        this.next = next;
    }

    public MetaLocationResult resolve(MetaLocation location) {
        MetaLocationResult result = this.doResolve(location);
        if (next != null) {
            MetaLocationResult resolve = next.resolve(result.location());
            Set<String> resolvedLocations = Sets.newHashSet();
            resolvedLocations.addAll(result.resolvedLocations());
            resolvedLocations.addAll(resolve.resolvedLocations());
            return result.withResolvedLocations(resolvedLocations);
        }
        return result;
    }

    protected abstract MetaLocationResult doResolve(MetaLocation location);
}
