package co.jware.sojka.bot.jobs.scrapers.links;

import co.jware.sojka.core.domain.search.MetaLink;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.springframework.stereotype.Component;

import java.net.URI;
import java.util.Collections;
import java.util.List;

import static java.util.stream.Collectors.toList;

//@Component
public class StartupJobsCzLinksExtractor implements Extractor {
    @Override
    public List<MetaLink> extractLinks(Document doc, String baseUri) {
        throw new UnsupportedOperationException("Not implemented");
//        return doc.select("tr.offer")
//                .stream()
//                .map(e -> {
//                    MetaLink.Builder builder = MetaLink.builder()
//                            .baseUri(EXTRACTOR_STARTUPJOBS_CZ)
//                            .address("n/a")
//                            .company("n/a")
//                            .href("n/a")
//                            .title("n/a");
//                    Element nalin = e.select("td.positionCol h3 a").first();
//                    String title = nalin.text();
//                    String link = EXTRACTOR_STARTUPJOBS_CZ + nalin.attr("href");
//                    Element comp = e.select("span.startupName.mobile-only").first();
//                    String company = comp.text();
//                    Element adr = e.select("td.cityCol").first();
//                    String address = adr.text();
//                    builder.href(link).company(company).title(title).address(address);
//                    return builder;
//                })
//                .map(MetaLink.Builder::build)
//                .collect(toList());
    }

    @Override
    public URI getNextPageUrl(Document doc) {
        return null;
    }

    @Override
    public String extractorKey() {
//        return EXTRACTOR_STARTUPJOBS_CZ;
        throw new UnsupportedOperationException("Not implemented");
    }
}
