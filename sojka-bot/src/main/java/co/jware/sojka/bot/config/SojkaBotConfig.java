package co.jware.sojka.bot.config;

import co.jware.sojka.bot.jobs.RequestBuilder;
import co.jware.sojka.bot.jobs.scrapers.links.Extractor;
import co.jware.sojka.core.domain.search.MetaLink;
import co.jware.sojka.core.domain.search.MetaSearch;
import co.jware.sojka.core.domain.search.MetaSearchLink;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import com.hazelcast.core.IQueue;
import com.squareup.okhttp.OkHttpClient;
import io.vertx.core.Vertx;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.eventbus.MessageCodec;
import org.apache.tika.langdetect.OptimaizeLangDetector;
import org.apache.tika.language.detect.LanguageDetector;
import org.asynchttpclient.AsyncHttpClient;
import org.asynchttpclient.DefaultAsyncHttpClientConfig;
import org.asynchttpclient.Dsl;
import org.asynchttpclient.extras.rxjava2.DefaultRxHttpClient;
import org.asynchttpclient.extras.rxjava2.RxHttpClient;
import org.jdeferred.DeferredManager;
import org.jdeferred.impl.DefaultDeferredManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.ResolvableType;
import org.springframework.core.task.AsyncTaskExecutor;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;
import org.springframework.retry.annotation.EnableRetry;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.web.servlet.config.annotation.AsyncSupportConfigurer;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.OffsetDateTime;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static java.util.stream.Collectors.toMap;

@Configuration
@EnableRetry
public class SojkaBotConfig implements WebMvcConfigurer {
    public static final String META_SEARCH_QUEUE = "metaSearchQueue";
    public static final String META_SEARCH_LINKS_QUEUE = "metaSearchLinksQueue";
    public static final String META_SEARCHES_MAP = "meta_searches";
    public static final String META_LINKS_MAP = "map_meta_links";
//    @Autowired
//    private List<MessageCodec> codecs;

    @Autowired
    private List<Extractor> extractors;

    @Bean
    public Jackson2ObjectMapperBuilder jacksonBuilder() {
        Jackson2ObjectMapperBuilder b = new Jackson2ObjectMapperBuilder();
        b.indentOutput(true)
                .serializationInclusion(JsonInclude.Include.NON_NULL)
                .dateFormat(new SimpleDateFormat("yyyy-MM-dd"));
        return b;
    }

    @Bean
    public AsyncHttpClient asyncHttpClient() {
        DefaultAsyncHttpClientConfig.Builder configBuilder = Dsl.config();
        configBuilder.setFollowRedirect(true).setKeepAlive(true);
        return Dsl.asyncHttpClient(configBuilder);
    }

    @Bean
    public RxHttpClient rxHttpClient() {
        return new DefaultRxHttpClient(asyncHttpClient());
    }

    @Bean
    public RequestBuilder requestBuilder() {
        return new RequestBuilder();
    }

    @Bean
    public Map<String, Extractor> baseUriExtractorMap() {
        return extractors.stream()
                .collect(toMap(Extractor::extractorKey, e -> e));
    }


    @Bean
    public EventBus eventBus(List<MessageCodec> codecs) {
        // TODO: 10/28/2018 DO we need event bus ?
        EventBus eventBus = Vertx.vertx().eventBus();
        codecs.forEach(codec -> {
            ResolvableType type = ResolvableType.forClass(MessageCodec.class, codec.getClass());
            ResolvableType[] generics = type.getGenerics();
            Class<?> clazz = generics[0].getRawClass();
            eventBus.registerDefaultCodec(clazz, codec);
        });
        return eventBus;
    }

    @Bean
    public IQueue<MetaSearchLink> metaSearchLinkQueue(@Qualifier("hazelcastInstance") HazelcastInstance hz) {
        return hz.getQueue(META_SEARCH_LINKS_QUEUE);
    }

    @Bean
    public IMap<MetaSearch, OffsetDateTime> metaSearchMap(@Qualifier("hazelcastInstance") HazelcastInstance hz) {
        return hz.getMap(META_SEARCHES_MAP);
    }

    @Bean
    public IMap<MetaLink, Boolean> metaLinksMap(@Qualifier("hazelcastInstance") HazelcastInstance hz) {
        return hz.getMap(META_LINKS_MAP);
    }

    @Bean
    public IQueue<MetaSearch> metaSearchQueue(@Qualifier("hazelcastInstance") HazelcastInstance hz) {
        return hz.getQueue(META_SEARCH_QUEUE);
    }

    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**")
                .allowedOrigins("*").allowedMethods("GET", "POST");
    }

    @Bean
    public AsyncTaskExecutor taskExecutor() {
        ThreadPoolTaskExecutor taskExecutor = new ThreadPoolTaskExecutor();
        taskExecutor.setCorePoolSize(16);
        taskExecutor.setMaxPoolSize(32);
        taskExecutor.setQueueCapacity(200);
        return taskExecutor;
    }

    @Override
    public void configureAsyncSupport(AsyncSupportConfigurer configurer) {
        configurer.setTaskExecutor(taskExecutor())
                .setDefaultTimeout(10);
    }

    @Bean
    public DeferredManager deferredManager() {
        ExecutorService executorService = Executors.newScheduledThreadPool(20);
        return new DefaultDeferredManager(executorService);
    }

    @Bean
    public LanguageDetector languageDetector() throws IOException {
        OptimaizeLangDetector languageDetector = new OptimaizeLangDetector();
        languageDetector.loadModels();
        return languageDetector;
    }

    @Bean
    public OkHttpClient httpClient() {
        return new OkHttpClient();
    }
}
