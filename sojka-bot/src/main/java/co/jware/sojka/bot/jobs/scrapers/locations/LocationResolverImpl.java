package co.jware.sojka.bot.jobs.scrapers.locations;

import co.jware.sojka.core.domain.search.MetaLocation;
import co.jware.sojka.core.domain.search.MetaLocationResult;
import org.springframework.stereotype.Component;

import java.util.Collections;

@Component
public class LocationResolverImpl implements LocationResolver {
    @Override
    public MetaLocationResult resolve(MetaLocation location) {
        return MetaLocationResult.builder()
                .location(location)
                .resolvedLocations(Collections.emptySet())
                .build();
    }
}
