package co.jware.sojka.bot.jobs.scrapers.pages.impl;

import co.jware.sojka.bot.jobs.scrapers.SiteInfo;
import co.jware.sojka.core.domain.search.page.PageMetaData;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.springframework.stereotype.Component;
import org.springframework.web.util.UriComponentsBuilder;

@Component
public class PraceCzPageDetailExtractor extends BasePageDetailExtractor {

    public PraceCzPageDetailExtractor() {
        super();
        siteInfo = SiteInfo.PRACE_CZ;
    }

    @Override
    public String extractRichText(Document doc) {
        String location = doc.location();
//        if (isRedirected(location)) {
        Elements cseMarker = doc.select("#cse-gtm");
        Elements g2Marker = doc.select(".g2-detail");
        if (!cseMarker.isEmpty()) {
            return "cse";
        } else if (!g2Marker.isEmpty()) {
            return g2Marker.text();
        } else {
            return doc.select(".advert__richtext").text();
        }
//        return Commons.NOT_AVAILABLE;
    }

    private boolean isRedirected(String location) {
        String locationHost = UriComponentsBuilder.fromHttpUrl(location).build().getHost();
        String siteHost = siteInfo.baseUri().getHost();
        return !siteHost.equals(locationHost);
    }

    @Override
    public PageMetaData extractMetaData(Document doc) {
        Elements info = doc.select("div:has(dl).double-standalone");
        PageMetaData result = this.emptyPageMetaData;
        if (info.size() > 0) {
            String rawData = info.select("dl").get(0).outerHtml();
            result = result.withRawData(rawData);
        }
        result = result.withLocale(pageLocale(doc));
        return result;
    }

}
