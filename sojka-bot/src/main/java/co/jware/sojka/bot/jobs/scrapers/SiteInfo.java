package co.jware.sojka.bot.jobs.scrapers;

import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;

public enum SiteInfo {
    BAZOSPRACE_CZ("https://prace.bazos.cz"),
    CAREER_JET_CZ("https://www.careerjet.cz"),
    CHCI_PRACI_CZ("https://www.chcipraci.cz"),
    CRUNCHJOBS_CZ("https://jobs.czechcrunch.cz"),
    DOBRA_PRACE_CZ("https://www.dobraprace.cz"),
    EASY_PRACE_CZ("https://www.easy-prace.cz"),
    EXPATS_CZ("https://www.expats.cz"),
    HYPERPRACE_CZ("http://www.hyperprace.cz"),
    JEN_PRACE_CZ("https://www.jenprace.cz"),
    JOBLIST_CZ("http://www.joblist.cz"),
    JOBS_CZ("https://www.jobs.cz"),
    MONSTER_CZ("https://www.monster.cz"),
    PRACE_CZ("https://www.prace.cz"),
    PRACE_JOBCITY_CZ("https://www.prace-jobcity.cz"),
    PRACEPRACE_CZ("http://praceprace.cz"),
    PROFESIA_CZ("https://www.profesia.cz"),
    SUPERKARIERA_CZ("https://www.superkariera.cz");
    //// TODO: 9/27/2017 : Implement support for javascript generated sites
    //    STARTUPJOBS_CZ("https://www.startupjobs.cz");

    private final URI baseUri;

    SiteInfo(String baseUri) {
        UriComponents uriComponents = UriComponentsBuilder
                .fromHttpUrl(baseUri)
                .build(true);
        this.baseUri = uriComponents.toUri();
    }

    public URI baseUri() {
        return this.baseUri;
    }
}
