package co.jware.sojka.bot.jobs.scrapers.links;

import co.jware.sojka.core.domain.search.MetaLink;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.List;

import static java.util.stream.Collectors.toList;

@Component
public class SuperkarieraCzLinksExtractor implements Extractor {

    private final Logger logger = LoggerFactory.getLogger(SuperkarieraCzLinksExtractor.class);

    private final MetaLink emptyMetaLink = Extractor.emptyBuilder()
            .baseUri(EXTRACTOR_SUPERKARIERA_CZ)
            .build();

    @Override
    public List<MetaLink> extractLinks(Document doc, String baseUri) {
        return doc.select("div.jobad").stream()
                .map(e -> {
                    MetaLink metaLink = MetaLink.copyOf(emptyMetaLink);
                    Element link = e.select("a").first();
                    String title = link.text();
                    String href = link.attr("href");
                    if (href.startsWith("//")) {
                        href = "https:" + href;
                    }
                    String company = e.select("p.bottom").first().text();
                    String[] parts = company.split("-");
                    metaLink = metaLink
                            .withTitle(title)
                            .withHref(href)
                            .withCompany(parts[0].trim()).withAddress(parts[1].trim());
                    if (logger.isTraceEnabled()) {
                        logger.trace("MetaLink: " + metaLink.toString());
                    }
                    return metaLink;

                }).collect(toList());
    }

    @Override
    public String extractorKey() {
        return Extractor.EXTRACTOR_SUPERKARIERA_CZ;
    }
}
