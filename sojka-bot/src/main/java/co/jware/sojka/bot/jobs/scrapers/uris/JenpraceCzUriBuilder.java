package co.jware.sojka.bot.jobs.scrapers.uris;

import co.jware.sojka.bot.jobs.scrapers.SiteInfo;
import co.jware.sojka.core.domain.search.MetaSearch;
import com.beust.jcommander.internal.Lists;
import org.springframework.stereotype.Component;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.util.List;

import static java.util.Optional.ofNullable;

@Component
public class JenpraceCzUriBuilder extends BaseUriBuilder {

    private final UriComponentsBuilder builder = UriComponentsBuilder.fromUri(baseUri())
            .path("nabidky");

    @Override
    public List<URI> buildUris(MetaSearch search) {
        List<URI> result = Lists.newArrayList();
        UriComponentsBuilder builderClone = this.builder.cloneBuilder();
        filterKeyWords(search).forEach(k -> {
            builderClone.queryParam("subjects[]", k);
        });
        int page = search.page();
        ofNullable(search.locations())
                .ifPresent(locations -> {
                    filterLocations(search).forEach(loc -> {
                        builderClone.queryParam("locations[]", loc);
                    });
                });
        if (page > 1) {
            builderClone.queryParam("page", page);
        }
        result.add(builderClone
                .build()
                .encode()
                .toUri());
        return result;
    }

    @Override
    URI baseUri() {
        return SiteInfo.JEN_PRACE_CZ.baseUri();
    }
}
