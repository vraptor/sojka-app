package co.jware.sojka.bot.jobs.scrapers.executors.impl;


import co.jware.sojka.bot.jobs.RequestBuilder;
import co.jware.sojka.bot.jobs.scrapers.SiteInfo;
import co.jware.sojka.bot.jobs.scrapers.executors.MetaSearchExecutor;
import co.jware.sojka.bot.jobs.scrapers.executors.MetaSearchExecutorHelper;
import co.jware.sojka.bot.jobs.scrapers.links.Extractor;
import co.jware.sojka.core.Events;
import co.jware.sojka.core.domain.search.Commons;
import co.jware.sojka.core.domain.search.MetaLink;
import co.jware.sojka.core.domain.search.MetaSearch;
import co.jware.sojka.core.domain.search.MetaSearchResult;
import co.jware.sojka.core.service.MetaLinkService;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import io.reactivex.Flowable;
import io.reactivex.Maybe;
import io.reactivex.Observable;
import io.reactivex.Scheduler;
import io.reactivex.internal.schedulers.ExecutorScheduler;
import io.reactivex.schedulers.Schedulers;
import io.vertx.core.Handler;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.eventbus.Message;
import org.asynchttpclient.AsyncHttpClient;
import org.asynchttpclient.Response;
import org.asynchttpclient.extras.rxjava2.RxHttpClient;
import org.jdeferred.DeferredManager;
import org.jsoup.Jsoup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.core.task.TaskExecutor;
import org.springframework.stereotype.Component;

import java.net.URI;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.concurrent.TimeUnit;

import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toSet;

@Component("metaSearchExecutor")
public class MetaSearchExecutorImpl implements MetaSearchExecutor, ApplicationListener<ContextRefreshedEvent> {

    private static final Logger logger = LoggerFactory.getLogger(MetaSearchExecutorImpl.class);
    public static final int MAX_PAGES = 100;


    @Autowired
    private MetaLinkService metaLinkService;
    @Autowired
    private AsyncHttpClient asyncHttpClient;
    @Autowired
    private RxHttpClient rxHttpClient;
    @Autowired
    private RequestBuilder requestBuilder;
    @Autowired
    @Qualifier("baseUriExtractorMap")
    private Map<String, Extractor> baseUriExtractorMap;
    @Autowired
    private MetaSearchExecutorHelper executorHelper;
    @Autowired
    private EventBus eventBus;

    @Value("${system.batch.crawl.enable:true}")
    private boolean crawlEnabled;
    @Value("${system.batch.crawl.initial_delay:15}")
    private int initialDelay;
    @Value("${system.batch.crawl.period:75}")
    private long crawlPeriod;
    @Value("${system.batch.crawl.count:500}")
    private long crawlCount;
    @Value("${system.batch.crawl.timeout:12000}")
    private int crawlTimeout;
    @Qualifier("hazelcastInstance")
    @Autowired
    private HazelcastInstance hazelcastInstance;
    @Autowired
    private IMap<MetaLink, Boolean> metaLinksMap;
    @Autowired
    private TaskExecutor taskExecutor;
    @Autowired
    private DeferredManager deferredManager;

    @Override
    public Observable<MetaSearchResult> metaSearchResult(String url) {
        return fetch(url)
                .flatMap(r -> Maybe.just(MetaSearchResult.builder()
                        .statusCode(r.getStatusCode())
                        .htmlPayload(r.getResponseBody(StandardCharsets.UTF_8))
                        .uri(r.getUri().toJavaNetURI())
                        .build()))
                .toObservable();
    }

    @Override
    public Flowable<MetaSearchResult> metaSearchResults(Collection<String> urls) {
        return Flowable.fromIterable(urls)
                .flatMap(url -> fetch(url)
                        .doOnError(t -> logger.error("Error fetching url {}", url))
                        .onErrorResumeNext(Maybe.empty())
                        .toFlowable())
                .flatMap((Response r) -> Flowable.just(
                        MetaSearchResult
                                .builder()
                                .statusCode(r.getStatusCode())
                                .htmlPayload(r.getResponseBody(StandardCharsets.UTF_8))
                                .uri(r.getUri().toJavaNetURI())
                                .build()));
    }

    @Override
    public Flowable<MetaLink> metaLinks(MetaSearch metaSearch) {
        if (metaSearch != null) {
            List<URL> urls = requestBuilder.buildUri(metaSearch);
            Collections.shuffle(urls);
            List<String> urlList = urls.stream()
                    .map(URL::toString)
                    .collect(toList());
            int nextPage = metaSearch.page() + 1;
            if (nextPage < MAX_PAGES) {
                return extractLinks(metaSearchResults(urlList))
                        .concatWith(Flowable.defer(() -> metaLinks(metaSearch.withPage(nextPage))));
            }
            return Flowable.empty();
        }
        return Flowable.empty();
    }

    @Override
    public Flowable<MetaLink> metaLinks(MetaSearch metaSearch, Set<SiteInfo> siteInfos) {
        if (metaSearch != null) {
            List<URI> baseUris = siteInfos.stream().map(SiteInfo::baseUri).collect(toList());
            List<URL> urls = requestBuilder.buildUri(metaSearch, baseUris);
            int nextPage = metaSearch.page() + 1;
            if (nextPage < MAX_PAGES) {
                Set<String> urlSet = urls.stream().map(URL::toString).collect(toSet());
                return extractLinks(metaSearchResults(urlSet))
                        .concatWith(Flowable.defer(() -> this.metaLinks(metaSearch.withPage(nextPage), siteInfos)));
            }
            return Flowable.empty();
        }
        return Flowable.empty();
    }

    @Override
    public Flowable<MetaSearchResult> metaSearchResults(MetaSearch search) {
        return requestBuilder.buildUris(search)
                .flatMap(uri -> rxHttpClient.prepare(asyncHttpClient
                        .prepareGet(uri.toString()).build()).onErrorComplete().toFlowable())
                .map(r -> MetaSearchResult.builder()
                        .uri(r.getUri().toJavaNetURI())
                        .htmlPayload(r.getResponseBody(StandardCharsets.UTF_8))
                        .statusCode(r.getStatusCode())
                        .build());
    }


    public Maybe<Response> fetch(String url) {
        logger.info("Fetching URL: {}", url);
        return rxHttpClient.prepare(asyncHttpClient.prepareGet(url)
                .setRequestTimeout(crawlTimeout).build())
                .onErrorComplete();
    }

    public Flowable<MetaLink> extractLinks(Flowable<MetaSearchResult> result) {
        return result.flatMap(m -> Flowable.fromCallable(() -> Jsoup.parse(m.htmlPayload(), m.baseUri().toString()))
                .flatMap(doc -> Flowable.fromCallable(() -> {
                    URI uri = m.baseUri();
                    Extractor extractor;
                    List<MetaLink> metaLinks = Collections.emptyList();
                    try {
                        String uriString = uri.toString();
                        // TODO: 11/12/2018 Make extractor selection more robust.
                        extractor = baseUriExtractorMap.get(uriString);
                        metaLinks = extractor.extractLinks(doc, uriString);
                    } catch (Exception e) {
                        logger.error("Extraction failed for {}: {}", uri, e.getMessage());
                    }
                    return metaLinks;
                }))
                .flatMap(Flowable::fromIterable));
    }

    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {
        MetaSearch search = MetaSearch.builder().build();
        metaLinksMap = hazelcastInstance.getMap(Commons.META_LINKS_MAP);
        Scheduler scheduler = new ExecutorScheduler(taskExecutor);
        Flowable.interval(initialDelay, crawlPeriod, TimeUnit.SECONDS, scheduler)
                .forEach(aLong -> {
                    if (crawlEnabled) {
                        doExecuteSearch(search);
                    }
                });
        eventBus.consumer(Events.META_SEARCH_SUBMIT,
                (Handler<Message<MetaSearch>>) event -> doExecuteSearch(event.body()));
    }

    private void doExecuteSearch(MetaSearch search) {
        MetaSearch runnableSearch = executorHelper.findNextSearch(search);
        logger.debug("Executing search: {}", runnableSearch);
        metaLinks(runnableSearch)
                .subscribeOn(Schedulers.io())
                .filter(metaLink -> !"n/a".equalsIgnoreCase(metaLink.href()))
                .filter(metaLink -> !metaLinksMap.containsKey(metaLink))
                .take(crawlCount)
                .buffer(50)
                .flatMap(Flowable::fromIterable)
                .subscribe(
                        metaLink -> {
                            MetaLink link = executorHelper.enhanceMetaLink(metaLink, runnableSearch);
                            this.processMetaLink(link);
                            metaLinksMap.putIfAbsent(link, true);
                        },
                        err -> logger.error("Error crawling", err),
                        () -> logger.info("Map has {} entries", metaLinksMap.size()));
    }

    private void processMetaLink(MetaLink m) throws InterruptedException {
        deferredManager.when(() -> metaLinkService.findOrCreate(m))
                .then((MetaLink result) -> {
                    eventBus.publish(Events.META_LINK_SUBMIT, result);
                })
                .done(ml -> logger.info("Meta link: {}", ml.href()))
                .fail(t -> logger.error("Error processing link ", t));
    }
}
