package co.jware.sojka.bot.jobs.scrapers.pages.impl;

import co.jware.sojka.bot.jobs.scrapers.SiteInfo;
import co.jware.sojka.bot.jobs.scrapers.pages.PageDetailExtractor;
import co.jware.sojka.core.domain.search.Commons;
import co.jware.sojka.core.domain.search.page.PageDetail;
import co.jware.sojka.core.domain.search.page.PageMetaData;
import co.jware.sojka.core.enums.HirerType;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang.StringUtils;
import org.apache.tika.exception.TikaException;
import org.apache.tika.io.IOUtils;
import org.apache.tika.language.detect.LanguageDetector;
import org.apache.tika.language.detect.LanguageResult;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.parser.AutoDetectParser;
import org.apache.tika.parser.html.BoilerpipeContentHandler;
import org.apache.tika.sax.BodyContentHandler;
import org.jetbrains.annotations.NotNull;
import org.jsoup.nodes.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.io.InputStream;
import java.util.Locale;
import java.util.Optional;


public abstract class BasePageDetailExtractor implements PageDetailExtractor {

    static final private Logger logger = LoggerFactory.getLogger(BasePageDetailExtractor.class);

    final PageMetaData emptyPageMetaData = PageMetaData.builder()
            .company(Commons.NOT_AVAILABLE)
            .locale(Locale.getDefault())
            .hirerType(HirerType.EMPLOYER)
            .build();
    @Autowired
    ObjectMapper mapper;

    @Autowired
    private LanguageDetector languageDetector;

    protected SiteInfo siteInfo;

    @Override
    public PageDetail extract(Document doc) {
        String richText = doExtractText(doc);
        richText = StringUtils.isNotBlank(richText) ? richText : Commons.NOT_IMPLEMENTED;
        PageMetaData pageMetaData = extractMetaData(doc)
                .withLocale(doExtractLocale(richText));
        return PageDetail.builder()
                .richText(richText)
                .pageMetaData(pageMetaData)
                .url(doc.location())
                .title(doc.title())
                .build();
    }

    @NotNull
    private Locale doExtractLocale(String richText) {
        if (StringUtils.isBlank(richText)) {
            return Locale.getDefault();
        }
        LanguageResult languageResult = languageDetector.detect(richText);
        String language = languageResult.getLanguage();
        if (StringUtils.isNotBlank(language)) {
            return new Locale(language);
        }
        return Locale.getDefault();
    }

    @Override
    public abstract String extractRichText(Document doc);


    private String doExtractText(Document doc) {
        String richText = extractRichText(doc);
        BoilerpipeContentHandler handler = new BoilerpipeContentHandler(new BodyContentHandler());
        Metadata metadata = new Metadata();
        AutoDetectParser parser = new AutoDetectParser();
        richText =
//                StringUtils.isNotBlank(richText) ? richText :
                        doc.outerHtml();
        try (InputStream stream = IOUtils.toInputStream(richText);) {
            parser.parse(stream, handler, metadata);
        } catch (IOException | SAXException | TikaException e) {
            logger.error("error extracting document text", e);
        }
        return handler.getTextDocument().getText(true, false);
    }


    @Override
    public abstract PageMetaData extractMetaData(Document doc);

    @Override
    public String baseUri() {
        return siteInfo.baseUri().toString();
    }

    Locale pageLocale(Document doc) {
        Locale locale = Locale.getDefault();
        if (doc.select("html").hasAttr("lang")) {
            String lang = doc.select("html").attr("lang");
            locale = Optional.ofNullable(lang)
                    .map(Locale::forLanguageTag)
                    .orElse(locale);
        }
        return locale;
    }
}
