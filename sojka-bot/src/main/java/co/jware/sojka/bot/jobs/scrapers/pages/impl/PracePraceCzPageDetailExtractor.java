package co.jware.sojka.bot.jobs.scrapers.pages.impl;

import co.jware.sojka.bot.jobs.scrapers.SiteInfo;
import co.jware.sojka.core.domain.search.MetaLink;
import co.jware.sojka.core.domain.search.page.PageMetaData;
import co.jware.sojka.core.enums.HirerType;
import co.jware.sojka.core.service.MetaLinkService;
import org.jsoup.nodes.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.Locale;

@Component
public class PracePraceCzPageDetailExtractor extends BasePageDetailExtractor {

    @Autowired
    private MetaLinkService metaLinkService;

    public PracePraceCzPageDetailExtractor() {
        super();
        siteInfo = SiteInfo.PRACEPRACE_CZ;
    }

    @Override
    public String extractRichText(Document doc) {
        // TODO: 11/11/2018  Create metalink
        String richText = "Extracted metalink";
        String adPageUrl = doc.select("frame[name=adpage]").first().absUrl("src");
        String baseUri = UriComponentsBuilder.fromHttpUrl(adPageUrl)
                .replacePath("")
                .replaceQuery("")
                .build()
                .toUriString();
        MetaLink metaLink = MetaLink.builder()
                .title(doc.title())
                .href(adPageUrl)
                .baseUri(baseUri)
                .build();
        metaLinkService.findOrCreate(metaLink);
        //        try {
//            // TODO: 11/12/2018 Extract metalink only
//            Document adPageDoc = Jsoup.connect(adPageUrl).validateTLSCertificates(false).get();
//            richText = adPageDoc.outerHtml();
//
//        } catch (IOException e) {
//            throw new RuntimeException(e);
//        }
        return richText;
    }

    @Override
    public PageMetaData extractMetaData(Document doc) {
        return PageMetaData.builder()
                .locale(Locale.getDefault())
                .company("Damage Inc.")
                .hirerType(HirerType.UNKNOWN)
                .build();
    }
}
