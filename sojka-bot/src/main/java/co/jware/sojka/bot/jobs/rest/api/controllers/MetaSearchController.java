package co.jware.sojka.bot.jobs.rest.api.controllers;

import co.jware.sojka.bot.jobs.rest.api.service.MetaSearchApiSvc;
import co.jware.sojka.core.domain.search.MetaLink;
import co.jware.sojka.core.domain.search.MetaSearch;
import co.jware.sojka.rest.exceptions.HttpNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.PagedResources;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.ResourceAssembler;
import org.springframework.hateoas.mvc.ControllerLinkBuilder;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;
import java.util.UUID;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping("/meta-search")
@CrossOrigin()
public class MetaSearchController {

    @Autowired
    private MetaSearchApiSvc metaSearchApiSvc;
    private ResourceAssembler<MetaLink, Resource<MetaLink>> resourceAssembler = entity -> new Resource<>(entity);

    @PostMapping(consumes = APPLICATION_JSON_VALUE,
            produces = APPLICATION_JSON_VALUE)
    public PagedResources<Resource<Resource<MetaLink>>> executeMetaSearch(@RequestBody MetaSearch search, Pageable page,
                                                                          PagedResourcesAssembler<Resource<MetaLink>> assembler) {
        PagedResources<Resource<Resource<MetaLink>>> resources = prepareSearchResults(search, page, assembler);
        if (!resources.getContent().isEmpty()) {
            return resources;
        }
        throw new HttpNotFoundException("MetaSearch returned no data");
    }

    @GetMapping("/")
    public PagedResources<Resource<Resource<MetaLink>>> getDefaultSearch(@PageableDefault Pageable page,
                                                                         PagedResourcesAssembler<Resource<MetaLink>> assembler) {
        MetaSearch metaSearch = metaSearchApiSvc.defaultSearch();
        return prepareSearchResults(metaSearch, page, assembler);
    }

    @GetMapping(path = "/{uuid}")
    public PagedResources<Resource<Resource<MetaLink>>> getMetaSearch(@PathVariable UUID uuid, @PageableDefault() Pageable page,
                                                                      PagedResourcesAssembler<Resource<MetaLink>> assembler) {

        Optional<MetaSearch> search = metaSearchApiSvc.getSearch(uuid);
        return search.map(s -> prepareSearchResults(s, page, assembler))
                .orElseThrow(() -> new HttpNotFoundException(String.format("MetaSearch with uuid %s not found", uuid)));
    }

    private PagedResources<Resource<Resource<MetaLink>>> prepareSearchResults(MetaSearch search, Pageable page,
                                                                              PagedResourcesAssembler<Resource<MetaLink>> assembler) {
        // FIXME: 5/11/2017 Loses keywords and/or locations
        MetaSearch metaSearch = metaSearchApiSvc.findOrCreate(search.withPage(page.getPageNumber() + 1));
        ControllerLinkBuilder controllerLinkBuilder = ControllerLinkBuilder.linkTo(MetaLinkController.class);
        Link selfLink = ControllerLinkBuilder.linkTo(this.getClass()).slash(metaSearch.uuid()).withSelfRel();
        return assembler.toResource(metaSearchApiSvc.getMetaLinks(search, page)
                .map(resourceAssembler::toResource)
                .map(r -> {
                    r.add(controllerLinkBuilder.slash(r.getContent().uuid()).withSelfRel());
                    return r;
                }), selfLink);
    }
}
