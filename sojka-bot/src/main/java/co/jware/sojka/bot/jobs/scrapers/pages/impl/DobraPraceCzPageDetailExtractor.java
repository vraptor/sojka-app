package co.jware.sojka.bot.jobs.scrapers.pages.impl;

import co.jware.sojka.bot.jobs.scrapers.SiteInfo;
import co.jware.sojka.core.domain.search.page.PageMetaData;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.safety.Whitelist;
import org.jsoup.select.Elements;
import org.springframework.stereotype.Component;
import sun.util.locale.LocaleUtils;

import java.util.Locale;
import java.util.Optional;

@Component
public class DobraPraceCzPageDetailExtractor extends BasePageDetailExtractor {
    public DobraPraceCzPageDetailExtractor() {
        super();
        siteInfo = SiteInfo.DOBRA_PRACE_CZ;
    }

    @Override
    public String extractRichText(Document doc) {
        String html = doc.outerHtml();
        return Jsoup.clean(html, Whitelist.simpleText());
    }

    @Override
    public PageMetaData extractMetaData(Document doc) {
        return PageMetaData.empty();
    }
}
