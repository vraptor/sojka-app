package co.jware.sojka.bot.jobs.scrapers.links;

import co.jware.sojka.bot.jobs.scrapers.SiteInfo;
import co.jware.sojka.core.domain.search.MetaLink;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.springframework.stereotype.Component;

import java.util.List;

import static java.util.stream.Collectors.toList;

@Component
public class ExpatsCzLinksExtractor implements Extractor {
    @Override
    public List<MetaLink> extractLinks(Document doc, String baseUri) {
        return doc.select("div.list_item").stream().map(e -> {
            MetaLink.Builder builder = Extractor.emptyBuilder()
                    .baseUri(baseUri);
            Element link = e.select("a.job-title").first();
            String title = link.text();
            String url = link.attr("abs:href");
            Element comp = e.select("p.desc").first();
            String company = comp.text();
            Element right = e.select("div.right span").first();
            if (right != null) {
                builder.address(right.text());
            }
            builder.title(title);

            builder.href(url);
            builder.company(company);
            return builder.build();
        }).collect(toList());

    }

    @Override
    public String extractorKey() {
        return EXTRACTOR_EXPATS_CZ;
    }
}
