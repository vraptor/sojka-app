package co.jware.sojka.bot.jobs.scrapers.pages.impl;

import co.jware.sojka.bot.jobs.scrapers.SiteInfo;
import co.jware.sojka.core.domain.search.Commons;
import co.jware.sojka.core.domain.search.page.PageMetaData;
import org.jsoup.nodes.Document;
import org.springframework.stereotype.Component;

@Component
public class CareerJetCzPageDetailExtractor extends BasePageDetailExtractor {

    public CareerJetCzPageDetailExtractor() {
        super();
        siteInfo = SiteInfo.CAREER_JET_CZ;
    }

    @Override
    public String extractRichText(Document doc) {
        return Commons.NOT_IMPLEMENTED;
    }

    @Override
    public PageMetaData extractMetaData(Document doc) {
        return PageMetaData.empty();
    }
}
