package co.jware.sojka.bot.jobs.scrapers.uris;

import co.jware.sojka.bot.jobs.scrapers.SiteInfo;
import co.jware.sojka.core.domain.search.MetaSearch;
import org.springframework.stereotype.Component;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toSet;

@Component
public class PracePraceCzUriBuilder extends BaseUriBuilder {
    UriComponentsBuilder builder = UriComponentsBuilder
            .fromUri(baseUri())
            .path("nabidka-prace/vysledky-hledani.htm")
            .queryParam("action", "search");
    private int pageSize = 20;

    @Override
    public List<URI> buildUris(MetaSearch search) {
        UriComponentsBuilder cloneBuilder = this.builder.cloneBuilder();
        Set<String> keyWords = filterKeyWords(search).collect(toSet());
        if (!keyWords.isEmpty()) {
            cloneBuilder.queryParam("cond[fulltext]", keyWords.stream().collect(joining(" ")));
        }
//        return filterKeyWords(search).map(k -> {
//            cloneBuilder.queryParam("cond[fulltext]", k);
        int page = search.page();
        if (page > 1) {
            int position = (page - 1) * pageSize;
            cloneBuilder.queryParam("list_param[from]", position);
        }
        return Collections.singletonList(cloneBuilder.build()
                .encode()
                .toUri());
//        }).collect(Collectors.toList());
    }

    @Override
    URI baseUri() {
        return SiteInfo.PRACEPRACE_CZ.baseUri();
    }
}
