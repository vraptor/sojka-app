package co.jware.sojka.bot.service.impl;

import co.jware.sojka.bot.core.OsmResponse;
import co.jware.sojka.bot.service.NominatimService;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.CollectionLikeType;
import com.fasterxml.jackson.databind.type.TypeFactory;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;
import org.springframework.stereotype.Service;
import org.springframework.web.util.UriComponentsBuilder;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Service("nominatimService")
public class DefaultNominatimService implements NominatimService {

    private final ObjectMapper objectMapper;
    private final OkHttpClient httpClient;

    public DefaultNominatimService(ObjectMapper objectMapper, OkHttpClient httpClient) {
        this.objectMapper = objectMapper;
        this.httpClient = httpClient;
    }

    @Override
    public List<OsmResponse> search(String searchQuery) throws IOException {
        return search(searchQuery, 1);
    }

    @Override
    public List<OsmResponse> search(String searchQuery, int limit) throws IOException {
        String url = "https://nominatim.openstreetmap.org/search";
        String fullUri = UriComponentsBuilder.fromHttpUrl(url)
                .queryParam("q", searchQuery)
                .queryParam("format", "jsonv2")
                .queryParam("email", "vraptor@jware.co")
                .queryParam("addressdetails", 1)
                .queryParam("limit", limit).build()
                .encode()
                .toUriString();
        Request request = new Request.Builder().url(fullUri).get().build();
        Response response = httpClient.newCall(request).execute();
        int status = response.code();
        if (status == 200) {
            String asString = response.body().string();
            TypeFactory typeFactory = this.objectMapper.getTypeFactory();
            CollectionLikeType arrayType = typeFactory.constructCollectionLikeType(ArrayList.class, OsmResponse.class);
            return objectMapper.readValue(asString, arrayType);
        }
        return Collections.emptyList();
    }
}

