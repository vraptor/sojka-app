package co.jware.sojka.bot.jobs.scrapers.pages;


import co.jware.sojka.core.domain.search.page.PageDetail;
import co.jware.sojka.core.domain.search.page.PageMetaData;
import org.jsoup.nodes.Document;

public interface PageDetailExtractor {
    String extractRichText(Document doc);

    PageMetaData extractMetaData(Document doc);

    PageDetail extract(Document doc);

    String baseUri();
}
