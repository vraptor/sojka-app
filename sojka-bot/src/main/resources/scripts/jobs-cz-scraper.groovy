@Grapes(
        @Grab(group = 'org.jsoup', module = 'jsoup', version = '1.9.2'))
import org.jsoup.Jsoup

def doc = Jsoup.connect('http://jobs.cz/prace').get()
def items = doc.select('.standalone.search-list__item')