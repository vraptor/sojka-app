package co.jware.sojka.bot.jobs.scrapers.locations;

import co.jware.sojka.core.domain.search.MetaLocation;
import co.jware.sojka.core.domain.search.MetaLocationResult;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
public class LocationResolverImplTest {
    @Autowired
    private LocationResolver resolver;

    @Test
    public void resolve() throws Exception {
        MetaLocation location = MetaLocation.builder().name("Praha").build();
        MetaLocationResult result = resolver.resolve(location);
        assertThat(result.location()).isEqualTo(location);
    }

    @TestConfiguration
    static class TestConfig {
        @Bean
        public LocationResolver locationResolver() {
            return new LocationResolverImpl();
        }
    }
}