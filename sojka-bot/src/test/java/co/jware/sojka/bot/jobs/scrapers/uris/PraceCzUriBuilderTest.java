package co.jware.sojka.bot.jobs.scrapers.uris;

import co.jware.sojka.core.domain.search.MetaSearch;
import org.junit.Test;

import java.net.URI;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;


public class PraceCzUriBuilderTest {
    @Test
    public void buildUris() throws Exception {
        MetaSearch search = MetaSearch.builder()
                .addKeywords("java", "javascript")
                .page(3)
                .build();
        List<URI> list = new PraceCzUriBuilder().buildUris(search);
        assertThat(list).isNotEmpty();
    }

}