package co.jware.sojka.bot.jobs.scrapers.links;

import co.jware.sojka.bot.jobs.TestConfig;
import co.jware.sojka.bot.jobs.scrapers.executors.MetaSearchExecutor;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;


@RunWith(SpringRunner.class)
@ContextConfiguration(classes = TestConfig.class)
public class StartupJobsCzLinksExtractorTest {
    @Autowired
    private MetaSearchExecutor executor;

    @Test
    @Ignore
    public void extractLinks() throws Exception {
//        MetaSearch search = MetaSearch.builder()
//                .addKeyword("javascript")
//                .build();
//        TestSubscriber<MetaLink> ts = executor.metaLinks(search, Sets.newHashSet(STARTUPJOBS_CZ))
//                .take(5)
//                .test();
//        ts.awaitTerminalEvent();
//        ts.assertValueCount(5);
    }

    @Test
    @Ignore
    public void extractLinks_no_keywords() throws Exception {
//        MetaSearch search = MetaSearch.builder().build();
//        TestSubscriber<MetaLink> ts = executor.metaLinks(search, Sets.newHashSet(STARTUPJOBS_CZ))
//                .take(45)
//                .test();
//        ts.awaitTerminalEvent();
//        ts.assertValueCount(45);
    }
}