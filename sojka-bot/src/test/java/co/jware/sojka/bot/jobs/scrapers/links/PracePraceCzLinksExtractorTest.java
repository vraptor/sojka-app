package co.jware.sojka.bot.jobs.scrapers.links;

import co.jware.sojka.bot.jobs.TestConfig;
import co.jware.sojka.bot.jobs.scrapers.SiteInfo;
import co.jware.sojka.bot.jobs.scrapers.executors.MetaSearchExecutor;
import co.jware.sojka.core.domain.search.MetaLink;
import co.jware.sojka.core.domain.search.MetaSearch;
import com.google.common.collect.Sets;
import io.reactivex.subscribers.TestSubscriber;
import org.asynchttpclient.extras.rxjava2.RxHttpClient;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = TestConfig.class)
public class PracePraceCzLinksExtractorTest {

    @Autowired
    private MetaSearchExecutor executor;
    @Autowired
    private RxHttpClient rxHttpClient;

    @Test
    public void extractLinks() throws Exception {
        MetaSearch search = MetaSearch.builder()
//                .addKeyword("javascript")
                .build();
        TestSubscriber<MetaLink> ts = executor.metaLinks(search, Sets.newHashSet(SiteInfo.PRACEPRACE_CZ))
                .take(45).test();
        ts.awaitTerminalEvent();
        ts.assertValueCount(45);
        List<MetaLink> values = ts.values();
        assertThat(values).isNotEmpty();
    }

    @Test
    public void portalList() throws Exception {
        executor.metaLinks(MetaSearch.builder()
                        .addKeyword("javascript")
                        .build(),
                Sets.newHashSet(SiteInfo.PRACEPRACE_CZ))
                .take(10)
                .blockingForEach(metaLink -> {
                    String uriString = metaLink.href();
                    Document pageDoc = Jsoup.connect(uriString).get();
                    String noFrames = pageDoc.select("noframes").text();
                    Document noFramesDoc = Jsoup.parseBodyFragment(noFrames);
                    Elements links = noFramesDoc.select("a");
                    String href = links.attr("href");
                    System.err.println(href);
                    String portalList = pageDoc.select("#portal_list").text();
                    System.err.println(portalList);
                });
    }
}
