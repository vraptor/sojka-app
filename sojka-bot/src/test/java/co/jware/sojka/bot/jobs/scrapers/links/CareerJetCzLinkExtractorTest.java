package co.jware.sojka.bot.jobs.scrapers.links;

import java.util.List;

import co.jware.sojka.bot.jobs.RequestBuilder;
import co.jware.sojka.bot.jobs.TestConfig;
import co.jware.sojka.bot.jobs.scrapers.executors.MetaSearchExecutor;
import co.jware.sojka.core.domain.search.MetaLink;
import co.jware.sojka.core.domain.search.MetaSearch;
import com.google.common.collect.Sets;
import io.reactivex.subscribers.TestSubscriber;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static co.jware.sojka.bot.jobs.scrapers.SiteInfo.CAREER_JET_CZ;
import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = TestConfig.class)
public class CareerJetCzLinkExtractorTest {
    @Autowired
    private MetaSearchExecutor executor;
    @Autowired
    private CareerJetCzLinkExtractor extractor;
    @Autowired
    private RequestBuilder requestBuilder;

    private MetaSearch search;

    @Before
    public void setUp() throws Exception {
        search = MetaSearch.builder()
//                .addKeyword("javascript")
                .build();
    }

    @Test
    public void extractLinks() throws Exception {
        TestSubscriber<MetaLink> ts = executor.metaLinks(search, Sets.newHashSet(CAREER_JET_CZ))
                .take(45)
                .test();
        ts.awaitTerminalEvent();
        ts.assertComplete();
        ts.assertValueCount(45);
        List<MetaLink> values = ts.values();
        assertThat(values).isNotEmpty();
    }
}
