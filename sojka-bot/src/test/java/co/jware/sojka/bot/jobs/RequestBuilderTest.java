package co.jware.sojka.bot.jobs;

import co.jware.sojka.bot.jobs.scrapers.SiteInfo;
import co.jware.sojka.bot.jobs.scrapers.uris.UriBuilder;
import co.jware.sojka.core.domain.search.MetaSearch;

import com.beust.jcommander.internal.Lists;

import io.reactivex.subscribers.TestSubscriber;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.net.URL;
import java.time.OffsetDateTime;
import java.util.Collections;
import java.util.List;

import static co.jware.sojka.bot.jobs.scrapers.SiteInfo.*;
import static java.util.Collections.singletonList;
import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
public class RequestBuilderTest {
    @Autowired
    private RequestBuilder requestBuilder;
    private MetaSearch metaSearch;

    @Before
    public void setUp() throws Exception {
        metaSearch = MetaSearch.builder()
                .addKeywords("A", "B")
                .addLocations("Praha", "Brno")
                .created(OffsetDateTime.now())
                .build();
    }

    @Test
    public void buildUri() throws Exception {
        List<URL> uriList = requestBuilder.buildUri(metaSearch);
        assertThat(uriList).isNotNull();
    }

    @Test
    public void buildUri_null_list() throws Exception {
        List<URL> urlList = requestBuilder.buildUri(metaSearch, null);
        assertThat(urlList).isEmpty();
    }

    @Test
    public void buildUri_some_urls() throws Exception {
        URI baseUri = CAREER_JET_CZ.baseUri();
        List<URL> urls = requestBuilder.buildUri(metaSearch, singletonList(baseUri));
        assertThat(urls).hasSize(1);

    }

    @Test
    public void buildUri_invalid_uri() throws Exception {
        List<URL> urlList = requestBuilder.buildUri(metaSearch, Lists.newArrayList(new URI("__BOGUS__")));
        assertThat(urlList).isEmpty();

    }

    @Test
    public void buildUris() throws Exception {
        TestSubscriber<URI> ts = new TestSubscriber<>();
        requestBuilder.buildUris(metaSearch)
                .doOnNext(System.out::println)
                .subscribe(ts);
        ts.awaitTerminalEvent();
        ts.assertComplete();
    }

    @TestConfiguration
    @ComponentScan(basePackageClasses = UriBuilder.class)
    static class TestConfig {
        @Bean
        public RequestBuilder requestBuilder() {
            return new RequestBuilder();
        }
    }
}