package co.jware.sojka.bot.jobs.scrapers.executors;

import co.jware.sojka.core.domain.search.MetaLink;
import co.jware.sojka.core.domain.search.MetaSearch;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;

import static org.assertj.core.api.Assertions.assertThat;

public class MetaSearchExecutorHelperTest {

    private MetaLink metaLink;
    private MetaSearch metaSearch;
    private MetaSearchExecutorHelper helper;

    @Before
    public void setUp() throws Exception {
        metaLink
                = MetaLink.builder()
                .href("NOWHERE")
                .title("BOGUS")
                .addKeyword("java")
                .build();
        metaSearch = MetaSearch.builder()
                .addKeywords("javascript", "python")
                .build();
        helper = new MetaSearchExecutorHelper();
    }

    @Test
    public void enhanceMetaLink_keywords() throws Exception {
        MetaLink result = helper.enhanceMetaLink(metaLink, metaSearch);
        assertThat(result.keywords())
                .containsAll(Arrays.asList("java", "javascript", "python"));
    }

    @Test
    public void enhanceMetaLink_null_keywords() throws Exception {
        MetaLink result = helper.enhanceMetaLink(metaLink.withKeywords(), metaSearch);
        assertThat(result.keywords())
                .containsExactlyInAnyOrder("javascript", "python");
    }

    @Test
    public void enhanceMetaLink_locations() throws Exception {
        MetaLink result = helper.enhanceMetaLink(metaLink.withLocations("brno"), metaSearch.withLocations("praha"));
        assertThat(result.locations())
                .containsExactlyInAnyOrder("brno", "praha");
    }

}