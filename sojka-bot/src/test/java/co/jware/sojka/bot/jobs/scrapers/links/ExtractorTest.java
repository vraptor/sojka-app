package co.jware.sojka.bot.jobs.scrapers.links;

import org.jsoup.nodes.Document;
import org.junit.Test;

import java.lang.invoke.MethodHandle;
import java.util.Arrays;
import java.util.List;

import static java.lang.invoke.MethodHandles.lookup;
import static java.lang.invoke.MethodType.methodType;
import static org.assertj.core.api.Assertions.assertThat;

public class ExtractorTest {
    @Test
    public void methodHandle() throws Throwable {
        MethodHandle handle = lookup().findVirtual(Extractor.class, "extractLinks",
                methodType(List.class, Document.class, String.class));
        assertThat(handle).isNotNull();
        handle.invoke(new JobsCzLinksExtractor(), new Document(""), "");
    }

    @Test
    public void cleanTitle() throws Exception {
        String[] titles = {"► JAVA Developer - Praha", "Enterprise Business Architect⋘ (Western Europe )",
                "JAVA GURU - vyvíjej platební software ⋘"};
        Arrays.stream(titles).forEach(title -> {
        });
        String title = "JAVA GURU - vyvíjej platební software ⋘";
        String result = new JobsCzLinksExtractor().cleanTitle(title);
        assertThat(result).isEqualTo("JAVA GURU - vyvíjej platební software");
    }
}