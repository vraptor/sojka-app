package co.jware.sojka.bot.jobs.scrapers.links;

import co.jware.sojka.bot.jobs.TestConfig;
import co.jware.sojka.bot.jobs.scrapers.executors.MetaSearchExecutor;
import co.jware.sojka.core.domain.search.MetaLink;
import co.jware.sojka.core.domain.search.MetaSearch;
import com.google.common.collect.Sets;
import io.reactivex.subscribers.TestSubscriber;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.Objects;

import static co.jware.sojka.bot.jobs.scrapers.SiteInfo.PRACE_CZ;
import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = TestConfig.class)
public class PraceCzLinksExtractorTest {
    @Autowired
    private MetaSearchExecutor executor;

    @Test
    public void extractLinks() throws Exception {
        MetaSearch search = MetaSearch.builder()
                .addKeyword("javascript")
                .build();
        TestSubscriber<MetaLink> ts = executor.metaLinks(search, Sets.newHashSet(PRACE_CZ))
                .take(60)
                .test();
        ts.awaitTerminalEvent();
        ts.assertValueCount(60);
        List<MetaLink> values = ts.values();
        boolean allMatch = values.stream().allMatch(m -> Objects.equals(PRACE_CZ.baseUri().toString(), m.baseUri()));
        assertThat(allMatch).isTrue();
    }

}
