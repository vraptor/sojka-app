package co.jware.sojka.bot.jobs.scrapers.uris;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.junit4.SpringRunner;

import java.net.URI;
import java.util.List;

import static java.util.stream.Collectors.toList;
import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
public class BaseUriBuilderTest {

    @Autowired
    private List<UriBuilder> uriBuilders;
    @Autowired
    private List<BaseUriBuilder> baseUriBuilders;

    @Test
    public void supportsAny() throws Exception {
        assertThat(baseUriBuilders).isNotNull();
        List<URI> uriList = baseUriBuilders.stream()
                .map(BaseUriBuilder::baseUri)
                .collect(toList());
        assertThat(baseUriBuilders.stream().allMatch(b -> b.supportsAny(uriList)))
                .isTrue();
    }

    @Test
    public void haveUriBuilderImplementor() throws Exception {
        assertThat(uriBuilders.size()).isEqualTo(baseUriBuilders.size() + 1);
    }

    @Test
    public void name() throws Exception {
    }

    @Configuration
    @ComponentScan(basePackageClasses = {UriBuilder.class})
    static class TestConfig {

    }
}