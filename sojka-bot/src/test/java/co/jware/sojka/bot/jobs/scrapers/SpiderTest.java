package co.jware.sojka.bot.jobs.scrapers;

import org.asynchttpclient.AsyncHttpClient;
import org.asynchttpclient.DefaultAsyncHttpClient;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
public class SpiderTest {
    @Autowired
    private Spider spider;


    @Test
    @Ignore
    public void collect() throws Exception {
        spider.collect("http://www.prace.cz/nabidky");
    }

    @TestConfiguration
    static class TestConfig {

        @Bean
        public Spider spider() {
            return new Spider();
        }

        @Bean()
        public AsyncHttpClient asyncClient() {
            return new DefaultAsyncHttpClient();
        }
    }
}
