package co.jware.sojka.bot.jobs.scrapers.executors;

import io.reactivex.Flowable;
import io.reactivex.subscribers.TestSubscriber;
import org.asynchttpclient.AsyncHttpClient;
import org.asynchttpclient.DefaultAsyncHttpClient;
import org.asynchttpclient.Response;
import org.asynchttpclient.extras.rxjava2.DefaultRxHttpClient;
import org.asynchttpclient.extras.rxjava2.RxHttpClient;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
public class RxMetaSearchExecutorImplTest {
    @Autowired
    AsyncHttpClient asyncHttpClient;
    @Autowired
    private RxHttpClient rxHttpClient;

    @Test
    public void name() throws Exception {
        TestSubscriber<Response> to = Flowable.fromArray("http://www.jobs.cz", "http://profesia.cz", "http://ccc", "http://www.prace.cz", "http://aaa.aa")
                .flatMap(u -> rxHttpClient.prepare(asyncHttpClient.prepareGet(u).build())
                        .onErrorComplete()
                        .toFlowable())
                .test();
        to.awaitTerminalEvent();
        to.assertValueCount(3);
        to.assertComplete();
    }


    @Configuration
    static class TestConfig {
        @Bean
        public RxHttpClient rxHttpClient() {
            return new DefaultRxHttpClient(asyncHttpClient());
        }

        @Bean
        public DefaultAsyncHttpClient asyncHttpClient() {
            return new DefaultAsyncHttpClient();
        }
    }
}