package co.jware.sojka.bot.jobs.scrapers.uris;

import co.jware.sojka.core.domain.search.MetaSearch;
import org.springframework.stereotype.Component;

import java.net.URI;
import java.util.Collections;
import java.util.List;

@Component
public class BogusUriBuilder implements UriBuilder {
    @Override
    public List<URI> buildUris(MetaSearch search) {
        return Collections.emptyList();
    }

    @Override
    public boolean supportsAny(List<URI> baseUris) {
        return false;
    }

    @Override
    public boolean active() {
        return false;
    }
}
