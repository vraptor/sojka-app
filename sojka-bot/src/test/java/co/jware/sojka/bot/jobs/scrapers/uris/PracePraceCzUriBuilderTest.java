package co.jware.sojka.bot.jobs.scrapers.uris;

import co.jware.sojka.core.domain.search.MetaSearch;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

import java.net.URI;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
public class PracePraceCzUriBuilderTest {
    private PracePraceCzUriBuilder uriBuilder;

    @Before
    public void setUp() throws Exception {
        uriBuilder = new PracePraceCzUriBuilder();
    }

    @Test
    public void buildUris() throws Exception {
        MetaSearch search = MetaSearch.builder()
                .addKeyword("java")
                .page(3)
                .build();
        List<URI> uris = uriBuilder.buildUris(search);
        assertThat(uris).isNotEmpty();
    }

}