package co.jware.sojka.bot.service;

import co.jware.sojka.bot.core.OsmResponse;
import co.jware.sojka.bot.service.impl.DefaultNominatimService;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.squareup.okhttp.OkHttpClient;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Comparator;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
public class DefaultNominatimServiceTest {

    @Autowired
    private NominatimService nominatimService;

    @Test
    public void search() throws Exception {
        List<OsmResponse> list = nominatimService.search("Liberec X-Františkov");
        assertThat(list).isNotEmpty();
        OsmResponse osmResponse = list.stream().max(Comparator.comparing(OsmResponse::importance))
                .orElseThrow(IllegalStateException::new);
        assertThat(osmResponse).isNotNull();
    }

    @Configuration
    static class ContextConfig {
        @Bean
        public ObjectMapper objectMapper() {
            ObjectMapper objectMapper = new ObjectMapper();
            objectMapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
            return objectMapper;
        }

        @Bean
        public OkHttpClient httpClient() {
            return new OkHttpClient();
        }

        @Bean
        public NominatimService nominatimService(@Qualifier("objectMapper") ObjectMapper mapper,
                                                 OkHttpClient httpClient) {
            return new DefaultNominatimService(mapper, httpClient);
        }
    }
}
