package co.jware.sojka.bot.jobs;

import co.jware.sojka.bot.codecs.MetaSearchLinkCodec;
import co.jware.sojka.bot.jobs.scrapers.executors.MetaSearchExecutor;
import co.jware.sojka.bot.jobs.scrapers.executors.MetaSearchExecutorHelper;
import co.jware.sojka.bot.jobs.scrapers.executors.impl.MetaSearchExecutorImpl;
import co.jware.sojka.bot.jobs.scrapers.links.Extractor;
import co.jware.sojka.bot.jobs.scrapers.uris.UriBuilder;
import co.jware.sojka.core.domain.search.MetaLink;
import co.jware.sojka.core.domain.search.MetaSearch;
import co.jware.sojka.core.domain.search.MetaSearchLink;
import co.jware.sojka.core.service.MetaLinkService;
import co.jware.sojka.core.service.MetaSearchLinkService;
import co.jware.sojka.core.service.entity.MetaSearchService;
import co.jware.sojka.core.service.impl.MetaLinkServiceImpl;
import co.jware.sojka.core.service.impl.MetaSearchServiceImpl;
import co.jware.sojka.repository.search.MetaLinkRepository;
import co.jware.sojka.repository.search.MetaSearchRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import com.hazelcast.core.IQueue;
import io.vertx.core.Vertx;
import io.vertx.core.eventbus.EventBus;
import org.asynchttpclient.AsyncHttpClient;
import org.asynchttpclient.DefaultAsyncHttpClient;
import org.asynchttpclient.extras.rxjava2.DefaultRxHttpClient;
import org.asynchttpclient.extras.rxjava2.RxHttpClient;
import org.jdeferred.DeferredManager;
import org.jdeferred.impl.DefaultDeferredManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.task.TaskExecutor;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.time.OffsetDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static java.util.stream.Collectors.toMap;
import static org.mockito.Mockito.mock;

@Configuration()
@ComponentScan(basePackageClasses = {UriBuilder.class, Extractor.class})
public class TestConfig {

    @MockBean
    public IMap<MetaSearch, OffsetDateTime> metaSearchMap;
    @MockBean(name = "hazelcastInstance")
    HazelcastInstance hazelcastInstance;
    @MockBean
    com.hazelcast.core.IQueue<MetaSearchLink> metaSearchLinkQueue;
    @MockBean
    private MetaSearchLinkService metaSearchLinkService;
    @MockBean
    private MetaSearchRepository metaSearchRepository;
    @MockBean
    private MetaLinkRepository metaLinkRepository;
    @MockBean(name = "metaSearchQueue")
    private IQueue<MetaSearch> metaSearchQueue;
    @Autowired
    private List<Extractor> extractors;

    @Bean
    public RequestBuilder requestBuilder() {
        return new RequestBuilder();
    }

    @Bean
    public AsyncHttpClient asyncHttpClient() {
        return new DefaultAsyncHttpClient();
    }

    @Bean
    public RxHttpClient rxHttpClient() {
        return new DefaultRxHttpClient(asyncHttpClient());
    }

    @Bean
    public EventBus eventBus() {
        EventBus eventBus = Vertx.vertx().eventBus();
        eventBus.registerDefaultCodec(MetaSearchLink.class, new MetaSearchLinkCodec());
        return eventBus;
    }

    @Bean
    public MetaSearchExecutor metaSearchExecutor() {
        return new MetaSearchExecutorImpl();
    }

    @Bean
    public MetaSearchService metaSearchService() {
        return new MetaSearchServiceImpl();
    }

    @Bean
    public MetaLinkService metaLinkService() {
        return new MetaLinkServiceImpl();
    }

    @Bean
    public MetaSearchExecutorHelper metaSearchExecutorHelper() {
        return new MetaSearchExecutorHelper();
    }

    @Bean
    public Map<MetaSearch, OffsetDateTime> metaSearchMap() {
        return new HashMap<>();
    }

    @Bean
    public ObjectMapper objectMapper() {
        return Jackson2ObjectMapperBuilder.json().build();
    }

    @Bean
    @Qualifier("baseUriExtractorMap")
    public Map<String, Extractor> baseUriExtractorMap() {
        return extractors.stream()
                .collect(toMap(Extractor::extractorKey, e -> e));
    }

    @Bean
    public IMap<MetaLink, Boolean> metaLinksMap() {
        return mock(IMap.class);
    }

    @Bean
    public TaskExecutor taskExecutor() {
        ThreadPoolTaskExecutor taskExecutor = new ThreadPoolTaskExecutor();
        taskExecutor.setCorePoolSize(16);
        taskExecutor.setMaxPoolSize(32);
        taskExecutor.setQueueCapacity(200);
        return taskExecutor;
    }

    @Bean
    public DeferredManager deferredManager() {
        ExecutorService executorService = Executors.newScheduledThreadPool(20);
        return new DefaultDeferredManager(executorService);
    }
}
