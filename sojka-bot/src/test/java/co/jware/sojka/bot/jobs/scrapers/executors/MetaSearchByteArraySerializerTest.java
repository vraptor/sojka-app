package co.jware.sojka.bot.jobs.scrapers.executors;

import co.jware.sojka.core.domain.search.MetaSearch;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
public class MetaSearchByteArraySerializerTest {

    @Autowired
    private MetaSearchByteArraySerializer serializer;

    @Test
    public void write() throws Exception {
        byte[] write = serializer.write(MetaSearch.builder().page(1).build());
        assertThat(write).isNotEmpty();
    }

    @Test
    public void read() throws Exception {
        MetaSearch search = MetaSearch.builder().page(1).build();
        byte[] write = serializer.write(search);
        MetaSearch readMetaSearch = serializer.read(write);
        assertThat(readMetaSearch).isEqualTo(search);
    }

    @Configuration
    static class TestConfig {
        @Bean
        public ObjectMapper objectMapper() {
            return Jackson2ObjectMapperBuilder.json().build();
        }

        @Bean
        public MetaSearchByteArraySerializer metaSearchByteArraySerializer(ObjectMapper mapper) {
            return new MetaSearchByteArraySerializer(mapper);
        }
    }
}