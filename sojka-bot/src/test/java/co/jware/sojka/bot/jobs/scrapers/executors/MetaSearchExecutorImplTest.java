package co.jware.sojka.bot.jobs.scrapers.executors;

import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import org.assertj.core.util.Lists;
import org.asynchttpclient.AsyncHttpClient;
import org.asynchttpclient.DefaultAsyncHttpClient;
import org.asynchttpclient.Response;
import org.asynchttpclient.extras.rxjava2.DefaultRxHttpClient;
import org.asynchttpclient.extras.rxjava2.RxHttpClient;
import org.jdeferred.DeferredManager;
import org.jdeferred.impl.DefaultDeferredManager;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.task.SimpleAsyncTaskExecutor;
import org.springframework.core.task.TaskExecutor;
import org.springframework.test.context.junit4.SpringRunner;

import com.hazelcast.core.IQueue;

import co.jware.sojka.bot.jobs.RequestBuilder;
import co.jware.sojka.bot.jobs.scrapers.SiteInfo;
import co.jware.sojka.bot.jobs.scrapers.executors.impl.MetaSearchExecutorImpl;
import co.jware.sojka.bot.jobs.scrapers.links.Extractor;
import co.jware.sojka.bot.jobs.scrapers.uris.UriBuilder;
import co.jware.sojka.core.domain.search.MetaLink;
import co.jware.sojka.core.domain.search.MetaSearch;
import co.jware.sojka.core.domain.search.MetaSearchResult;
import co.jware.sojka.core.service.MetaLinkService;
import co.jware.sojka.core.service.entity.MetaSearchService;
import io.reactivex.Flowable;
import io.reactivex.Observable;
import io.reactivex.observers.TestObserver;
import io.reactivex.subscribers.TestSubscriber;
import io.vertx.core.Vertx;
import io.vertx.core.eventbus.EventBus;

import static java.util.stream.Collectors.toMap;

@RunWith(SpringRunner.class)
public class MetaSearchExecutorImplTest {
    @Autowired
    private MetaSearchExecutor executor;

    @Test
    public void metaSearchResult() throws Exception {
        Observable<MetaSearchResult> result = executor.metaSearchResult("http://jobs.cz/prace/");
        TestObserver<MetaSearchResult> to = new TestObserver<>();
        result.subscribe(to);
        to.awaitTerminalEvent();
    }

    @Test
    public void metaSearchResults() throws Exception {
        ArrayList<String> urls = Lists.newArrayList("http://jobs.cz/prace/",
                "http://www.prace.cz/nabidky/");
        Flowable<MetaSearchResult> result = executor.metaSearchResults(urls);
        TestSubscriber<MetaSearchResult> to = result.test();
        to.awaitTerminalEvent();
        to.assertValueCount(2);
    }

    @Test
    public void metaLinks() throws Exception {
        MetaSearch metaSearch = MetaSearch.builder()
                .addKeyword("javascript")
                .build();
        TestObserver<MetaLink> to = new TestObserver<>();
        TestSubscriber<MetaLink> ts = executor.metaLinks(metaSearch)
                .take(50)
                .sorted(Comparator.comparing(MetaLink::address))
                .doOnNext(m -> System.err.println(m.href())).test();
        ts.awaitTerminalEvent();
        ts.assertValueCount(50);
    }

    @Test
    @Ignore
    public void fetch_invalid() throws Exception {
        TestObserver<Response> to = new TestObserver<>();
        ((MetaSearchExecutorImpl) executor).fetch("aaa")
//                .onErrorResumeNext(Observable.<Response>empty().singleOrError())
                .subscribe(to);
        to.awaitTerminalEvent();
        to.assertValueCount(0);
    }

    @Test
    public void getMetaSearchResults() throws Exception {
        MetaSearch search = MetaSearch.builder()
                .addKeyword("javascript")
                .build();
//        TestSubscriber<MetaSearchResult> ts = new TestSubscriber<>();
        TestSubscriber<MetaSearchResult> ts = executor.metaSearchResults(search)
                .take(20)
                .test();

//                .subscribe(ts);
        ts.awaitTerminalEvent();
        ts.assertComplete();
    }

    @Test
    public void metaLinks_by_site_info() throws Exception {
        MetaSearch search = MetaSearch.builder().build();
        Arrays.stream(SiteInfo.values()).forEach(siteInfo -> {
            System.out.println("=======" + siteInfo.baseUri().toString());
            TestSubscriber<MetaLink> ts = executor.metaLinks(search, Collections.singleton(siteInfo))
                    .take(45)
                    .test();
            ts.awaitTerminalEvent();
            ts.assertValueCount(45);
        });
    }

    @Configuration
    @ComponentScan(basePackageClasses = {Extractor.class, UriBuilder.class})
    static class TestConfig {
        @MockBean
        public IQueue<MetaSearch> metaSearchQueue;
        @MockBean
        IMap<MetaLink, Boolean> metaLinksMap;
        @MockBean
        MetaSearchExecutorHelper executorHelper;
        @MockBean
        private MetaSearchService metaSearchService;
        @MockBean
        private MetaLinkService metaLinkService;
        private Map<MetaSearch, OffsetDateTime> metaSearchMap;
        @Autowired
        private List<Extractor> extractors;

        @Bean
        public EventBus eventBus() {
            return Vertx.vertx().eventBus();
        }

        @Bean
        public MetaSearchExecutor metaSearchExecutor() {
            return new MetaSearchExecutorImpl();
        }

        @Bean
        public AsyncHttpClient asyncHttpClient() {
            return new DefaultAsyncHttpClient();
        }

        @Bean
        public RxHttpClient rxHttpClient() {
            return new DefaultRxHttpClient(asyncHttpClient());
        }

        @Bean
        public RequestBuilder requestBuilder() {
            return new RequestBuilder();
        }

        @Bean
        public TaskExecutor taskExecutor() {
            return new SimpleAsyncTaskExecutor();
        }

        @Bean
        public DeferredManager deferredManager() {
            return new DefaultDeferredManager();
        }

        @Bean
        @Qualifier("baseUriExtractorMap")
        public Map<String, Extractor> baseUriExtractorMap() {
            return extractors.stream()
                    .collect(toMap(Extractor::extractorKey, e -> e));
        }

        @Bean
        public HazelcastInstance hazelcastInstance() {
            return Hazelcast.newHazelcastInstance();
        }
    }


}