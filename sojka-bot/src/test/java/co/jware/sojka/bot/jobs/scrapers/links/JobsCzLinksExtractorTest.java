package co.jware.sojka.bot.jobs.scrapers.links;

import java.util.List;
import java.util.Set;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.google.common.collect.Sets;

import co.jware.sojka.bot.jobs.TestConfig;
import co.jware.sojka.bot.jobs.scrapers.executors.MetaSearchExecutor;
import co.jware.sojka.core.domain.search.MetaLink;
import co.jware.sojka.core.domain.search.MetaSearch;
import io.reactivex.subscribers.TestSubscriber;

import static co.jware.sojka.bot.jobs.scrapers.SiteInfo.JOBS_CZ;
import static java.util.stream.Collectors.toSet;
import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = TestConfig.class)
public class JobsCzLinksExtractorTest {
    @Autowired
    private MetaSearchExecutor executor;

    @Test
    public void extractLinks() throws Exception {
        MetaSearch search = MetaSearch.builder()
                .addKeywords("javascript", "java")
                .build();
        TestSubscriber<MetaLink> ts = executor.metaLinks(search, Sets.newHashSet(JOBS_CZ))
                .take(120)
                .test();
        ts.awaitTerminalEvent();
        List<MetaLink> values = ts.values();
        boolean allMatch = values.stream()
                .allMatch(m -> JOBS_CZ.baseUri().toString().equals(m.baseUri()));
        assertThat(allMatch).isTrue();
        Set<MetaLink> metaLinks = values.stream().distinct().collect(toSet());
    }
}
