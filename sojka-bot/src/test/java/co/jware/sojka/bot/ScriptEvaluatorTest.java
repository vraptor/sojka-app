package co.jware.sojka.bot;

import org.junit.Test;

import static org.hamcrest.core.IsNull.notNullValue;
import static org.junit.Assert.assertThat;


public class ScriptEvaluatorTest {
    @Test
    public void evaluate() throws Exception {
        ScriptEvaluator evaluator = new ScriptEvaluator();
        Object result = evaluator.evaluate("classpath:scripts/jobs-cz-scraper.groovy");
        assertThat(result, notNullValue());
    }
}