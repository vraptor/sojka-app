package co.jware.sojka.bot.jobs.scrapers.links;

import co.jware.sojka.bot.jobs.TestConfig;
import co.jware.sojka.bot.jobs.scrapers.executors.MetaSearchExecutor;
import co.jware.sojka.core.domain.search.MetaLink;
import co.jware.sojka.core.domain.search.MetaSearch;
import com.google.common.collect.Sets;
import io.reactivex.subscribers.TestSubscriber;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import static co.jware.sojka.bot.jobs.scrapers.SiteInfo.CHCI_PRACI_CZ;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = TestConfig.class)
public class ChciPraciCzLinksExtractorTest {
    @Autowired
    private MetaSearchExecutor executor;

    @Test
    public void extractLinks() throws Exception {
        MetaSearch search = MetaSearch.builder()
                .addKeyword("javascript")
                .build();
        TestSubscriber<MetaLink> ts = executor.metaLinks(search, Sets.newHashSet(CHCI_PRACI_CZ))
                .take(20)
                .test();
        ts.awaitTerminalEvent();
        ts.assertValueCount(20);
    }
}
