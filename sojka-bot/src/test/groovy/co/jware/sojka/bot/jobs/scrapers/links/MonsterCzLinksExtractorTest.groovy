package co.jware.sojka.bot.jobs.scrapers.links

import co.jware.sojka.core.domain.search.MetaLink
import org.jsoup.Jsoup
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.test.context.junit4.SpringRunner

@RunWith(SpringRunner.class)
public class MonsterCzLinksExtractorTest {
    @Test
    public void extractLinks() throws Exception {
        def extractor = new MonsterCzLinksExtractor()
        def doc = Jsoup.connect('https://www.monster.cz/prace/hledat/?q=java-rest&where=jinonice&cy=cz')
                .execute()
                .parse()
        List<MetaLink> links = extractor.extractLinks(doc, "http://monster.cz")
        assert links
    }

}