package co.jware.sojka.bot.jobs.scrapers.pages.impl;

import co.jware.sojka.bot.jobs.scrapers.pages.PageDetailExtractor;
import co.jware.sojka.core.domain.search.Commons;
import co.jware.sojka.core.domain.search.page.PageDetail;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.tika.langdetect.OptimaizeLangDetector;
import org.apache.tika.language.detect.LanguageDetector;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;


@RunWith(SpringRunner.class)
public class PraceCzPageDetailExtractorTest {

    @Autowired
    private PageDetailExtractor extractor;

    @Test
    @Ignore
    public void extractRichText_no_redirect() throws Exception {
        Connection.Response response = Jsoup.connect("https://www.prace.cz/nabidka/1052207241/?rps=78").execute();
        Document doc = response.parse();
        String richText = extractor.extractRichText(doc);
        assertThat(richText).isNotEqualTo(Commons.NOT_AVAILABLE);
    }

    @Test
    public void extractMetaData() throws Exception {
    }

    @Test
    @Ignore
    public void extract() throws Exception {
        Document doc = Jsoup.connect("https://www.prace.cz/nabidka/1161277186/?rps=78")
                .execute()
                .parse();
        PageDetail pageDetail = extractor.extract(doc);
        assertThat(pageDetail).isNotNull();
    }

    @Configuration
    static class TestConfig {
        @Bean
        public PageDetailExtractor pageDetailExtractor() {
            return new PraceCzPageDetailExtractor();
        }

        @Bean
        public ObjectMapper objectMapper() {
            return Jackson2ObjectMapperBuilder.json().build();
        }

        @Bean
        public LanguageDetector languageDetector() {
            return new OptimaizeLangDetector();
        }
    }
}