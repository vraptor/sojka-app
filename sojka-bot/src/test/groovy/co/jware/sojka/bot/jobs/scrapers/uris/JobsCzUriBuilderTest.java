package co.jware.sojka.bot.jobs.scrapers.uris;

import co.jware.sojka.core.domain.search.Commons;
import co.jware.sojka.core.domain.search.MetaSearch;
import com.google.common.collect.Sets;
import org.junit.Before;
import org.junit.Test;
import org.springframework.util.ReflectionUtils;
import org.springframework.web.util.UriComponentsBuilder;

import java.lang.reflect.Field;
import java.net.URI;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;


public class JobsCzUriBuilderTest {

    private MetaSearch metaSearch;
    private JobsCzUriBuilder uriBuilder;

    @Before
    public void setUp() throws Exception {
        metaSearch = MetaSearch.builder()
                .addKeyword(Commons.NOT_AVAILABLE)
                .build();
        uriBuilder = new JobsCzUriBuilder();
    }

    @Test
    public void buildUris_N_A() throws Exception {
        List<URI> uris = uriBuilder.buildUris(metaSearch);
        assertThat(uris).isNotEmpty();
        assertThat(uris.get(0)).isEqualTo(new URI("https://www.jobs.cz/prace/"));
    }

    @Test
    public void buildUris_page() throws Exception {
        List<URI> uris = uriBuilder.buildUris(metaSearch.withPage(3));
        assertThat(uris).isNotEmpty();
        assertThat(uris.get(0)).isEqualTo(new URI("https://www.jobs.cz/prace/?page=3"));
    }

    @Test
    public void buildUris_empty() throws Exception {
        List<URI> uris = uriBuilder.buildUris(metaSearch.withKeywords(Collections.emptySet()));
        assertThat(uris).isNotEmpty();
        assertThat(uris.get(0)).isEqualTo(new URI("https://www.jobs.cz/prace/"));
    }

    @Test
    public void buildUris_empty_keyword() throws Exception {
        List<URI> uris = uriBuilder.buildUris(metaSearch.withKeywords(""));
        assertThat(uris).isNotEmpty();
        assertThat(uris.get(0)).isEqualTo(new URI("https://www.jobs.cz/prace/"));
    }

    @Test
    public void buildUris_empty_location() throws Exception {
        List<URI> uris = uriBuilder.buildUris(metaSearch.withLocations(""));
        assertThat(uris).isNotEmpty();
        assertThat(uris.get(0)).isEqualTo(new URI("https://www.jobs.cz/prace/"));
    }

    @Test
    public void buildUris_location() throws Exception {
        List<URI> uris = uriBuilder.buildUris(metaSearch.withLocations("Praha"));
        assertThat(uris).isNotEmpty();
        assertThat(uris.get(0)).isEqualTo(new URI("https://www.jobs.cz/prace/?locality%5Blabel%5D=Praha"));
    }

    @Test
    public void notActive() throws Exception {
        Field active = ReflectionUtils.findField(uriBuilder.getClass(), "active");
        ReflectionUtils.makeAccessible(active);
        ReflectionUtils.setField(active, uriBuilder, false);
        List<URI> uris = uriBuilder.buildUris(metaSearch);
        assertThat(uris).isEmpty();
    }

    @Test
    public void generatePairs_empty_locations() throws Exception {
        HashSet<String> left = Sets.newHashSet("a");
        Set<String> right = Collections.emptySet();
        Set<UriComponentsBuilder> result = uriBuilder.generatePairs(left, right);
        assertThat(result.size()).isEqualTo(1);
    }

    @Test
    public void generatePairs_empty_keywords() throws Exception {
        HashSet<String> left = Sets.newHashSet("a");
        Set<String> right = Collections.emptySet();
        Set<UriComponentsBuilder> result = uriBuilder.generatePairs(right, left);
        assertThat(result.size()).isEqualTo(1);
    }

    @Test
    public void generatePairs_empty() throws Exception {
        HashSet<String> left = Sets.newHashSet("a");
        Set<String> right = Collections.emptySet();
        Set<UriComponentsBuilder> result = uriBuilder.generatePairs(right, right);
        assertThat(result.size()).isEqualTo(0);
    }
}