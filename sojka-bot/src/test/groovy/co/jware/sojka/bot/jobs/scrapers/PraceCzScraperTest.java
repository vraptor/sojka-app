package co.jware.sojka.bot.jobs.scrapers;

import java.util.List;

import org.junit.Ignore;
import org.junit.Test;

import co.jware.sojka.core.domain.JobListing;

import static org.junit.Assert.assertNotNull;


public class PraceCzScraperTest {
    @Test
    @Ignore
    public void collectJobs() throws Exception {
        List<JobListing> jobs = new PraceCzScraper().collectJobs();
        assertNotNull(jobs);
    }
}