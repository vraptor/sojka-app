package co.jware.sojka.bot.jobs.scrapers

import co.jware.sojka.core.domain.Location
import co.jware.sojka.core.service.PersonService
import co.jware.sojka.core.service.entity.CompanyService
import co.jware.sojka.core.service.entity.HirerService
import co.jware.sojka.core.service.entity.JobListingService
import org.junit.Ignore
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit4.SpringRunner

import static org.mockito.Mockito.mock

@RunWith(SpringRunner)
@ContextConfiguration(classes = [TestConfig])
public class JobsCzScraperTest {
    @Autowired
    JobsCzScraper scraper

    @Autowired
    CompanyService companyService

    @Test
    @Ignore
    public void collectJobs() throws Exception {
        def jobs = this.scraper.collectJobs()
        assert jobs
    }

    @Test
    @Ignore
    public void doCollect() throws Exception {
        assert collected
    }

    @Test
    @Ignore
    public void buildLocation() throws Exception {
        Location loc = scraper.buildLocation([location: 'České Budějovice – České Budějovice 2 + 2 další lokality'])
        assert loc
        assert loc.region() == 'České Budějovice 2'
    }
//        when(companyService.getOrCreate(any(Company)))
//                .thenReturn(Optional.of(mock(Company)))
    def collected = this.scraper.doCollect()


    @Configuration
    static class TestConfig {
        @Bean
        JobsCzScraper jobsCzScraper() {
            new JobsCzScraper()
        }

        @Bean
        JobListingService jobListingService() {
            mock JobListingService
        }

        @Bean
        CompanyService companyService() {
            mock CompanyService
        }

        @Bean
        PersonService personService() {
            mock PersonService
        }

        @Bean
        HirerService hirerService() {
            mock HirerService
        }
    }
}