package co.jware.sojka.bot.jobs.scrapers

import co.jware.sojka.core.domain.SalaryRange
import org.junit.Test

public class JobScraperHelperTest {

    def helper = new JobScraperHelper()

    @Test
    public void parseSalaryRange() throws Exception {
        SalaryRange salaryRange = helper.parseSalary("10000 - 100  000Kč").range()
        assert salaryRange.salaryFrom() == 10_000 as BigDecimal
        assert salaryRange.salaryTo() == 100_000 as BigDecimal
    }

    @Test
    public void parseSalaryRange_one_amount() throws Exception {
        SalaryRange salaryRange = helper.parseSalary("120000Kč").range()
        assert salaryRange.salaryFrom() == 120_000 as BigDecimal
        assert salaryRange.salaryTo() == 120_000 as BigDecimal
    }

    @Test
    public void parseSalaryRange_empty() throws Exception {
        SalaryRange salaryRange = helper.parseSalary("").range()
        assert salaryRange.salaryFrom() == BigDecimal.ZERO
        assert salaryRange.salaryTo() == BigDecimal.ZERO
    }

    @Test
    public void parseSalaryRange_null() throws Exception {
        SalaryRange salaryRange = helper.parseSalary(null).range()
        assert salaryRange.salaryFrom() == BigDecimal.ZERO
        assert salaryRange.salaryTo() == BigDecimal.ZERO
    }

}