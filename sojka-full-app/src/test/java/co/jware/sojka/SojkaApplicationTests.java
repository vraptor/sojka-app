package co.jware.sojka;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = SojkaApplication.class)
public class SojkaApplicationTests {

    @Test
    public void loadContexts() throws Exception {

    }
}