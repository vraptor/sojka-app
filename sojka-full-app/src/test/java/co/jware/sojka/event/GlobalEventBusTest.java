package co.jware.sojka.event;

import co.jware.sojka.spring.EventBusFactoryBean;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = GlobalEventBusTest.TestConfig.class)
public class GlobalEventBusTest {

    @Test
    public void eventBus() throws Exception {
        assertThat(GlobalEventBus.eventBus(), notNullValue());
    }

    @Test
    public void register() throws Exception {
        GlobalEventBus.register(this);
    }

    @Test
    public void unregister() throws Exception {
        GlobalEventBus.unregister(this);
    }

    @ComponentScan
    @Configuration
    static class TestConfig {
        @Bean
        public EventBusFactoryBean eventBusFactoryBean() {
            return new EventBusFactoryBean();
        }
    }
}