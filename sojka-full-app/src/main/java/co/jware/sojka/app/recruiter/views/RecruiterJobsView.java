package co.jware.sojka.app.recruiter.views;

import co.jware.sojka.app.recruiter.RecruiterUI;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;

import javax.annotation.PostConstruct;

import static co.jware.sojka.app.recruiter.views.RecruiterJobsView.VIEW_NAME;

@SpringView(ui = RecruiterUI.class, name = VIEW_NAME)
public class RecruiterJobsView extends VerticalLayout implements View {

    public static final String VIEW_NAME = "jobs";

    @PostConstruct
    void init() {
        setSizeFull();
        addComponent(new Label("Recruiter jobs"));
    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {

    }
}
