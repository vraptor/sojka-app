package co.jware.sojka.app.event;


import co.jware.sojka.core.domain.Account;

import java.io.Serializable;

public class LoginEvent implements Serializable {

    private Account account;

    public LoginEvent(Account account) {
        this.account = account;
    }

    public Account getAccount() {
        return this.account;
    }
}
