package co.jware.sojka.app.candidate;

import co.jware.sojka.app.SojkaBaseApp;
import co.jware.sojka.app.candidate.views.CandidateHomeView;
import com.vaadin.spring.annotation.UIScope;
import com.vaadin.spring.navigator.SpringViewProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

//@Component
public class CandidateApp extends SojkaBaseApp {
    @Autowired
    public CandidateApp(SpringViewProvider viewProvider) {
        super(viewProvider);
    }

    @Override
    protected void registerViews() {
        addView(CandidateHomeView.class);
    }
}
