package co.jware.sojka.app.candidate.views;

import co.jware.sojka.app.candidate.CandidateUI;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;

import javax.annotation.PostConstruct;


@SpringView(ui = CandidateUI.class, name = CandidateHomeView.VIEW_NAME)
public class CandidateHomeView extends VerticalLayout implements View {

    public static final String VIEW_NAME = "home";

    @PostConstruct
    void init() {
        addComponent(new Label("Candidate Home View"));
    }
    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {

    }
}
