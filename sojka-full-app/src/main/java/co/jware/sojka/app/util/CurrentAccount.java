package co.jware.sojka.app.util;


import co.jware.sojka.core.domain.Account;
import com.vaadin.server.VaadinSession;

public class CurrentAccount {

    public static final String CURRENT_ACCOUNT_KEY = "currentAccount";

    public static boolean isLoggedIn() {
        return get() != null;
    }

    public static Account get() {
        return (Account) VaadinSession.getCurrent().getAttribute(CURRENT_ACCOUNT_KEY);
    }

    public static void set(Account account) {
        VaadinSession.getCurrent().setAttribute(CURRENT_ACCOUNT_KEY, account);
    }
}
