package co.jware.sojka.app.teacher;

import co.jware.sojka.app.SojkaBaseApp;
import co.jware.sojka.app.teacher.views.TeacherHomeView;
import com.vaadin.spring.navigator.SpringViewProvider;

public class TeacherApp extends SojkaBaseApp {

    public TeacherApp(SpringViewProvider viewProvider) {
        super(viewProvider);
    }

    @Override
    protected void registerViews() {
        addView(TeacherHomeView.class);
    }
}
