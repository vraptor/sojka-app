package co.jware.sojka.app.teacher.views;


import co.jware.sojka.app.teacher.TeacherUI;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;

import javax.annotation.PostConstruct;

@SpringView(ui = TeacherUI.class, name = TeacherHomeView.VIEW_NAME)
public class TeacherHomeView extends VerticalLayout implements View {

    public static final String VIEW_NAME = "home";

    @PostConstruct
    void init(){
        addComponent(new Label("Teacher Home View"));
    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {

    }
}
