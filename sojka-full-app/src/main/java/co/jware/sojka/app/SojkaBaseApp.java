package co.jware.sojka.app;

import com.vaadin.navigator.Navigator;
import com.vaadin.navigator.View;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.spring.navigator.SpringViewProvider;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Panel;
import com.vaadin.ui.UI;

import javax.annotation.PostConstruct;
import javax.validation.constraints.NotNull;

public abstract class SojkaBaseApp extends HorizontalLayout {

    protected SpringViewProvider viewProvider;
    protected Panel content;
    protected Navigator navigator;
    protected NavBar navBar;

    public SojkaBaseApp(SpringViewProvider viewProvider) {
        this.viewProvider = viewProvider;
    }

    @PostConstruct
    void init() {
        initLayouts();
        setupNavigator();
    }

    protected void initLayouts() {
        setSizeFull();
        content = new Panel();
        content.setSizeFull();
        content.addStyleName(SojkaTheme.PANEL_BORDERLESS);
        navBar = new NavBar();
        addComponents(navBar, content);
        setExpandRatio(content, 1);
    }

    protected void addView(Class<? extends View> viewClass) {
        String viewName = getViewName(viewClass);
        navigator.addView(viewName, viewClass);
        navBar.addView(viewName, viewName);
    }

    @NotNull
    String getViewName(Class<? extends View> viewClass) {
        SpringView springView = viewClass.getAnnotation(SpringView.class);
        return springView.name();
    }

    protected void setupNavigator() {
        navigator = new Navigator(UI.getCurrent(), content);
        navigator.addProvider(viewProvider);
        registerViews();
        String state = navigator.getState();
        if (state.isEmpty()) {
            state = "home";
        }
        navigator.navigateTo(state);
    }

    protected abstract void registerViews();
}
