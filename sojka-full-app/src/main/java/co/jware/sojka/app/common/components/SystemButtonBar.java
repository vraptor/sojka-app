package co.jware.sojka.app.common.components;


import co.jware.sojka.app.SojkaTheme;
import co.jware.sojka.app.event.LogoutEvent;
import co.jware.sojka.app.event.SignInEvent;
import co.jware.sojka.app.util.CurrentAccount;
import co.jware.sojka.event.GlobalEventBus;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.Sizeable;
import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;

import javax.validation.constraints.NotNull;

public class SystemButtonBar extends HorizontalLayout {

    public SystemButtonBar() {
        init();
    }

    @NotNull
    private void init() {
        boolean loggedIn = CurrentAccount.isLoggedIn();
        Label spacer = new Label("");
        addComponent(spacer);
        if (!loggedIn) {
            Button register = addRegisterButton();
            addComponent(register);
        }
        if (loggedIn) {
            Button logout = addLogoutButton();
           addComponent(logout);
        } else {
            Button login = addLoginButton();
           addComponent(login);
        }
        setWidth(100, Sizeable.Unit.PERCENTAGE);
        setExpandRatio(spacer, 1);
        setSpacing(true);
    }

    @NotNull
    private Button addLogoutButton() {
        Button logout = new Button("Logout", event -> GlobalEventBus.post(new LogoutEvent()));
        logout.setIcon(FontAwesome.SIGN_IN);
        logout.addStyleName(SojkaTheme.BUTTON_ICON_ONLY);
        logout.addStyleName(SojkaTheme.BUTTON_PRIMARY);
        return logout;
    }

    @NotNull
    private Button addLoginButton() {
        Button login = new Button("Login", event -> GlobalEventBus.post(new SignInEvent()));
        login.setIcon(FontAwesome.SIGN_IN);
        login.addStyleName(SojkaTheme.BUTTON_ICON_ONLY);
        login.addStyleName(SojkaTheme.BUTTON_PRIMARY);
        return login;
    }

    @NotNull
    private Button addRegisterButton() {
        Button register = new Button("Register");
        register.setIcon(FontAwesome.USER_PLUS);
        register.addStyleName(SojkaTheme.BUTTON_ICON_ONLY);
        register.addStyleName(SojkaTheme.BUTTON_PRIMARY);
        return register;
    }
}
