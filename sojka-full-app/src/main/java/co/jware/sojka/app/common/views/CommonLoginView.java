package co.jware.sojka.app.common.views;

import co.jware.sojka.app.event.LoginEvent;
import co.jware.sojka.app.util.CurrentAccount;
import co.jware.sojka.app.views.LoginView;
import co.jware.sojka.event.GlobalEventBus;
import com.google.common.eventbus.Subscribe;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;
import java.util.Optional;

@SpringView(name = CommonLoginView.VIEW_NAME)
public class CommonLoginView extends VerticalLayout implements View {
    public static final String VIEW_NAME = "login";

    LoginView loginView;

    @Autowired
    public CommonLoginView(LoginView loginView) {
        super();
        GlobalEventBus.eventBus().register(this);
        this.loginView = loginView;
    }

    @PostConstruct
    void init() {
        addComponent(loginView);
        setSizeFull();
        setComponentAlignment(loginView, Alignment.MIDDLE_CENTER);
    }

    @Subscribe
    public void loggedIn(LoginEvent event) {
        Optional.ofNullable(event.getAccount()).map(acc -> {
            CurrentAccount.set(acc);
            UI.getCurrent().getNavigator().navigateTo(CommonHomeView.VIEW_NAME);
            return acc;
        }).orElseThrow(RuntimeException::new);
    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {

    }
}
