package co.jware.sojka.app.recruiter;

import co.jware.sojka.app.SojkaBaseUI;
import co.jware.sojka.app.SojkaTheme;
import co.jware.sojka.ui.BaseUI;
import com.vaadin.annotations.Theme;
import com.vaadin.spring.annotation.SpringUI;
import org.springframework.beans.factory.annotation.Autowired;

@Theme(SojkaTheme.THEME_NAME)
@SpringUI(path = "/recruiter")
public class RecruiterUI extends SojkaBaseUI {


    RecruiterApp app;

    @Autowired
    public RecruiterUI(RecruiterApp app) {
         this.app = app;
    }

    @Override
    protected void setApp() {
        setContent(app);
    }

}
