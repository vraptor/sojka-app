package co.jware.sojka.app.candidate;


import co.jware.sojka.app.SojkaBaseUI;
import co.jware.sojka.app.SojkaTheme;
import co.jware.sojka.app.teacher.TeacherApp;
import com.vaadin.annotations.Theme;
import com.vaadin.spring.annotation.SpringUI;

@SpringUI(path = "/candidate")
@Theme(SojkaTheme.THEME_NAME)
public class CandidateUI extends SojkaBaseUI {


    @Override
    protected void setApp() {
        setContent(new CandidateApp(viewProvider));
    }
}
