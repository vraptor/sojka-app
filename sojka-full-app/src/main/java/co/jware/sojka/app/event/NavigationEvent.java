package co.jware.sojka.app.event;


import java.io.Serializable;

public class NavigationEvent implements Serializable {
    private final String viewName;

    public NavigationEvent(String viewName) {
        this.viewName = viewName;
    }

    public String getViewName() {
        return viewName;
    }
}
