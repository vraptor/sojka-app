package co.jware.sojka.app;

import co.jware.sojka.app.event.LoginEvent;
import co.jware.sojka.app.event.LogoutEvent;
import co.jware.sojka.app.event.NavigationEvent;
import co.jware.sojka.app.util.CurrentAccount;
import co.jware.sojka.app.views.LoginView;
import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.vaadin.server.Page;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinSession;
import com.vaadin.server.WrappedSession;
import com.vaadin.spring.navigator.SpringViewProvider;
import com.vaadin.ui.UI;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;

public abstract class SojkaBaseUI extends UI {

    @Autowired
    protected EventBus eventBus;
    @Autowired
    protected LoginView loginView;
    @Autowired
    protected SpringViewProvider viewProvider;

    public static SojkaBaseUI getCurrent() {
        return (SojkaBaseUI) UI.getCurrent();
    }

    public static EventBus getEventBus() {
        return getCurrent().eventBus;
    }

    @PostConstruct
    void init() {
        setupEventBus();
    }

    @Subscribe
    public void navigateTo(NavigationEvent event) {
        getNavigator().navigateTo(event.getViewName());
    }

    @Subscribe
    public void userLogout(LogoutEvent event) {
        WrappedSession session = VaadinSession.getCurrent().getSession();
        if (session != null) {
            session.invalidate();
            VaadinSession.getCurrent().close();
            Page.getCurrent().open("/", null);
        }
    }

    private void setupEventBus() {
        eventBus.register(this);
    }

    @Override
    protected void init(VaadinRequest request) {
        if (CurrentAccount.isLoggedIn()) {
            setApp();
        } else {
            setContent(loginView);
        }

    }

    protected abstract void setApp();

    @Subscribe
    public void userLoggedIn(LoginEvent event) {
        CurrentAccount.set(event.getAccount());
        setApp();
    }
}
