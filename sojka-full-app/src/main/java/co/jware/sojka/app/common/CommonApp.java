package co.jware.sojka.app.common;


import com.vaadin.navigator.Navigator;
import com.vaadin.navigator.ViewDisplay;
import com.vaadin.ui.HorizontalLayout;

public class CommonApp extends HorizontalLayout {


    public ViewDisplay getContent() {
        return new Navigator.ComponentContainerViewDisplay(this);
    }
}
