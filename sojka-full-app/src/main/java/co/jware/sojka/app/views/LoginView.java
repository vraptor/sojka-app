package co.jware.sojka.app.views;


import co.jware.sojka.app.event.LoginEvent;
import co.jware.sojka.core.domain.Account;
import co.jware.sojka.service.LoginService;
import co.jware.sojka.ui.LoginForm;
import com.google.common.eventbus.EventBus;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.spring.annotation.UIScope;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.VerticalLayout;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Optional;
@UIScope
@SpringComponent("uiLoginView")
public class LoginView extends VerticalLayout {
    @Autowired
    LoginService loginService;
    @Autowired
    EventBus eventBus;
    public LoginView() {
        setSizeFull();
        setSpacing(true);
        setDefaultComponentAlignment(Alignment.MIDDLE_CENTER);
        addComponent(new LoginForm(this::login));
    }

    private boolean login(String username, String password) {
        Account account = loginService.login(username, password);
        return Optional.ofNullable(account).map(a -> {
            eventBus.post(new LoginEvent(a));
            return true;
        }).orElse(false);
    }
}
