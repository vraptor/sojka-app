package co.jware.sojka.app.common.views;


import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;

import javax.annotation.PostConstruct;

@SpringView(name = CommonAboutView.VIEW_NAME)
public class CommonAboutView extends VerticalLayout implements View {
    public static final String VIEW_NAME = "about";

    @PostConstruct
    void init() {
        addComponent(new Label("Common About View"));
        setMargin(true);
    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent viewChangeEvent) {

    }
}