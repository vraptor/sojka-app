package co.jware.sojka.app;


import com.vaadin.ui.themes.ValoTheme;

public class SojkaTheme extends ValoTheme {

    public static final String THEME_NAME = "sojka";
    public static final String MENU_ROOT = "valo-menu";
    public static final String NAVBAR = "navbar";
    public static final String LOGO_IMAGE = "image";
    public static final String BUTTON_LOGOUT = "logout";
    public static final String LOGO_PANEL = "logo-panel";
    public static final String SELECTED = "selected";
}
