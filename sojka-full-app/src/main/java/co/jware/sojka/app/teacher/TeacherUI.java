package co.jware.sojka.app.teacher;

import co.jware.sojka.app.SojkaBaseUI;
import co.jware.sojka.app.SojkaTheme;
import com.vaadin.annotations.Theme;
import com.vaadin.spring.annotation.SpringUI;

@SpringUI(path = "/teacher")
@Theme(SojkaTheme.THEME_NAME)
public class TeacherUI extends SojkaBaseUI {

    @Override
    protected void setApp() {
        setContent(new TeacherApp(viewProvider));
    }
}
