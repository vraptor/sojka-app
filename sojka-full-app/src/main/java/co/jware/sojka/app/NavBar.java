package co.jware.sojka.app;

import co.jware.sojka.app.event.LogoutEvent;
import co.jware.sojka.app.event.NavigationEvent;
import co.jware.sojka.app.util.CurrentAccount;
import co.jware.sojka.event.GlobalEventBus;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.server.ClassResource;
import com.vaadin.server.FontAwesome;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.*;

import java.util.HashMap;
import java.util.Map;

import static co.jware.sojka.app.SojkaTheme.BUTTON_LOGOUT;
import static co.jware.sojka.app.SojkaTheme.LOGO_IMAGE;

public class NavBar extends CssLayout implements ViewChangeListener {


    private Map<String, Button> buttonMap;

    public NavBar() {
        buttonMap = new HashMap();
        setHeight("100%");
        addStyleName(SojkaTheme.MENU_ROOT);
        addStyleName(SojkaTheme.NAVBAR);
        ClassResource logoResource = new ClassResource("/images/sojka_logo_3.svg");
        Image logoImage = new Image("", logoResource);
        logoImage.addStyleName(LOGO_IMAGE);

        Label logo = new Label("<strong>Sojka</strong>App", ContentMode.HTML);
        logo.addStyleName(SojkaTheme.MENU_TITLE);
        VerticalLayout content = new VerticalLayout(logoImage);
        content.setComponentAlignment(logoImage, Alignment.MIDDLE_CENTER);
        Panel logoPanel = new Panel(content);
        logoPanel.addStyleName(SojkaTheme.LOGO_PANEL);
        addComponent(logoPanel);
        addComponent(logo);
        if (CurrentAccount.isLoggedIn()) {
            addLogoutButton();
        } else {
            addComponent(new Label(""));
        }
    }

    private void addLogoutButton() {
        Button logout = new Button("Logout", evt ->
                SojkaBaseUI.getEventBus().post(new LogoutEvent()));
        addComponent(logout);
        logout.addStyleName(BUTTON_LOGOUT);
        logout.addStyleName(SojkaTheme.BUTTON_BORDERLESS);
        logout.setIcon(FontAwesome.SIGN_OUT);
    }

    @Override
    public boolean beforeViewChange(ViewChangeEvent event) {
        return true;
    }

    @Override
    public void afterViewChange(ViewChangeEvent event) {
        buttonMap.values().forEach(button -> button.removeStyleName(SojkaTheme.SELECTED));
        Button button = buttonMap.get(event.getViewName());
        if (button != null) button.addStyleName(SojkaTheme.SELECTED);
    }

    public void addView(String uri, String displayName) {
        Button viewButton = new Button(displayName,
                click -> GlobalEventBus.post(new NavigationEvent(uri)));
        viewButton.addStyleName(SojkaTheme.MENU_ITEM);
        viewButton.addStyleName(SojkaTheme.BUTTON_BORDERLESS);
        buttonMap.put(uri, viewButton);
        addComponent(viewButton, components.size() - 1);
    }
}
