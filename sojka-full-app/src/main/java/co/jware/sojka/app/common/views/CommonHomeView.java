package co.jware.sojka.app.common.views;

import co.jware.sojka.app.SojkaTheme;
import co.jware.sojka.app.common.components.SystemButtonBar;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.server.FontAwesome;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.*;

import javax.annotation.PostConstruct;
import javax.validation.constraints.NotNull;

@SpringView(name = CommonHomeView.VIEW_NAME)
public class CommonHomeView extends VerticalLayout implements View {
    public static final String VIEW_NAME = "home";

    @PostConstruct
    void init() {
        Panel searchPanel = new Panel(setupLayout());
        addComponent(new SystemButtonBar());
        searchPanel.setWidth(75, Unit.PERCENTAGE);
        addComponent(searchPanel);
        setComponentAlignment(searchPanel, Alignment.TOP_LEFT);
        setMargin(true);
    }

    @NotNull
    private TextField addKeywordsField() {
        TextField keywordsField = new TextField();
        keywordsField.setWidth(100, Unit.PERCENTAGE);
        return keywordsField;
    }

    @NotNull
    private HorizontalLayout setupLayout() {
        TextField locationField = addLocationField();
        TextField keywordsField = addKeywordsField();
        Button searchButton = addSearchButton();
        HorizontalLayout layout = new HorizontalLayout(locationField, keywordsField, searchButton);
        layout.setMargin(true);
        layout.setSpacing(true);
        layout.setExpandRatio(locationField, 2);
        layout.setExpandRatio(keywordsField, 3);
        layout.setWidth(100, Unit.PERCENTAGE);
        return layout;
    }

    @NotNull
    private Button addSearchButton() {
        Button searchButton = new Button();
        searchButton.setIcon(FontAwesome.SEARCH);
        searchButton.addStyleName(SojkaTheme.BUTTON_ICON_ONLY);
        searchButton.addStyleName(SojkaTheme.BUTTON_PRIMARY);
        return searchButton;
    }

    @NotNull
    private TextField addLocationField() {
        TextField locationField = new TextField();
        locationField.setWidth(100, Unit.PERCENTAGE);
        locationField.setInputPrompt("Location(s)");
        return locationField;
    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent viewChangeEvent) {

    }
}
