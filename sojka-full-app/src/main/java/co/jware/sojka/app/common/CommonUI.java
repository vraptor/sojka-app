package co.jware.sojka.app.common;


import co.jware.sojka.app.NavBar;
import co.jware.sojka.app.SojkaTheme;
import co.jware.sojka.app.common.views.CommonAboutView;
import co.jware.sojka.app.common.views.CommonHomeView;
import co.jware.sojka.app.common.views.CommonLoginView;
import co.jware.sojka.app.event.LogoutEvent;
import co.jware.sojka.app.event.NavigationEvent;
import co.jware.sojka.app.event.SignInEvent;
import co.jware.sojka.app.views.LoginView;
import co.jware.sojka.event.GlobalEventBus;
import com.google.common.eventbus.Subscribe;
import com.vaadin.annotations.Theme;
import com.vaadin.navigator.Navigator;
import com.vaadin.navigator.View;
import com.vaadin.server.Page;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinSession;
import com.vaadin.server.WrappedSession;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.spring.navigator.SpringViewProvider;
import com.vaadin.ui.Component;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Panel;
import com.vaadin.ui.UI;
import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.constraints.NotNull;
import java.util.stream.Stream;

@SpringUI(path = "/common")
@Theme(SojkaTheme.THEME_NAME)
public class CommonUI extends UI {

    private Panel content;
    private Navigator navigator;
    private SpringViewProvider springViewProvider;
    private NavBar navBar;
    private LoginView loginView;

    @Autowired
    public CommonUI(SpringViewProvider springViewProvider, LoginView loginView) {
        this.springViewProvider = springViewProvider;
        this.loginView = loginView;
    }

    @Override
    protected void init(VaadinRequest request) {
        GlobalEventBus.eventBus().register(this);
        setContent(commonLayout());
        navigator.navigateTo(CommonHomeView.VIEW_NAME);
    }

    private Component commonLayout() {
        content = new Panel();
        content.setSizeFull();
        navigator = new Navigator(this, content);
        navigator.addProvider(springViewProvider);
        navBar = new NavBar();
        Stream<Class> views = Stream.of(CommonHomeView.class, CommonAboutView.class);
        views.forEach(this::addView);
        navigator.addViewChangeListener(navBar);
        HorizontalLayout layout = new HorizontalLayout(navBar, content);
        layout.setSizeFull();
        layout.setExpandRatio(content, 1);
        return layout;
    }

    @Subscribe
    public void navigateTo(NavigationEvent event) {
        String viewName = event.getViewName();
        getNavigator().navigateTo(viewName);
    }

    @Subscribe
    public void userLogout(LogoutEvent event) {
        WrappedSession session = VaadinSession.getCurrent().getSession();
        if (session != null) {
            session.invalidate();
            VaadinSession.getCurrent().close();
            Page.getCurrent().reload();
        }
    }

    @Subscribe
    public void doLogin(SignInEvent event) {
        getNavigator().navigateTo(CommonLoginView.VIEW_NAME);
    }

    protected void addView(Class<? extends View> viewClass) {
        String viewName = getViewName(viewClass);
        navigator.addView(viewName, viewClass);
        navBar.addView(viewName, viewName);
    }

    @NotNull
    String getViewName(Class<? extends View> viewClass) {
        SpringView springView = viewClass.getAnnotation(SpringView.class);
        return springView.name();
    }
}
