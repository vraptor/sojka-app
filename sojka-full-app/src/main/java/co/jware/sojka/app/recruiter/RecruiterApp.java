package co.jware.sojka.app.recruiter;

import co.jware.sojka.app.SojkaBaseApp;
import co.jware.sojka.app.recruiter.views.RecruiterHomeView;
import co.jware.sojka.app.recruiter.views.RecruiterJobsView;
import com.vaadin.spring.annotation.UIScope;
import com.vaadin.spring.navigator.SpringViewProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@UIScope
public class RecruiterApp extends SojkaBaseApp {

    @Autowired
    public RecruiterApp(SpringViewProvider viewProvider) {
        super(viewProvider);
    }
    @Override
    protected void registerViews() {
        addView(RecruiterHomeView.class);
        addView(RecruiterJobsView.class);
    }
}
