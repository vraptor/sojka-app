package co.jware.sojka.views;

import co.jware.sojka.core.domain.JobListing;
import co.jware.sojka.core.enums.JobListingOrigin;
import co.jware.sojka.core.service.entity.JobListingService;
import co.jware.sojka.ui.RecruitUI;
import co.jware.sojka.vaadin.NavBar;
import co.jware.sojka.vaadin.beans.JobListBean;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.vaadin.data.fieldgroup.BeanFieldGroup;
import com.vaadin.data.fieldgroup.FieldGroup;
import com.vaadin.navigator.View;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.*;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;
import java.util.Optional;
import java.util.UUID;

import static co.jware.sojka.views.JobCreateView.VIEW_NAME;
import static com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;

@SpringView(ui = RecruitUI.class, name = VIEW_NAME)
public class JobCreateView extends VerticalLayout implements View {
    public static final String VIEW_NAME = "create";

    @Autowired
    ObjectMapper mapper;
    @Autowired
    JobListingService jobListingService;
    private BeanFieldGroup<JobListBean> binder;
    @Autowired
    private NavBar navBar;

    @PostConstruct
    void init() {
        FieldGroup fieldGroup = new FieldGroup();
        fieldGroup.buildAndBindMemberFields(new JobListBean());
        addComponent(navBar);
        binder = new BeanFieldGroup<>(JobListBean.class);
        GridLayout form = new GridLayout(2, 6);
        form.setColumnExpandRatio(0, 0.7f);
        form.setColumnExpandRatio(1, 0.3f);
        buildName(binder, form);
        buildFunction(binder, form);
        buildDescription(binder, form);
        buildRequirements(binder, form);
        buildBenefits(binder, form);
        form.setSpacing(true);
        form.setMargin(true);
        form.setSizeFull();
        Button saveButton = new Button("Save", evt -> {
            try {
                binder.commit();
                JobListBean jobListBean = binder.getItemDataSource().getBean();
                JobListing jobListing = mapper.convertValue(jobListBean, JobListing.class);
                jobListing = jobListing.withOrigin(JobListingOrigin.INTERNAL);
                if (jobListing.uuid().isPresent()) {
                    jobListingService.update(jobListing);
                } else {
                    jobListingService.createJobListing(jobListing);
                }
            } catch (FieldGroup.CommitException e) {
                e.printStackTrace();
            }
            getUI().getNavigator().navigateTo(RecruitView.VIEW_NAME);
        });
        form.addComponent(saveButton, 0, 5);
        HorizontalLayout layout = new HorizontalLayout(form);
        layout.setSizeFull();
        addComponent(layout);
    }

    private void buildBenefits(BeanFieldGroup<JobListBean> binder, GridLayout form) {
        TextArea benefits = binder.buildAndBind("Benefits", "benefits", TextArea.class);
        benefits.setRows(6);
        benefits.setWidth(100, Unit.PERCENTAGE);
        form.addComponent(benefits, 0, 4, 0, 4);
    }

    private void buildRequirements(BeanFieldGroup<JobListBean> binder, GridLayout form) {
        TextArea requirements = binder.buildAndBind("Requirements", "requirements", TextArea.class);
        requirements.setRows(6);
        requirements.setWidth(100, Unit.PERCENTAGE);
        form.addComponent(requirements, 0, 3, 0, 3);
    }

    private void buildDescription(BeanFieldGroup<JobListBean> binder, GridLayout form) {
        TextArea description = binder.buildAndBind("Description", "description", TextArea.class);
        description.setRows(4);
        description.setWidth(100, Unit.PERCENTAGE);
        form.addComponent(description, 0, 2, 0, 2);
    }

    private void buildFunction(BeanFieldGroup<JobListBean> binder, GridLayout form) {
        TextField function = binder.buildAndBind("Position", "function", TextField.class);
        function.setWidth(100, Unit.PERCENTAGE);
        form.addComponent(function, 0, 1, 0, 1);
    }

    private void buildName(BeanFieldGroup<JobListBean> binder, GridLayout form) {
        TextField name = binder.buildAndBind("Name", "name", TextField.class);
        name.setWidth(100, Unit.PERCENTAGE);
        name.setTabIndex(0);
        form.addComponent(name, 0, 0, 0, 0);
    }

    @Override
    public void enter(ViewChangeEvent event) {
        String parameters = event.getParameters();
        UUID uuid = null;
        JobListBean jobListBean = null;
        try {
            uuid = UUID.fromString(parameters);
            Optional<JobListing> jobListing = jobListingService.findByUuid(uuid);
            jobListBean = jobListing.map(j -> mapper.convertValue(j, JobListBean.class))
                    .orElse(new JobListBean());
        } catch (IllegalArgumentException e) {
            jobListBean = new JobListBean();
        }
        binder.setItemDataSource(jobListBean);
    }
}