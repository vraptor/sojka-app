package co.jware.sojka.views;

import co.jware.sojka.ui.LearnUI;
import co.jware.sojka.vaadin.NavBar;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;

import javax.annotation.PostConstruct;

@SpringView(ui = LearnUI.class, name = LearnView.VIEW_NAME)
public class LearnView extends VerticalLayout implements View {
    public static final String VIEW_NAME = "";

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {

    }
    @PostConstruct
    void init(){
        setSizeFull();
        VerticalLayout layout = mainContent();
        addComponent(new NavBar());
        addComponent(layout);
        addComponent(new Label("jWare"));
        setExpandRatio(layout, 1.0f);
    }

    private VerticalLayout mainContent() {
        VerticalLayout layout = new VerticalLayout();
        layout.setSizeFull();
        layout.setMargin(true);
        layout.addComponent(new Label("Learn to Skill Up"));
        return layout;
    }
}
