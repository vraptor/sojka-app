package co.jware.sojka.views;

import co.jware.sojka.ui.LoginForm;
import co.jware.sojka.ui.SojkaUI;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.server.VaadinService;
import com.vaadin.shared.communication.PushMode;
import com.vaadin.shared.ui.ui.Transport;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.PushConfiguration;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;

import javax.annotation.PostConstruct;

@Deprecated
@SpringView(ui = SojkaUI.class, name = LoginView.VIEW_NAME)
public class LoginView extends VerticalLayout implements View {

    public static final String VIEW_NAME = "login";
    @Autowired
    private AuthenticationManager authenticationManager;

    @PostConstruct
    void init() {
        setSizeFull();
        LoginForm loginForm = new LoginForm(this::login);
        addComponent(loginForm);
        setComponentAlignment(loginForm, Alignment.MIDDLE_CENTER);

    }

    private boolean login(String username, String password) {
        try {
            UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(username, password);
            Authentication auth = authenticationManager.authenticate(token);
            VaadinService.reinitializeSession(VaadinService.getCurrentRequest());
            SecurityContextHolder.getContext().setAuthentication(auth);
            PushConfiguration pushConfiguration = UI.getCurrent().getPushConfiguration();
            pushConfiguration.setTransport(Transport.WEBSOCKET);
            pushConfiguration.setPushMode(PushMode.AUTOMATIC);
            getUI().getNavigator().navigateTo(DefaultView.VIEW_NAME);
            return true;
        } catch (AuthenticationException e) {
            return false;
        }
    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {

    }
}
