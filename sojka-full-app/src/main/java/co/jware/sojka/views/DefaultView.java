package co.jware.sojka.views;

import co.jware.sojka.ui.SojkaUI;
import co.jware.sojka.vaadin.NavBar;
import co.jware.sojka.vaadin.beans.AccountHolder;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.server.ClassResource;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;

import javax.annotation.PostConstruct;

@SpringView(ui = SojkaUI.class, name = DefaultView.VIEW_NAME)
@Secured("ROLE_USER")
public class DefaultView extends VerticalLayout implements View {

    public static final String VIEW_NAME = "";
    @Autowired
    private NavBar navBar;
    @Autowired
    private AccountHolder accountHolder;

    @PostConstruct
    void init() {
        setSizeFull();
        addComponent(navBar);
        Layout panes = buildPanes();
        setExpandRatio(panes, 1.0f);
        setComponentAlignment(panes, Alignment.MIDDLE_CENTER);
        addComponent(new Label("jWare"));
    }

    private MenuBar buildMenu() {
        MenuBar menuBar = new MenuBar();
        menuBar.setSizeFull();
        MenuBar.MenuItem loginItem = menuBar.addItem("Login", null);
        menuBar.addItem("Registration",null);
        return menuBar;
    }

    private Layout buildPanes() {
        HorizontalLayout panes = new HorizontalLayout();
        panes.addComponents(buildJobsPanel(), buildRecruitPanel(), buildEducationPanel());
        panes.setSpacing(true);
        addComponent(panes);
        return panes;
    }

    private Panel buildJobsPanel() {
        Panel findJobPanel = new Panel("Find a Job", buildLogo());
        findJobPanel.addClickListener(
                event -> getUI().getPage().open("/candidate", null));
        return findJobPanel;
    }

    private Panel buildRecruitPanel() {
        Panel panel = new Panel("Post a Job", buildLogo());
        panel.addClickListener(event -> getUI().getPage().open("/recruiter", null));
        return panel;
    }

    private Panel buildEducationPanel() {
        Panel panel = new Panel("Join a Course", buildLogo());
        panel.addClickListener(event -> getUI().getPage().open("/teacher", null));
        return panel;
    }

    private Image buildLogo() {
        ClassResource logoResource = new ClassResource("/images/sojka_logo_2.svg");
        return new Image("", logoResource);
    }


    @Override
    public void enter(ViewChangeListener.ViewChangeEvent viewChangeEvent) {
    }
}
