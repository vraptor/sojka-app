package co.jware.sojka.views;


import co.jware.sojka.core.domain.party.Hirer;
import co.jware.sojka.core.domain.JobListing;
import co.jware.sojka.core.service.entity.JobListingService;
import co.jware.sojka.ui.RecruitUI;
import co.jware.sojka.vaadin.NavBar;
import co.jware.sojka.vaadin.beans.AccountHolder;
import co.jware.sojka.vaadin.beans.JobListBean;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.eventbus.Subscribe;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.util.AbstractBeanContainer.BeanIdResolver;
import com.vaadin.data.util.BeanContainer;
import com.vaadin.navigator.Navigator;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.server.FontAwesome;
import com.vaadin.shared.ui.MultiSelectMode;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.*;
import com.vaadin.ui.themes.ValoTheme;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.UUID;

import static java.util.Comparator.comparing;
import static java.util.stream.Collectors.toList;

@SpringView(ui = RecruitUI.class, name = RecruitView.VIEW_NAME)
public class RecruitView extends VerticalLayout implements View {

    public static final String VIEW_NAME = "";
    @Autowired
    ObjectMapper mapper;
    @Autowired
    JobListingService jobListingService;
    private Table jobsTable;
    private Button editButton;
    @Autowired
    private NavBar navBar;
    @Autowired
    private AccountHolder accountHolder;
    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {
        checkPermission();
        System.out.println("enter called");
//        loadData();
    }

    @Subscribe()
    private void loadData() {
        if (!accountHolder.isHirer()) {
            return;
        }
        BeanContainer<UUID, JobListBean> container = new BeanContainer<>(JobListBean.class);
        container.setBeanIdResolver((BeanIdResolver<UUID, JobListBean>) JobListBean::getUuid);
        PageRequest pageRequest = new PageRequest(1, 20);
        Page<JobListing> page = jobListingService.findByOwner((Hirer) accountHolder.owner(),
                pageRequest);
        if (page.hasContent()) {
            List<JobListBean> list = page.getContent().stream()
                    .sorted(comparing(JobListing::validFrom).reversed())
                    .map(e -> mapper.convertValue(e, JobListBean.class))
                    .collect(toList());
            container.addAll(list);
            jobsTable.setContainerDataSource(container);
            jobsTable.setVisibleColumns("name", "description");
            jobsTable.setWidth(85, Unit.PERCENTAGE);
            jobsTable.setColumnHeaders("Position", "Description");
            jobsTable.setColumnExpandRatio("name", 0.4f);
            jobsTable.setColumnExpandRatio("description", 0.6f);
            jobsTable.setSelectable(true);
            jobsTable.setMultiSelectMode(MultiSelectMode.DEFAULT);
            jobsTable.addValueChangeListener((ValueChangeListener) event -> {
                if (null != event.getProperty().getValue()) {
                    editButton.setEnabled(true);
                }
            });
        }
    }

    @PostConstruct
    void init() {
        System.out.println("post construct called");
        setSizeFull();
        addComponent(navBar);
        jobsTable = new Table("Job Listings");
        Panel panel = new Panel(jobsTable);
        panel.setSizeFull();
        Button createButton = createButton();
        editButton = editButton();
        HorizontalLayout buttons = new HorizontalLayout(createButton, editButton);
        addComponent(buttons);
        addComponent(panel);
        setExpandRatio(panel, 1.0f);
        addComponent(buildFooter());
    }

    private void checkPermission() {
        if (accountHolder == null || !accountHolder.isHirer()) {
            Navigator navigator = UI.getCurrent().getNavigator();
            navigator.addView(LoginView.VIEW_NAME, new LoginView());
            navigator.navigateTo(LoginView.VIEW_NAME);
        }
    }

    private Button editButton() {
        Button button = new Button("Edit", FontAwesome.EDIT);
        button.setStyleName(ValoTheme.BUTTON_BORDERLESS);
        button.setEnabled(false);
        button.addClickListener(evt -> {
            UUID uuid = (UUID) this.jobsTable.getValue();
            getUI().getNavigator().navigateTo(JobCreateView.VIEW_NAME + "/" +
                    uuid.toString());
        });
        return button;
    }

    private Button createButton() {
        Button button = new Button("Create new", FontAwesome.PLUS_SQUARE);
        button.setStyleName(ValoTheme.BUTTON_BORDERLESS);
        button.addClickListener(evt -> getUI().getNavigator().navigateTo(JobCreateView.VIEW_NAME));
        return button;
    }

    private Layout buildFooter() {
        Label copyRight = new Label("Powered By jWare");
        HorizontalLayout layout = new HorizontalLayout(copyRight);
        layout.setComponentAlignment(copyRight, Alignment.TOP_CENTER);
        return layout;
    }

}
