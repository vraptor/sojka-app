package co.jware.sojka.views;

import co.jware.sojka.core.domain.JobListing;
import co.jware.sojka.core.enums.JobListingOrigin;
import co.jware.sojka.core.enums.JobListingStatus;
import co.jware.sojka.core.service.PersonService;
import co.jware.sojka.core.service.entity.JobListingService;
import co.jware.sojka.ui.JobsUI;
import co.jware.sojka.vaadin.NavBar;
import co.jware.sojka.vaadin.SuggestingComboBox;
import co.jware.sojka.vaadin.SuggestingContainer;
import com.vaadin.data.Property;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.server.FontAwesome;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.UUID;

import static com.vaadin.server.Sizeable.Unit.PERCENTAGE;

@SpringView(ui = JobsUI.class, name = JobsView.VIEW_NAME)
public class JobsView extends VerticalLayout implements View {
    public static final String VIEW_NAME = "";

    private Table table;

    @Autowired
    private JobListingService jobListingService;
    @Autowired
    private PersonService personService;
    @PostConstruct
    void init() {
        setSpacing(true);
        setMargin(true);
        addComponent(new NavBar());
        table = jobsTable();
        addComponent(table);
        table.addValueChangeListener((Property.ValueChangeListener) event -> {
            Property property = event.getProperty();
            Class type = property.getType();
            Object value = property.getValue();
            type.cast(value);
        });
        addComponent(new Button("Update"));
    }

    private Table jobsTable() {
        Page<JobListing> jobListings = jobListingService.findJobListings(new PageRequest(0, 25));
        Table table = new Table("Jobs Listing");
        table.addContainerProperty("Title", String.class, null);
        table.addContainerProperty("Status", JobListingStatus.class, null);
        table.addContainerProperty("Origin", JobListingOrigin.class, null);
        table.addContainerProperty("Action", Button.class, null);
        if (jobListings.hasContent()) {
            List<JobListing> jobs = jobListings.getContent();
            jobs.stream().forEach(job -> {
                UUID uuid = job.uuid().orElse(UUID.randomUUID());
                table.addItem(new Object[]{
                        job.title().orElse("UNNAMED"),
                        job.status(),
                        job.origin(),
                        recommendButton(job)}, uuid);
            });
            table.setCurrentPageFirstItemIndex(0);
            table.setSizeFull();
        }

        table.setPageLength(15);
        table.setEditable(false);
        return table;
    }

    private Button recommendButton(JobListing jobListing) {
        Button button = new Button("", FontAwesome.THUMBS_O_UP);
        button.setDescription("Recommend");
        button.setData(jobListing);
        button.addClickListener((Button.ClickListener) event -> {
            Window window = recommendWindow((JobListing) event.getButton().getData());
            UI.getCurrent().addWindow(window);
        });
        return button;
    }

    private Window recommendWindow(JobListing jobListing) {
        String title = jobListing.title().orElse("");
        Window window = new Window("Job Recommendation");
        window.center();
        window.setModal(true);
        window.setWidth(50, PERCENTAGE);
        window.setHeight(75, PERCENTAGE);
        window.setIcon(FontAwesome.THUMBS_O_UP);
        VerticalLayout content = new VerticalLayout();
        content.setSizeFull();
        Button save = new Button("Save", FontAwesome.SAVE);
        Button cancel = new Button("Cancel", FontAwesome.CLOSE);
        cancel.addClickListener((Button.ClickListener) event -> {
            window.close();
        });
        content.addComponent(new Label(title));
        SuggestingComboBox receiver = new SuggestingComboBox("Receiver");
        receiver.setImmediate(true);
        receiver.setTextInputAllowed(true);
        receiver.setNewItemsAllowed(true);
        receiver.addValueChangeListener(new Property.ValueChangeListener() {
            @Override
            public void valueChange(Property.ValueChangeEvent event) {
                Object value = event.getProperty().getValue();
            }
        });
        receiver.setContainerDataSource(new SuggestingContainer(personService));
        content.addComponent(receiver);
        HorizontalLayout layout = new HorizontalLayout();
        layout.addComponent(save);
        layout.addComponent(cancel);
        layout.setWidth(25, PERCENTAGE);
        layout.setComponentAlignment(save, Alignment.BOTTOM_LEFT);
        layout.setComponentAlignment(cancel, Alignment.BOTTOM_RIGHT);
        content.addComponent(layout);
        content.setSpacing(true);
        content.setMargin(true);
        content.setComponentAlignment(layout, Alignment.BOTTOM_CENTER);
        window.setContent(content);
        return window;
    }

    private ListSelect statusList() {
        ListSelect status = new ListSelect();
        status.addItems(JobListingStatus.values());
        status.setRows(1);
        status.setImmediate(true);
        return status;
    }

    private ListSelect originList() {
        ListSelect origin = new ListSelect();
        origin.addItems(JobListingOrigin.values());
        origin.setConvertedValue(JobListingOrigin.EXTERNAL);
        origin.setRows(1);
        origin.setImmediate(true);
        return origin;
    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {
    }
}
