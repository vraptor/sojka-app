package co.jware.sojka.views;

import co.jware.sojka.ui.SojkaUI;
import co.jware.sojka.vaadin.NavBar;
import co.jware.sojka.vaadin.beans.RegisterBean;
import com.vaadin.data.fieldgroup.BeanFieldGroup;
import com.vaadin.data.fieldgroup.FieldGroup;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;

import javax.annotation.PostConstruct;

@SpringView(ui = SojkaUI.class, name = RegisterView.VIEW_NAME)
public class RegisterView extends VerticalLayout implements View {

    public static final String VIEW_NAME = "register";

    @Autowired
    private MessageSource msg;

    @PostConstruct
    void init() {
        addComponent(new NavBar());
        setSizeFull();
        Panel register = new Panel("Register", buildRegister());
        register.setSizeUndefined();
        addComponent(register);
        setExpandRatio(register, 1.0f);
        setComponentAlignment(register, Alignment.MIDDLE_CENTER);
    }

    private Component buildRegister() {
        FieldGroup fieldGroup = new BeanFieldGroup<>(RegisterBean.class);
        String caption = "First Name";
        TextField firstName = fieldGroup.buildAndBind(msg.getMessage("firstName", null, LocaleContextHolder.getLocale()),
                "firstName", TextField.class);
        TextField lastName = fieldGroup.buildAndBind(msg.getMessage("lastName", null, LocaleContextHolder.getLocale()),
                "lastName", TextField.class);
        TextField email = fieldGroup.buildAndBind("E-Mail", "email", TextField.class);
        FormLayout form = new FormLayout(firstName, lastName, email);
        Component buttons = buildButtons();
        VerticalLayout layout = new VerticalLayout(form, buttons);
        layout.setComponentAlignment(buttons, Alignment.BOTTOM_CENTER);
        layout.setMargin(true);
        return layout;
    }

    private Component buildButtons() {
        HorizontalLayout layout = new HorizontalLayout(new Button("Submit"),
                buildCancel());
        layout.setSpacing(true);
        layout.setSizeUndefined();
        return layout;
    }

    private Button buildCancel() {
        Button cancel = new Button("Cancel", evt -> getUI().getNavigator().navigateTo(DefaultView.VIEW_NAME));
        return cancel;
    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {
    }
}
