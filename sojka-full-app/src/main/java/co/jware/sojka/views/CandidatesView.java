package co.jware.sojka.views;

import co.jware.sojka.app.candidate.CandidateUI;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;

import javax.annotation.PostConstruct;

@SpringView(ui = CandidateUI.class, name = CandidatesView.VIEW_NAME)
public class CandidatesView extends VerticalLayout implements View {
    public static final String VIEW_NAME = "";

    @PostConstruct
    void init() {
        addComponent(new Label("Candidates"));
    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {

    }
}
