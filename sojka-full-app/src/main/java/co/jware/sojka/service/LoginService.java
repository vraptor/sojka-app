package co.jware.sojka.service;


import co.jware.sojka.core.domain.Account;
import co.jware.sojka.core.domain.UserInfo;
import co.jware.sojka.core.service.entity.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Service;

@Service("uiLoginService")
public class LoginService {
    private AuthenticationManager authenticationManager;
    private AccountService accountService;

    @Autowired
    public LoginService(AuthenticationManager authenticationManager, AccountService accountService) {
        this.authenticationManager = authenticationManager;
        this.accountService = accountService;
    }

    public Account login(String userName, String password) {
        UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(userName, password);
        try {
            Authentication auth = authenticationManager.authenticate(token);
            Account account = accountService.findByUsername(userName).map(a -> a).orElse(null);
            UserInfo userEntity = (UserInfo) auth.getPrincipal();
            return account;
        } catch (AuthenticationException e) {
            return null;
        }
    }
}
