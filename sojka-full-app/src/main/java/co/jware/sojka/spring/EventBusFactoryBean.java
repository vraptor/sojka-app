package co.jware.sojka.spring;


import com.google.common.eventbus.EventBus;
import org.springframework.beans.factory.config.AbstractFactoryBean;

public class EventBusFactoryBean extends AbstractFactoryBean<EventBus> {

    private EventBus eventBus;

    @Override
    public boolean isSingleton() {
        return true;
    }

    @Override
    public Class<?> getObjectType() {
        return EventBus.class;
    }

    @Override
    protected EventBus createInstance() throws Exception {

        eventBus = new EventBus((exception, context) -> exception.printStackTrace());
        return eventBus;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        super.afterPropertiesSet();
//        uiList.forEach(eventBus::register);
    }
}
