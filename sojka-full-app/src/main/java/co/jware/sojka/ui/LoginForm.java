package co.jware.sojka.ui;


import com.vaadin.event.ShortcutAction;
import com.vaadin.server.ClassResource;
import com.vaadin.server.FontAwesome;
import com.vaadin.ui.*;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.themes.ValoTheme;

import static com.vaadin.server.Sizeable.Unit.REM;

public class LoginForm extends VerticalLayout {

    public LoginForm(LoginCallback callback) {
        setSizeUndefined();
        setStyleName("login-form");
//        setWidth(30, Unit.PERCENTAGE);
//        setHeight(14, REM);
        setSpacing(true);
//        setMargin(true);
        HorizontalLayout fields = buildFields(callback);
//        addComponent(new Label("LOGIN"));
//        addComponent(buildLogo());
        addComponent(fields);
        addComponent(forgotten());
        setExpandRatio(fields, 1.0f);
        setComponentAlignment(fields, Alignment.MIDDLE_CENTER);
    }

    private Image buildLogo() {
        ClassResource logoResource = new ClassResource("/images/sojka_logo_4 svg");
        Image image = new Image("", logoResource);
        image.setHeight(3, REM);
        return image;
    }
    private HorizontalLayout buildFields(LoginCallback callback) {
        HorizontalLayout fields = new HorizontalLayout();
        fields.setSpacing(true);

        TextField userName = new TextField("Username");
        userName.setIcon(FontAwesome.USER);
        userName.addStyleName(ValoTheme.TEXTFIELD_INLINE_ICON);
        PasswordField passwordField = new PasswordField("Password");
        passwordField.setIcon(FontAwesome.LOCK);
        passwordField.addStyleName(ValoTheme.TEXTFIELD_INLINE_ICON);
        Button login = new Button("Login");
        login.addStyleName(ValoTheme.BUTTON_PRIMARY);
        login.addClickListener(evt -> {
            String username = userName.getValue();
            String password = passwordField.getValue();
            passwordField.setValue("");
            if (!callback.login(username, password)) {
                Notification.show("Login failed", Type.ERROR_MESSAGE);
                userName.focus();
            }
        });
        login.setClickShortcut(ShortcutAction.KeyCode.ENTER);
        fields.addComponents(userName, passwordField, login);
        fields.setComponentAlignment(login, Alignment.BOTTOM_LEFT);
        return fields;
    }

    private Button forgotten() {
        Button button = new Button("Forgotten Password", evt -> {
            Window window = buildForgottenWindow();
            UI.getCurrent().addWindow(window);
        });
        button.setStyleName(ValoTheme.BUTTON_LINK);
        return button;
    }

    private Window buildForgottenWindow() {
        Window window = new Window("Forgotten Password");
        window.setWidth(50, Unit.PERCENTAGE);
        window.setModal(true);
        window.center();
        TextField email = new TextField("E-Mail");
        email.setWidth(75, Unit.PERCENTAGE);
        Button button = new Button("Send Reset Link", evt -> {
            Notification.show("E-mail with password reset link sent", Type.HUMANIZED_MESSAGE);
            window.close();
        });
        HorizontalLayout layout = new HorizontalLayout(email, button);
        layout.setWidth(100, Unit.PERCENTAGE);
        layout.setComponentAlignment(button, Alignment.BOTTOM_RIGHT);
        layout.setExpandRatio(email, 1.0f);
        layout.setMargin(true);
        layout.setSpacing(true);
        window.setContent(layout);
        return window;
    }

    @FunctionalInterface
    public interface LoginCallback {
        boolean login(String userName, String password);
    }
}
