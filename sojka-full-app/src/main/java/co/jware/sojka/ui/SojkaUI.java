package co.jware.sojka.ui;


import com.vaadin.annotations.Theme;
import com.vaadin.navigator.Navigator;
import com.vaadin.server.VaadinRequest;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.spring.navigator.SpringViewProvider;
import com.vaadin.ui.Panel;
import com.vaadin.ui.VerticalLayout;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Optional;
import java.util.UUID;

@SpringUI
@Theme(BaseUI.THEME_NAME)
public class SojkaUI extends BaseUI {

    @Autowired
    private SpringViewProvider viewProvider;

    @Override
    protected void init(VaadinRequest vaadinRequest) {
        String action = vaadinRequest.getParameter("action");
        if (action != null) {
            String uuid = vaadinRequest.getParameter("uuid");
            Optional.ofNullable(uuid)
                    .map(u -> UUID.fromString(uuid));
        }
        final VerticalLayout root = new VerticalLayout();
        root.setSizeFull();
        setContent(root);
        final Panel viewContainer = new Panel();
        viewContainer.setSizeFull();
        root.addComponent(viewContainer);
        root.setExpandRatio(viewContainer, 1.0f);
        Navigator navigator = new Navigator(this, viewContainer);
        navigator.addProvider(viewProvider);
    }

    @Override
    protected void refresh(VaadinRequest request) {
        super.refresh(request);
    }
}
