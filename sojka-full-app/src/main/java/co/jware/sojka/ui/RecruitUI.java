package co.jware.sojka.ui;

import co.jware.sojka.core.service.entity.JobListingService;
import co.jware.sojka.views.RecruitView;
import com.vaadin.server.VaadinRequest;
import org.springframework.beans.factory.annotation.Autowired;

@Deprecated
public class RecruitUI extends BaseUI {
    @Autowired
    JobListingService jobListingService;

    @Override
    protected void init(VaadinRequest request) {
        super.init(request);
        getNavigator().addView(RecruitView.VIEW_NAME, new RecruitView());
    }
}
