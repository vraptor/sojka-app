package co.jware.sojka.vaadin;


import co.jware.sojka.core.domain.Person;
import co.jware.sojka.core.service.PersonService;
import com.vaadin.data.Container;
import com.vaadin.data.Item;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.data.util.filter.UnsupportedFilterException;

import java.util.List;
import java.util.UUID;

import static java.util.stream.Collectors.toList;

public class SuggestingContainer extends BeanItemContainer<EmailBean> {

    private PersonService personService;

    public SuggestingContainer(PersonService personService) throws IllegalArgumentException {
        super(EmailBean.class);
        this.personService = personService;
    }

    @Override
    public void addContainerFilter(Filter filter) throws UnsupportedFilterException {
        SuggestionFilter suggestionFilter = (SuggestionFilter) filter;
        filterItems(suggestionFilter.getFilterString());
    }


    private void filterItems(String filterString) {
        if ("".equals(filterString) || filterString.length() < 3) {
            return;
        }

        List<Person> emails = personService.findByEmailContaining(filterString);
        removeAllItems();
//        if (!emails.isEmpty()) {
            addAll(emails.stream().map(p -> new EmailBean(p.email(), p.uuid().get()))
                    .collect(toList()));
//        } else
            addBean(new EmailBean(filterString, UUID.randomUUID()));
    }


    public static class SuggestionFilter implements Container.Filter {

        private String filterString;

        public SuggestionFilter(String filterString) {
            this.filterString = filterString;
        }

        @Override
        public boolean passesFilter(Object itemId, Item item) throws UnsupportedOperationException {
            return false;
        }

        @Override
        public boolean appliesToProperty(Object propertyId) {
            return false;
        }

        public String getFilterString() {
            return filterString;
        }
    }
}
