package co.jware.sojka.vaadin;


import com.vaadin.data.Container;
import com.vaadin.shared.ui.combobox.FilteringMode;
import com.vaadin.ui.ComboBox;

public class SuggestingComboBox extends ComboBox {

    public SuggestingComboBox(String caption) {
        super(caption);
        setItemCaptionMode(ItemCaptionMode.PROPERTY);
        setItemCaptionPropertyId("email");
    }

    public SuggestingComboBox() {
        this(null);
    }

    @Override
    protected Filter buildFilter(String filterString, FilteringMode filteringMode) {
        return new SuggestingContainer.SuggestionFilter(filterString);
    }
}
