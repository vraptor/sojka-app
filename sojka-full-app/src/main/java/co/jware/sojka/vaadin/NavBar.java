package co.jware.sojka.vaadin;


import co.jware.sojka.vaadin.beans.AccountHolder;
import co.jware.sojka.views.LoginView;
import co.jware.sojka.views.RegisterView;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.Page;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.spring.annotation.ViewScope;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.UI;
import com.vaadin.ui.themes.ValoTheme;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;

@SpringComponent
@ViewScope
public class NavBar extends HorizontalLayout {
    @Autowired
    private AccountHolder accountHolder;

    public NavBar() {
        super();
    }

    @PostConstruct
    public void init() {
        setStyleName("navbar");
        setWidth(100, Unit.PERCENTAGE);
        setHeight(3, Unit.REM);
        addComponent(homeButton());
        if (SecurityUtils.isFullyAuthenticated()) {
            String email = accountHolder.owner().email();
            Button logout = buildLogout(email);
            Button emailButton = new Button(email);
            emailButton.setStyleName(ValoTheme.BUTTON_LINK);
            HorizontalLayout right = new HorizontalLayout(emailButton, logout);
            addComponent(right);
            setComponentAlignment(right, Alignment.TOP_RIGHT);
        } else {
            Button login = buildLogin();
            Button register = buildRegister();
            HorizontalLayout rightBar = new HorizontalLayout(login, register);
            addComponent(rightBar);
            setComponentAlignment(rightBar, Alignment.TOP_RIGHT);
        }
    }

    private Button homeButton() {
        Button home = new Button("Home", evt -> Page.getCurrent().setLocation("/"));
        home.setIcon(FontAwesome.HOME);
        setButtonStyles(home);
        return home;
    }

    private Button buildLogout(String email) {
        Button logout = new Button(email, evt -> {
            Page.getCurrent().setLocation("/logout");
            UI.getCurrent().getSession().close();
        });
        logout.setIcon(FontAwesome.SIGN_OUT);
        setButtonStyles(logout);
//        logout.setStyleName(ValoTheme.BUTTON_BORDERLESS);
//        logout.setStyleName(ValoTheme.BUTTON_LARGE);
        return logout;
    }

    private Button buildLogin() {
        Button login = new Button("Login", evt -> {
            getUI().getNavigator().navigateTo(LoginView.VIEW_NAME);
        });
        login.setIcon(FontAwesome.SIGN_IN, "Login");
        setButtonStyles(login);
        return login;
    }

    private Button buildRegister() {
        Button register = new Button("Register", evt -> {
            getUI().getNavigator().navigateTo(RegisterView.VIEW_NAME);
        });
        register.setIcon(FontAwesome.USER_PLUS, "Register");
        setButtonStyles(register);
        return register;
    }

    private void setButtonStyles(Button button) {
        button.addStyleName(ValoTheme.BUTTON_ICON_ONLY);
        button.addStyleName(ValoTheme.BUTTON_BORDERLESS);
        button.addStyleName(ValoTheme.BUTTON_LARGE);
    }
}
