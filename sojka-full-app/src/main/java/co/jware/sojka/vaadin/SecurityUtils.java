package co.jware.sojka.vaadin;


import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.Objects;

public final class SecurityUtils {

    public static boolean isLoggedIn() {
        Authentication auth = SecurityContextHolder
                .getContext()
                .getAuthentication();
        return auth != null && auth.isAuthenticated();
    }

    public static boolean isFullyAuthenticated() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        return auth != null && auth.isAuthenticated() && !AnonymousAuthenticationToken.class.isInstance(auth);
    }

    public static boolean hasRole(String role) {
        String roleRequired = Objects.requireNonNull(role, "Role is required");
        Authentication auth = SecurityContextHolder
                .getContext()
                .getAuthentication();
        SimpleGrantedAuthority authority = new SimpleGrantedAuthority(roleRequired);
        return auth != null && auth.getAuthorities().contains(authority);
    }
}
