package co.jware.sojka.event;

import com.google.common.eventbus.EventBus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class GlobalEventBus {

    private static EventBus EVENTBUS;

    @Autowired
    void setEventBus(EventBus eventBus) {
        EVENTBUS = eventBus;
    }

    public static EventBus eventBus() {
        return EVENTBUS;
    }

    public static void register(Object obj) {
        EVENTBUS.register(obj);
    }

    public static void unregister(Object obj) {
        EVENTBUS.unregister(obj);
    }

    public static void post(Object event) {
        EVENTBUS.post(event);
    }
}
