package co.jware.sojka.event;


import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.SubscriberExceptionContext;
import com.google.common.eventbus.SubscriberExceptionHandler;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.spring.annotation.VaadinSessionScope;
import org.springframework.stereotype.Component;

//@SpringComponent
//@VaadinSessionScope
@Component
public class SojkaEventBus implements SubscriberExceptionHandler {

    private final EventBus eventBus = new EventBus();

    private static EventBus _instance;

    public void register(Object object) {
        eventBus.register(object);
    }

    public void unregister(Object object) {
        eventBus.unregister(object);
    }

    public void post(Object event) {
        eventBus.post(event);
    }

    @Override
    public void handleException(Throwable throwable, SubscriberExceptionContext subscriberExceptionContext) {

    }
}
