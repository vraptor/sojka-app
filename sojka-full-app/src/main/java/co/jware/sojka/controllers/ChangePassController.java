package co.jware.sojka.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.UUID;

@Controller
@RequestMapping(path = "/change-pass")
public class ChangePassController {

    @RequestMapping(path = "/{uuid}")
    public ModelAndView changePass(@PathVariable UUID uuid, Model model) {
        model.addAttribute("action", "changePass");
        model.addAttribute("uuid", uuid);
        ModelAndView mv = new ModelAndView("redirect:/");
        mv.addAllObjects(model.asMap());
        return mv;
    }
}
