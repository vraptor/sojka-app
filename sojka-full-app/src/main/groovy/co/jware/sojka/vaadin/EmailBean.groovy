package co.jware.sojka.vaadin;

import groovy.transform.Canonical;

@Canonical
public class EmailBean {
    String email
    UUID uuid

    EmailBean(String email, UUID uuid) {
        this.email = email
        this.uuid = uuid
    }
}
