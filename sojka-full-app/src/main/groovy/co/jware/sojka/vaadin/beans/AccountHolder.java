package co.jware.sojka.vaadin.beans;

import co.jware.sojka.core.domain.Account;
import co.jware.sojka.core.domain.party.Hirer;
import co.jware.sojka.core.domain.Party;
import co.jware.sojka.core.domain.User;
import co.jware.sojka.core.enums.Role;
import co.jware.sojka.vaadin.SecurityUtils;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.spring.annotation.VaadinSessionScope;

import java.io.Serializable;

@VaadinSessionScope
@SpringComponent
public class AccountHolder implements Serializable {

    private Account account;

    public User user() {
        return account.user()
                .orElseThrow(IllegalArgumentException::new);
    }


    public Party owner() {
        return account.owner()
                .orElseThrow(IllegalArgumentException::new);
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public boolean isHirer() {
        return account != null && (Hirer.class.isInstance(owner()) ||
                SecurityUtils.hasRole(Role.HIRER.name()));
    }
}
