package co.jware.sojka.vaadin.beans

import co.jware.sojka.core.enums.JobListingStatus
import groovy.transform.Canonical

@Canonical
class JobListBean {

    UUID uuid
    String title = ''
    String name = ''
    String function = ''
    String description = ''
    String requirements = ''
    String benefits = ''
    JobListingStatus status
    Long version

}
