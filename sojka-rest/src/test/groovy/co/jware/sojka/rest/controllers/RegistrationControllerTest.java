package co.jware.sojka.rest.controllers;

import co.jware.sojka.core.enums.RegistrationType;
import co.jware.sojka.core.service.RegisterService;
import co.jware.sojka.rest.ApiConstants;
import co.jware.sojka.rest.core.dto.RegisterDto;
import co.jware.sojka.rest.service.api.RegisterApiService;
import co.jware.sojka.rest.service.api.impl.RegisterApiServiceImpl;
import mockit.MockUp;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@ContextConfiguration
public class RegistrationControllerTest extends BaseControllerTest {

    @Autowired
    private RegisterService registerService;

    @Test
    @WithMockUser
    public void register() throws Exception {
        String content = mapper.writeValueAsString(RegisterDto.builder()
                .email("e.k@voidmail.com")
                .registrationType(RegistrationType.CANDIDATE)
                .build());
        mockMvc.perform(post(ApiConstants.REGISTRATION_API)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .accept(MediaType.APPLICATION_JSON_UTF8)
                .content(content)
                .with(csrf())
        ).andExpect(status().isCreated());
    }

    @Configuration
    static class TestConfig {
        @Bean
        RegistrationController registrationController() {
            return new RegistrationController(registerApiService());
        }

        @Bean
        RegisterApiService registerApiService() {
            return new RegisterApiServiceImpl(registerService());
        }

        @Bean
        RegisterService registerService() {
            return new MockUp<RegisterService>(RegisterService.class) {
            }.getMockInstance();
        }
    }
}