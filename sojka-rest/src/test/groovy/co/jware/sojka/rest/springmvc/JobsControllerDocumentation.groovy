package co.jware.sojka.rest.springmvc

import co.jware.sojka.rest.SojkaRestApplication
import com.fasterxml.jackson.databind.ObjectMapper
import com.jayway.restassured.builder.RequestSpecBuilder
import com.jayway.restassured.http.ContentType
import com.jayway.restassured.response.ValidatableResponse
import com.jayway.restassured.specification.RequestSpecification
import org.junit.Before
import org.junit.Ignore
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.restdocs.JUnitRestDocumentation
import org.springframework.restdocs.operation.preprocess.OperationRequestPreprocessor
import org.springframework.restdocs.operation.preprocess.OperationResponsePreprocessor
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.junit4.SpringRunner

import static com.jayway.restassured.RestAssured.given
import static org.hamcrest.CoreMatchers.*
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT
import static org.springframework.restdocs.operation.preprocess.Preprocessors.*
import static org.springframework.restdocs.payload.PayloadDocumentation.fieldWithPath
import static org.springframework.restdocs.payload.PayloadDocumentation.relaxedResponseFields
import static org.springframework.restdocs.request.RequestDocumentation.parameterWithName
import static org.springframework.restdocs.request.RequestDocumentation.pathParameters
import static org.springframework.restdocs.restassured.RestAssuredRestDocumentation.document
import static org.springframework.restdocs.restassured.RestAssuredRestDocumentation.documentationConfiguration
import static org.springframework.restdocs.restassured.operation.preprocess.RestAssuredPreprocessors.modifyUris

@RunWith(SpringRunner.class)
@SpringBootTest(classes = SojkaRestApplication.class, webEnvironment = RANDOM_PORT)
@ActiveProfiles("test")
public class JobsControllerDocumentation {

    public static final String METHOD_NAME = "{class-name}/{method-name}"
    @Autowired
    JdbcTemplate jdbcTemplate
    @Autowired
    ObjectMapper mapper
    @Rule
    public JUnitRestDocumentation restDocumentation = new JUnitRestDocumentation("target/generated-snippets")
    @Value('${local.server.port}')
    private int port
//    @Value('${local.server.contextPath}')
//    private String prefix
    private RequestSpecification documentationSpec
    private OperationRequestPreprocessor requestPreprocessor
    private OperationResponsePreprocessor responsePreprocessor

    @Before
    public void setUp() {
        requestPreprocessor = preprocessRequest(modifyUris()
                .host("api.sojka.jware.co")
                .removePort())
        responsePreprocessor = preprocessResponse(prettyPrint())
        this.documentationSpec = new RequestSpecBuilder()
                .addFilter(documentationConfiguration(restDocumentation))
                .build()
    }

    @Test
    @Ignore
    public void getJobListing() throws Exception {
        List<String> uuids = jdbcTemplate.queryForList("select uuid from job_listing", String.class)
        UUID uuid = Optional.of(uuids.get(0))
                .map({ u -> UUID.fromString(u) })
                .orElse(UUID.randomUUID())
        given(this.documentationSpec)
                .accept("application/json")
                .filters(document(METHOD_NAME, requestPreprocessor, responsePreprocessor, pathParameters(
                        parameterWithName("uuid").description("UUID of the Job Listing")))
                        , document(METHOD_NAME, relaxedResponseFields(fieldWithPath("link").description("Link to the Job Listing"),
                                fieldWithPath("title").description("Title of the Job Listing"))))
                .when()
                .port(this.port).get("/jobs/{uuid}", uuid)
                .then()
                .assertThat().statusCode(is(200))
    }

    @Test
    @Ignore
    public void getJobListings() throws Exception {
        given(this.documentationSpec)
                .accept("application/json")
                .filter(document(METHOD_NAME, requestPreprocessor, responsePreprocessor))
                .when()
                .port(this.port).get("/jobs")
                .then()
                .assertThat().statusCode(is(200))
    }

    @Test
    @Ignore
    public void jobListingNotFound() throws Exception {
        ValidatableResponse response = given(this.documentationSpec)
                .accept("application/json")
                .filters(document(METHOD_NAME, requestPreprocessor, responsePreprocessor, pathParameters(
                        parameterWithName("uuid").description("UUID of the Job Listing"))),
                        document(METHOD_NAME, relaxedResponseFields(fieldWithPath("status").description("Http status code"),
                                fieldWithPath("errorMessage").description("Description of the error"))))
                .when()
                .port(this.port)
                .get("/jobs/{uuid}", UUID.randomUUID())
                .then()
                .assertThat().statusCode(is(404))
                .body("errorMessage", notNullValue())

    }

    @Test
    @Ignore
    public void create() throws Exception {
        def jobListing = [
                'link': 'rest',
                name  : 'NONAME'
        ]
        String payload = mapper.writeValueAsString(jobListing)
        given(this.documentationSpec)
                .contentType(ContentType.JSON)
                .filters(document(METHOD_NAME, requestPreprocessor, preprocessResponse(prettyPrint())))
                .when().port(this.port).body(payload)
                .post("/jobs").then()
                .assertThat()
                .statusCode(is(201))
                .body("uuid", notNullValue(UUID.class))
                .body("link", is("rest"))
                .body("origin", is("INTERNAL"))
                .body("owner", nullValue())

    }
}