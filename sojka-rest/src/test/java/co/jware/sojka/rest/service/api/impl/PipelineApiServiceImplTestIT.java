package co.jware.sojka.rest.service.api.impl;

import co.jware.sojka.core.BaseDataJpaTest;
import co.jware.sojka.core.Fakers;
import co.jware.sojka.core.domain.pipelines.OwnerPipeline;
import co.jware.sojka.core.service.pipelines.PipelineService;
import co.jware.sojka.core.service.pipelines.impl.PipelineServiceImpl;
import co.jware.sojka.repository.pipelines.PipelineRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@ContextConfiguration
public class PipelineApiServiceImplTestIT extends BaseDataJpaTest {

    @Autowired
    private PipelineService pipelineService;

    @Test
    public void createOwnerPipeline() throws Exception {
        OwnerPipeline result = pipelineService.getOrCreateOwnerPipeline(Fakers.ownerPipeline());
        assertThat(result.isNew()).isFalse();
    }

    @Test
    public void createJobPipeline() throws Exception {
    }

    @Test
    public void createPersonPipeline() throws Exception {
    }

    @Test
    public void getOwnerPipeLine() throws Exception {
    }

    @Test
    public void getJobPipeline() throws Exception {
    }

    static class TestConfig {
        @Autowired
        private PipelineRepository pipelineRepository;

        public PipelineService pipelineService() {
            return new PipelineServiceImpl();
        }
    }

}