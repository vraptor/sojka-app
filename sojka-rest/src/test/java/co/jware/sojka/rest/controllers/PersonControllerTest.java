package co.jware.sojka.rest.controllers;

import co.jware.sojka.core.domain.party.Candidate;
import co.jware.sojka.core.domain.party.Party;
import co.jware.sojka.core.domain.party.Person;
import co.jware.sojka.core.service.PersonService;
import co.jware.sojka.rest.ApiConstants;
import co.jware.sojka.rest.service.api.PersonApiService;
import co.jware.sojka.rest.service.api.impl.PersonApiServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.UUID;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@ContextConfiguration
public class PersonControllerTest extends BaseControllerTest {
    @Autowired
    private PersonService personService;

    @Test
    public void getPerson() throws Exception {
    }

    @Test
    public void getCandidate() throws Exception {
    }

    @Test
    @WithMockUser
    public void createPerson() throws Exception {

        Candidate candidate = Candidate.NULL;
        when(personService.createPerson(any()))
                .thenReturn(candidate.withUuid(UUID.randomUUID()));
        mockMvc.perform(post(ApiConstants.PERSON_API)
                .accept(MediaType.APPLICATION_JSON_UTF8)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(mapper.writeValueAsString(candidate))
                .with(csrf()))
                .andExpect(status().isCreated());
    }

    @Configuration
    static class TestConfig {
        @Bean
        PersonController personController() {
            return new PersonController(personApiService());
        }

        @Bean
        PersonApiService personApiService() {
            return new PersonApiServiceImpl(personService());
        }

        @Bean
        PersonService personService() {
            return mock(PersonService.class);

        }
    }
}