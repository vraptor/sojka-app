package co.jware.sojka.rest.service.api.impl;

import co.jware.sojka.core.domain.JobListing;
import co.jware.sojka.core.domain.Recommendation;
import co.jware.sojka.core.domain.party.Candidate;
import co.jware.sojka.core.domain.party.External;
import co.jware.sojka.core.service.PersonService;
import co.jware.sojka.core.service.entity.RecommendationService;
import mockit.Expectations;
import mockit.Injectable;
import mockit.Tested;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@ContextConfiguration
public class RecommendationApiServiceImplTest {
    @Tested
    private RecommendationApiServiceImpl recommendationApiService;
    @Injectable
    private RecommendationService recommendationService;
    @Injectable
    private PersonService personService;

    @Test
    public void create() throws Exception {
        Recommendation<JobListing> recommendation = Recommendation.<JobListing>builder()
                .subject(JobListing.NULL)
                .receiver(External.NULL)
                .sender(Candidate.NULL)
                .build();
        new Expectations() {{
            recommendationService.createRecommendation((Recommendation) any);
            result = recommendation.withUuid(UUID.randomUUID());
        }};
        Recommendation<JobListing> personRecommendation = recommendationApiService.create(recommendation);
        assertThat(personRecommendation).isNotNull();
    }
}