package co.jware.sojka.rest.controllers;

import co.jware.sojka.core.Fakers;
import co.jware.sojka.core.domain.Resume;
import co.jware.sojka.core.domain.party.Candidate;
import co.jware.sojka.core.service.PersonService;
import co.jware.sojka.core.service.ResumeService;
import co.jware.sojka.rest.assemblers.ResumeResourceAssembler;
import co.jware.sojka.rest.service.api.impl.ResumeApiServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.UUID;

import static co.jware.sojka.rest.ApiConstants.*;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@ContextConfiguration
public class ResumeControllerTest extends BaseControllerTest {
    @Autowired
    private ResumeService resumeService;

    @Autowired
    private PersonService personService;

    @Test
    @WithMockUser
    public void getByOwner() throws Exception {
        when(resumeService.findByOwner(any(Candidate.class), any(Pageable.class)))
                .thenReturn(new PageImpl<>(Arrays.asList(
                        Fakers.resume().withUuid(UUID.randomUUID()),
                        Fakers.resume().withUuid(UUID.randomUUID())
                ), new PageRequest(1, 20), 2));

        when(personService.findByUuid(any(UUID.class)))
                .thenReturn(Fakers.candidate().withUuid(UUID.randomUUID()));
        mockMvc.perform(get(RESUME_API + "/{uuid}/owner", UUID.randomUUID())
                .accept(MediaType.APPLICATION_JSON_UTF8)
        ).andExpect(status().isOk());
    }

    @Test
    @WithMockUser
    public void getBySubmitter() throws Exception {
        when(resumeService.findBySubmitter(any(Candidate.class), any(Pageable.class)))
                .thenReturn(new PageImpl<>(Arrays.asList(
                        Fakers.resume().withUuid(UUID.randomUUID()),
                        Fakers.resume().withUuid(UUID.randomUUID())
                ), new PageRequest(1, 20), 2));

        when(personService.findByUuid(any(UUID.class)))
                .thenReturn(Fakers.candidate().withUuid(UUID.randomUUID()));
        mockMvc.perform(get(RESUME_API + "/{uuid}/submitter", UUID.randomUUID())
                .accept(MediaType.APPLICATION_JSON_UTF8)
        ).andExpect(status().isOk());
    }

    @Test
    public void getResume() throws Exception {
    }

    @Test
    @WithMockUser
    public void createResume() throws Exception {
        Resume resume = Resume.builder()
                .owner(Candidate.NULL)
                .build();
        when(resumeService.createResume(any(Resume.class)))
                .thenReturn(resume.withUuid(UUID.randomUUID()));
        String payload = mapper.writeValueAsString(resume);
        mockMvc.perform(post(RESUME_API)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(payload)
                .with(csrf())
        ).andExpect(status().isCreated());

    }

    @Configuration
    static class TestConfig {
        @Bean
        public ResumeController resumeController() {
            return new ResumeController(resumeApiService());
        }

        @Bean
        public ResumeApiServiceImpl resumeApiService() {
            return new ResumeApiServiceImpl(resumeService());
        }

        @Bean
        public ResumeService resumeService() {
            return mock(ResumeService.class);
        }

        @Bean
        public PersonService personService() {
            return mock(PersonService.class);
        }

        @Bean
        ResumeResourceAssembler resumeResourceAssembler() {
            return new ResumeResourceAssembler();
        }
    }
}