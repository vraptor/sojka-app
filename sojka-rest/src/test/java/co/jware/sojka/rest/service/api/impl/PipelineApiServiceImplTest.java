package co.jware.sojka.rest.service.api.impl;

import co.jware.sojka.core.domain.JobListing;
import co.jware.sojka.core.domain.party.Candidate;
import co.jware.sojka.core.domain.party.Hirer;
import co.jware.sojka.core.domain.pipelines.JobPipeline;
import co.jware.sojka.core.domain.pipelines.OwnerPipeline;
import co.jware.sojka.core.domain.pipelines.PersonPipeline;
import co.jware.sojka.core.service.entity.AccountService;
import co.jware.sojka.core.service.pipelines.PipelineService;
import co.jware.sojka.entities.core.pipelines.StageBo;
import co.jware.sojka.entities.core.pipelines.SystemPipelineBo;
import co.jware.sojka.repository.pipelines.JobPipelineRepository;
import co.jware.sojka.repository.pipelines.OwnerPipelineRepository;
import co.jware.sojka.repository.pipelines.PersonPipelineRepository;
import co.jware.sojka.repository.pipelines.SystemPipelineRepository;
import com.fasterxml.jackson.databind.ObjectMapper;

import mockit.Injectable;
import mockit.Tested;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Collections;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@ContextConfiguration
public class PipelineApiServiceImplTest {
    @Injectable
    private AccountService accountService;
    @Injectable
    private SystemPipelineRepository systemPipelineRepository;
    @Injectable
    private OwnerPipelineRepository ownerPipelineRepository;
    @Injectable
    private JobPipelineRepository jobPipelineRepository;
    @Injectable
    private PersonPipelineRepository personPipelineRepository;
    @Injectable
    private PipelineService pipelineService;
    @Injectable
    @Autowired
    private ObjectMapper objectMapper;

    @Tested
    private PipelineApiServiceImpl pipelineApiService;

    @Test
    public void createPersonPipeline() throws Exception {
        JobPipeline jobPipeline = JobPipeline.builder()
                .jobListing(JobListing.NULL)
                .name("JOB_PIPELINE")
                .build();

        PersonPipeline personPipeline = PersonPipeline
                .builder()
                .name("PERSON_PIPELINE")
                .person(Candidate.NULL)
                .parent(jobPipeline)
                .build();
        pipelineApiService.createPersonPipeline(personPipeline);
    }

    @Test
    public void getOwnerPipeLine() throws Exception {
        pipelineApiService.getOwnerPipeLine(UUID.randomUUID());
    }

    @Test
    public void getJobPipeline() throws Exception {
        pipelineApiService.getJobPipeline(UUID.randomUUID());
    }

    @Test
    @Ignore
    public void createOwnerPipeline() throws Exception {
        SystemPipelineBo entity = new SystemPipelineBo();
        entity.setName("BOGUS_SYSTEM");
        StageBo stageEntity = new StageBo();
        stageEntity.setName("DUMMY_STAGE");
        entity.setStages(Collections.singletonList(stageEntity));
        when(systemPipelineRepository.findAll(any(PageRequest.class)))
                .thenReturn(new PageImpl<>(Collections.singletonList(entity)));
        OwnerPipeline ownerPipeline = OwnerPipeline
                .builder()
                .name("BOGUS")
                .owner(Hirer.NULL)
                .build();
        OwnerPipeline result = pipelineApiService.createOwnerPipeline(ownerPipeline);
        assertThat(result).isNotNull();
    }

    @Test
    public void createJobPipeline() throws Exception {
        JobPipeline jobPipeline = JobPipeline.builder()
                .name("JOB_PIPELINE")
                .jobListing(JobListing.NULL)
                .build();
        JobPipeline result = pipelineApiService.createJobPipeline(jobPipeline);
        assertThat(result).isNotNull();
    }


    @Configuration
    static class TestConfig {

        @Bean
        public ObjectMapper objectMapper() {
            return Jackson2ObjectMapperBuilder.json().build();
        }
    }
}