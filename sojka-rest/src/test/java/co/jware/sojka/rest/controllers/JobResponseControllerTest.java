package co.jware.sojka.rest.controllers;

import co.jware.sojka.core.Fakers;
import co.jware.sojka.core.domain.JobListing;
import co.jware.sojka.core.domain.JobResponse;
import co.jware.sojka.core.domain.party.Candidate;
import co.jware.sojka.core.service.PersonService;
import co.jware.sojka.core.service.entity.JobListingService;
import co.jware.sojka.core.service.entity.JobResponseService;
import co.jware.sojka.rest.assemblers.JobResponseResourceAssembler;
import co.jware.sojka.rest.service.api.JobResponseApiService;
import co.jware.sojka.rest.service.api.impl.JobResponseApiServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.Optional;
import java.util.UUID;

import static co.jware.sojka.rest.ApiConstants.*;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@ContextConfiguration
public class JobResponseControllerTest extends BaseControllerTest {
    @Autowired
    private JobResponseService jobResponseService;
    @Autowired
    private JobListingService jobListingService;
    @Autowired
    private PersonService personService;

    @Test
    @WithMockUser
    public void getJobResponsesByJobListing() throws Exception {
        when(jobResponseService.findByJobListing(any(JobListing.class), any(Pageable.class)))
                .thenReturn(new PageImpl<>(Arrays.asList(
                        Fakers.jobResponse().withUuid(UUID.randomUUID()),
                        Fakers.jobResponse().withUuid(UUID.randomUUID())), new PageRequest(1, 20), 2));
        when(jobListingService.findByUuid(any(UUID.class)))
                .thenReturn(Optional.ofNullable(Fakers.jobListing().withUuid(UUID.randomUUID())));
        mockMvc.perform(get(JOB_RESPONSE_API + "/{uuid}/job-listing", UUID.randomUUID())
                .accept(MediaType.APPLICATION_JSON_UTF8)
        ).andExpect(status().isOk());
    }

    @Test
    @WithMockUser
    public void getJobResponsesByRespondent() throws Exception {
        when(jobResponseService.findByRespondent(any(), any(Pageable.class)))
                .thenReturn(new PageImpl<>(Arrays.asList(
                        Fakers.jobResponse().withUuid(UUID.randomUUID()),
                        Fakers.jobResponse().withUuid(UUID.randomUUID())), new PageRequest(1, 20), 2));
        when(personService.findByUuid(any()))
                .thenReturn(Fakers.candidate().withUuid(UUID.randomUUID()));
        mockMvc.perform(get(JOB_RESPONSE_API + "/{uuid}/respondent", UUID.randomUUID())
                .accept(MediaType.APPLICATION_JSON_UTF8)
        ).andExpect(status().isOk());

    }


    @Test
    @WithMockUser
    public void createJobResponse() throws Exception {
        JobResponse jobResponse = JobResponse.builder()
                .respondent(Candidate.NULL)
                .jobListing(JobListing.NULL)
                .build();
        when(jobResponseService.createJobResponse(any()))
                .thenReturn(java.util.Optional.ofNullable(jobResponse.withUuid(UUID.randomUUID())));
        mockMvc.perform(post(JOB_RESPONSE_API)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .accept(MediaType.APPLICATION_JSON_UTF8)
                .content(mapper.writeValueAsString(jobResponse))
                .with(csrf()))
                .andExpect(status().isCreated());
    }

    @Configuration
    static class TestConfig {

        @Bean
        JobResponseController jobResponseController() {
            return new JobResponseController(jobResponseApiService());
        }

        @Bean
        JobResponseApiService jobResponseApiService() {
            return new JobResponseApiServiceImpl(jobResponseService());
        }

        @Bean
        JobResponseService jobResponseService() {
            return mock(JobResponseService.class);
        }

        @Bean
        JobListingService jobListingService() {
            return mock(JobListingService.class);
        }

        @Bean
        PersonService personService() {
            return mock(PersonService.class);
        }

        @Bean
        JobResponseResourceAssembler jobResponseResourceAssembler() {
            return new JobResponseResourceAssembler();
        }
    }
}