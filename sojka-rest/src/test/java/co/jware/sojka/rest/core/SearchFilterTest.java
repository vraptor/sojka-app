package co.jware.sojka.rest.core;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.skyscreamer.jsonassert.JSONAssert;

public class SearchFilterTest {
    @Test
    public void serialize() throws Exception {
        SearchFilter filter = SearchFilter.builder()
                .keywords("java", "c++")
                .locations("Brno", "Praha")
                .build();
        ObjectMapper mapper = new ObjectMapper()
                .setSerializationInclusion(JsonInclude.Include.NON_EMPTY);
        String json = mapper.writeValueAsString(filter);
        JSONAssert.assertEquals("{\"locations\":[\"Praha\",\"Brno\"],\"keywords\":[\"java\",\"c++\"]}",
                json, false);
    }
}