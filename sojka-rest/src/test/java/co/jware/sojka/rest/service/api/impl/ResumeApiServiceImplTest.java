package co.jware.sojka.rest.service.api.impl;

import co.jware.sojka.core.Fakers;
import co.jware.sojka.core.domain.party.Candidate;
import co.jware.sojka.core.domain.party.Hirer;
import co.jware.sojka.core.domain.Resume;
import co.jware.sojka.core.service.PersonService;
import co.jware.sojka.core.service.ResumeService;
import co.jware.sojka.rest.service.api.ResumeApiService;
import com.fasterxml.jackson.databind.ObjectMapper;
import mockit.Expectations;
import mockit.Injectable;
import mockit.Tested;
import mockit.integration.junit4.JMockit;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;

@RunWith(SpringRunner.class)
@ContextConfiguration
public class ResumeApiServiceImplTest {
    @Tested
    private ResumeApiServiceImpl resumeApiService;
    @Injectable
    private ResumeService resumeService;
    @Injectable
    private PersonService personService;
    @Autowired
    @Injectable
    private ObjectMapper mapper;

    @Test
    public void getResume() throws Exception {
    }

    @Test
    public void getByOwner() throws Exception {
        new Expectations() {{
            personService.findByUuid((UUID) any);
            result = Candidate.NULL;
        }};
        resumeApiService.getByOwner(UUID.randomUUID(), new PageRequest(1, 20));
    }

    @Test
    public void getBySubmitter() throws Exception {
        new Expectations() {{
            personService.findByUuid((UUID) any);
            result = Hirer.NULL;
        }};
        resumeApiService.getBySubmitter(UUID.randomUUID(), new PageRequest(1, 20));
    }

    @Test
    public void isBaseService() throws Exception {
        assertThat(resumeApiService).isInstanceOf(BaseApiService.class);
    }

    @Test
    public void createResume() throws Exception {
        Resume resume = Resume.builder()
                .submitter(Fakers.hirer())
                .build();
        new Expectations() {{
            resumeService.createResume((Resume) any);
            result = resume.withUuid(UUID.randomUUID());
        }};
        Resume result = resumeApiService.createResume(resume);
        assertThat(result.uuid()).isNotNull();
    }

    @Configuration
    static class TestConfig {
        @Bean
        public ObjectMapper objectMapper() {
            return Jackson2ObjectMapperBuilder.json().build();
        }
    }
}