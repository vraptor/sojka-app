package co.jware.sojka.rest.service.api.impl;

import co.jware.sojka.core.domain.dashboards.Dashboard;
import co.jware.sojka.core.domain.party.Hirer;
import co.jware.sojka.core.service.dashboards.DashboardService;
import co.jware.sojka.rest.exceptions.HttpNotFoundException;
import com.fasterxml.jackson.databind.ObjectMapper;
import mockit.Injectable;
import mockit.Tested;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
public class DashboardApiServiceImplTest {
    @Tested
    private DashboardApiServiceImpl dashboardApiService;

    @Injectable
    private DashboardService dashboardService;
    @Injectable
    @Autowired
    private ObjectMapper mapper;

    @Test(expected = HttpNotFoundException.class)
    public void getDashboard_not_found() throws Exception {
        dashboardApiService.getDashboard(UUID.randomUUID());
    }

    @Test
    public void updateDashboard() throws Exception {
        dashboardApiService.updateDashboard(
                Dashboard.builder()
                        .owner(Hirer.NULL)
                        .build()
        );
    }

    @Test
    public void deleteDashboard() throws Exception {
    }

    @Test
    public void isBaseService() throws Exception {
        assertThat(dashboardApiService).isInstanceOf(BaseApiService.class);
    }

    @Test
    public void createDashboard() throws Exception {
        dashboardApiService.createDashboard(Dashboard.builder()
                .owner(Hirer.NULL)
                .build());
    }

    @Configuration
    static class TestConfig {

        @Bean
        public ObjectMapper objectMapper() {
            return Jackson2ObjectMapperBuilder.json().build();
        }
    }

}