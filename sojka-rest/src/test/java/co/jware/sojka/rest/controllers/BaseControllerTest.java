package co.jware.sojka.rest.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.web.HateoasPageableHandlerMethodArgumentResolver;
import org.springframework.data.web.PageableArgumentResolver;
import org.springframework.data.web.config.EnableSpringDataWebSupport;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_EMPTY;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

@WebAppConfiguration
@ContextConfiguration
@EnableWebMvc
@EnableSpringDataWebSupport
public class BaseControllerTest {
    protected MockMvc mockMvc;
    @Autowired
    protected ObjectMapper mapper;
    @Autowired
    private WebApplicationContext wac;
    @Autowired
    private PageableArgumentResolver pageableArgumentResolver;

    @Before
    public void setUp() throws Exception {
        mockMvc = webAppContextSetup(wac)
                .apply(springSecurity()
                ).build();
    }

    @Configuration
    @EnableWebSecurity
    static class TestConfig {

        @Bean
        public ObjectMapper objectMapper() {
            return Jackson2ObjectMapperBuilder.json()
                    .serializationInclusion(NON_EMPTY)
                    .build();
        }

        @Bean
        public PageableArgumentResolver pageableArgumentResolver() {
            return new HateoasPageableHandlerMethodArgumentResolver();
        }
    }
}
