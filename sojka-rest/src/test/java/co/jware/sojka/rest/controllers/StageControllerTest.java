package co.jware.sojka.rest.controllers;

import co.jware.sojka.core.service.StageService;
import co.jware.sojka.rest.service.api.StageApiService;
import co.jware.sojka.rest.service.api.impl.StageApiServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import static org.mockito.Mockito.mock;

@RunWith(SpringRunner.class)
@ContextConfiguration
public class StageControllerTest {
    @Test
    public void getStage() throws Exception {
    }

    @Configuration
    static class TestConfig {
        @Bean
        StageController stageController() {
            return new StageController(stageApiService());
        }

        @Bean
        StageApiService stageApiService() {
            return new StageApiServiceImpl(stageService());
        }

        @Bean
        StageService stageService() {
            return mock(StageService.class);
        }
    }
}