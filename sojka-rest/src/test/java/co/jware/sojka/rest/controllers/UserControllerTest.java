package co.jware.sojka.rest.controllers;

import co.jware.sojka.core.service.entity.UserService;
import co.jware.sojka.rest.service.api.UserApiService;
import co.jware.sojka.rest.service.api.impl.UserApiServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import static org.mockito.Mockito.mock;

@RunWith(SpringRunner.class)
@ContextConfiguration
public class UserControllerTest extends BaseControllerTest {
    @Test
    public void getUser() throws Exception {
    }

    @Configuration
    static class TestConfig {
        @Bean
        UserController userController() {
            return new UserController(userApiService());
        }

        @Bean
        UserApiService userApiService() {
            return new UserApiServiceImpl(userService());
        }

        @Bean
        UserService userService() {
            return mock(UserService.class);
        }
    }
}