package co.jware.sojka.rest.controllers;

import co.jware.sojka.core.domain.party.Hirer;
import co.jware.sojka.core.domain.pipelines.OwnerPipeline;
import co.jware.sojka.core.service.entity.AccountService;
import co.jware.sojka.rest.ApiConstants;
import co.jware.sojka.rest.service.api.PipelineApiService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.UUID;

import static co.jware.sojka.rest.ApiConstants.*;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@ContextConfiguration
public class PipelineControllerTest extends BaseControllerTest {

    @Autowired
    private PipelineApiService pipelineApiService;

    @Autowired
    private ObjectMapper mapper;

    @Test
    public void getOwnerPipeline() throws Exception {
    }

    @Test
    public void getJobPipeline() throws Exception {
    }

    @Test
    public void createJobPipeline() throws Exception {
    }

    @Test
    public void createPersonPipeline() throws Exception {
    }

    @Test
    @WithMockUser
    public void createOwnerPipeline() throws Exception {
        UUID uuid = UUID.randomUUID();
        OwnerPipeline ownerPipeline = OwnerPipeline.builder()
                .name("BOGUS")
                .owner(Hirer.NULL).build();
        String payload = mapper.writeValueAsString(ownerPipeline);
        when(pipelineApiService.createOwnerPipeline(any()))
                .thenReturn(ownerPipeline.withUuid(uuid));
        String content = mockMvc.perform(post(PIPELINE_API + "/owner")
                .accept(MediaType.APPLICATION_JSON_UTF8)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(payload)
                .with(csrf())
        )
                .andExpect(status().isCreated())
                .andReturn()
                .getResponse().getHeader("Location");
        assertThat(content).isNotBlank();
    }

    @Configuration
    static class TestConfig {

        @Bean
        AccountService accountService() {
            return mock(AccountService.class);
        }

        @Bean
        public PipelineApiService pipelineApiService() {
            return Mockito.mock(PipelineApiService.class);
        }

        @Bean
        public PipelineController pipelineController() {
            return new PipelineController(pipelineApiService());
        }
    }
}