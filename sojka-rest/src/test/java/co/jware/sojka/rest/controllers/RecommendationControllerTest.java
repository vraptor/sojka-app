package co.jware.sojka.rest.controllers;

import co.jware.sojka.core.Fakers;
import co.jware.sojka.core.domain.Recommendation;
import co.jware.sojka.core.service.PersonService;
import co.jware.sojka.core.service.entity.RecommendationService;
import co.jware.sojka.rest.ApiConstants;
import co.jware.sojka.rest.assemblers.PersonRecommendationResourceAssembler;
import co.jware.sojka.rest.service.api.RecommendationApiService;
import co.jware.sojka.rest.service.api.impl.RecommendationApiServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import static org.mockito.Mockito.mock;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@ContextConfiguration
public class RecommendationControllerTest extends BaseControllerTest {


    @Test
    @WithMockUser
    public void createPersonRecommendation() throws Exception {
        Recommendation<Object> recommendation = Recommendation.builder()
                .subject(Fakers.jobListing())
                .sender(Fakers.candidate())
                .receiver(Fakers.external())
                .build();
        mockMvc.perform(post(ApiConstants.RECOMMENDATION_API + "/person")
                .accept(MediaType.APPLICATION_JSON_UTF8)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(mapper.writeValueAsString(recommendation))
                .with(csrf())
        ).andExpect(status().isCreated());
    }

    @Configuration
    static class TestConfig {
        @Bean
        RecommendationController recommendationController() {
            return new RecommendationController(recommendationApiService());
        }

        @Bean
        RecommendationApiService recommendationApiService() {
            return new RecommendationApiServiceImpl(recommendationService());
        }

        @Bean
        PersonService personService() {
            return mock(PersonService.class);
        }

        @Bean
        RecommendationService recommendationService() {
            return mock(RecommendationService.class);
        }

        @Bean
        PersonRecommendationResourceAssembler personRecommendationResourceAssembler() {
            return new PersonRecommendationResourceAssembler();
        }

    }
}