package co.jware.sojka.rest.service.api.impl;

import co.jware.sojka.core.domain.JobListing;
import co.jware.sojka.core.domain.JobResponse;
import co.jware.sojka.core.domain.party.Candidate;
import co.jware.sojka.core.service.PersonService;
import co.jware.sojka.core.service.entity.JobListingService;
import co.jware.sojka.core.service.entity.JobResponseService;
import co.jware.sojka.rest.service.api.JobResponseApiService;
import com.fasterxml.jackson.databind.ObjectMapper;
import mockit.Expectations;
import mockit.Injectable;
import mockit.Tested;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;

@RunWith(SpringRunner.class)
public class JobResponseApiServiceImplTest {

    @Tested
    private JobResponseApiServiceImpl jobResponseApiService;
    @Injectable
    private JobResponseService jobResponseService;
    @Injectable
    private JobListingService jobListingService;
    @Injectable
    private PersonService personService;
    @Injectable
    @Autowired
    private ObjectMapper mapper;

    @Test
    public void isBaseService() throws Exception {
        assertThat(jobResponseApiService).isInstanceOf(BaseApiService.class);
    }

    @Test
    public void createJobResponse() throws Exception {
        JobResponse jobResponse = JobResponse.builder()
                .respondent(Candidate.NULL)
                .jobListing(JobListing.NULL)
                .build();
        new Expectations() {{
            jobResponseService.createJobResponse((JobResponse) any);
            result = Optional.of(jobResponse.withUuid(UUID.randomUUID()));
        }};
        jobResponseApiService.createJobResponse(
                jobResponse
        );
    }

    @Test
    public void findByJobListing() throws Exception {
        new Expectations() {{
            jobListingService.findByUuid((UUID) any);
            result = Optional.of(JobListing.NULL);
        }};
        Page<JobResponse> page = jobResponseApiService.findByJobListing(UUID.randomUUID(), new PageRequest(1, 20));
    }

    @Test
    public void findByRespondent() throws Exception {
        jobResponseApiService.findByRespondent(UUID.randomUUID(), new PageRequest(1, 20));
    }

    @Configuration
    static class TestConfig {
        @Bean
        public ObjectMapper objectMapper() {
            return Jackson2ObjectMapperBuilder.json().build();
        }
    }

}