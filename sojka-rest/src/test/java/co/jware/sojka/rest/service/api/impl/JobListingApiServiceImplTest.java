package co.jware.sojka.rest.service.api.impl;

import co.jware.sojka.core.domain.JobListing;
import co.jware.sojka.core.domain.party.Hirer;
import co.jware.sojka.core.service.PersonService;
import co.jware.sojka.core.service.entity.JobListingService;
import com.fasterxml.jackson.databind.ObjectMapper;
import mockit.Expectations;
import mockit.Injectable;
import mockit.Tested;
import mockit.integration.junit4.JMockit;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.data.domain.PageRequest;

import java.util.Optional;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(JMockit.class)
public class JobListingApiServiceImplTest {

    @Tested
    private JobListingApiServiceImpl apiService;
    @Injectable
    private JobListingService jobListingService;
    @Injectable
    private PersonService personService;
    @Injectable
    private ObjectMapper mapper;

    @Test
    public void findByOwner() throws Exception {
        new Expectations() {{
            personService.findByUuid((UUID) any);
            result = Hirer.NULL;
        }};
        apiService.findByOwner(UUID.randomUUID(), new PageRequest(1, 20));
    }

    @Test
    public void getListings() throws Exception {
    }

    @Test
    public void isBaseService() throws Exception {
        assertThat(apiService).isInstanceOf(BaseApiService.class);
    }

    @Test
    public void create() throws Exception {
        JobListing jobListing = JobListing.NULL;
        new Expectations() {{
            personService.findByUuid((UUID) any);
            result = Hirer.NULL.withUuid(UUID.randomUUID());
            jobListingService.createJobListing((JobListing) any);
            result = Optional.of(jobListing.withUuid(UUID.randomUUID()));
        }};
        JobListing result = apiService.create(jobListing);
        assertThat(result.uuid()).isNotNull();
    }
}