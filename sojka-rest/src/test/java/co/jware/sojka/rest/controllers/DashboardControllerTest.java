package co.jware.sojka.rest.controllers;

import co.jware.sojka.core.domain.dashboards.Dashboard;
import co.jware.sojka.core.domain.party.Hirer;
import co.jware.sojka.core.service.dashboards.DashboardService;
import co.jware.sojka.rest.ApiConstants;
import co.jware.sojka.rest.assemblers.DashBoardResourceAssembler;
import co.jware.sojka.rest.service.api.DashboardApiService;
import co.jware.sojka.rest.service.api.impl.DashboardApiServiceImpl;
import co.jware.sojka.rest.springmvc.RestAdvice;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.config.EnableSpringDataWebSupport;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithAnonymousUser;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Collections;
import java.util.Optional;
import java.util.UUID;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(value = DashboardController.class)
public class DashboardControllerTest {
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private DashboardService dashboardService;
    @Autowired
    private ObjectMapper mapper;

    @Test
    @WithAnonymousUser
    public void getDashboard() throws Exception {
        when(dashboardService.getDashboard(any(UUID.class)))
                .thenReturn(Optional.of(Dashboard.builder().owner(Hirer.NULL).build()));
        mockMvc.perform(get(ApiConstants.DASHBOARD_API + "/{uuid}", UUID.randomUUID()))
                .andExpect(status().isOk());
    }

    @Test
    @WithMockUser
    public void getDashboard_not_found() throws Exception {
        when(dashboardService.getDashboard(any(UUID.class)))
                .thenReturn(Optional.empty());
        mockMvc.perform(get(ApiConstants.DASHBOARD_API + "/{uuid}", UUID.randomUUID()))
                .andExpect(status().isNotFound());
    }

    @Test
    public void getByOwner_not_found() throws Exception {
        when(dashboardService.getByOwner(any(UUID.class), any(Pageable.class)))
                .thenReturn(new PageImpl<>(Collections.emptyList()));
        String urlTemplate = ApiConstants.DASHBOARD_API + "/{uuid}/owner";
        mockMvc.perform(get(urlTemplate, UUID.randomUUID())
                .accept(MediaType.APPLICATION_JSON_UTF8)
        ).andExpect(status().isNotFound());
    }

    @Test
    public void getByOwner() throws Exception {
        Dashboard dashboard = Dashboard.builder()
                .owner(Hirer.NULL)
                .uuid(UUID.randomUUID())
                .build();
        when(dashboardService.getByOwner(any(UUID.class), any(Pageable.class)))
                .thenReturn(new PageImpl<>(Collections.singletonList(dashboard), new PageRequest(1, 20), 1));
        String urlTemplate = ApiConstants.DASHBOARD_API + "/{uuid}/owner";
        mockMvc.perform(get(urlTemplate, UUID.randomUUID())
                .accept(MediaType.APPLICATION_JSON_UTF8)
        ).andExpect(status().isOk());
    }

    @Test
    @WithMockUser(roles = "USER")
    public void create() throws Exception {
        when(dashboardService.createDashboard(any(Dashboard.class)))
                .thenReturn(Dashboard.builder().owner(Hirer.NULL).build());
        Dashboard dashboard = Dashboard.builder()
                .owner(Hirer.NULL)
                .build();
        String content = mapper.writeValueAsString(dashboard);
        mockMvc.perform(post("/dashboard")
                .accept(MediaType.APPLICATION_JSON_UTF8)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(content)
                .with(csrf())
        ).andExpect(status().isCreated());
    }

    @Test
    public void deleteDashboard() throws Exception {
    }

    @Test
    public void updateDashboard() throws Exception {
    }

    @Configuration
    @EnableSpringDataWebSupport
    static class TestConfig {
        @Bean
        public DashboardController dashboardController() {
            return new DashboardController(dashboardApiService());
        }

        @Bean
        public DashboardApiService dashboardApiService() {
            return new DashboardApiServiceImpl(dashboardService());
        }

        @Bean
        public DashboardService dashboardService() {
            return Mockito.mock(DashboardService.class);
        }

        @Bean
        public DashBoardResourceAssembler dashBoardResourceAssembler() {
            return new DashBoardResourceAssembler();
        }

        @Bean
        public RestAdvice restAdvice() {
            return new RestAdvice();
        }


    }


}