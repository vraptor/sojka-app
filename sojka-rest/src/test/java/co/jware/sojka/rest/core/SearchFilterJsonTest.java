package co.jware.sojka.rest.core;

import org.json.JSONArray;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.json.JsonTest;
import org.springframework.boot.test.json.JacksonTester;
import org.springframework.boot.test.json.JsonContent;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;


@RunWith(SpringRunner.class)
@JsonTest
public class SearchFilterJsonTest {
    @Autowired
    private JacksonTester<SearchFilter> json;

    @Test
    public void serialization_no_locations() throws Exception {
        SearchFilter filter = SearchFilter.builder()
                .keywords("java", "c++")
                .build();
        JsonContent<SearchFilter> jsonContent = json.write(filter);
        assertThat(jsonContent)
                .extractingJsonPathArrayValue("@.keywords")
                .contains("c++", "java");
        assertThat(jsonContent).doesNotHaveJsonPathValue("'@.locations");
    }

    @Test
    public void serialization() throws Exception {
        SearchFilter filter = SearchFilter.builder()
                .keywords("project", "management")
                .locations("Praha", "Brno")
                .build();
        assertThat(json.write(filter))
                .extractingJsonPathArrayValue("@.locations")
                .contains("Brno");
    }
}