package co.jware.sojka.rest.controllers;

import co.jware.sojka.core.Fakers;
import co.jware.sojka.core.domain.Account;
import co.jware.sojka.core.service.PersonService;
import co.jware.sojka.core.service.entity.AccountService;
import co.jware.sojka.rest.service.api.AccountApiService;
import co.jware.sojka.rest.service.api.impl.AccountApiServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;
import java.util.UUID;

import static co.jware.sojka.rest.ApiConstants.ACCOUNT_API;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@ContextConfiguration
public class AccountControllerTest extends BaseControllerTest {
    @Autowired
    private AccountService accountService;
    @Autowired
    private PersonService personService;

    @Test
    @WithMockUser
    public void getByUser() throws Exception {
        Account account = Fakers.account()
                .withUser(Fakers.user().withUuid(UUID.randomUUID()));
        when(accountService.findByUserUuid(any()))
                .thenReturn(Optional.of(account));
        mockMvc.perform(get(ACCOUNT_API + "/{uuid}/user", UUID.randomUUID())
                .accept(MediaType.APPLICATION_JSON_UTF8)
        ).andExpect(status().isOk());
    }


    @Test
    @WithMockUser
    public void getByPerson() throws Exception {
        Account account = Fakers.account()
                .withOwner(Fakers.candidate().withUuid(UUID.randomUUID()));
        when(accountService.findByOwnerUuid(any()))
                .thenReturn(Optional.of(account));
        mockMvc.perform(get(ACCOUNT_API + "/{uuid}/person", UUID.randomUUID())
        ).andExpect(status().isOk());
    }

    @Test
    @WithMockUser
    public void createAccount() throws Exception {
        when(personService.findByEmail(any()))
                .thenReturn(Optional.ofNullable(Fakers.candidate().withUuid(UUID.randomUUID())));
        when(accountService.createAccount(any()))
                .thenReturn(Fakers.account().withUuid(UUID.randomUUID()));
        mockMvc.perform(post(ACCOUNT_API).with(csrf())
                .accept(MediaType.APPLICATION_JSON_UTF8)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(mapper.writeValueAsString(Fakers.account().withUuid(UUID.randomUUID())))
        ).andExpect(status().isCreated());
    }

    @Configuration
    static class TestConfig {
        @Bean
        AccountController accountController() {
            return new AccountController(accountApiService());
        }

        @Bean
        AccountApiService accountApiService() {
            return new AccountApiServiceImpl(accountService());
        }

        @Bean
        AccountService accountService() {
            return mock(AccountService.class);
        }

        @Bean
        PersonService personService() {
            return mock(PersonService.class);
        }
    }
}