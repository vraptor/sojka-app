package co.jware.sojka.rest.controllers;

import co.jware.sojka.core.domain.JobListing;
import co.jware.sojka.core.domain.party.Hirer;
import co.jware.sojka.core.service.PersonService;
import co.jware.sojka.core.service.entity.JobListingService;
import co.jware.sojka.rest.assemblers.JobListingResourceAssembler;
import co.jware.sojka.rest.service.api.JobListingApiService;
import co.jware.sojka.rest.service.api.impl.JobListingApiServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Collections;
import java.util.UUID;

import static co.jware.sojka.rest.ApiConstants.JOB_LISTING_API;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@ContextConfiguration
public class JobListingControllerTest extends BaseControllerTest {
    @Autowired
    private JobListingService jobListingService;
    @Autowired
    private PersonService personService;

    @Test
    @WithMockUser
    public void getListings() throws Exception {
        JobListing jobListing = JobListing.NULL.withUuid(UUID.randomUUID());
        when(jobListingService.findJobListings(any(Pageable.class)))
                .thenReturn(new PageImpl<>(Collections.singletonList(jobListing), new PageRequest(1, 20), 1));
        mockMvc.perform(get(JOB_LISTING_API)
                .accept(MediaType.APPLICATION_JSON_UTF8)
        ).andExpect(status().isOk());
    }

    @Test
    public void getByOwner() throws Exception {

    }

    @Test
    @WithMockUser
    public void createJobListing() throws Exception {
        JobListing jobListing = JobListing.NULL;
        when(personService.findByUuid(any(UUID.class)))
                .thenReturn(Hirer.NULL);
        when(jobListingService.createJobListing(any()))
                .thenReturn(java.util.Optional.ofNullable(jobListing.withUuid(UUID.randomUUID())));
        mockMvc.perform(post(JOB_LISTING_API)
                .accept(MediaType.APPLICATION_JSON_UTF8)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(mapper.writeValueAsString(jobListing))
                .with(csrf())
        ).andExpect(status().isCreated());
    }

    @Configuration
    static class TestConfig {
        @Bean
        public JobListingController jobListingController() {
            return new JobListingController(jobListingApiService());
        }

        @Bean
        public JobListingApiService jobListingApiService() {
            return new JobListingApiServiceImpl(jobListingService());
        }

        @Bean
        public JobListingService jobListingService() {
            return mock(JobListingService.class);
        }

        @Bean
        public PersonService personService() {
            return mock(PersonService.class);
        }

        @Bean
        JobListingResourceAssembler jobListingResourcesAssembler() {
            return new JobListingResourceAssembler();
        }
    }
}