package co.jware.sojka.rest.service.api.impl;

import co.jware.sojka.core.domain.Account;
import co.jware.sojka.core.domain.User;
import co.jware.sojka.core.domain.party.Candidate;
import co.jware.sojka.core.service.PersonService;
import co.jware.sojka.core.service.entity.AccountService;
import co.jware.sojka.core.service.entity.AccountServiceImpl;
import co.jware.sojka.rest.service.api.AccountApiService;
import mockit.Expectations;
import mockit.Injectable;
import mockit.Mocked;
import mockit.Tested;
import mockit.integration.junit4.JMockit;
import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.rules.SpringClassRule;
import org.springframework.test.context.junit4.rules.SpringMethodRule;

import java.util.Optional;
import java.util.UUID;

import static org.junit.Assert.*;

@RunWith(JMockit.class)
public class AccountApiServiceImplTest {

    @ClassRule
    public static final SpringClassRule SPRING_CLASS_RULE = new SpringClassRule();

    @Rule
    public final SpringMethodRule springMethodRule = new SpringMethodRule();

    @Tested
    private AccountApiServiceImpl accountApiService;
    @Injectable
    private AccountService accountService;
    @Injectable
    private PersonService personService;

    @Test
    public void createAccount() throws Exception {

        new Expectations() {{
            personService.findByEmail(anyString);
            result = Optional.of(Candidate.NULL.withUuid(UUID.randomUUID()));
        }};
        accountApiService.createAccount(Account.builder()
                .owner(Candidate.NULL)
                .user(User.NULL)
                .build()
        );
    }
}