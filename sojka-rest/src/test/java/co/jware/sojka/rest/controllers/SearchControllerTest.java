package co.jware.sojka.rest.controllers;

import co.jware.sojka.rest.core.SearchFilter;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(SearchController.class)
public class SearchControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper mapper;



    @Test
    public void fuzzySearch_get() throws Exception {
        mockMvc.perform(get("/search"))
                .andExpect(status().isOk());
    }

    @Test
    public void fuzzySearch_post() throws Exception {
        SearchFilter filter = SearchFilter.builder()
                .keywords("java", "javascript")
                .locations("Praha")
                .build();
        mockMvc.perform(post("/search")
                .content(mapper.writeValueAsBytes(filter))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void fuzzySearch_post_empty() throws Exception {
        mockMvc.perform(post("/search")
                .content("{}")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void fuzzySearch_post_not_found() throws Exception {
        SearchFilter searchFilter = SearchFilter
                .builder()
                .keywords("NONSENSE")
                .build();
        mockMvc.perform(post("/search")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsBytes(searchFilter)))
                .andExpect(status().isOk());
    }
}