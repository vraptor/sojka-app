package co.jware.sojka.rest.vertx;


import io.vertx.core.AbstractVerticle;
import io.vertx.ext.web.Router;

public class RecommendVerticle extends AbstractVerticle {
    @Override
    public void start() throws Exception {
        Router router = Router.router(vertx);
        router.route()
                .handler(context -> {
                });
    }
}
