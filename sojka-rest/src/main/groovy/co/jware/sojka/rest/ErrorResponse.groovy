package co.jware.sojka.rest

import groovy.transform.Canonical

@Canonical
class ErrorResponse {
    int status
    String errorMessage

    ErrorResponse(int status, String errorMessage) {
        this.status = status
        this.errorMessage = errorMessage
    }
}
