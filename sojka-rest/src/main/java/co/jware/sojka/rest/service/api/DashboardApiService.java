package co.jware.sojka.rest.service.api;


import co.jware.sojka.core.domain.dashboards.Dashboard;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.UUID;

public interface DashboardApiService {

    Dashboard createDashboard(Dashboard dashboard);

    Dashboard getDashboard(UUID uuid);

    Page<Dashboard> getByOwner(UUID uuid, Pageable pageable);

    Dashboard updateDashboard(Dashboard dashboard);

    void deleteDashboard(UUID uuid);
}
