package co.jware.sojka.rest.service.api.impl;

import co.jware.sojka.core.domain.dashboards.Dashboard;
import co.jware.sojka.core.service.dashboards.DashboardService;
import co.jware.sojka.rest.controllers.DashboardController;
import co.jware.sojka.rest.exceptions.HttpNotFoundException;
import co.jware.sojka.rest.resources.DashboardResource;
import co.jware.sojka.rest.service.api.DashboardApiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.ResourceAssembler;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.UUID;

@Service("dashboardApiService")
public class DashboardApiServiceImpl extends BaseApiService implements DashboardApiService {

    private final DashboardService dashboardService;

//    private ResourceAssembler<Dashboard, DashboardResource> resourceAssembler =
//            entity -> new DashboardResource(entity);
//
//    @Autowired
//    PagedResourcesAssembler<Dashboard> pagedResourcesAssembler;

    @Autowired
    public DashboardApiServiceImpl(DashboardService dashboardService) {
        this.dashboardService = dashboardService;
    }

    @Override
    public Dashboard createDashboard(Dashboard dashboard) {
        return dashboardService.createDashboard(dashboard);
    }

    @Override
    public Dashboard getDashboard(UUID uuid) {
        return dashboardService.getDashboard(uuid)
                .orElseThrow(() -> new HttpNotFoundException("Dashboard not found"));
    }

    @Override
    public Page<Dashboard> getByOwner(UUID uuid, Pageable pageable) {
        return dashboardService.getByOwner(uuid, pageable);
    }

    @Override
    public Dashboard updateDashboard(Dashboard dashboard) {
        return dashboardService.updateDashboard(dashboard);
    }

    @Override
    public void deleteDashboard(UUID uuid) {
        dashboardService.deleteDashboard(uuid);
    }

}
