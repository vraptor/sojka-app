package co.jware.sojka.rest.assemblers;

import co.jware.sojka.core.domain.Interview;
import co.jware.sojka.rest.controllers.JobListingController;
import co.jware.sojka.rest.resources.InterviewResource;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import org.springframework.stereotype.Component;

@Component
public class InterviewResourceAssembler extends ResourceAssemblerSupport<Interview, InterviewResource> {

    public InterviewResourceAssembler() {
        super(JobListingController.class, InterviewResource.class);
    }

    @Override
    public InterviewResource toResource(Interview entity) {
        return createResourceWithId(entity.uuid(), entity);
    }

    @Override
    protected InterviewResource instantiateResource(Interview entity) {
        return new InterviewResource(Interview.copyOf(entity));
    }
}
