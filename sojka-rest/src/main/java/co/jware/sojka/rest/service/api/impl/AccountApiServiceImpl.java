package co.jware.sojka.rest.service.api.impl;

import co.jware.sojka.core.domain.Account;
import co.jware.sojka.core.domain.User;
import co.jware.sojka.core.domain.party.Party;
import co.jware.sojka.core.service.PersonService;
import co.jware.sojka.core.service.entity.AccountService;
import co.jware.sojka.rest.exceptions.HttpNotFoundException;
import co.jware.sojka.rest.service.api.AccountApiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.UUID;

@Service("accountApiService")
public class AccountApiServiceImpl implements AccountApiService {

    private final AccountService accountService;

    @Autowired
    private PersonService personService;

    public AccountApiServiceImpl(AccountService accountService) {
        this.accountService = accountService;
    }

    @Override
    public Account createAccount(Account account) {
        Party owner = Optional.ofNullable(account.owner())
                .orElseThrow(IllegalArgumentException::new);
        String email = owner.email();
        owner = personService.findByEmail(email)
                .orElseThrow(IllegalArgumentException::new);
        User user = Optional.ofNullable(account.user()).orElseThrow(IllegalArgumentException::new);

        return accountService.createAccount(account.withUser(user).withOwner(owner));
    }

    @Override
    public Account getByUser(UUID uuid) {
        return accountService.findByUserUuid(uuid)
                .orElseThrow(() -> new HttpNotFoundException("Account not found"));
    }

    @Override
    public Account getByPerson(UUID uuid) {
        Optional<Account> account = accountService.findByOwnerUuid(uuid);
        return account.orElseThrow(() -> new HttpNotFoundException("Account not found"));
    }
}
