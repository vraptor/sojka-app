package co.jware.sojka.rest.service.api.impl;

import co.jware.sojka.core.domain.party.Person;
import co.jware.sojka.core.service.PersonService;
import co.jware.sojka.rest.service.api.PersonApiService;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service("personApiService")
public class PersonApiServiceImpl implements PersonApiService {

    private final PersonService personService;

    public PersonApiServiceImpl(PersonService personService) {
        this.personService = personService;
    }

    @Override
    public Person getPerson(UUID uuid) {
        return null;
    }

    @Override
    public Person createPerson(Person person) {
        return personService.createPerson(person);
    }
}
