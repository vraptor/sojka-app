package co.jware.sojka.rest.service.api;


import co.jware.sojka.core.domain.User;

import java.util.UUID;

public interface UserApiService {
    User findUser(UUID uuid);
}
