package co.jware.sojka.rest.resources;


import co.jware.sojka.core.domain.Interview;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.Resource;

public class InterviewResource extends Resource<Interview> {

    public InterviewResource(Interview content, Link... links) {
        super(content, links);
    }
}
