package co.jware.sojka.rest.controllers;

import co.jware.sojka.core.domain.party.Candidate;
import co.jware.sojka.core.domain.party.Person;
import co.jware.sojka.rest.ApiConstants;
import co.jware.sojka.rest.exceptions.HttpNotFoundException;
import co.jware.sojka.rest.service.api.PersonApiService;
import io.swagger.annotations.*;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.Optional;
import java.util.UUID;

@RestController
@RequestMapping(ApiConstants.PERSON_API)
@Api(ApiConstants.PERSON_API)
public class PersonController {

    private final PersonApiService personApiService;

    public PersonController(PersonApiService personApiService) {
        this.personApiService = personApiService;
    }

    @GetMapping("/{uuid}")
    @ApiOperation(value = "Get person by UUID", code = 200, response = Person.class)
    public ResponseEntity<Person> getPerson(@PathVariable UUID uuid) {
        Person person = personApiService.getPerson(uuid);
        Person result = Optional.ofNullable(person)
                .orElseThrow(() -> new HttpNotFoundException("Person not found"));
        return ResponseEntity.ok(result);
    }

    @GetMapping("/{uuid}/candidate")
    public ResponseEntity<Candidate> getCandidate(@PathVariable UUID uuid) {
        Candidate candidate = Candidate.NULL;
        return ResponseEntity.ok(candidate);
    }

    @PostMapping
    @ApiOperation(value = "Creates person - Candidate,Hirer,Teacher,External",
            code = 201, responseHeaders = @ResponseHeader(name = "Location"))
    public ResponseEntity createPerson(@RequestBody @ApiParam(required = true) Person person) {
        Person retult = personApiService.createPerson(person);
        URI uri = ServletUriComponentsBuilder.fromCurrentRequest()
                .path("/{uuid}")
                .buildAndExpand(retult.uuid())
                .toUri();
        return ResponseEntity.created(uri)
                .build();
    }
}
