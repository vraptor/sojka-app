package co.jware.sojka.rest.service.api.impl;

import co.jware.sojka.core.domain.Resume;
import co.jware.sojka.core.domain.party.Candidate;
import co.jware.sojka.core.domain.party.Party;
import co.jware.sojka.core.domain.party.Submitter;
import co.jware.sojka.core.service.PersonService;
import co.jware.sojka.core.service.ResumeService;
import co.jware.sojka.rest.service.api.ResumeApiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service("resumeApiService")
public class ResumeApiServiceImpl extends BaseApiService implements ResumeApiService {

    private final ResumeService resumeService;
    @Autowired
    private PersonService personService;

    public ResumeApiServiceImpl(ResumeService resumeService) {
        this.resumeService = resumeService;
    }

    @Override
    public Resume createResume(Resume resume) {
        return resumeService.createResume(resume);
    }

    @Override
    public Resume getResume(UUID uuid) {
        return resumeService.findResume(uuid);
    }

    @Override
    public Page<Resume> getByOwner(UUID uuid, Pageable pageable) {
        Party owner = personService.findByUuid(uuid);
        if (owner instanceof Candidate) {
            Candidate candidate = (Candidate) owner;
            return resumeService.findByOwner(candidate, pageable);
        }
        throw new IllegalArgumentException("Invalid owner");
    }

    @Override
    public Page<Resume> getBySubmitter(UUID uuid, Pageable pageable) {
        Party owner = personService.findByUuid(uuid);
        if (owner instanceof Submitter) {
            Submitter submitter = (Submitter) owner;
            return resumeService.findBySubmitter(submitter, pageable);
        }
        throw new IllegalArgumentException("Invalid owner");
    }
}
