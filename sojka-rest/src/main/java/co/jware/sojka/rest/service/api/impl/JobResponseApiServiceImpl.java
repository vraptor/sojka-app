package co.jware.sojka.rest.service.api.impl;

import co.jware.sojka.core.domain.JobListing;
import co.jware.sojka.core.domain.JobResponse;
import co.jware.sojka.core.domain.party.Party;
import co.jware.sojka.core.service.PersonService;
import co.jware.sojka.core.service.entity.JobListingService;
import co.jware.sojka.core.service.entity.JobResponseService;
import co.jware.sojka.rest.service.api.JobResponseApiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.UUID;

@Service("jobResponseApiService")
public class JobResponseApiServiceImpl extends BaseApiService implements JobResponseApiService {

    private final JobResponseService jobResponseService;
    @Autowired
    private JobListingService jobListingService;
    @Autowired
    private PersonService personService;

    public JobResponseApiServiceImpl(JobResponseService jobResponseService) {
        this.jobResponseService = jobResponseService;
    }

    @Override
    public JobResponse createJobResponse(JobResponse jobResponse) {
        return jobResponseService.createJobResponse(jobResponse)
                .orElseThrow(RuntimeException::new);
    }

    @Override
    public Page<JobResponse> findByJobListing(UUID uuid, Pageable pageable) {
        JobListing jobListing = jobListingService.findByUuid(uuid)
                .orElseThrow(IllegalArgumentException::new);
        return jobResponseService.findByJobListing(jobListing, pageable);
    }

    @Override
    public Page<JobResponse> findByRespondent(UUID uuid, Pageable pageable) {
        Party respondent = Optional.ofNullable(personService.findByUuid(uuid))
                .orElseThrow(IllegalArgumentException::new);
        return jobResponseService.findByRespondent(respondent, pageable);
    }
}
