package co.jware.sojka.rest.service.api;


import co.jware.sojka.core.domain.Resume;
import co.jware.sojka.repository.ResumeRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.UUID;

public interface ResumeApiService {

    Resume createResume(Resume resume);

    Resume getResume(UUID uuid);

    Page<Resume> getByOwner(UUID uuid, Pageable pageable);

    Page<Resume> getBySubmitter(UUID uuid, Pageable pageable);
}
