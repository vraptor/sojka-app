package co.jware.sojka.rest.service.api.impl;

import co.jware.sojka.core.domain.User;
import co.jware.sojka.core.service.entity.UserService;
import co.jware.sojka.rest.service.api.UserApiService;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service("userApiService")
public class UserApiServiceImpl implements UserApiService {
    private final UserService userService;

    public UserApiServiceImpl(UserService userService) {
        this.userService = userService;
    }

    @Override
    public User findUser(UUID uuid) {
        return userService.findByUuid(uuid);
    }
}
