package co.jware.sojka.rest.resources;

import co.jware.sojka.core.domain.pipelines.OwnerPipeline;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.Resource;

public class OwnerPipelineResource extends Resource<OwnerPipeline> {
    public OwnerPipelineResource(OwnerPipeline content, Link... links) {
        super(content, links);
    }
}
