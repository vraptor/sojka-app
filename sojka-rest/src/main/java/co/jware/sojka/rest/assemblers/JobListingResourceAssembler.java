package co.jware.sojka.rest.assemblers;

import co.jware.sojka.core.domain.JobListing;
import co.jware.sojka.rest.controllers.JobListingController;
import co.jware.sojka.rest.resources.JobListingResource;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import org.springframework.stereotype.Component;

@Component
public class JobListingResourceAssembler extends ResourceAssemblerSupport<JobListing, JobListingResource> {

    public JobListingResourceAssembler() {
        super(JobListingController.class, JobListingResource.class);
    }

    @Override
    public JobListingResource toResource(JobListing entity) {
        return createResourceWithId(entity.uuid(), entity);
    }

    @Override
    protected JobListingResource instantiateResource(JobListing entity) {
        return new JobListingResource(JobListing.copyOf(entity));
    }
}
