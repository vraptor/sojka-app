package co.jware.sojka.rest.resources;


import co.jware.sojka.core.domain.Resume;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.Resource;

public class ResumeResource extends Resource<Resume> {
    public ResumeResource(Resume content, Link... links) {
        super(content, links);
    }
}
