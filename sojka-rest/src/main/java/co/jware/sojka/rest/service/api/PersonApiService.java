package co.jware.sojka.rest.service.api;

import co.jware.sojka.core.domain.party.Person;

import java.util.UUID;

public interface PersonApiService {
    Person getPerson(UUID uuid);

    Person createPerson(Person person);
}
