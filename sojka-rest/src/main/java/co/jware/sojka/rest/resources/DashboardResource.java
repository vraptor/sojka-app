package co.jware.sojka.rest.resources;


import co.jware.sojka.core.domain.dashboards.Dashboard;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.Resource;

public class DashboardResource extends Resource<Dashboard> {

    public DashboardResource(Dashboard content, Link... links) {
        super(content, links);
    }

}
