package co.jware.sojka.rest.controllers;

import co.jware.sojka.core.domain.Resume;
import co.jware.sojka.rest.ApiConstants;
import co.jware.sojka.rest.assemblers.ResumeResourceAssembler;
import co.jware.sojka.rest.exceptions.HttpNotFoundException;
import co.jware.sojka.rest.resources.ResumeResource;
import co.jware.sojka.rest.service.api.ResumeApiService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.PagedResources;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.UUID;

@RestController
@RequestMapping(ApiConstants.RESUME_API)
@Api
public class ResumeController {

    private final ResumeApiService apiService;
    @Autowired
    private ResumeResourceAssembler assembler;

    public ResumeController(ResumeApiService apiService) {
        this.apiService = apiService;
    }

    @PostMapping("")
    @Secured("hasRole('USER)")
    public ResponseEntity createResume(@RequestBody Resume resume) {
        Resume result = apiService.createResume(resume);
        URI uri = ServletUriComponentsBuilder.fromCurrentRequest()
                .path("/{uuid}")
                .buildAndExpand(result.uuid())
                .toUri();
        return ResponseEntity.created(uri)
                .build();
    }

    @GetMapping("/{uuid}/owner")
    public PagedResources<ResumeResource> getByOwner(@PathVariable UUID uuid, Pageable pageable,
                                                     PagedResourcesAssembler<Resume> pagedResourcesAssembler) {
        Page<Resume> result = apiService.getByOwner(uuid, pageable);
        if (result.hasContent()) {
            return pagedResourcesAssembler.toResource(result, assembler);
        }
        throw new HttpNotFoundException("Resumes not found");
    }

    @GetMapping("/{uuid}/submitter")
    public PagedResources<ResumeResource> getBySubmitter(@PathVariable UUID uuid, Pageable pageable,
                                                         PagedResourcesAssembler<Resume> pagedResourcesAssembler) {
        Page<Resume> result = apiService.getBySubmitter(uuid, pageable);
        if (result.hasContent()) {
            return pagedResourcesAssembler.toResource(result, assembler);
        }
        throw new HttpNotFoundException("Resumes not found");
    }

    @GetMapping("/{uuid}")
    public ResponseEntity<Resume> getResume(@PathVariable UUID uuid) {
        Resume result = apiService.getResume(uuid);
        return ResponseEntity.ok(result);
    }
}
