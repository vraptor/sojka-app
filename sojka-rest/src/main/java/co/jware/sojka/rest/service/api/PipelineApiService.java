package co.jware.sojka.rest.service.api;


import co.jware.sojka.core.domain.pipelines.JobPipeline;
import co.jware.sojka.core.domain.pipelines.OwnerPipeline;
import co.jware.sojka.core.domain.pipelines.PersonPipeline;

import java.util.UUID;

public interface PipelineApiService {

    OwnerPipeline createOwnerPipeline(OwnerPipeline pipeline);

    JobPipeline createJobPipeline(JobPipeline pipeline);

    PersonPipeline createPersonPipeline(PersonPipeline pipeline);

    OwnerPipeline getOwnerPipeLine(UUID uuid);

    JobPipeline getJobPipeline(UUID uuid);
}
