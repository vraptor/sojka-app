package co.jware.sojka.rest.service.api;

import co.jware.sojka.core.domain.Account;
import co.jware.sojka.core.service.entity.AccountService;

import java.util.UUID;

public interface AccountApiService {

    Account createAccount(Account account);

    Account getByUser(UUID uuid);

    Account getByPerson(UUID uuid);
}
