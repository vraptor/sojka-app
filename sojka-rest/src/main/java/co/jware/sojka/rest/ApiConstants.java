package co.jware.sojka.rest;


public class ApiConstants {

    public static final String DASHBOARD_API = "/dashboard";

    public static final String PIPELINE_API = "/pipeline";

    public static final String STAGE_API = "/stage";

    public static final String JOB_LISTING_API = "/job-listing";

    public static final String JOB_RESPONSE_API = "/job-response";

    public static final String RECOMMENDATION_API = "/recommendation";

    public static final String RESUME_API = "/resume";

    public static final String PERSON_API = "/person";

    public static final String ACCOUNT_API = "/account";

    public static final String USER_API = "/user";

    public static final String REGISTRATION_API = "/register";

    public static final String INTERVIEW_API = "/interview";
}
