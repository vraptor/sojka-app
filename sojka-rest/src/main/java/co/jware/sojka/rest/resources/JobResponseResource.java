package co.jware.sojka.rest.resources;


import co.jware.sojka.core.domain.JobResponse;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.Resource;

public class JobResponseResource extends Resource<JobResponse> {

    public JobResponseResource(JobResponse content, Link... links) {
        super(content, links);
    }
}
