package co.jware.sojka.rest.controllers;

import co.jware.sojka.core.domain.JobListing;
import co.jware.sojka.rest.assemblers.JobListingResourceAssembler;
import co.jware.sojka.rest.exceptions.HttpNotFoundException;
import co.jware.sojka.rest.resources.JobListingResource;
import co.jware.sojka.rest.service.api.JobListingApiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.PagedResources;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.UUID;

import static co.jware.sojka.core.enums.JobListingOrigin.*;
import static co.jware.sojka.rest.ApiConstants.JOB_LISTING_API;

@RestController
@RequestMapping(JOB_LISTING_API)
public class JobListingController {

    private final JobListingApiService apiService;

    @Autowired
    private JobListingResourceAssembler assembler;

    public JobListingController(JobListingApiService apiService) {
        this.apiService = apiService;
    }

    @GetMapping
    public PagedResources<JobListingResource> getListings(@PageableDefault(size = 20) Pageable pageable,
                                                          PagedResourcesAssembler<JobListing> pagedResourcesAssembler) {
        Page<JobListing> jobListings = apiService.getListings(pageable);
        if (jobListings.hasContent()) {
            return pagedResourcesAssembler.toResource(jobListings, assembler);
        }
        throw new HttpNotFoundException("Job listings not found");
    }

    @GetMapping("/{uuid}/owner")
    public PagedResources<JobListingResource> getByOwner(@PathVariable UUID uuid, Pageable pageable,
                                                         PagedResourcesAssembler<JobListing> pagedResourcesAssembler) {
        Page<JobListing> page = apiService.findByOwner(uuid, pageable);
        if (page.hasContent()) {
            return pagedResourcesAssembler.toResource(page, assembler);
        }
        throw new HttpNotFoundException("Job listings not found");
    }

    @PostMapping
    @Secured("hasRole('USER')")
    public ResponseEntity createJobListing(@RequestBody JobListing jobListing) {
        JobListing result = apiService.create(jobListing.withOrigin(INTERNAL));
        UUID uuid = result.uuid();
        URI uri = ServletUriComponentsBuilder.fromCurrentRequest()
                .path("/{uuid}")
                .buildAndExpand(uuid)
                .toUri();
        return ResponseEntity.created(uri).build();
    }

    @PutMapping
    public ResponseEntity<JobListing> updateJobListing(@RequestBody JobListing jobListing) {
        JobListing result =
//                JobListing.copyOf(jobListing);
                apiService.update(jobListing);
        return ResponseEntity.ok(result);
    }

    @DeleteMapping
    public ResponseEntity.HeadersBuilder<?> deleteJobListing(@PathVariable UUID uuid) {
        return ResponseEntity.noContent();
    }
}
