package co.jware.sojka.rest.core;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.immutables.value.Value;

import javax.annotation.Nullable;

@Value.Immutable
@JsonDeserialize(builder = SearchFilter.Builder.class, as = SearchFilter.class)
@JsonSerialize(as = SearchFilter.class)
public abstract class AbstractSearchFilter {
    @Nullable
    abstract public String[] locations();

    abstract public String[] keywords();
}
