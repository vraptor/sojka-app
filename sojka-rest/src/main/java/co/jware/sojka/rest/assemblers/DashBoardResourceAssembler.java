package co.jware.sojka.rest.assemblers;

import co.jware.sojka.core.domain.dashboards.Dashboard;
import co.jware.sojka.rest.controllers.DashboardController;
import co.jware.sojka.rest.resources.DashboardResource;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import org.springframework.stereotype.Component;

@Component
public class DashBoardResourceAssembler extends ResourceAssemblerSupport<Dashboard, DashboardResource> {

    public DashBoardResourceAssembler() {
        super(DashboardController.class, DashboardResource.class);
    }

    @Override
    public DashboardResource toResource(Dashboard entity) {
        DashboardResource resource = createResourceWithId(entity.uuid(), entity);
        return resource;
    }

    @Override
    protected DashboardResource instantiateResource(Dashboard entity) {
        return new DashboardResource(Dashboard.copyOf(entity));
    }
}
