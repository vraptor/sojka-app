package co.jware.sojka.rest.core.dto;

import co.jware.sojka.core.enums.RegistrationType;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.immutables.value.Value;

import javax.annotation.Nullable;

@Value.Immutable
@JsonDeserialize(builder = RegisterDto.Builder.class)
public abstract class AbstractRegisterDto {
    @Nullable
    public abstract String firstName();

    @Nullable
    public abstract String lastName();

    public abstract String email();

    abstract RegistrationType registrationType();

}
