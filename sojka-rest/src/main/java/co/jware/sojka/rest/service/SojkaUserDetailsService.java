package co.jware.sojka.rest.service;


import co.jware.sojka.entities.core.UserBo;
import co.jware.sojka.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;

public class SojkaUserDetailsService implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        UserBo user = userRepository.findByUsername(username);
        // TODO: 10/27/2016 Encode passwords in database
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        if (null == user) {
            throw new UsernameNotFoundException("User with name " + username + " not found");
        }
        return new SojkaUserDetails(user);
    }

}
