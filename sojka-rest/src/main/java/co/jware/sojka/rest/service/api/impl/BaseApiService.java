package co.jware.sojka.rest.service.api.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;

public abstract class BaseApiService {

    @Autowired
    protected ObjectMapper objectMapper;
}
