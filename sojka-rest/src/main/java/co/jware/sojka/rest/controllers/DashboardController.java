package co.jware.sojka.rest.controllers;

import co.jware.sojka.core.domain.dashboards.Dashboard;
import co.jware.sojka.rest.exceptions.HttpNotFoundException;
import co.jware.sojka.rest.assemblers.DashBoardResourceAssembler;
import co.jware.sojka.rest.resources.DashboardResource;
import co.jware.sojka.rest.service.api.DashboardApiService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.PagedResources;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.UUID;

import static co.jware.sojka.rest.ApiConstants.DASHBOARD_API;

@RestController()
@RequestMapping(path = DASHBOARD_API)
@Api()
public class DashboardController {

    private final DashboardApiService apiService;

    @Autowired
    private DashBoardResourceAssembler assembler;

    @Autowired
    public DashboardController(DashboardApiService apiService) {
        this.apiService = apiService;
    }

    @GetMapping("/{uuid}")
    @Secured("hasRole('USER')")
    public ResponseEntity<Dashboard> getDashboard(@PathVariable UUID uuid) {
        Dashboard dashboard = apiService.getDashboard(uuid);
        return ResponseEntity.ok(dashboard);
    }

    @GetMapping("/{uuid}/owner")
    @Secured("hasRole('USER')")
    public PagedResources<DashboardResource> getByOwner(@PathVariable UUID uuid, @PageableDefault(size = 20) Pageable pageable,
                                                        PagedResourcesAssembler<Dashboard> pagedResourcesAssembler) {
        Page<Dashboard> dashboardPage = apiService.getByOwner(uuid, pageable);

        if (dashboardPage.hasContent()) {
            PagedResources<DashboardResource> resources = pagedResourcesAssembler.toResource(dashboardPage, assembler);
            return resources;
        }
        throw new HttpNotFoundException(" Dashboards not found");
    }

    @PostMapping
    @Secured("hasRole('USER)")
    @ApiOperation(value = "Create Dashboard", code = 201)
    public ResponseEntity create(@RequestBody Dashboard dashboard) {
        Dashboard result = apiService.createDashboard(dashboard);
        URI uri = ServletUriComponentsBuilder.fromCurrentRequest()
                .path("/{uuid}")
                .buildAndExpand(result.uuid()).toUri();
        return ResponseEntity.created(uri).build();
    }

    @DeleteMapping("/{uuid}")
    @Secured("hasRole('USER)")
    @ApiOperation(value = "Delete Dashboard", code = 204)
    public ResponseEntity deleteDashboard(@PathVariable UUID uuid) {
        apiService.deleteDashboard(uuid);
        return ResponseEntity.noContent().build();
    }

    @PutMapping
    public ResponseEntity<Dashboard> updateDashboard(@RequestBody Dashboard dashboard) {
        return ResponseEntity.ok(apiService.updateDashboard(dashboard));
    }
}
