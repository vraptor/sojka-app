package co.jware.sojka.rest.service.api.impl;

import co.jware.sojka.core.domain.JobListing;
import co.jware.sojka.core.domain.Recommendation;
import co.jware.sojka.core.domain.Recommendation;
import co.jware.sojka.core.domain.party.External;
import co.jware.sojka.core.domain.party.Party;
import co.jware.sojka.core.service.PersonService;
import co.jware.sojka.core.service.entity.RecommendationService;
import co.jware.sojka.rest.core.dto.RecommendDto;
import co.jware.sojka.rest.exceptions.HttpNotFoundException;
import co.jware.sojka.rest.service.api.RecommendationApiService;

import org.jdeferred.DoneCallback;
import org.jdeferred.FailCallback;
import org.jdeferred.Promise;
import org.jdeferred.impl.DefaultDeferredManager;
import org.jdeferred.multiple.MultipleResults;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.UUID;

@Service("recommendationApiService")
public class RecommendationApiServiceImpl implements RecommendationApiService {

    private final RecommendationService recommendationService;
    @Autowired
    private PersonService personService;

    public RecommendationApiServiceImpl(RecommendationService recommendationService) {
        this.recommendationService = recommendationService;
    }


    @Override
    public Recommendation<JobListing> create(Recommendation<JobListing> recommendation) {
        return (Recommendation) recommendationService.createRecommendation(recommendation);
    }

    @Override
    public Page<Recommendation> getByReceiver(UUID uuid, Pageable pageable) {
        return Optional.ofNullable(personService.findByUuid(uuid))
                .map(receiver -> recommendationService.findByReceiver(receiver, pageable)
                        .map(Recommendation.class::cast))
                .orElseThrow(() -> new HttpNotFoundException("Receiver not found"));
    }

    @Override
    public Page<Recommendation> getBySender(UUID uuid, Pageable pageable) {
        return Optional.ofNullable(personService.findByUuid(uuid))
                .map(sender -> recommendationService.findBySender(sender, pageable)
                        .map(Recommendation.class::cast))
                .orElseThrow(() -> new HttpNotFoundException("Sender not found"));
    }

    @Override
    public Page<Recommendation> getBySubject(UUID uuid, Pageable pageable) {
        return recommendationService.findBySubject(uuid, pageable);
    }

    @Override
    public Recommendation recommendSubject(UUID uuid, RecommendDto dto) {
        String receiverEmail = dto.receiverEmail();
        Party receiver = personService.findByEmail(receiverEmail)
                .orElseGet(() -> External.builder().email(receiverEmail).build());
        Party sender = personService.findByEmail(dto.senderEmail())
                .orElseThrow(() -> new IllegalArgumentException("Sender not found"));
        return recommendationService.recommendSubject(sender, receiver, uuid);
    }

    @Override
    public void deleteRecommendation(UUID uuid) {
        Recommendation recommendation = recommendationService.findByUuid(uuid)
                .orElseThrow(() -> new HttpNotFoundException("Recommendation not found"));
        recommendationService.delete(recommendation);
    }
}
