package co.jware.sojka.rest.controllers;

import co.jware.sojka.rest.ApiConstants;
import co.jware.sojka.rest.core.dto.RegisterDto;
import co.jware.sojka.rest.service.api.RegisterApiService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.UUID;

@RestController
@RequestMapping(ApiConstants.REGISTRATION_API)
public class RegistrationController {


    private final RegisterApiService registerApiService;

    public RegistrationController(RegisterApiService registerApiService) {
        this.registerApiService = registerApiService;
    }

    @PostMapping
    public ResponseEntity register(@RequestBody RegisterDto registerDto) {
        URI uri = ServletUriComponentsBuilder.fromCurrentRequest()
                .path("/{uuid}")
                .buildAndExpand(UUID.randomUUID()).toUri();
        return ResponseEntity.created(uri)
                .build();
    }
}
