package co.jware.sojka.rest.controllers;

import co.jware.sojka.core.domain.User;
import co.jware.sojka.rest.ApiConstants;
import co.jware.sojka.rest.service.api.UserApiService;
import io.swagger.annotations.Api;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@RestController
@RequestMapping(ApiConstants.USER_API)
@Api
public class UserController {

    private final UserApiService apiService;

    public UserController(UserApiService apiService) {
        this.apiService = apiService;
    }

    @GetMapping("/{uuid}")
    public ResponseEntity<User> getUser(@PathVariable UUID uuid) {
        User user = apiService.findUser(uuid);
        return ResponseEntity.ok(user);
    }
}
