package co.jware.sojka.rest.controllers;

import co.jware.sojka.core.domain.JobListing;
import co.jware.sojka.core.domain.Recommendation;
import co.jware.sojka.rest.ApiConstants;
import co.jware.sojka.rest.assemblers.PersonRecommendationResourceAssembler;
import co.jware.sojka.rest.core.dto.RecommendDto;
import co.jware.sojka.rest.exceptions.HttpNotFoundException;
import co.jware.sojka.rest.resources.PersonRecommendationResource;
import co.jware.sojka.rest.service.api.RecommendationApiService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.PagedResources;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.UUID;

@RestController
@RequestMapping(ApiConstants.RECOMMENDATION_API)
public class RecommendationController {

    private final RecommendationApiService apiService;
    @Autowired
    private PersonRecommendationResourceAssembler assembler;

    public RecommendationController(RecommendationApiService apiService) {
        this.apiService = apiService;
    }

    @PostMapping("/person")
    @Secured("hasRole('USER')")
    public ResponseEntity createRecommendation(@RequestBody Recommendation<JobListing> recommendation) {
        Recommendation<JobListing> result = apiService.create(recommendation);
        UUID uuid = result.uuid();
        URI uri = ServletUriComponentsBuilder.fromCurrentRequest().path("/{uuid}")
                .buildAndExpand(uuid)
                .toUri();
        return ResponseEntity.created(uri).build();
    }

    @PostMapping("/{uuid}/subject")
    @Secured("hasRole('USER')")
    public ResponseEntity recommendSubject(@PathVariable("uuid") UUID uuid, @RequestBody RecommendDto dto) {
        Recommendation personRecommendation = apiService.recommendSubject(uuid, dto);
        URI uri = ServletUriComponentsBuilder.fromCurrentRequest()
                .path("/{uuid}").buildAndExpand(personRecommendation.uuid())
                .toUri();
        return ResponseEntity.created(uri).build();
    }

    @DeleteMapping("/{uuid}")
    public ResponseEntity delete(@PathVariable UUID uuid) {
        apiService.deleteRecommendation(uuid);
        return ResponseEntity.noContent().build();
    }

    @GetMapping("/{uuid}/receiver")
    @Secured("hasRole('USER')")
    public PagedResources<PersonRecommendationResource> getByReceiver(@PathVariable UUID uuid, Pageable pageable,
                                                                      PagedResourcesAssembler<Recommendation> pagedResourcesAssembler) {
        Page<Recommendation> page = apiService.getByReceiver(uuid, pageable);
        if (page.hasContent()) {
            return pagedResourcesAssembler.toResource(page, assembler);
        }
        throw new HttpNotFoundException("Recommendation not found");
    }

    @GetMapping("/{uuid}/sender")
    @Secured("hasRole('USER')")
    public PagedResources<PersonRecommendationResource> getBySender(@PathVariable("uuid") UUID uuid, Pageable pageable,
                                                                    PagedResourcesAssembler<Recommendation> pagedResourcesAssembler) {
        Page<Recommendation> page = apiService.getBySender(uuid, pageable);
        if (page.hasContent()) {
            return pagedResourcesAssembler.toResource(page, assembler);
        }
        throw new HttpNotFoundException("Recommendation not found");
    }

    @GetMapping("/{uuid}/subject")
    @Secured(("hasRole('USER')"))
    public PagedResources<PersonRecommendationResource> getBySubject(@PathVariable("uuid") UUID uuid, Pageable pageable,
                                                                     PagedResourcesAssembler<Recommendation> pagedResourcesAssembler) {
        Page<Recommendation> page = apiService.getBySubject(uuid, pageable);
        if (page.hasContent()) {
            return pagedResourcesAssembler.toResource(page, assembler);
        }
        throw new HttpNotFoundException("Recommendation not found");
    }
}
