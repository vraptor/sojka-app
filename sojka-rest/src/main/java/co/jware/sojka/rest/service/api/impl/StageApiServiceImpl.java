package co.jware.sojka.rest.service.api.impl;

import co.jware.sojka.core.domain.pipelines.Stage;
import co.jware.sojka.core.service.StageService;
import co.jware.sojka.rest.service.api.StageApiService;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service("stageApiService")
public class StageApiServiceImpl implements StageApiService {
    private final StageService stageService;

    public StageApiServiceImpl(StageService stageService) {
        this.stageService = stageService;
    }

    @Override
    public Stage getStage(UUID uuid) {
        return stageService.findStage(uuid);
    }
}
