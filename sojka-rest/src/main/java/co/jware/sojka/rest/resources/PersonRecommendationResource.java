package co.jware.sojka.rest.resources;


import co.jware.sojka.core.domain.Recommendation;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.Resource;

public class PersonRecommendationResource extends Resource<Recommendation> {

    public PersonRecommendationResource(Recommendation content, Link... links) {
        super(content, links);
    }
}
