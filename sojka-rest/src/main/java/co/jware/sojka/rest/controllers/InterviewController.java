package co.jware.sojka.rest.controllers;

import co.jware.sojka.core.domain.Interview;
import co.jware.sojka.rest.ApiConstants;
import co.jware.sojka.rest.assemblers.InterviewResourceAssembler;
import co.jware.sojka.rest.exceptions.HttpNotFoundException;
import co.jware.sojka.rest.resources.InterviewResource;
import co.jware.sojka.rest.service.api.InterviewApiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.PagedResources;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.UUID;

@RestController
@RequestMapping(ApiConstants.INTERVIEW_API)
public class InterviewController {

    private final InterviewApiService apiService;

    @Autowired
    private InterviewResourceAssembler assembler;

    public InterviewController(InterviewApiService apiService) {
        this.apiService = apiService;
    }

    @PostMapping()
    public ResponseEntity createInterview(@RequestBody Interview interview) {
        Interview result = apiService.createInterview(interview);
        URI uri = ServletUriComponentsBuilder.fromCurrentRequest()
                .path("/{uuid}")
                .buildAndExpand(result.uuid())
                .toUri();
        return ResponseEntity.created(uri).build();
    }

    @GetMapping("/{uuid}")
    public ResponseEntity<Interview> getInterview(@PathVariable("uuid") UUID uuid) {
        return ResponseEntity.ok(apiService.findByUuid(uuid));
    }

    @GetMapping("/{uuid}/candidate")
    public PagedResources<InterviewResource> getInterviewsByCandidate(@PathVariable("uuid") UUID uuid, Pageable pageable,
                                                                      PagedResourcesAssembler<Interview> pagedResourcesAssembler) {
        Page<Interview> page = apiService.findByCandidateUuid(uuid, pageable);
        if (page.hasContent()) {
            return pagedResourcesAssembler.toResource(page, assembler);
        }
        throw new HttpNotFoundException("Interviews not found");
    }

    @GetMapping("/{uuid}/job-listing")
    public PagedResources<InterviewResource> getInterviewsByJoblisting(@PathVariable("uuid") UUID uuid, Pageable pageable,
                                                                       PagedResourcesAssembler<Interview> pagedResourcesAssembler) {
        Page<Interview> page = apiService.findByJobListingUuid(uuid, pageable);
        if (page.hasContent()) {
            return pagedResourcesAssembler.toResource(page, assembler);
        }
        throw new HttpNotFoundException("Interviews not found");
    }
}
