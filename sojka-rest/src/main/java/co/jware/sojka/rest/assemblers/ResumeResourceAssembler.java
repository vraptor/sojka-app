package co.jware.sojka.rest.assemblers;

import co.jware.sojka.core.domain.Resume;
import co.jware.sojka.rest.controllers.ResumeController;
import co.jware.sojka.rest.resources.ResumeResource;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import org.springframework.stereotype.Component;

@Component
public class ResumeResourceAssembler extends ResourceAssemblerSupport<Resume, ResumeResource> {

    public ResumeResourceAssembler() {
        super(ResumeController.class, ResumeResource.class);
    }

    @Override
    protected ResumeResource instantiateResource(Resume entity) {
        return new ResumeResource(Resume.copyOf(entity));
    }

    @Override
    public ResumeResource toResource(Resume entity) {
        return createResourceWithId(entity.uuid(), entity);
    }
}
