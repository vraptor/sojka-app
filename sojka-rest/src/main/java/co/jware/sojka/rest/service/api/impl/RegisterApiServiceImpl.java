package co.jware.sojka.rest.service.api.impl;

import co.jware.sojka.core.service.RegisterService;
import co.jware.sojka.rest.service.api.RegisterApiService;
import org.springframework.stereotype.Service;

@Service("registerApiService")
public class RegisterApiServiceImpl implements RegisterApiService {

    private final RegisterService registerService;

    public RegisterApiServiceImpl(RegisterService registerService) {
        this.registerService = registerService;
    }
}
