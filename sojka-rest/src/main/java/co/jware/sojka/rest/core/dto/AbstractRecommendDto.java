package co.jware.sojka.rest.core.dto;

import org.hibernate.validator.constraints.Email;
import org.immutables.value.Value;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.google.common.base.Preconditions;

@Value.Immutable
@JsonDeserialize(builder = RecommendDto.Builder.class)
public interface AbstractRecommendDto {
    @Email
    String receiverEmail();

    @Email
    String senderEmail();

    @Value.Check
    default void check() {
        Preconditions.checkState(!receiverEmail().equalsIgnoreCase(senderEmail()));
    }
}
