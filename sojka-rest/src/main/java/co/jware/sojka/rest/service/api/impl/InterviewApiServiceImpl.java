package co.jware.sojka.rest.service.api.impl;

import co.jware.sojka.core.domain.Interview;
import co.jware.sojka.core.domain.JobListing;
import co.jware.sojka.core.domain.party.Candidate;
import co.jware.sojka.core.service.InterviewService;
import co.jware.sojka.core.service.PersonService;
import co.jware.sojka.core.service.entity.JobListingService;
import co.jware.sojka.rest.exceptions.HttpNotFoundException;
import co.jware.sojka.rest.service.api.InterviewApiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service("interviewApiService")
public class InterviewApiServiceImpl extends BaseApiService implements InterviewApiService {


    private final InterviewService interviewService;
    @Autowired
    private PersonService personService;
    @Autowired
    private JobListingService jobListingService;

    public InterviewApiServiceImpl(InterviewService interviewService) {
        this.interviewService = interviewService;
    }

    @Override
    public Interview createInterview(Interview interview) {
        Candidate candidate = interview.candidate();
        if (candidate.isNew()) {
            throw new IllegalArgumentException("Candidate does not exist");
        }
        JobListing jobListing = interview.jobListing();
        if (jobListing.isNew()) {
            throw new IllegalArgumentException("Job listing does not exist");
        }
        return interviewService.createInterview(interview)
                .orElseThrow(() -> new IllegalStateException("Error creating interview"));
    }

    @Override
    public Interview findByUuid(UUID uuid) {
        return interviewService.findByUuid(uuid)
                .orElseThrow(() -> new HttpNotFoundException("Interview not found"));
    }

    @Override
    public Page<Interview> findByCandidateUuid(UUID uuid, Pageable pageable) {
        Candidate candidate = personService.findCandidate(uuid)
                .orElseThrow(() -> new IllegalArgumentException("Candidate not found"));
        return interviewService.findByCandidate(candidate, pageable);
    }

    @Override
    public Page<Interview> findByJobListingUuid(UUID uuid, Pageable pageable) {
        JobListing jobListing = jobListingService.findByUuid(uuid)
                .orElseThrow(() -> new IllegalArgumentException("Job listing not found"));
        return interviewService.findByJbListing(jobListing, pageable);
    }
}
