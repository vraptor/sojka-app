package co.jware.sojka.rest.controllers;

import co.jware.sojka.core.domain.Account;
import co.jware.sojka.rest.ApiConstants;
import co.jware.sojka.rest.service.api.AccountApiService;
import io.swagger.annotations.Api;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.UUID;

@RestController
@RequestMapping(ApiConstants.ACCOUNT_API)
@Api
public class AccountController {
    private final AccountApiService apiService;

    public AccountController(AccountApiService apiService) {
        this.apiService = apiService;
    }

    @GetMapping("/{uuid}/user")
    public ResponseEntity<Account> getByUser(@PathVariable UUID uuid) {
        Account account = apiService.getByUser(uuid);
        return ResponseEntity.ok(account);
    }

    @GetMapping("/{uuid}/person")
    public ResponseEntity<Account> getByPerson(@PathVariable UUID uuid) {
        Account account = apiService.getByPerson(uuid);
        return ResponseEntity.ok(account);
    }

    @PostMapping()
    @Secured({"ROLE_ADMIN"})
    public ResponseEntity<Object> createAccount(@RequestBody Account account) {
        Account result = apiService.createAccount(account);
        UUID uuid = result.uuid();
        URI uri = ServletUriComponentsBuilder.fromCurrentRequest()
                .path("/{uuid}")
                .buildAndExpand(uuid).toUri();
        return ResponseEntity.created(uri).build();
    }
}
