package co.jware.sojka.rest;

import co.jware.sojka.rest.service.SojkaUserDetailsService;
import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;
import io.vertx.core.Vertx;
import io.vertx.core.eventbus.EventBus;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.security.core.userdetails.UserDetailsService;

@Configuration
@EnableJpaRepositories(basePackages = "co.jware.sojka.repository")
@EntityScan("co.jware.sojka.entities")
@ComponentScan(basePackages = {"co.jware.sojka.core"})
public class SojkaRestConfig {

    @Bean
    public UserDetailsService userDetailsService() {
        return new SojkaUserDetailsService();
    }

    @Bean
    public EventBus eventBus() {
        return Vertx.vertx().eventBus();
    }

    @Bean
    public HazelcastInstance hazelcastInstance() {
        return Hazelcast.newHazelcastInstance();
    }
}
