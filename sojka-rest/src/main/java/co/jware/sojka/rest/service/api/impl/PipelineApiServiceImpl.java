package co.jware.sojka.rest.service.api.impl;

import co.jware.sojka.core.domain.party.Hirer;
import co.jware.sojka.core.domain.party.Party;
import co.jware.sojka.core.domain.pipelines.*;
import co.jware.sojka.core.service.pipelines.PipelineService;
import co.jware.sojka.rest.service.api.PipelineApiService;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.UUID;

@Service("pipelineApiService")
public class PipelineApiServiceImpl extends BaseApiService implements PipelineApiService {

    private final PipelineService pipelineService;

    public PipelineApiServiceImpl(PipelineService pipelineService) {
        this.pipelineService = pipelineService;
    }


    @Override
    public OwnerPipeline createOwnerPipeline(OwnerPipeline pipeline) {
        SystemPipeline systemPipeline = pipelineService.getOrCreateSystemPipeline();
        return pipelineService.generateOwnerPipeline(systemPipeline, pipeline.owner());
    }

    @Override
    public JobPipeline createJobPipeline(JobPipeline pipeline) {
        Pipeline parent = Optional.ofNullable(pipeline.parent())
                .orElseGet(() -> {
                    Party owner = pipeline.jobListing()
                            .owner();
                    return pipelineService.generatePipelineByOwner(owner);
                });

        return pipeline.withParent(parent);
    }

    @Override
    public PersonPipeline createPersonPipeline(PersonPipeline pipeline) {
        Pipeline parent = pipeline.parent();
        if (parent instanceof JobPipeline) {
            JobPipeline jobPipeline = (JobPipeline) parent;
            return pipelineService.generatePersonPipeline(pipelineService.getOrCreateJobPipeline(jobPipeline), pipeline.person());
        } else throw new IllegalArgumentException("Invalid parent pipeline");
    }

    @Override
    public OwnerPipeline getOwnerPipeLine(UUID uuid) {
        return pipelineService.findOwnerPipeline(uuid);
    }

    @Override
    public JobPipeline getJobPipeline(UUID uuid) {
        return pipelineService.getJobPipeline(uuid);
    }
}


