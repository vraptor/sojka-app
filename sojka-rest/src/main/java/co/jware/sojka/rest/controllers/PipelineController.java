package co.jware.sojka.rest.controllers;

import co.jware.sojka.core.domain.pipelines.JobPipeline;
import co.jware.sojka.core.domain.pipelines.OwnerPipeline;
import co.jware.sojka.core.domain.pipelines.PersonPipeline;
import co.jware.sojka.rest.ApiConstants;
import co.jware.sojka.rest.service.api.PipelineApiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.UUID;

@RestController
@RequestMapping(ApiConstants.PIPELINE_API)
public class PipelineController {
    @Autowired
    private final PipelineApiService apiService;

    @Autowired
    public PipelineController(PipelineApiService apiService) {
        this.apiService = apiService;
    }

    @GetMapping("/owner/{uuid}")
    public ResponseEntity<OwnerPipeline> getOwnerPipeline(@PathVariable UUID uuid) {
        return ResponseEntity.ok(apiService.getOwnerPipeLine(uuid));
    }

    @GetMapping("/job/{uuid}")
    public ResponseEntity<JobPipeline> getJobPipeline(@PathVariable UUID uuid) {
        return ResponseEntity.ok(apiService.getJobPipeline(uuid));
    }

    public void getPersonPipeline() {

    }

    @PostMapping("/owner")
    public ResponseEntity createOwnerPipeline(@RequestBody OwnerPipeline pipeline) {
        OwnerPipeline result = apiService.createOwnerPipeline(pipeline);
        URI uri = ServletUriComponentsBuilder.fromCurrentRequestUri()
                .path("/{id}")
                .buildAndExpand(result.uuid())
                .toUri();
        return ResponseEntity.created(uri).build();
    }

    @PostMapping("/job")
    public ResponseEntity createJobPipeline(@RequestBody JobPipeline pipeline) {
        JobPipeline result = apiService.createJobPipeline(pipeline);
        URI uri = ServletUriComponentsBuilder.fromCurrentRequestUri()
                .path("/{id}")
                .buildAndExpand(result.uuid())
                .toUri();
        return ResponseEntity.created(uri).build();
    }

    @PostMapping("/person")
    public ResponseEntity createPersonPipeline(@RequestBody PersonPipeline pipeline) {
        PersonPipeline result = apiService.createPersonPipeline(pipeline);
        URI uri = ServletUriComponentsBuilder.fromCurrentRequestUri()
                .path("/{id}")
                .buildAndExpand(result.uuid())
                .toUri();
        return ResponseEntity.created(uri).build();
    }
}
