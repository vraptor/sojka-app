package co.jware.sojka.rest.service.api;


import co.jware.sojka.core.domain.pipelines.Stage;

import java.util.UUID;

public interface StageApiService {
    Stage getStage(UUID uuid);
}
