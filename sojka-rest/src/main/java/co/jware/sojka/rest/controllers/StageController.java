package co.jware.sojka.rest.controllers;

import co.jware.sojka.core.domain.pipelines.Stage;
import co.jware.sojka.rest.ApiConstants;
import co.jware.sojka.rest.service.api.StageApiService;
import io.swagger.annotations.Api;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@RestController
@RequestMapping(ApiConstants.STAGE_API)
@Api
public class StageController {
    private final StageApiService apiService;

    public StageController(StageApiService apiService) {
        this.apiService = apiService;
    }

    @GetMapping("/{uuid}")
    public ResponseEntity<Stage> getStage(@PathVariable UUID uuid) {
        Stage stage = apiService.getStage(uuid);
        return ResponseEntity.ok(stage);
    }
}
