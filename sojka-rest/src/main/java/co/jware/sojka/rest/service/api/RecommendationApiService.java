package co.jware.sojka.rest.service.api;


import co.jware.sojka.core.domain.JobListing;
import co.jware.sojka.core.domain.Recommendation;
import co.jware.sojka.rest.core.dto.RecommendDto;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.UUID;

public interface RecommendationApiService {
    Recommendation<JobListing> create(Recommendation<JobListing> recommendation);

    Page<Recommendation> getByReceiver(UUID uuid, Pageable pageable);

    Recommendation recommendSubject(UUID uuid, RecommendDto dto);

    void deleteRecommendation(UUID uuid);

    Page<Recommendation> getBySender(UUID uuid, Pageable pageable);

    Page<Recommendation> getBySubject(UUID uuid, Pageable pageable);
}
