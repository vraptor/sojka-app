package co.jware.sojka.rest.resources;


import co.jware.sojka.core.domain.JobListing;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.Resource;

public class JobListingResource extends Resource<JobListing> {
    public JobListingResource(JobListing content, Link... links) {
        super(content, links);
    }
}
