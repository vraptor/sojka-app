package co.jware.sojka.rest.service.api;

import co.jware.sojka.core.domain.Interview;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.UUID;

public interface InterviewApiService {

    Interview createInterview(Interview interview);

    Interview findByUuid(UUID uuid);

    Page<Interview> findByCandidateUuid(UUID uuid, Pageable pageable);

    Page<Interview> findByJobListingUuid(UUID uuid, Pageable pageable);
}
