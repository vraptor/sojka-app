package co.jware.sojka.rest.service.api.impl;

import co.jware.sojka.core.domain.JobListing;
import co.jware.sojka.core.domain.party.Hirer;
import co.jware.sojka.core.domain.party.Party;
import co.jware.sojka.core.service.PersonService;
import co.jware.sojka.core.service.entity.JobListingService;
import co.jware.sojka.rest.exceptions.HttpNotFoundException;
import co.jware.sojka.rest.service.api.JobListingApiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.UUID;

@Service("jobListingApiService")
public class JobListingApiServiceImpl extends BaseApiService implements JobListingApiService {

    private final JobListingService jobListingService;
    @Autowired
    private PersonService personService;

    public JobListingApiServiceImpl(JobListingService jobListingService) {
        this.jobListingService = jobListingService;
    }

    @Override
    public JobListing create(JobListing jobListing) {
        Party owner = Optional.ofNullable(jobListing.owner())
                .orElseThrow(IllegalArgumentException::new);
        Party resolved = Optional.ofNullable(personService.findByUuid(owner.uuid()))
                .orElseThrow(IllegalArgumentException::new);
        return jobListingService.createJobListing(jobListing.withOwner((Hirer) resolved))
                .orElseThrow(RuntimeException::new);
    }

    @Override
    public JobListing update(JobListing jobListing) {
        JobListing resolved = jobListingService.findByUuid(jobListing.uuid())
                .orElseThrow(() -> new HttpNotFoundException("Job listing not found"));
        if (resolved.equals(jobListing)) {
            return resolved;
        }
        return jobListingService.update(jobListing);
    }

    @Override
    public Page<JobListing> findByOwner(UUID uuid, Pageable pageable) {
        Party owner = personService.findByUuid(uuid);

        if (Hirer.class.isAssignableFrom(owner.getClass())) {
            Hirer hirer = (Hirer) owner;
            return jobListingService.findByOwner(hirer, pageable);
        }
        throw new IllegalArgumentException();
    }

    @Override
    public Page<JobListing> getListings(Pageable pageable) {
        return jobListingService.findJobListings(pageable);
    }
}
