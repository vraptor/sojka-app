package co.jware.sojka.rest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.data.web.config.EnableSpringDataWebSupport;

@SpringBootApplication
@EnableAutoConfiguration()
@EnableSpringDataWebSupport
public class SojkaRestApplication {
    public static void main(String[] args) {
        SpringApplication application = new SpringApplicationBuilder(SojkaRestApplication.class)
                .build();
        application.run(args);
    }
}
