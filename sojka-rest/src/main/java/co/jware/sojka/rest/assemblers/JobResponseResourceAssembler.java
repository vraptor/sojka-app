package co.jware.sojka.rest.assemblers;

import co.jware.sojka.core.domain.JobResponse;
import co.jware.sojka.rest.controllers.JobResponseController;
import co.jware.sojka.rest.resources.JobResponseResource;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import org.springframework.stereotype.Component;

@Component
public class JobResponseResourceAssembler extends ResourceAssemblerSupport<JobResponse, JobResponseResource> {

    public JobResponseResourceAssembler() {
        super(JobResponseController.class, JobResponseResource.class);
    }

    @Override
    public JobResponseResource toResource(JobResponse entity) {
        return createResourceWithId(entity.uuid(), entity);
    }

    @Override
    protected JobResponseResource instantiateResource(JobResponse entity) {
        return new JobResponseResource(JobResponse.copyOf(entity));
    }
}
