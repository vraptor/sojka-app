package co.jware.sojka.rest.service.api;


import co.jware.sojka.core.domain.JobResponse;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.UUID;

public interface JobResponseApiService {

    JobResponse createJobResponse(JobResponse jobResponse);

    Page<JobResponse> findByJobListing(UUID uuid, Pageable pageable);

    Page<JobResponse> findByRespondent(UUID uuid, Pageable pageable);
}
