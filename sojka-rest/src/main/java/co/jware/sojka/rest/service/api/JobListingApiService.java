package co.jware.sojka.rest.service.api;


import co.jware.sojka.core.domain.JobListing;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.UUID;

public interface JobListingApiService {

    Page<JobListing> findByOwner(UUID uuid, Pageable pageable);

    Page<JobListing> getListings(Pageable pageable);

    JobListing create(JobListing jobListing);

    JobListing update(JobListing jobListing);
}
