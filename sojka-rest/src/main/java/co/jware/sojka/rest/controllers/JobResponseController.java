package co.jware.sojka.rest.controllers;

import co.jware.sojka.core.domain.JobResponse;
import co.jware.sojka.rest.ApiConstants;
import co.jware.sojka.rest.assemblers.JobResponseResourceAssembler;
import co.jware.sojka.rest.exceptions.HttpNotFoundException;
import co.jware.sojka.rest.resources.JobResponseResource;
import co.jware.sojka.rest.service.api.JobResponseApiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.PagedResources;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.UUID;

@RestController
@RequestMapping(ApiConstants.JOB_RESPONSE_API)
public class JobResponseController {
    private final JobResponseApiService apiService;
    @Autowired
    private JobResponseResourceAssembler assembler;

    public JobResponseController(JobResponseApiService jobResponseApiService) {
        this.apiService = jobResponseApiService;
    }

    @PostMapping
    @Secured("hasRole('USER')")
    public ResponseEntity createJobResponse(@RequestBody JobResponse jobResponse) {
        JobResponse result = apiService.createJobResponse(jobResponse);
        URI uri = ServletUriComponentsBuilder.fromCurrentRequest()
                .path("/{uuid}")
                .buildAndExpand(result.uuid())
                .toUri();
        return ResponseEntity.created(uri).build();
    }

    @GetMapping("/{uuid}/job-listing")
    public PagedResources<JobResponseResource> getJobResponsesByJobListing(@PathVariable UUID uuid, Pageable pageable,
                                                                           PagedResourcesAssembler<JobResponse> pagedResourcesAssembler) {
        Page<JobResponse> page = apiService.findByJobListing(uuid, pageable);
        if (page.hasContent()) {
            return pagedResourcesAssembler.toResource(page, assembler);
        }
        throw new HttpNotFoundException("Jobresponses not found");
    }

    @GetMapping("/{uuid}/respondent")
    public PagedResources<JobResponseResource> getJobResponsesByRespondent(@PathVariable UUID uuid, Pageable pageable,
                                                                           PagedResourcesAssembler<JobResponse> pagedResourcesAssembler) {
        Page<JobResponse> page = apiService.findByRespondent(uuid, pageable);
        if (page.hasContent()) {
            return pagedResourcesAssembler.toResource(page, assembler);
        }
        throw new HttpNotFoundException("Jobresponses not found");
    }
}
