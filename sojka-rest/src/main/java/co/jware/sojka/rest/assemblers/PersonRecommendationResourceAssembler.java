package co.jware.sojka.rest.assemblers;

import co.jware.sojka.core.domain.Recommendation;
import co.jware.sojka.rest.controllers.RecommendationController;
import co.jware.sojka.rest.resources.PersonRecommendationResource;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import org.springframework.stereotype.Component;

@Component
public class PersonRecommendationResourceAssembler extends ResourceAssemblerSupport<Recommendation, PersonRecommendationResource> {
    public PersonRecommendationResourceAssembler() {
        super(RecommendationController.class, PersonRecommendationResource.class);
    }

    @Override
    public PersonRecommendationResource toResource(Recommendation entity) {
        return createResourceWithId(entity.uuid(), entity);
    }

    @Override
    protected PersonRecommendationResource instantiateResource(Recommendation entity) {
        return new PersonRecommendationResource(entity);
    }
}
