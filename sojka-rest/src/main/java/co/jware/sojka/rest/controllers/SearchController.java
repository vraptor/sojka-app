package co.jware.sojka.rest.controllers;

import co.jware.sojka.core.domain.JobListing;
import co.jware.sojka.core.domain.wrapped.Keyword;
import co.jware.sojka.core.domain.wrapped.LocationEntry;
import co.jware.sojka.rest.core.SearchFilter;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static java.util.stream.Collectors.toList;

@RestController()
@RequestMapping(path = "/search")
public class SearchController {

//    @Autowired
//    private JobListingFulltextService service;

    @PostMapping()
    public List<JobListing> fuzzySearch(@RequestBody SearchFilter filter) {
        List<LocationEntry> locations = Optional.ofNullable(filter.locations())
                .map(l -> Arrays.stream(l)
                        .map(LocationEntry::of)
                        .collect(toList()))
                .orElse(Collections.emptyList());
        List<Keyword> keywords = Optional.ofNullable(filter.keywords())
                .map(k -> Arrays.stream(k)
                        .map(Keyword::of)
                        .collect(toList()))
                .orElse(Collections.emptyList());
//        List<JobListingDocument> documents = service.fuzzySearch(locations, keywords);
//        if (documents.isEmpty()) {
//            throw new HttpNotFoundException("No job listings found");
//        }
//        return documents.stream().map(service::toJobListing).collect(toList());
        return null;
    }

    @GetMapping()
    public List<JobListing> fuzzySearch() {
        return Collections.emptyList();
    }
}
