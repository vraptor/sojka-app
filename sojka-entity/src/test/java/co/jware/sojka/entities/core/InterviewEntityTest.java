package co.jware.sojka.entities.core;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Sets;
import org.junit.Test;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import static com.fasterxml.jackson.annotation.JsonInclude.Include;
import static org.assertj.core.api.Assertions.assertThat;

public class InterviewEntityTest {

    private final ObjectMapper objectMapper = Jackson2ObjectMapperBuilder.json()
            .serializationInclusion(Include.NON_EMPTY)
            .build();

    @Test
    public void serialize() throws Exception {
        InterviewBo entity = new InterviewBo();
        entity.setNotes(Sets.newHashSet(new EmbeddedNote("Note One"), new EmbeddedNote("Note Two")));
        String asString = objectMapper.writeValueAsString(entity);
        assertThat(asString).isNotEmpty();
    }
}