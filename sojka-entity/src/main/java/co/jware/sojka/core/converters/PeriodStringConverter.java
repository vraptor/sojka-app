package co.jware.sojka.core.converters;

import javax.persistence.AttributeConverter;
import java.time.Period;
import java.util.Optional;

public class PeriodStringConverter implements AttributeConverter<Period, String> {
    @Override
    public String convertToDatabaseColumn(Period period) {
        return Optional.ofNullable(period)
                .map(Period::toString)
                .orElse("");
    }

    @Override
    public Period convertToEntityAttribute(String dbData) {
        return Optional.ofNullable(dbData)
                .map(Period::parse)
                .orElse(Period.ZERO);
    }

}
