package co.jware.sojka.entities.core;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonTypeName;
import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@DiscriminatorValue("HIRER")
@JsonTypeName("HIRER")
public class HirerBo extends PersonBo {

    @OneToMany(fetch = FetchType.LAZY)
    @JsonIgnore
    @JoinTable(name = "hirer_jobs", inverseJoinColumns = @JoinColumn(name = "fk_job_listing"), joinColumns = @JoinColumn(name = "fk_person"))
    private List<JobListingBo> jobs;
    @ManyToOne(fetch = FetchType.LAZY, optional = true)
    @JoinTable(name = "person_company", inverseJoinColumns = @JoinColumn(name = "company_fk"), joinColumns = @JoinColumn(name = "person_fk"))
    private CompanyBo company;
}
