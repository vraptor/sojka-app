package co.jware.sojka.entities.core;

import co.jware.sojka.entities.Persistent;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.MappedSuperclass;
import javax.validation.constraints.NotNull;

@Data
@MappedSuperclass
@DiscriminatorColumn(length = 31)
public class PartyBo extends Persistent {

    @NotNull
    @Column(nullable = false)
    private String email;
}
