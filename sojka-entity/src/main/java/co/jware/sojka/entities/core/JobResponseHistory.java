package co.jware.sojka.entities.core;

import lombok.Data;

import javax.persistence.Embeddable;
import java.time.OffsetDateTime;

@Data
@Embeddable
public class JobResponseHistory {

    private OffsetDateTime createDate = OffsetDateTime.now();
}
