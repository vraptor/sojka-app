package co.jware.sojka.entities.core;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Data
@Embeddable
public class PaymentInfo {

    @Column(name = "account_no")
    private String accountNo;
    @Column(name = "bank_name")
    private String bankName;
}
