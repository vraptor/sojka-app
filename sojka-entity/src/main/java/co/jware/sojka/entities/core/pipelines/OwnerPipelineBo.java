package co.jware.sojka.entities.core.pipelines;

import co.jware.sojka.entities.core.HirerBo;
import com.fasterxml.jackson.annotation.JsonTypeName;
import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@DiscriminatorValue("OWNER")
@JsonTypeName("OWNER_PIPELINE")
public class OwnerPipelineBo extends PipelineBo {

    @ManyToOne
    @JoinColumn(name = "pipeline_owner_fk", foreignKey = @ForeignKey(name = "fk_pipeline_owner"))
    private HirerBo owner;
}
