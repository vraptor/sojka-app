package co.jware.sojka.entities.core;

import co.jware.sojka.core.enums.*;
import co.jware.sojka.entities.Persistent;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeName;
import lombok.Data;
import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.Nationalized;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.OffsetDateTime;
import java.util.List;
import java.util.Set;

import static co.jware.sojka.core.enums.JobListingOrigin.UNKNOWN;
import static co.jware.sojka.core.enums.JobListingStatus.INACTIVE;

@Data
@Entity
@Table(name = "job_listing", indexes = {@Index(name = "ix_contract_type", columnList = "contract_type")})
@JsonTypeName("JOB_LISTING")
public class JobListingBo extends Persistent {

    private int capacity;
    @Column(name = "free_capacity")
    private int freeCapacity;
    @Column(name = "link", length = 512)
    private String link;
    @Column(name = "valid_from")
    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSZZ", shape = JsonFormat.Shape.STRING)
    private OffsetDateTime validFrom;
    @Column(name = "valid_to")
    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSZZ", shape = JsonFormat.Shape.STRING)
    private OffsetDateTime validTo;
    private String title;
    private String name;
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "fk_owner")
    @JsonTypeInfo(use = JsonTypeInfo.Id.NAME)
    @NotNull
    private HirerBo owner;
    @ElementCollection(targetClass = co.jware.sojka.entities.core.EmbeddedTag.class)
    @CollectionTable(name = "job_listing_tags", joinColumns = @JoinColumn(name = "job_listing_fk"))
    @Column(name = "tag")
    private Set<String> tags;
    @Enumerated(EnumType.STRING)
    @Column(length = 16)
    private JobListingOrigin origin = UNKNOWN;
    @Enumerated(EnumType.STRING)
    @Column(length = 16)
    private JobListingStatus status = INACTIVE;
    @Column(name = "source_site", length = 192)
    private String source;
    @Column(name = "contract_type", length = 16)
    @Enumerated(EnumType.STRING)
    private ContractType contractType;
    @Enumerated(EnumType.STRING)
    @Column(name = "work_hours", length = 24)
    private WorkHours workHours;
    @Enumerated(EnumType.STRING)
    @Column(name = "work_venue", length = 16)
    private WorkVenue workVenue;
    @Embedded
    private EmbeddedSalaryRange salaryRange;
    @CollectionTable(name = "job_location", joinColumns = @JoinColumn(name = "uuid"))
    @ElementCollection
    private List<EmbeddedAddress> locations;
    @Column(length = 2000)
    private String description;
    @Lob
    @Nationalized
    private String warranty;
    @Column(length = 1000)
    private String summary;
    @Column(length = 2000)
    private String requirements;
    @Column(length = 2000)
    private String benefits;
    @Column(name = "job_function")
    private String function;
    @Column(name = "education_level", length = 32)
    @Enumerated(EnumType.STRING)
    private EducationLevel educationLevel;
    @Column(name = "external_job_id", length = 128)
    private String externalJobId;
    @Column(name = "created_at")
    private OffsetDateTime createdAt = OffsetDateTime.now();
    @Type(type = "boolean")
    @ColumnDefault("false")
    private Boolean active;
}
