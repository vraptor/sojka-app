package co.jware.sojka.entities.core.metasearch.page;

import co.jware.sojka.core.enums.HirerType;
import lombok.Data;
import org.hibernate.annotations.Type;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import java.util.Locale;

@Data
@Embeddable
public class PageMetaDataEmbedded {

    private String company;
    @Column(name = "hirer_type", length = 32)
    @Enumerated(EnumType.STRING)
    private HirerType hirerType;
    @Column(name = "raw_data", length = 2048)
    private String rawData;
    @Column(length = 16)
    @Type(type = "locale")
    private Locale locale;
}
