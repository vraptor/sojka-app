package co.jware.sojka.entities.core.geo;

import co.jware.sojka.core.enums.GeoLocationStatus;
import co.jware.sojka.entities.Persistent;
import lombok.Data;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
@Table(name = "geo_locations")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class GeoLocationBo extends Persistent {

    @CreationTimestamp()
    private Date created = new Date();

    @Column(length = 128)
//    @Field
    private String name;
    @Column(name = "place_id", length = 1024, unique = true)
//    @Field
    private String placeId;
    @Column(name = "full_result", length = 4096)
    private String fullResult;
    //    @Latitude
    private Double latitude;
    //    @Longitude
    private Double longitude;

    @Column(length = 16)
    @Enumerated(EnumType.STRING)
    private GeoLocationStatus status = GeoLocationStatus.PENDING;

    @Embedded
    private GeoLocationAddressBo address;

}
