package co.jware.sojka.entities;

import org.immutables.builder.Builder;
import org.immutables.value.Value;

import java.util.UUID;

@Value.Style(typeImmutable = "*")
@Value.Immutable
public abstract class AbstractTenantId {

    @Builder.Parameter
    abstract UUID tenantId();
}
