package co.jware.sojka.entities.core;

import lombok.Data;
import org.hibernate.annotations.Type;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

@Data
@Embeddable
public class EmbeddedResult {

    @Type(type = "boolean")
    private Boolean result;
    @Max(value = 100L)
    @Min(value = 0L)
    @Column
    private int percentage;
}
