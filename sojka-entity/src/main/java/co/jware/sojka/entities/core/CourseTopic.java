package co.jware.sojka.entities.core;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.time.Duration;

@Embeddable
public class CourseTopic {
    @Column(length = 192)
    private String name;
    @Column(length = 1000)
    private String description;
    private Duration duration;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Duration getDuration() {
        return duration;
    }

    public void setDuration(Duration duration) {
        this.duration = duration;
    }
}
