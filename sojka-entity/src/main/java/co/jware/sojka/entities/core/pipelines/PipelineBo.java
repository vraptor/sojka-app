package co.jware.sojka.entities.core.pipelines;

import co.jware.sojka.entities.Persistent;
import co.jware.sojka.entities.core.dashboard.DashboardBo;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import lombok.Data;

import javax.persistence.*;
import java.util.List;
import java.util.Set;

@Data
@Entity
@Table(name = "pipelines")
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME)
public class PipelineBo extends Persistent {

    @Column(length = 192)
    private String name;
    @ElementCollection(targetClass = LabelEmbedded.class)
    @CollectionTable(name = "pipeline_labels", joinColumns = @JoinColumn(name = "pipeline_fk"), foreignKey = @ForeignKey(name = "fk_pipeline_labels"))
    private Set<LabelEmbedded> labels;
    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "pipeline_fk", foreignKey = @ForeignKey(name = "fk_pipeline_stage"))
    private List<StageBo> stages;
    @ManyToOne(fetch = FetchType.LAZY)
    private DashboardBo dashboard;
}
