package co.jware.sojka.entities.core.pipelines;

import co.jware.sojka.entities.core.CandidateBo;
import com.fasterxml.jackson.annotation.JsonTypeName;
import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@DiscriminatorValue("PERSON")
@JsonTypeName("PERSON_PIPELINE")
public class PersonPipelineBo extends PipelineBo {

    @ManyToOne
    @JoinColumn(name = "pipeline_person_fk", foreignKey = @ForeignKey(name = "fk_pipeline_person"))
    private CandidateBo person;
}
