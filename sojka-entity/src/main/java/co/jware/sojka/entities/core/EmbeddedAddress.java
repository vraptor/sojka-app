package co.jware.sojka.entities.core;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Data
@Embeddable
public class EmbeddedAddress {

    private String city;
    private String region;
    private String country;
    @Column(name = "postal_code", length = 16)
    private String postalCode;
    @Column(name = "line_one")
    private String lineOne;
    @Column(name = "line_two")
    private String lineTwo;
}
