package co.jware.sojka.entities.core;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeName;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME)
@JsonTypeName("EXTERNAL")
@Table(name = "external")
@DiscriminatorValue("EXTERNAL")
public class ExternalBo extends PartyBo {
}
