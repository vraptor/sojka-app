package co.jware.sojka.entities.core;

import co.jware.sojka.core.enums.InterviewStatus;
import co.jware.sojka.entities.Persistent;
import lombok.Data;

import javax.persistence.*;
import java.time.OffsetDateTime;
import java.util.List;
import java.util.Set;

@Data
@Entity
@Table(name = "interviews")
public class InterviewBo extends Persistent {

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "candidate_fk", foreignKey = @ForeignKey(name = "candidate_fk"))
    private CandidateBo candidate;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "job_listing_fk", foreignKey = @ForeignKey(name = "job_listing_fk"))
    private JobListingBo jobListing;
    @Column(name = "interview_order")
    private Integer order;
    @Column(name = "interview_date")
    private OffsetDateTime interviewDate;
    @Enumerated(EnumType.STRING)
    @Column(columnDefinition = "char(16)")
    private InterviewStatus status;
    @CollectionTable(name = "interview_notes", joinColumns = @JoinColumn(name = "interview_fk", nullable = false))
    @ElementCollection(fetch = FetchType.LAZY)
    private Set<EmbeddedNote> notes;
    @Embedded
    private EmbeddedResult result;
    @OrderColumn(name = "pos")
    @CollectionTable(name = "interview_history", joinColumns = @JoinColumn(name = "interview_fk", nullable = false))
    @ElementCollection(fetch = FetchType.LAZY)
    private List<InterviewHistory> history;
}
