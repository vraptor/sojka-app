package co.jware.sojka.entities.core;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("TEACHER")
public class TeacherBo extends PersonBo {
}
