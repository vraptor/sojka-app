package co.jware.sojka.entities.core;

import co.jware.sojka.core.enums.PartyType;
import co.jware.sojka.core.enums.RecommendationStatus;
import co.jware.sojka.core.enums.SubjectType;
import co.jware.sojka.entities.Persistent;
import co.jware.sojka.entities.core.bots.BotBo;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import lombok.Data;
import org.hibernate.annotations.*;
import org.hibernate.annotations.CascadeType;

import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.time.OffsetDateTime;

@Data
@Entity
@Table(name = "recommendation")
public class RecommendationBo extends Persistent {

    @Any(metaColumn = @Column(name = "subject_type", length = 32, columnDefinition = "varchar(32)", nullable = false), optional = false)
    @AnyMetaDef(idType = "uuid-char", metaType = "string", metaValues = {@MetaValue(targetEntity = JobListingBo.class, value = "JOB_LISTING"), @MetaValue(targetEntity = CompanyBo.class, value = "COMPANY")})
    @JoinColumn(name = "subject_id", columnDefinition = "char(36)")
    @JsonTypeInfo(use = JsonTypeInfo.Id.NAME)
    @JsonSubTypes({
            @JsonSubTypes.Type(name = "JOB_LISTING", value = JobListingBo.class),
            @JsonSubTypes.Type(name = "COMPANY", value = CompanyBo.class)})
    @Cascade(CascadeType.MERGE)
    @NotNull
    private Persistent subject;
    @NotNull
    @JoinColumn(name = "fk_receiver", nullable = false)
    @JsonTypeInfo(use = JsonTypeInfo.Id.NAME, defaultImpl = PersonBo.class)
    @JsonSubTypes({
            @JsonSubTypes.Type(name = "PERSON", value = PersonBo.class),
            @JsonSubTypes.Type(name = "EXTERNAL", value = ExternalBo.class)})
    @Any(metaColumn = @Column(name = "receiver_type", length = 16, columnDefinition = "varchar(16)", nullable = false), optional = false)
    @AnyMetaDef(idType = "uuid-char", metaType = "string", metaValues = {
            @MetaValue(targetEntity = CandidateBo.class, value = "CANDIDATE"),
            @MetaValue(targetEntity = HirerBo.class, value = "HIRER"),
            @MetaValue(targetEntity = TeacherBo.class, value = "TEACHER"),
            @MetaValue(targetEntity = ExternalBo.class, value = "EXTERNAL")})
    private PartyBo receiver;
    @NotNull
    @JsonTypeInfo(use = JsonTypeInfo.Id.NAME, defaultImpl = PersonBo.class)
    @JsonSubTypes({
            @JsonSubTypes.Type(name = "PERSON", value = PersonBo.class),
            @JsonSubTypes.Type(name = "EXTERNAL", value = ExternalBo.class),
            @JsonSubTypes.Type(name = "BOT", value = BotBo.class)
    })
    @JoinColumn(name = "fk_sender", nullable = false)
    @Any(metaColumn = @Column(name = "sender_type", length = 16, columnDefinition = "varchar(16)", nullable = false), optional = false)
    @AnyMetaDef(idType = "uuid-char", metaType = "string", metaValues = {
            @MetaValue(targetEntity = CandidateBo.class, value = "CANDIDATE"),
            @MetaValue(targetEntity = HirerBo.class, value = "HIRER"),
            @MetaValue(targetEntity = TeacherBo.class, value = "TEACHER"),
            @MetaValue(targetEntity = ExternalBo.class, value = "EXTERNAL"),
            @MetaValue(targetEntity = BotBo.class, value = "BOT")})
    private PartyBo sender;
    @Enumerated(EnumType.STRING)
    @Column(length = 32)
    private RecommendationStatus status = RecommendationStatus.PENDING;
    @Formula("subject_type")
    @Enumerated(EnumType.STRING)
    private SubjectType subjectType;
    @Formula("sender_type")
    @Enumerated(EnumType.STRING)
    private PartyType senderPartyType;
    @Formula("receiver_type")
    @Enumerated(EnumType.STRING)
    private PartyType receiverPartyType;
    @CreationTimestamp
    @Column(name = "created_at")
    private OffsetDateTime createdAt;
    @Embedded
    private EmbeddedRecWeight weight;
}
