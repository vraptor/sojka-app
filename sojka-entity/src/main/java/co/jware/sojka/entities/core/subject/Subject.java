package co.jware.sojka.entities.core.subject;


import co.jware.sojka.entities.Persistent;

public interface Subject<T extends Persistent> {

    T getSubject();

    void setSubject(T subject);
}
