package co.jware.sojka.entities.core;

import co.jware.sojka.core.enums.InterviewStatus;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Enumerated;
import java.time.OffsetDateTime;

import static javax.persistence.EnumType.STRING;

@Data
@Embeddable
public class InterviewHistory {

    private String note;
    @Column(name = "create_date")
    private OffsetDateTime createDate;
    @Enumerated(STRING)
    @Column(name = "from_status", length = 32)
    private InterviewStatus fromStatus;
    @Enumerated(STRING)
    @Column(name = "to_status", length = 32)
    private InterviewStatus toStatus;
}
