package co.jware.sojka.entities.core;

import lombok.Data;

import javax.persistence.Embeddable;
import java.math.BigDecimal;

@Data
@Embeddable
public class EmbeddedTag {

    private String name;
    private String description;
    private BigDecimal weight = BigDecimal.ONE;
}
