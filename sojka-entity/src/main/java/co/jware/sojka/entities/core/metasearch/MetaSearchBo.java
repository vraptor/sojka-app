package co.jware.sojka.entities.core.metasearch;

import co.jware.sojka.entities.Persistent;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.Data;
import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.net.URL;
import java.time.OffsetDateTime;
import java.util.HashSet;
import java.util.Set;

@Data
@Entity
@Table(name = "meta_search")
public class MetaSearchBo extends Persistent {

    @ElementCollection
    @CollectionTable(name = "meta_search_keywords", joinColumns = @JoinColumn(name = "meta_search_uuid"))
    @Column(name = "keyword")
    @JsonDeserialize(as = HashSet.class)
//    @Field
    private Set<String> keywords;
    @ElementCollection
    @CollectionTable(name = "meta_search_locations", joinColumns = @JoinColumn(name = "meta_search_uuid"))
    @Column(name = "location")
    @JsonDeserialize(as = HashSet.class)
//    @Field
    private Set<String> locations;
    @ElementCollection
    @CollectionTable(name = "meta_search_urls", indexes = @Index(name = "ux_meta_search_urls", columnList = "url, meta_search_uuid", unique = true), joinColumns = @JoinColumn(name = "meta_search_uuid"))
    @Column(name = "url", length = 1_000)
    @JsonDeserialize(as = HashSet.class)
    private Set<URL> resolved;
    @Column
    @CreationTimestamp
    private OffsetDateTime created;
    @Column
    @UpdateTimestamp
    private OffsetDateTime updated;
    @ColumnDefault("1")
    @Column(name = "search_page")
    private int page;
}
