package co.jware.sojka.entities.core.geo;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.UUID;

@Entity
@Table(name = "osm_response")
@Getter
@Setter
public class OsmResponseBo {

    //    @Id
//    @GenericGenerator(
//            name = "sequenceGenerator",
//            strategy = "enhanced-sequence",
//            parameters = {
//                    @org.hibernate.annotations.Parameter(
//                            name = "optimizer",
//                            value = "pooled"
//                    ),
//                    @org.hibernate.annotations.Parameter(
//                            name = "initial_value",
//                            value = "1"
//                    ),
//                    @org.hibernate.annotations.Parameter(
//                            name = "increment_size",
//                            value = "10"
//                    )
//            }
//    )
//    @GeneratedValue(
//            strategy = GenerationType.SEQUENCE,
//            generator = "sequenceGenerator"
//    )
//    private Long id;
    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    @Type(type = "uuid-binary")
    private UUID uuid;
}
