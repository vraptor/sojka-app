package co.jware.sojka.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.springframework.data.domain.Persistable;

import javax.persistence.*;
import java.util.UUID;

@Data
@MappedSuperclass
@JsonIgnoreProperties({"id", "new"})
public class Persistent implements Persistable<UUID> {
    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid2")
    @Column(name = "uuid", columnDefinition = "char(36)")
    @Type(type = "uuid-char")
    private UUID uuid;
    @Version
    private Long version;

    @Override
    @Transient
    @JsonIgnore
    public UUID getId() {
        return uuid;
    }

    @Override
    @Transient
    @JsonIgnore
    public boolean isNew() {
        return uuid == null;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Persistent)) return false;
        Persistent that = (Persistent) o;
        return uuid.equals(that.uuid);
    }

    @Override
    public int hashCode() {
        return 97;
    }
}
