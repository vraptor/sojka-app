package co.jware.sojka.entities.core;

import co.jware.sojka.core.enums.FavoriteType;
import co.jware.sojka.entities.Persistent;
import org.hibernate.annotations.Any;
import org.hibernate.annotations.AnyMetaDef;
import org.hibernate.annotations.Formula;
import org.hibernate.annotations.MetaValue;

import javax.persistence.*;

@Entity
@Table(name = "favorites")
public class FavoriteBo extends Persistent {
    @OneToOne
    @JoinColumn(name = "owner_fk")
    private PersonBo owner;
    @Any(metaColumn = @Column(name = "favorite_type", columnDefinition = "varchar(64)"), fetch = FetchType.LAZY)
    @AnyMetaDef(idType = "uuid-char", metaType = "string", metaValues = {
            @MetaValue(targetEntity = JobListingBo.class, value = "JOB"),
            @MetaValue(targetEntity = CandidateBo.class, value = "CANDIDATE")})
    @JoinColumn(name = "subject_fk", columnDefinition = "char(36)")
    private Persistent subject;
    @Formula("favorite_type")
    @Enumerated(EnumType.STRING)
    private FavoriteType favoriteType;

    public PersonBo getOwner() {
        return owner;
    }

    public void setOwner(PersonBo owner) {
        this.owner = owner;
    }

    public Persistent getSubject() {
        return subject;
    }

    public void setSubject(Persistent subject) {
        this.subject = subject;
    }

    public FavoriteType getFavoriteType() {
        return favoriteType;
    }

    public void setFavoriteType(FavoriteType favoriteType) {
        this.favoriteType = favoriteType;
    }
}
