package co.jware.sojka.entities.core.pipelines;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Data
@Embeddable
public class LabelEmbedded {

    private String name;
    @Column(length = 24)
    private String color;
}
