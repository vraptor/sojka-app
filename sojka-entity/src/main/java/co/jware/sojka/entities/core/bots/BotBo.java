package co.jware.sojka.entities.core.bots;

import co.jware.sojka.entities.core.PartyBo;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Data
@Entity
@Table(name = "bot")
public class BotBo extends PartyBo {

    @Column(name = "bot_identity", length = 96)
    private String botIdentity;
}
