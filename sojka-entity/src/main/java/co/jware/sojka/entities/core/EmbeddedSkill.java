package co.jware.sojka.entities.core;

import co.jware.sojka.core.enums.SkillLevel;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import java.time.Period;

@Data
@Embeddable
public class EmbeddedSkill {

    private String name;
    @Enumerated(EnumType.STRING)
    @Column(length = 16)
    private SkillLevel level;
    @Column(name = "experience_years")
    private int experienceYears;
    @Column(length = 64)
    private Period experience;
}
