package co.jware.sojka.entities.core.dashboard;

import co.jware.sojka.core.enums.OwnerType;
import co.jware.sojka.core.enums.Visibility;
import co.jware.sojka.entities.Persistent;
import co.jware.sojka.entities.core.CandidateBo;
import co.jware.sojka.entities.core.HirerBo;
import co.jware.sojka.entities.core.PersonBo;
import co.jware.sojka.entities.core.TeacherBo;
import co.jware.sojka.entities.core.pipelines.LabelEmbedded;
import co.jware.sojka.entities.core.pipelines.PipelineBo;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import lombok.Data;
import org.hibernate.annotations.*;

import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.Table;
import java.util.List;
import java.util.Set;

import static org.hibernate.annotations.CascadeType.DETACH;
import static org.hibernate.annotations.CascadeType.MERGE;

@Data
@Entity
@Table(name = "dashboard")
public class DashboardBo extends Persistent {

    @Cascade({MERGE, DETACH})
    @Any(metaColumn = @Column(name = "owner_type", columnDefinition = "varchar(24)"), fetch = FetchType.LAZY)
    @AnyMetaDef(idType = "uuid-char", metaType = "string", metaValues = {
            @MetaValue(targetEntity = CandidateBo.class, value = "CANDIDATE"),
            @MetaValue(targetEntity = HirerBo.class, value = "HIRER"),
            @MetaValue(targetEntity = TeacherBo.class, value = "TEACHER")})
    @JoinColumn(name = "owner_fk", columnDefinition = "char(36)")
    @JsonTypeInfo(use = JsonTypeInfo.Id.NAME)
    @JsonSubTypes({
            @JsonSubTypes.Type(value = CandidateBo.class, name = "CANDIDATE"),
            @JsonSubTypes.Type(value = HirerBo.class, name = "HIRER"),
            @JsonSubTypes.Type(value = TeacherBo.class, name = "TEACHER")})
    private PersonBo owner;
    @Formula("owner_type")
    @Enumerated(EnumType.STRING)
    private OwnerType ownerType;
    @Column(length = 192)
    private String name;
    @Column(length = 512)
    private String description;
    @OneToMany
    @JoinTable(name = "dashboard_pipeline", joinColumns = @JoinColumn(name = "dashboard_fk", foreignKey = @ForeignKey(name = "fk_dashboard_pipeline")), inverseJoinColumns = @JoinColumn(name = "pipeline_fk", foreignKey = @ForeignKey(name = "fk_pipeline_dashboard")))
    private List<PipelineBo> pipelines;
    @ElementCollection
    @CollectionTable(name = "dashboard_labels", joinColumns = @JoinColumn(name = "dashboard_uuid"))
    private Set<LabelEmbedded> labels;
    @Column(length = 16)
    @Enumerated(EnumType.STRING)
    private Visibility visibility;
    @Type(type = "boolean")
    private boolean starred;
}
