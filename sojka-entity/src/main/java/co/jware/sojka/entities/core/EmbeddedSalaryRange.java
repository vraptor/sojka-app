package co.jware.sojka.entities.core;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.math.BigDecimal;
import java.util.Currency;

@Data
@Embeddable
public class EmbeddedSalaryRange {

    @Column(name = "salary_from")
    private BigDecimal salaryFrom;
    @Column(name = "salary_to")
    private BigDecimal salaryTo;
    private Currency currency;
}
