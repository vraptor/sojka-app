package co.jware.sojka.entities.core.geo;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
@Getter
@Setter
public class GeoLocationAddressBo {

    private String county;
    private String state;
    private String city;
    private String town;
    private String village;
    private String suburb;
    private String hamlet;
    private String country;
    @Column(name = "country_code",length = 16)
    private String countryCode;
}
