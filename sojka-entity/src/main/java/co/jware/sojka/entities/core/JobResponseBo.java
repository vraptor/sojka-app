package co.jware.sojka.entities.core;

import co.jware.sojka.core.enums.JobResponseStatus;
import co.jware.sojka.core.enums.RespondentType;
import co.jware.sojka.entities.Persistent;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import lombok.Data;
import org.hibernate.annotations.*;

import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.util.List;

import static org.hibernate.annotations.CascadeType.DETACH;
import static org.hibernate.annotations.CascadeType.MERGE;

@Data
@Entity
@Table(name = "job_response", uniqueConstraints = @UniqueConstraint(name = "ux_job_response", columnNames = {"fk_person", "fk_job_listing"}))
public class JobResponseBo extends Persistent {

    @Cascade({MERGE, DETACH})
    @Any(metaColumn = @Column(name = "respondent_type", columnDefinition = "varchar(64)"), fetch = FetchType.LAZY)
    @AnyMetaDef(idType = "uuid-char", metaType = "string", metaValues = {
            @MetaValue(targetEntity = CandidateBo.class, value = "CANDIDATE"),
            @MetaValue(targetEntity = ExternalBo.class, value = "EXTERNAL")})
    @JoinColumn(name = "fk_person", columnDefinition = "char(36)")
    @JsonTypeInfo(use = JsonTypeInfo.Id.NAME)
    @JsonSubTypes({
            @JsonSubTypes.Type(value = CandidateBo.class, name = "CANDIDATE"),
            @JsonSubTypes.Type(value = ExternalBo.class, name = "EXTERNAL")})
    private PersonBo respondent;
    @ManyToOne
    @JoinColumn(name = "fk_job_listing", nullable = false)
    @NotNull
    private JobListingBo jobListing;
    @Enumerated(EnumType.STRING)
    @Column(length = 32)
    private JobResponseStatus status;
    @OneToMany(fetch = FetchType.LAZY)
    @JoinTable(joinColumns = @JoinColumn(name = "response_fk"), inverseJoinColumns = @JoinColumn(name = "interview_fk"))
    @OrderColumn(name = "pos")
    private List<InterviewBo> interviews;
    @OneToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "job_response_tests", joinColumns = @JoinColumn(name = "response_fk"), inverseJoinColumns = @JoinColumn(name = "test_fk"))
    @OrderColumn(name = "pos")
    private List<TestBo> tests;
    @Formula("respondent_type")
    @Enumerated(EnumType.STRING)
    private RespondentType respondentType;
}
