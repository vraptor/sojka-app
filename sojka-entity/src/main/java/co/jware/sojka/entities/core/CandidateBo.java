package co.jware.sojka.entities.core;

import com.fasterxml.jackson.annotation.JsonTypeName;
import lombok.Data;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Data
@Entity
@DiscriminatorValue("CANDIDATE")
@JsonTypeName("CANDIDATE")
public class CandidateBo extends PersonBo {
}
