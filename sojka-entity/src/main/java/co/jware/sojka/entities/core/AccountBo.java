package co.jware.sojka.entities.core;

import co.jware.sojka.core.enums.AccountOwnerType;
import co.jware.sojka.entities.Persistent;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import lombok.Data;
import org.hibernate.annotations.Any;
import org.hibernate.annotations.AnyMetaDef;
import org.hibernate.annotations.Formula;
import org.hibernate.annotations.MetaValue;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Data
@Entity
@Table(name = "account")
public class AccountBo extends Persistent {

    @Any(metaColumn = @Column(name = "owner_type", columnDefinition = "varchar(12)"), fetch = FetchType.EAGER)
    @AnyMetaDef(idType = "uuid-char", metaType = "string", metaValues = {
            @MetaValue(targetEntity = CandidateBo.class, value = "CANDIDATE"),
            @MetaValue(targetEntity = HirerBo.class, value = "HIRER"),
            @MetaValue(targetEntity = TeacherBo.class, value = "TEACHER")})
    @JoinColumn(name = "owner_id", columnDefinition = "char(36)")
    @NotNull
    @JsonTypeInfo(use = JsonTypeInfo.Id.NAME)
    private PersonBo owner;
    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "user_id", foreignKey = @ForeignKey(name = "user_fk"))
    private UserBo user;
    @Embedded
    private PaymentInfo billingInfo;
    @Formula("owner_type")
    @Enumerated(EnumType.STRING)
    private AccountOwnerType ownerType;
}
