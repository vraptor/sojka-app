package co.jware.sojka.entities.core;

import co.jware.sojka.entities.Persistent;
import lombok.Data;

import javax.persistence.*;
import java.time.OffsetDateTime;
import java.util.List;
import java.util.UUID;

@Data
@Entity
@Table(name = "course_class")
public class CourseClassBo extends Persistent {

    @ManyToOne()
    @JoinColumn(name = "owner_fk")
    private TeacherBo owner;
    @ManyToOne()
    @JoinColumn(name = "course_fk")
    private CourseBo course;
    @Column(name = "start_date")
    private OffsetDateTime startDate;
    @Column(name = "end_date")
    private OffsetDateTime endDate;
    @Column(name = "course_capacity")
    private int capacity;
    @ManyToMany(targetEntity = co.jware.sojka.entities.core.PersonBo.class)
    @JoinTable(name = "course_students", joinColumns = @JoinColumn(name = "course_fk"),
            inverseJoinColumns = @JoinColumn(name = "student_fk"), foreignKey = @ForeignKey(name = "course_fk"))
    @OrderColumn(name = "pos")
    private List<UUID> students;
}
