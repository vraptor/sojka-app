package co.jware.sojka.entities.core;

import co.jware.sojka.core.enums.PersonType;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import lombok.Data;
import org.hibernate.annotations.Formula;
import org.hibernate.annotations.Type;

import javax.persistence.*;

@Data
@Entity
@Table(name = "person")
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME)
@JsonSubTypes({
        @JsonSubTypes.Type(name = "CANDIDATE", value = CandidateBo.class),
        @JsonSubTypes.Type(name = "HIRER", value = HirerBo.class),
        @JsonSubTypes.Type(name = "TEACHER", value = TeacherBo.class)})
public class PersonBo extends PartyBo {

    @Column(name = "first_name")
    private String firstName;
    @Column(name = "surname")
    private String surname;
    @Column(name = "last_name")
    private String lastName;
    @Type(type = "true_false")
    @Column(length = 1)
    private Boolean registered = false;
    @Enumerated(EnumType.STRING)
    @Formula("dtype")
    private PersonType personType;
}
