package co.jware.sojka.entities.core;

import co.jware.sojka.core.enums.EvaluationStatus;
import co.jware.sojka.entities.Persistent;
import lombok.Data;

import javax.persistence.*;
import java.time.OffsetDateTime;

@Data
@Entity
@Table(name = "tests")
public class TestBo extends Persistent {

    @Column(name = "test_date")
    private OffsetDateTime testDate;
    @Enumerated(EnumType.STRING)
    @Column(length = 16)
    private EvaluationStatus status;
    @Embedded
    private EmbeddedResult result;
}
