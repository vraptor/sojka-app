package co.jware.sojka.entities.core.metasearch.page;

import co.jware.sojka.entities.Persistent;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.time.OffsetDateTime;

@Data
@Entity
@Table(name = "page_detail")
@JsonIgnoreProperties({"id", "new"})
public class PageDetailBo extends Persistent {
    //    @Id
//    @GeneratedValue(generator = "assigned-uuid")
//    @GenericGenerator(name = "assigned-uuid", strategy = "assigned")
//    @Column(name = "uuid", columnDefinition = "char(36)")
//    @Type(type = "uuid-char")
//    private UUID uuid;
//    @Version
//    private Long version;
    @Column(length = 1024)
//    @Field
    private String title;
    @Column(name = "url", length = 1024, unique = true, nullable = false)
    private String url;
    @Column(name = "rich_text", length = 16384)
    private String richText;
    @Column(name = "stemmed_text", length = 16384)
    private String stemmedText;
    @Embedded
    private PageMetaDataEmbedded pageMetaData;
    @Column
    @CreationTimestamp
//    @Field(type = FieldType.Date)
    private OffsetDateTime created;
    @Column
    @UpdateTimestamp
    private OffsetDateTime updated;

}
