package co.jware.sojka.entities.core.metasearch;

import lombok.Data;

import java.io.Serializable;
import java.util.UUID;

@Data
public class MetaSearchLinkId implements Serializable {
    private UUID metaSearchUuid;
    private UUID metaLinkUuid;
}
