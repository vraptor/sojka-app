package co.jware.sojka.entities.core.pipelines;

import co.jware.sojka.entities.core.JobListingBo;
import com.fasterxml.jackson.annotation.JsonTypeName;
import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@DiscriminatorValue("JOB")
@JsonTypeName("JOB_PIPELINE")
public class JobPipelineBo extends PipelineBo {

    @ManyToOne
    @JoinColumn(name = "pipeline_job_fk", foreignKey = @ForeignKey(name = "fk_pipeline_job"))
    private JobListingBo jobListing;
}
