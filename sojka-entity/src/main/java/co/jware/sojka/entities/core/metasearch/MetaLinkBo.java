package co.jware.sojka.entities.core.metasearch;

import co.jware.sojka.entities.Persistent;
import co.jware.sojka.entities.core.geo.GeoLocationBo;
import lombok.Data;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.time.OffsetDateTime;
import java.util.LinkedHashSet;
import java.util.Set;

@Data
@Entity
@Table(name = "meta_link")
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class MetaLinkBo extends Persistent {

    @Column(name = "base_uri")
    private String baseUri;
    @Column(length = 1_000)
    private String href;
    private String title;
    private String company;
    private String address;
    @Column(length = 1_000)
    private String summary;
    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name = "meta_link_source", joinColumns = @JoinColumn(name = "meta_link_uuid"))
    @Column(name = "source", length = 96)
    private Set<String> sources = new LinkedHashSet<String>();
    @ElementCollection
    @CollectionTable(name = "meta_link_keywords", joinColumns = @JoinColumn(name = "meta_link_uuid"))
    @Column(name = "keyword", length = 128)
//    @Field
    private Set<String> keywords = new LinkedHashSet<String>();
    @ElementCollection
    @CollectionTable(name = "meta_link_locations", joinColumns = @JoinColumn(name = "meta_link_uuid"))
    @Column(name = "location", length = 128)
    private Set<String> locations = new LinkedHashSet<String>();
    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "meta_link_geo_locations",
            joinColumns = @JoinColumn(name = "meta_link_uuid"),
            inverseJoinColumns = @JoinColumn(name = "geo_location_uuid"))
    private Set<GeoLocationBo> geoLocations = new LinkedHashSet<GeoLocationBo>();

    @ColumnDefault("200")
    @Column(name = "http_status")
    private int httpStatus;
    @Column
    private OffsetDateTime created = OffsetDateTime.now();

    @Column
    @UpdateTimestamp
    private OffsetDateTime updated;
}
