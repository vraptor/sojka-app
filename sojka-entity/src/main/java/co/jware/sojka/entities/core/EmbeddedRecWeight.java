package co.jware.sojka.entities.core;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.IOException;

@Embeddable
@JsonDeserialize(using = EmbeddedRecWeight.Deserializer.class)
public class EmbeddedRecWeight {
    @Column(name = "weight")
    private Double value;

    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }

    public static class Deserializer extends JsonDeserializer<EmbeddedRecWeight> {
        @Override
        public EmbeddedRecWeight deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException, JsonProcessingException {
            JsonNode node = jp.getCodec().readTree(jp);
            double value = node.asDouble();
            EmbeddedRecWeight recWeight = new EmbeddedRecWeight();
            recWeight.setValue(value);
            return recWeight;
        }

    }
}
