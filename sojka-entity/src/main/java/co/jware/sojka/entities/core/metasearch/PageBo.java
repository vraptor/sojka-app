package co.jware.sojka.entities.core.metasearch;

import co.jware.sojka.entities.Persistent;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.Table;

@Data
@Entity
@Table(name = "page")
public class PageBo extends Persistent {

    @Column(length = 400, unique = true, nullable = false)
    private String url;
    @Lob
    @Column(length = 20971520)
    private String content;
}
