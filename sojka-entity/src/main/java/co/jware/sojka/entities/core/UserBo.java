package co.jware.sojka.entities.core;

import co.jware.sojka.core.enums.Role;
import co.jware.sojka.entities.Persistent;
import lombok.Data;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.Set;

@Data
@Entity
@Table(name = "users")
public class UserBo extends Persistent {

    @NotNull
    @Email
    @Column(name = "username", unique = true, nullable = false)
    private String username;
    @NotNull
    @Column(name = "passwd", nullable = false)
    private String password;
    @Column(name = "enabled", nullable = true)
    @Type(type = "yes_no")
    private Boolean enabled;
    @Column(name = "last_passwd_reset_date")
    private Date lastPasswordResetDate = new Date();
    @CollectionTable(name = "user_roles", joinColumns = @JoinColumn(name = "user_fk", foreignKey = @ForeignKey(name = "user_fk")))
    @ElementCollection(fetch = FetchType.EAGER)
    @Column(name = "user_role", columnDefinition = "char(16)")
    @Enumerated(EnumType.STRING)
    private Set<Role> roles;
}
