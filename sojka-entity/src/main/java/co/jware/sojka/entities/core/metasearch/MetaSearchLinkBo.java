package co.jware.sojka.entities.core.metasearch;

import co.jware.sojka.entities.Persistent;
import lombok.Data;
import org.hibernate.annotations.ColumnDefault;

import javax.persistence.*;

@Data
@Entity
@Table(name = "meta_search_link")
public class MetaSearchLinkBo extends Persistent {

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "meta_link_uuid", nullable = false)
    private MetaLinkBo metaLink;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "meta_search_uuid", nullable = false)
    private MetaSearchBo metaSearch;
    @ColumnDefault("0")
    private Long position = 0L;
    private Boolean relevant;
}
