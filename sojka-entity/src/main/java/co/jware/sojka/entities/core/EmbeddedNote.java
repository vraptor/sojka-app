package co.jware.sojka.entities.core;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.persistence.Embeddable;

@Data
@Embeddable
public class EmbeddedNote {

    @JsonProperty("value")
    private String text;

    EmbeddedNote(String text) {
        this.text = text;
    }
}
