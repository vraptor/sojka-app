package co.jware.sojka.entities.core;

import co.jware.sojka.entities.Persistent;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.List;

@Data
@Entity
@Table(name = "company")
public class CompanyBo extends Persistent {

    private String name;
    private String summary;
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "company", targetEntity = HirerBo.class)
    @JsonIgnore
    private List<PersonBo> personnel;
}
