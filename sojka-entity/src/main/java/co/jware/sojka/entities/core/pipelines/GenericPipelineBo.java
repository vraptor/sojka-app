package co.jware.sojka.entities.core.pipelines;

import com.fasterxml.jackson.annotation.JsonTypeName;

import javax.persistence.DiscriminatorValue;

@DiscriminatorValue("GENERIC")
@JsonTypeName("GENERIC_PIPELINE")
public class GenericPipelineBo extends PipelineBo {
}
