package co.jware.sojka.entities.core.pipelines;

import co.jware.sojka.core.enums.CardType;
import co.jware.sojka.core.enums.Visibility;
import co.jware.sojka.entities.Persistent;
import co.jware.sojka.entities.core.*;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import lombok.Data;
import org.hibernate.annotations.*;
import org.hibernate.annotations.CascadeType;

import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.Table;
import java.util.Set;

@Data
@Entity
@Table(name = "pipeline_stage_card")
public class CardBo extends Persistent {

    private String name;
    @Column(length = 512)
    private String description;
    @Cascade(CascadeType.ALL)
    @Any(metaColumn = @Column(name = "card_type", columnDefinition = "varchar(64)"), fetch = FetchType.LAZY)
    @AnyMetaDef(idType = "uuid-char", metaType = "string", metaValues = {@MetaValue(targetEntity = InterviewBo.class, value = "INTERVIEW"),
            @MetaValue(targetEntity = TestBo.class, value = "TEST"),
            @MetaValue(targetEntity = JobListingBo.class, value = "JOB_LISTING"),
            @MetaValue(targetEntity = CandidateBo.class, value = "CANDIDATE"),
            @MetaValue(targetEntity = JobResponseBo.class, value = "JOB_RESPONSE")})
    @JoinColumn(name = "content_fk", columnDefinition = "char(36)")
    @JsonTypeInfo(use = JsonTypeInfo.Id.NAME)
    @JsonSubTypes({@JsonSubTypes.Type(value = InterviewBo.class, name = "INTERVIEW"),
            @JsonSubTypes.Type(value = TestBo.class, name = "TEST"),
            @JsonSubTypes.Type(value = JobListingBo.class, name = "JOB_LISTING"),
            @JsonSubTypes.Type(value = CandidateBo.class, name = "CANDIDATE"),
            @JsonSubTypes.Type(value = JobResponseBo.class, name = "JOB_RESPONSE")})
    private Persistent content;
    @ElementCollection
    @CollectionTable(name = "card_labels", joinColumns = @JoinColumn(name = "card_uuid"))
    private Set<LabelEmbedded> labels;
    @Column(length = 16)
    @Enumerated(EnumType.STRING)
    private Visibility visibility;
    @Formula("position")
    private int position;
    @Formula("card_type")
    @Enumerated(EnumType.STRING)
    private CardType cardType;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "stage_fk", foreignKey = @ForeignKey(name = "fk_stage_labels"))
    private StageBo stage;
}
