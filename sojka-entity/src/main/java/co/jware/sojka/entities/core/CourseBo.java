package co.jware.sojka.entities.core;

import co.jware.sojka.entities.Persistent;
import lombok.Data;

import javax.persistence.*;
import java.time.OffsetDateTime;
import java.util.List;

@Data
@Entity
@Table(name = "course")
public class CourseBo extends Persistent {

    @ManyToOne(optional = false)
    @JoinColumn(name = "owner_fk")
    private TeacherBo owner;
    private String title;
    private String description;
    @Column(name = "course_date")
    private OffsetDateTime courseDate;
    private int seats;
    @ElementCollection
    @CollectionTable(name = "course_topic", joinColumns = @JoinColumn(name = "course_id"), foreignKey = @ForeignKey(name = "course_fk"))
    @OrderColumn(name = "pos")
    private List<CourseTopic> courseTopics;
}
