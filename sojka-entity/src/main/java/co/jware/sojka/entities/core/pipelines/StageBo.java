package co.jware.sojka.entities.core.pipelines;

import co.jware.sojka.core.enums.StageType;
import co.jware.sojka.entities.Persistent;
import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@Table(name = "pipeline_stage")
public class StageBo extends Persistent {

    private String name;
    @Column(length = 512)
    private String description;
    @Column(name = "stage_type", updatable = false, insertable = false, columnDefinition = "varchar(64)")
    @Enumerated(EnumType.STRING)
    private StageType stageType = StageType.GENERIC;
    @Column(name = "stage_status", length = 32)
    private String stageStatus;
    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "stage_fk", foreignKey = @ForeignKey(name = "fk_stage"))
    @OrderColumn(name = "position")
    private List<CardBo> cards;
    private int position = 0;
    @ManyToOne(fetch = FetchType.LAZY)
    private PipelineBo pipeline;
}
