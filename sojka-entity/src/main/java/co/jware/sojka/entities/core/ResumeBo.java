package co.jware.sojka.entities.core;

import co.jware.sojka.core.enums.ResumeStatus;
import co.jware.sojka.entities.Persistent;
import com.fasterxml.jackson.annotation.JsonTypeName;
import groovy.transform.IndexedProperty;
import lombok.Data;
import org.hibernate.annotations.Any;
import org.hibernate.annotations.AnyMetaDef;
import org.hibernate.annotations.MetaValue;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Set;

@Data
@Entity
@Table(name = "resume")
@JsonTypeName("RESUME")
public class ResumeBo extends Persistent {


    @ManyToOne(optional = true, fetch = FetchType.LAZY)
    @JoinColumn(name = "fk_person", foreignKey = @ForeignKey(name = "fk_person"))
    private CandidateBo owner;
    @Any(metaColumn = @Column(name = "submitter_type", columnDefinition = "varchar(12)"))
    @AnyMetaDef(idType = "uuid-char", metaType = "string", metaValues = {
            @MetaValue(targetEntity = CandidateBo.class, value = "CANDIDATE"),
            @MetaValue(targetEntity = HirerBo.class, value = "HIRER")})
    @JoinColumn(name = "submitter_fk", columnDefinition = "CHAR(36)")
    @NotNull
    private PersonBo submitter;
    @ElementCollection()
    @CollectionTable(name = "resume_tags", joinColumns = @JoinColumn(name = "resume_fk"))
    @Column(name = "tag")
    private Set<EmbeddedTag> tags;
    @CollectionTable(name = "resume_skills", joinColumns = @JoinColumn(name = "resume_fk", foreignKey = @ForeignKey(name = "resume_fk")))
    @ElementCollection()
    private Set<EmbeddedSkill> skills;
    @Enumerated(EnumType.STRING)
    @Column(length = 16)
    private ResumeStatus status;
    @CollectionTable(name = "resume_employments", joinColumns = @JoinColumn(name = "resume_fk"))
    @ElementCollection()
    @IndexedProperty
    @OrderColumn(name = "pos", nullable = false)
    private List<EmbeddedEmployment> employments;
    @Lob
    private String description;
}
