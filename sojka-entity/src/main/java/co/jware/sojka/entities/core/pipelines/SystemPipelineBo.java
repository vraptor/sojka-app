package co.jware.sojka.entities.core.pipelines;

import com.fasterxml.jackson.annotation.JsonTypeName;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@JsonTypeName("SYSTEM_PIPELINE")
@DiscriminatorValue(value = "SYSTEM")
public class SystemPipelineBo extends PipelineBo {
}
