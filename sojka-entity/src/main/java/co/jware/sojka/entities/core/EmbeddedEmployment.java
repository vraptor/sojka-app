package co.jware.sojka.entities.core;

import lombok.Data;

import javax.persistence.*;
import java.time.LocalDate;

@Data
@Embeddable
public class EmbeddedEmployment {

    private String position;
    @Column(name = "from_date")
    private LocalDate fromDate;
    @Column(name = "to_date")
    private LocalDate toDate;
    @Column(columnDefinition = "clob")
    @Lob
    @Basic(fetch = FetchType.LAZY)
    private String description;
}
