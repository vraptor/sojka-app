package co.jware.sojka.rest.exceptions;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.immutables.builder.Builder;
import org.immutables.value.Value;

@Value.Immutable
@JsonDeserialize(builder = ErrorResponse.Builder.class)
public abstract class AbstractErrorResponse {
    @Builder.Parameter
    public abstract int errorCode();

    @Builder.Parameter
    public abstract String message();
}
