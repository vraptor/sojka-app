package co.jware.sojka.rest.exceptions;


public class HttpPageGoneException extends RuntimeException {
    public HttpPageGoneException(String message) {
        super(message);
    }
}
