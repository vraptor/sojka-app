package co.jware.sojka.utils;


import java.security.SecureRandom;

public class NanoId {
    private final static String alphabet = "_~0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
    private static int idLen = 22;

    public static String generate() {return generate(idLen);}

    public static String generate(int len) {
        SecureRandom random = new SecureRandom();
        byte[] randomBytes = new byte[len];
        random.nextBytes(randomBytes);
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < randomBytes.length; i++) {
            builder.append(alphabet.charAt(randomBytes[i] & 63));
        }
        return builder.toString();
    }
}
