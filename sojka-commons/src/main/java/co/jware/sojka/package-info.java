@Value.Style(typeImmutable = "*", typeModifiable = "*VO",
        defaultAsDefault = true, jdkOnly = true,
        deepImmutablesDetection = true,
        additionalJsonAnnotations = JsonUnwrapped.class)
package co.jware.sojka;

import com.fasterxml.jackson.annotation.JsonUnwrapped;
import org.immutables.value.Value;