package co.jware.sojka.conversion;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.immutables.value.Value;

@Value.Immutable
@JsonSerialize
@JsonDeserialize
public abstract class AbstractParameterKey<T> {

    public abstract T key();

    public abstract Class<?> keyClass();
}
