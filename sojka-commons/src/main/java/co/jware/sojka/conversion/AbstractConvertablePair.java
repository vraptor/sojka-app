package co.jware.sojka.conversion;

import org.immutables.value.Value;

@Value.Immutable()
@Value.Style(typeImmutable = "*", allParameters = true)
public abstract class AbstractConvertablePair {

    public abstract Class<?> source();

    public abstract Class<?> target();

}
