package co.jware.sojka.core.enums;


public enum InvitationState {
    PENDING_SENT,
    SENT,
    PENDING_RECEIVE,
    RECEIVED,
    CANCELLED,
    ACCEPTED,
    REJECTED
}
