package co.jware.sojka.core.enums;


public enum FavoriteType implements KeyedEnum {
    CANDIDATE("favorite.type.candidate"),
    JOB("favorite.type.job");

    private final String key;

    FavoriteType(String key) {
        this.key = key;
    }

    @Override
    public String key() {
        return key;
    }
}
