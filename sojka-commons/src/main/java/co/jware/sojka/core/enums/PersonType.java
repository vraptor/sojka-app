package co.jware.sojka.core.enums;

public enum PersonType implements KeyedEnum {

    CANDIDATE("person.type.candidate"),
    HIRER("person.type.hirer"),
    TEACHER("person.type.teacher"),
    EXTERNAL("person.type.external");

    private final String key;

    PersonType(String key) {
        this.key = key;
    }

    @Override
    public String key() {
        return this.key;
    }
}
