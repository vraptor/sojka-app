package co.jware.sojka.core.enums;


public enum JobListingOrigin implements KeyedEnum {

    UNKNOWN("job_listing.origin.unknown"),
    EXTERNAL("job_listing.origin.external"),
    INTERNAL("job_listing.origin.internal");

    private final String key;

    JobListingOrigin(String key) {
        this.key = key;
    }

    @Override
    public String key() {
        return key;
    }
}
