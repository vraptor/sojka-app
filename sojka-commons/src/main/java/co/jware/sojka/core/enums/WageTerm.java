package co.jware.sojka.core.enums;


public enum WageTerm {
    MONTHLY, WEEKLY, HOURLY;
}
