package co.jware.sojka.core.enums;

public enum CardType {
    INTERVIEW,
    TEST,
    JOB_LISTING,
    CANDIDATE,
    JOB_RESPONSE
}
