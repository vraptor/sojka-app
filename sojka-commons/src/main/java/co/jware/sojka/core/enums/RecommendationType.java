package co.jware.sojka.core.enums;


public enum RecommendationType implements KeyedEnum {

    UNKNOWN("recommendation.type.unknown"),
    PERSON("recommendation.type.person"),
    BOT("recommendation.type.bot"),
    EXTERNAL("recommendation.type.external");
    private final String key;

    RecommendationType(String key) {
        this.key = key;
    }

    @Override
    public String key() {
        return key;
    }
}
