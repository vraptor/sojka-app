package co.jware.sojka.core.enums;


public enum Role implements KeyedEnum {
    ANONYMOUS("role.anonymous"),
    USER("role.user"),
    ADMIN("role.admin"),
    BOT("role.bot"),
    HIRER("role.hirer"),
    CANDIDATE("role.candidate"),
    TEACHER("role.teacher");

    private final String key;

    Role(String key) {
        this.key = key;
    }

    @Override
    public String key() {
        return this.key;
    }
}
