package co.jware.sojka.core.enums;


public interface KeyedEnum {

    String key();
}
