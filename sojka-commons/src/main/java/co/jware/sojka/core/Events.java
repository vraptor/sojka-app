package co.jware.sojka.core;


public interface Events {

    String META_SEARCH_SUBMIT = "meta_search.submit";
    String META_LINK_SUBMIT = "meta_link.submit";
    String META_LINK_ACCESSED = "meta_link.accessed";
}
