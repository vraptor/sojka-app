package co.jware.sojka.core.enums;


public enum EvaluationStatus {
    PENDING, PASSED, FAILED, CANCELLED,
    INVITATION_SENT, INVITATION_ACCEPTED, INVITATION_REJECTED
}
