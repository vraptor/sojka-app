package co.jware.sojka.core.enums;


public enum StageType {
    GENERIC,
    SOURCED,
    APPLIED,
    PHONE_SCREEN,
    INTERVIEW,
    TEST,
    OFFER,
    HIRED
}
