package co.jware.sojka.core.enums;


public enum JobResponseStatus implements KeyedEnum {
    PENDING("job_response.status.pending"),
    RESPONSE_SENT("job_response.status.response_sent"),
    INVITATION_RECEIVED("job_response.status.invitation_received"),
    INVITATION_ACCEPTED("job_response.status.invitation_accepted"),
    INVITATION_REJECTED("job_response.status.invitation_rejected"),
    OFFER_RECEIVED("job_response.status.offer_sent"),
    OFFER_ACCEPTED("job_response.status.offer_accepted"),
    OFFER_REJECTED("job_response.status.offer_rejected");

    private final String key;

    JobResponseStatus(String key) {
        this.key = key;
    }

    public JobResponseStatus next() {
        int ordinal = this.ordinal();
        JobResponseStatus[] values = JobResponseStatus.values();
        if ((ordinal + 1) == values.length) {
            return this;
        } else {
            return values[(ordinal + 1) % values.length];
        }
    }

    public String key() {
        return key;
    }
}
