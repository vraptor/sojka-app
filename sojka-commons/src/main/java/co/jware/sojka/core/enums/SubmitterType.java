package co.jware.sojka.core.enums;


public enum SubmitterType {
    CANDIDATE, HIRER
}

