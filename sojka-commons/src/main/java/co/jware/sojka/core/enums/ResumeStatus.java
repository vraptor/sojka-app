package co.jware.sojka.core.enums;

public enum ResumeStatus implements KeyedEnum {
    ACTIVE("resume.status.active"),
    HIDDEN("resume.status.hidden"),
    INACTIVE("resume.status.inactive");

    private final String key;

    ResumeStatus(String key) {
        this.key = key;
    }

    @Override
    public String key() {
        return this.key;
    }
}
