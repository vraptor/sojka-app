package co.jware.sojka.core.enums;

public enum SkillLevel implements KeyedEnum {

    BEGINNER("skill.level.beginner"),
    INTERMEDIATE("skill.level.intermediate"),
    EXPERT("skill.level.expert");

    private final String key;

    SkillLevel(String key) {
        this.key = key;
    }

    @Override
    public String key() {
        return this.key;
    }
}
