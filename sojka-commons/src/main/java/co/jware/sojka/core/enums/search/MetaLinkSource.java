package co.jware.sojka.core.enums.search;


public enum MetaLinkSource {
    EMPLOYER,
    AGENCY,
    RECRUITER
}
