package co.jware.sojka.core.enums;


public enum EducationLevel implements KeyedEnum {
    ELEMENTARY("education.level."),
    COLLEGE("education.level."),
    UNIVERSITY_DEGREE("education.level.");

    private final String key;

    EducationLevel(String key) {
        this.key = key;
    }

    @Override
    public String key() {
        return key;
    }
}
