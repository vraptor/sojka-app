package co.jware.sojka.core.enums;


public enum HirerType implements KeyedEnum {

    UNKNOWN("hirer.type.unknown"),
    EMPLOYER("hirer.type.employer"),
    AGENCY("hirer.type.agency"),
    RECRUITER("hirer.type.recruiter");

    private final String key;

    HirerType(String key) {
        this.key = key;
    }

    public String key() {
        return this.key;
    }
}
