package co.jware.sojka.core.enums;

public enum GeoLocationStatus {
    UNKNOWN,
    PENDING,
    READY
}
