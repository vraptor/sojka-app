package co.jware.sojka.core.enums;


public enum Visibility {
    PRIVATE,
    TEAM,
    PUBLIC
}
