package co.jware.sojka.core.enums;


public enum RecurrencePeriod {
    MONTHLY, DAILY
}
