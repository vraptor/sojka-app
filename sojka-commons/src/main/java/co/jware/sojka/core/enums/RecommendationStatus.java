package co.jware.sojka.core.enums;


public enum RecommendationStatus implements KeyedEnum {
    PENDING(0, "recommendation.status.pending"),
    RESPONSE_SENT(1, "recommendation.status.response_sent"),
    INTERVIEW(2, "recommendation.status.interview"),
    OFFER(3, "recommendation.status.offer"),
    HIRED(4, "recommendation.status.hired"),
    REJECTED(5, "recommendation.status.rejected"),
    DEDLINED(6, "recommendation.status.declined");

    private static final RecommendationStatus[] vals = values();
    private final int order;
    private final String key;

    RecommendationStatus(int order, String key) {
        this.order = order;
        this.key = key;
    }

    public int order() {
        return this.order;
    }

    public RecommendationStatus next() {
        int ordinal = this.ordinal();
        if (ordinal + 1 < vals.length)
            return vals[(ordinal + 1) % vals.length];
        return this;
    }


    @Override
    public String key() {
        return this.key;
    }
}
