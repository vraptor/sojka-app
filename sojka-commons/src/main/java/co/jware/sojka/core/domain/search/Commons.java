package co.jware.sojka.core.domain.search;

public interface Commons {

    String META_LINKS_MAP = "map_meta_links";

    String NOT_IMPLEMENTED = "Not Implemented";

    String NOT_AVAILABLE = "N/A";

    int META_SEARCH_TYPE_ID = 10_000;

}
