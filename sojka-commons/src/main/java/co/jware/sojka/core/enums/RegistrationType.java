package co.jware.sojka.core.enums;

public enum RegistrationType {
    CANDIDATE,
    HIRER,
    TEACHER
}
