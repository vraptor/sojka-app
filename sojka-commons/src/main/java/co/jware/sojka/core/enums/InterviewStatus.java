package co.jware.sojka.core.enums;


public enum InterviewStatus implements KeyedEnum {

    PENDING("interview.status.pending"),
    PASSED("interview.status.passed"),
    FAILED("interview.status.failed"),
    CANCELLED("interview.status.cancelled"),
    INVITATION_SENT("interview.status.invitation_sent"),
    INVITATION_ACCEPTED("interview.status.invitation_accepted"),
    INVITATION_REJECTED("interview.status.invitation_rejected"),
    OFFER_SENT("interview.status.offer_sent"),
    OFFER_ACCEPTED("interview.status.offer_accepted"),
    OFFER_REJECTED("interview.status.offer_rejected");

    private final String key;

    InterviewStatus(String key) {
        this.key = key;
    }

    @Override
    public String key() {
        return this.key;
    }
}
