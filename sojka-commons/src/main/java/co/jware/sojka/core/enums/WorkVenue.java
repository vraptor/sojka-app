package co.jware.sojka.core.enums;


public enum WorkVenue implements KeyedEnum {

    REMOTE("work_venue.remote"),
    ON_SITE("work_venue.on_site");

    private final String key;

    WorkVenue(String key) {
        this.key = key;
    }

    @Override
    public String key() {
        return this.key;
    }
}
