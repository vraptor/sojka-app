package co.jware.sojka.core.enums;


public enum JobListingStatus implements KeyedEnum {

    PENDING("job_listing.status.pending"),
    ACTIVE("job_listing.status.active"),
    INACTIVE("job_listing.status.inactive"),
    EXPIRED("job_listing.status.expired"),
    CLOSED("job_listing.status.closed");

    private final String key;

    JobListingStatus(String key) {
        this.key = key;
    }

    @Override
    public String key() {
        return this.key;
    }
}
