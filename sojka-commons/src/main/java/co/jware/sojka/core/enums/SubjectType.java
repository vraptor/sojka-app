package co.jware.sojka.core.enums;

public enum SubjectType implements KeyedEnum {

    JOB_LISTING("subject.type.job_listing"),
    COMPANY("subject.type.company");

    private final String key;

    SubjectType(String key) {
        this.key = key;
    }

    @Override
    public String key() {
        return this.key;
    }
}
