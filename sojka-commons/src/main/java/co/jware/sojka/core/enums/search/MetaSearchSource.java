package co.jware.sojka.core.enums.search;


public enum MetaSearchSource {
    ANY,
    EMPLOYER,
    AGENCY;
}
