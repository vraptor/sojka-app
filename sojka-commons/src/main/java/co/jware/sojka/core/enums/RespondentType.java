package co.jware.sojka.core.enums;


public enum RespondentType {
    CANDIDATE,
    EXTERNAL
}
