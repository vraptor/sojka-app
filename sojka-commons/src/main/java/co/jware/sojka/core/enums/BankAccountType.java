package co.jware.sojka.core.enums;


public enum BankAccountType implements KeyedEnum {

    GENERIC("payment.account.type."),
    SAVINGS("payment.account.type."),
    CHECK("payment.account.type.");

    private final String key;

    BankAccountType(String key) {
        this.key = key;
    }

    @Override
    public String key() {
        return key;
    }
}
