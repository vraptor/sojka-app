package co.jware.sojka.core.enums;

public enum PartyType implements KeyedEnum {

    EXTERNAL("party.type.external");

    private final String key;


    PartyType(String key) {
        this.key = key;
    }

    @Override
    public String key() {
        return this.key;
    }
}
