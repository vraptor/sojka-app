package co.jware.sojka.core.enums;


public enum ContractType implements KeyedEnum {
    PERMANENT("contract.type.permanent"),
    TERMED("contract.type.termed"),
    PROJECT("contract.type.project"),
    BRIGADE("contract.type.brigade"),
    INTERN("contract.type.intern");


    private final String key;

    ContractType(String key) {
        this.key = key;
    }

    @Override
    public String key() {
        return key;
    }
}