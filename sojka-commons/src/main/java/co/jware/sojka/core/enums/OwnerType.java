package co.jware.sojka.core.enums;

public enum OwnerType {
    CANDIDATE,
    HIRER,
    TEACHER,
    EXTERNAL
}
