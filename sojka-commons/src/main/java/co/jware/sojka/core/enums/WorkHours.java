package co.jware.sojka.core.enums;


public enum WorkHours {
    FULL_TIME, PART_TIME;
}
