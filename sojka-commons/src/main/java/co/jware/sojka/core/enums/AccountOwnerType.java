package co.jware.sojka.core.enums;

public enum AccountOwnerType implements KeyedEnum {


    CANDIDATE("account.owner.type.candidate"),
    HIRER("account.owner.type.hirer"),
    TEACHER("account.owner.type.teacher");


    private final String key;

    AccountOwnerType(String key) {
        this.key = key;
    }

    @Override
    public String key() {
        return key;
    }
}
