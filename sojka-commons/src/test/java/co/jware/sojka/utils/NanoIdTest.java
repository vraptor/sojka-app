package co.jware.sojka.utils;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class NanoIdTest {
    @Test
    public void nanoId() throws Exception {
        String id = NanoId.generate();
        assertThat(id).hasSize(22);
    }
}