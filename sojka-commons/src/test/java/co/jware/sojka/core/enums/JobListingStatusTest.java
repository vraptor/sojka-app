package co.jware.sojka.core.enums;

import org.junit.Test;

import java.util.Arrays;

import static org.apache.commons.lang3.StringUtils.isNotBlank;
import static org.junit.Assert.assertTrue;


public class JobListingStatusTest {
    @Test
    public void keysNonBlank() throws Exception {
        assertTrue(Arrays.stream(JobListingStatus.values()).allMatch(s -> isNotBlank(s.key())));
    }
}