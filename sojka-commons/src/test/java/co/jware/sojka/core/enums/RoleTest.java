package co.jware.sojka.core.enums;

import org.junit.Test;

import java.util.Arrays;

import static org.apache.commons.lang3.StringUtils.isNoneBlank;
import static org.junit.Assert.assertTrue;


public class RoleTest {
    @Test
    public void key() throws Exception {
        boolean allMatch = Arrays.stream(Role.values())
                .allMatch(s -> isNoneBlank(s.key()));
        assertTrue(allMatch);
    }
}