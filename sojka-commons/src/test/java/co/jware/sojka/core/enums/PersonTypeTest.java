package co.jware.sojka.core.enums;

import org.junit.Test;

import java.util.Arrays;

import static org.apache.commons.lang3.StringUtils.isNotBlank;
import static org.assertj.core.api.Java6Assertions.assertThat;
import static org.junit.Assert.assertTrue;


public class PersonTypeTest {
    @Test
    public void key() throws Exception {
        assertThat(PersonType.CANDIDATE.key())
                .isEqualToIgnoringCase("person.type.candidate");
    }

    @Test
    public void keysNonBlank() throws Exception {
        assertTrue(Arrays.stream(PersonType.values()).allMatch(s -> isNotBlank(s.key())));
    }
}