package co.jware.sojka.core.enums;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;

import java.util.Arrays;

import static org.assertj.core.api.Java6Assertions.assertThat;


public class InterviewStatusTest {
    @Test
    public void key() throws Exception {
        boolean allMatch = Arrays.stream(InterviewStatus.values())
                .allMatch(s -> StringUtils.isNotBlank(s.key()));
        assertThat(allMatch).isTrue();
    }

}