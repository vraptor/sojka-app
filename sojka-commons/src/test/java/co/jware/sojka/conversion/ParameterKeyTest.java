package co.jware.sojka.conversion;

import org.junit.Test;

import static org.assertj.core.api.Java6Assertions.assertThat;


public class ParameterKeyTest {
    @Test
    public void name() throws Exception {
        ParameterKey<String> parameterKey = ParameterKey.<String>builder()
                .key("clef")
                .keyClass(String.class)
                .build();
        assertThat(parameterKey.key()).isInstanceOf(String.class);
    }
}