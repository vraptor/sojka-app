package co.jware.sojka.conversion;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.json.JsonTest;
import org.springframework.boot.test.json.JacksonTester;
import org.springframework.boot.test.json.JsonContent;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@JsonTest()
public class ParameterKeyJsonTest {
    @Autowired
    JacksonTester<ParameterKey> tester;

    @Test
    public void serialize() throws Exception {
        ParameterKey<String> parameterKey = ParameterKey.<String>builder()
                .key("clef")
                .keyClass(String.class)
                .build();
        JsonContent<ParameterKey> jsonContent = tester.write(parameterKey);
        String json = jsonContent.getJson();
        assertThat(json).isNotNull();
        assertThat(jsonContent).hasJsonPathValue("@.key");
    }

    @Configuration
    public static class Config {

    }
}