package co.jware.sojka.rest.spring;


import com.jayway.restassured.RestAssured;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Rule;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.test.context.junit4.rules.SpringClassRule;
import org.springframework.test.context.junit4.rules.SpringMethodRule;

public class RestBaseTest {
    @ClassRule
    public static final SpringClassRule springClassRule = new SpringClassRule();
    @Rule
    public SpringMethodRule springMethodRule = new SpringMethodRule();
    @Value("${local.server.port}")
    private int serverPort;

    @Before
    public void setUp() throws Exception {
        RestAssured.port = serverPort;
        RestAssured.basePath = "/rest";
    }
}
