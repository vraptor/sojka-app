package co.jware.sojka.rest;

import co.jware.sojka.SojkaAppApplication;
import com.jayway.restassured.RestAssured;
import org.apache.http.HttpStatus;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.rules.SpringClassRule;
import org.springframework.test.context.junit4.rules.SpringMethodRule;
import org.springframework.test.context.web.WebAppConfiguration;

import java.util.UUID;

import static com.jayway.restassured.RestAssured.when;
import static org.codehaus.groovy.tools.shell.util.Logger.io;
import static org.hamcrest.Matchers.equalTo;

@SpringApplicationConfiguration(classes = SojkaAppApplication.class)
@WebAppConfiguration
@IntegrationTest("server.port:0")
public class RecommendEndpointIT {

    @ClassRule
    public static final SpringClassRule springClassRule = new SpringClassRule();
    @Rule
    public SpringMethodRule springMethodRule = new SpringMethodRule();

    @Value("${local.server.port}")
    private int serverPort;

    @Before
    public void setUp() throws Exception {
        RestAssured.port = serverPort;
        RestAssured.basePath = "/api";
    }

    @Test
    public void getRecommendation() throws Exception {
        String uuid = UUID.randomUUID().toString();
        when().get("/recommend/{uuid}", uuid)
                .then().assertThat()
                .statusCode(HttpStatus.SC_OK)
                .body(equalTo(uuid));
    }
}