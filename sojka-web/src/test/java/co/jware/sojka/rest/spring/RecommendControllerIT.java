package co.jware.sojka.rest.spring;

import co.jware.sojka.SojkaAppApplication;
import org.apache.http.HttpStatus;
import org.junit.Test;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.web.WebAppConfiguration;

import java.util.UUID;

import static com.jayway.restassured.RestAssured.when;
import static org.hamcrest.Matchers.containsString;

@SpringApplicationConfiguration(classes = SojkaAppApplication.class)
@WebAppConfiguration
@IntegrationTest("server.port:0")
public class RecommendControllerIT extends RestBaseTest {

    @Test
    public void getRecommendation() throws Exception {
        String uuid = UUID.randomUUID().toString();
        when().get("/recommend/{uuid}", uuid)
                .then().assertThat()
                .statusCode(HttpStatus.SC_NOT_FOUND)
                .body("error.message", containsString("No recommendation found"));
    }

}