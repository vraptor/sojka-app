package co.jware.sojka;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.sql.DataSource;

@Configuration
@Profile("test")
public class TestConfig {
    @Bean
    public DataSource dataSource() {
        DataSource database = new EmbeddedDatabaseBuilder()
                .setType(EmbeddedDatabaseType.DERBY)
//                .addScript("classpath:/org/springframework/session/jdbc/schema-derby.sql")
                .build();
        return database;
    }

//    @Bean
//    @Primary
//    public PasswordEncoder passwordEncoder() {
//        return new PasswordEncoder() {
//
//            @Override
//            public String encode(CharSequence rawPassword) {
//                return rawPassword.toString();
//            }
//
//            @Override
//            public boolean matches(CharSequence rawPassword, String encodedPassword) {
//                return rawPassword.equals(encodedPassword);
//            }
//        };
//    }
}
