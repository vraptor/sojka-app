package co.jware.sojka.rest.spring

import co.jware.sojka.SojkaAppApplication
import com.jayway.restassured.http.ContentType
import groovy.json.JsonOutput
import org.apache.http.HttpStatus
import org.junit.Test
import org.springframework.boot.test.IntegrationTest
import org.springframework.boot.test.SpringApplicationConfiguration
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.web.WebAppConfiguration

import static org.hamcrest.Matchers.containsString
import static org.hamcrest.Matchers.notNullValue

@SpringApplicationConfiguration(classes = SojkaAppApplication.class)
@WebAppConfiguration
@IntegrationTest("server.port:0")
@ActiveProfiles('prod')
public class JobListingControllerIT extends RestBaseTest {

    @Test
    public void getJobListing() throws Exception {
        String uuid = UUID.randomUUID().toString();
        when().get("/job-listing/{uuid}", uuid)
                .then().assertThat()
                .statusCode(HttpStatus.SC_NOT_FOUND)
                .body("error.message", containsString("No job listing found"))
    }

    @Test
    public void postJobListing() throws Exception {
        def json = JsonOutput.toJson([title:'Java Dev',uuid: UUID.randomUUID()])
        given().body(json)
                .contentType(ContentType.JSON)
                .when().post("/job-listing")
                .then()
                .assertThat()
                .statusCode(HttpStatus.SC_CREATED)
        .body('uuid',notNullValue())
    }
}