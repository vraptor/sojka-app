package co.jware.sojka.rest.spring

import co.jware.sojka.SojkaAppApplication
import co.jware.sojka.core.service.PersonService
import co.jware.sojka.core.service.entity.JobListingService
import com.fasterxml.jackson.databind.ObjectMapper
import com.jayway.restassured.builder.RequestSpecBuilder
import com.jayway.restassured.specification.RequestSpecification
import org.junit.Before
import org.junit.ClassRule
import org.junit.Rule
import org.junit.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.test.IntegrationTest
import org.springframework.boot.test.SpringApplicationConfiguration
import org.springframework.restdocs.JUnitRestDocumentation
import org.springframework.test.annotation.Rollback
import org.springframework.test.context.junit4.rules.SpringClassRule
import org.springframework.test.context.junit4.rules.SpringMethodRule
import org.springframework.test.context.web.WebAppConfiguration
import org.springframework.transaction.annotation.Transactional

import javax.persistence.EntityManager
import javax.persistence.PersistenceContext

import static com.jayway.restassured.RestAssured.given
import static com.jayway.restassured.RestAssured.preemptive
import static groovy.json.JsonOutput.prettyPrint
import static groovy.json.JsonOutput.toJson
import static org.apache.http.HttpStatus.SC_NOT_FOUND
import static org.hamcrest.Matchers.containsString
import static org.springframework.restdocs.request.RequestDocumentation.parameterWithName
import static org.springframework.restdocs.request.RequestDocumentation.pathParameters
import static org.springframework.restdocs.restassured.RestAssuredRestDocumentation.document
import static org.springframework.restdocs.restassured.RestAssuredRestDocumentation.documentationConfiguration

@SpringApplicationConfiguration(classes = SojkaAppApplication.class)
@WebAppConfiguration
@IntegrationTest("server.port:0")
@Transactional
public class RecommendControllerDocumentation {

    @ClassRule
    public static final SpringClassRule springClassRule = new SpringClassRule();
    public static final String APPLICATION_JSON = "application/json";
    @Rule
    public SpringMethodRule springMethodRule = new SpringMethodRule();
    @Value('${local.server.port}')
    private int serverPort;

    @Autowired
    private ObjectMapper mapper
    @Autowired
    PersonService personService
    @Autowired
    JobListingService jobListingService
    @PersistenceContext
    EntityManager em
    @Rule
    public JUnitRestDocumentation restDocumentation =
            new JUnitRestDocumentation("target/generated-snippets");

    private RequestSpecification spec;

    @Before
    public void setUp() {
        this.spec = new RequestSpecBuilder()
                .setPort(serverPort)
                .setBasePath("/rest")
                .setAuthentication(preemptive().basic("demo@sojka.co", "sojka"))
                .addFilter(documentationConfiguration(this.restDocumentation))
                .build();
    }


    @Test
    public void createRecommendation() throws Exception {
        def rec = [
                type    : 'PERSON',
                subject : [
                        'subjectType': 'job_listing',
                        uuid         : 'df480367-e138-406a-a659-628a716777fb'
                ],
                sender  : [
                        'type'  : 'CANDIDATE',
                        uuid     : '1c9a977b-4ced-41e5-b5c5-a395e80a3b85',
                        firstName: 'Jane',
                        lastName : 'Doyle',
                        email    : 'jane@jware.co'
                ],
                receiver: [
                        'type': 'EXTERNAL',
                        uuid   : '',
                        email  : 'john.doe@acme.co'
                ]
        ]

        def content = prettyPrint toJson(rec)
//        def recommendation = mapper.readValue(content, PersonRecommendation)
//        def sender = personService.createPerson(recommendation.sender())
//        def receiver = personService.createPerson(recommendation.receiver())
//        def jobListing = jobListingService.createJobListing(recommendation.subject())
//        recommendation = recommendation.withReceiver(receiver)
//                .withSender(sender)
//                .withSubject(jobListing)
//        def json = mapper.writeValueAsString(recommendation)
//        em.flush()
        given(this.spec)
                .contentType(APPLICATION_JSON)

                .filter(document("recommend/create"))
                .content(content)
                .when().post("/recommend")
                .then().assertThat()
                .statusCode(SC_NOT_FOUND);
    }

    @Test
    public void getRecommendation() throws Exception {
        String uuid = UUID.randomUUID().toString();
        given(this.spec)
                .accept(APPLICATION_JSON)
                .filter(document("recommend/get", pathParameters(
                parameterWithName("uuid").description("Unique id")
        )))
                .when().get("/recommend/{uuid}", uuid)
                .then().assertThat()
                .statusCode(SC_NOT_FOUND)
                .body("error.message", containsString("No recommendation found"));
    }

//    @Test
//    public void getByJobListing() throws Exception {
//        String uuid = UUID.randomUUID().toString();
//        given(this.spec)
//                .accept(APPLICATION_JSON)
//                .filter(document("recommend/get/job-listing",
//                pathParameters(parameterWithName("uuid").description("UUID of lob listing")
//                )))
//                .when().get("recommend/job-listing/{uuid)", uuid)
//                .then().assertThat()
//                .statusCode(SC_NOT_FOUND)
//                .body("error.message", containsString("No recommendations found"));
//    }
}