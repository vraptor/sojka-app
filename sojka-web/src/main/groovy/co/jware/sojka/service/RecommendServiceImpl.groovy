package co.jware.sojka.service

import co.jware.sojka.core.domain.BotRecommendation
import co.jware.sojka.core.domain.party.External
import co.jware.sojka.core.domain.PersonRecommendation
import co.jware.sojka.core.domain.Recommendation
import co.jware.sojka.core.domain.mapping.ImmutableMapper
import co.jware.sojka.core.service.PersonService
import co.jware.sojka.core.service.entity.ExternalService
import co.jware.sojka.core.service.entity.JobListingService
import co.jware.sojka.core.service.entity.RecommendationService
import co.jware.sojka.rest.HttpNotFoundException
import groovy.util.logging.Slf4j
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service('web.recommendService')
@Slf4j()
class RecommendServiceImpl implements RecommendApiService {

    @Autowired
    RecommendationService recommendationService
    @Autowired
    PersonService personService
    @Autowired
    ExternalService externalService
    @Autowired
    JobListingService jobListingService

    @Override
    Recommendation getRecommendation(String uuid) {
        Optional<Recommendation> recommendation = recommendationService.findByUuid(uuid)
        recommendation.orElseThrow({ -> new HttpNotFoundException() })
    }

    @Override
    List<Recommendation> findByReceiver(String uuid) {
        def receiver = personService.findByUuid(uuid)
        if (receiver) {
            def entities = recommendationService.findByReceiver(receiver)
            if (entities?.size()) {
                def result = []
                entities.each { entity ->
                    def mapper = entity as ImmutableMapper
                    result += mapper.fromEntity()
                }
                result
            }
        }
    }

    @Override
    List<Recommendation> findByJobListing(String uuid) {
        def jobListing = jobListingService.findByUuid(UUID.fromString(uuid))
        if (jobListing) {
            def entities = recommendationService.findByJobListing(jobListing)
            if (entities?.size()) {
                def result = []
                entities.each { entity ->
                    def mapper = entity as ImmutableMapper
                    result += mapper.fromEntity()
                }
                result
            }
        }
    }

    @Override
    Recommendation createRecommendation(Recommendation recommendation) {
        def result = recommendationService.createRecommendation(processRecommendation(recommendation))
        result
    }

    def processRecommendation(PersonRecommendation recommendation) {
        def result = PersonRecommendation.copyOf(recommendation)
        result = processJobListing(result)
        result = processReceiver(result)
        result = processSender(result)
        result
    }

    private PersonRecommendation processJobListing(PersonRecommendation result) {
        def jobListing = result.subject()
        def uuid = jobListing.uuid().orElseThrow({ -> new IllegalArgumentException('UUID is required') })
        jobListing = Optional.ofNullable(jobListingService.findByUuid(uuid))
                .orElseThrow({ -> new HttpNotFoundException("Job listing with UUID $uuid not found") })
        result = result.withSubject(jobListing)
        return result
    }

    private PersonRecommendation processReceiver(PersonRecommendation result) {
        def receiver = result.receiver()
        receiver = (receiver.class == External) ? externalService.findOrCreate(receiver as External) :
                personService.findByUuid(receiver.uuid() as UUID)
        if (receiver) {
            result = result.withReceiver(receiver)
        } else {
            throw new HttpNotFoundException("Receiver with UUID ${receiver.uuid()}  not found")
        }
        return result
    }

    private PersonRecommendation processSender(PersonRecommendation result) {
        def sender = result.sender()
        def senderUuid = sender.uuid().orElseThrow({ -> new IllegalArgumentException('UUID is required') })
        sender = personService.findByUuid(senderUuid as UUID)
        if (sender) {
            result = result.withSender(sender)
        } else {
            throw new HttpNotFoundException("Sender with UUID ${senderUuid}  not found")
        }
        result = result.withSender(sender)
        return result
    }


    def processRecommendation(BotRecommendation recommendation) {
        recommendation
    }

}
