package co.jware.sojka.service

import co.jware.sojka.core.domain.JobListing
import co.jware.sojka.core.service.entity.JobListingService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service;

@Service('web.jobListingService')
public class JobListingApiServiceImpl implements JobListingApiService {
    @Autowired
    JobListingService jobListingService

    @Override
    JobListing getJobListing(UUID uuid) {
       jobListingService.findByUuid(uuid)
    }

    @Override
    JobListing createJobListing(JobListing jobListing) {
        return jobListingService.createJobListing(jobListing)
    }
}
