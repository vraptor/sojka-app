package co.jware.sojka.rest.spring

import co.jware.sojka.rest.HttpNotFoundException
import com.fasterxml.jackson.databind.JsonMappingException
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler

@ControllerAdvice
public class RestControllerAdvice {

    @ExceptionHandler(HttpNotFoundException)
    public ResponseEntity<Map> httpNotFoundExceptionHandler(HttpNotFoundException e) {
        return new ResponseEntity(['error': ['message': e.getMessage()]], HttpStatus.NOT_FOUND)
    }

    @ExceptionHandler([IllegalStateException,JsonMappingException])
    public ResponseEntity<Map> illegalStateExceptionHandler(Exception e) {
        new ResponseEntity(['error': ['message': e.getMessage()]], HttpStatus.BAD_REQUEST)
    }

//    @ExceptionHandler(JsonMappingException)
//    public void jsonMappingExceptionHandler(){
//
//    }
}
