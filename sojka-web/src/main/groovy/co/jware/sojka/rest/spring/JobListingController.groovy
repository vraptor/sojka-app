package co.jware.sojka.rest.spring

import co.jware.sojka.core.domain.JobListing
import co.jware.sojka.rest.HttpNotFoundException
import co.jware.sojka.service.JobListingApiService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping('/rest/job-listing')
class JobListingController {

    @Autowired
    JobListingApiService jobListingApiService

    @RequestMapping(value = '/{uuid}', method = RequestMethod.GET)
    ResponseEntity<JobListing> getJobListing(@PathVariable('uuid') UUID uuid) {
        def jobListing = jobListingApiService.getJobListing(uuid)
        if (jobListing) {
            return new ResponseEntity<>(jobListing, HttpStatus.OK)
        }
        throw new HttpNotFoundException("No job listing found for $uuid")
    }

    @RequestMapping(method = RequestMethod.POST)
    ResponseEntity<JobListing> postJobListing(@RequestBody JobListing jobListing) {
        assert jobListing
        def result = jobListingApiService.createJobListing(jobListing)
        return new ResponseEntity<JobListing>(result, HttpStatus.CREATED)
    }
}
