package co.jware.sojka.web;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.HashMap;

import static org.springframework.web.bind.annotation.RequestMethod.GET;


@Controller
@RequestMapping(value = "/")
public class HomeController {
    @RequestMapping(method = GET)
    public String home(Model model) {
        HashMap<String, String> panes = new HashMap<>();
        panes.put("left","home :: left");
        model.addAttribute("panes", panes);
        return "index";
    }
}
