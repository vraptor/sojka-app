package co.jware.sojka;

import co.jware.sojka.core.service.impl.DefaultUserDetailsService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SojkaAppConfig {
    @Bean()
    public DefaultUserDetailsService defaultUserDetailsService() {
        return new DefaultUserDetailsService();
    }

}
