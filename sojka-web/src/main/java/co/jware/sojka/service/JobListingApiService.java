package co.jware.sojka.service;


import co.jware.sojka.core.domain.JobListing;

import java.util.UUID;

public interface JobListingApiService {

    JobListing getJobListing(UUID uuid);

    JobListing createJobListing(JobListing jobListing);
}
