package co.jware.sojka.service;


import co.jware.sojka.core.domain.Recommendation;

import java.util.List;

public interface RecommendApiService {
    Recommendation getRecommendation(String uuid);

    List<Recommendation> findByReceiver(String uuid);

    List<Recommendation> findByJobListing(String uuid);

    Recommendation createRecommendation(Recommendation recommendation);
}
