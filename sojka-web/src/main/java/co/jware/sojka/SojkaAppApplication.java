package co.jware.sojka;


import co.jware.sojka.bot.cantidates.CandidateSojkaBot;
import co.jware.sojka.bot.jobs.JobsSojkaBot;
import io.vertx.core.Vertx;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.session.jdbc.config.annotation.web.http.EnableJdbcHttpSession;

import javax.annotation.PostConstruct;
import java.lang.invoke.MethodHandles;

@SpringBootApplication
@EnableAsync
@EnableScheduling()
@ComponentScan()
@EnableJpaRepositories()
@EnableJdbcHttpSession
public class SojkaAppApplication {

    static private Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
    @Autowired
    CandidateSojkaBot candidateSojkaBot;
    @Autowired
    JobsSojkaBot jobsSojkaBot;
    public static void main(String[] args) {
        SpringApplication.run(SojkaAppApplication.class, args);
    }

    @PostConstruct
    public void deployVerticles() {

        Vertx vertx = Vertx.vertx();
        vertx.deployVerticle(candidateSojkaBot);
        vertx.deployVerticle(jobsSojkaBot);
    }
}
