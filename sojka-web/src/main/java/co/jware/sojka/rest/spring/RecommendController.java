package co.jware.sojka.rest.spring;

import co.jware.sojka.core.domain.Recommendation;
import co.jware.sojka.rest.HttpNotFoundException;
import co.jware.sojka.service.RecommendApiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/rest/recommend")
public class RecommendController {

    @Autowired
    RecommendApiService recommendApiService;

    @RequestMapping("/{uuid}")
    public ResponseEntity<Recommendation> getRecommendation(@PathVariable("uuid") String uuid) {
        Recommendation recommendation = recommendApiService.getRecommendation(uuid);
        if (null == recommendation)
            throw new HttpNotFoundException("No recommendation found for " + uuid);
        return new ResponseEntity<>(recommendation, HttpStatus.OK);
    }

    @RequestMapping("/receiver/{uuid}")
    public ResponseEntity<List<Recommendation>> getByReceiver(@PathVariable("uuid") String uuid) {
        List<Recommendation> recommendations = recommendApiService.findByReceiver(uuid);
        if (recommendations != null && !recommendations.isEmpty()) {
            return new ResponseEntity<>(recommendations, HttpStatus.OK);
        }
        throw new HttpNotFoundException("No recommendations found for " + uuid);
    }

    @RequestMapping("/job-listing/{uuid}")
    public ResponseEntity<List<Recommendation>> getByJobListing(@PathVariable("uuid") String uuid) {
        List<Recommendation> recommendations = recommendApiService.findByJobListing(uuid);
        if (recommendations != null && !recommendations.isEmpty()) {
            return new ResponseEntity<>(recommendations, HttpStatus.OK);
        }
        throw new HttpNotFoundException("No recommendations found for " + uuid);
    }

    @RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<Recommendation> createRecommendation(@RequestBody Recommendation recommendation) {
        return new ResponseEntity<>(recommendApiService.createRecommendation(recommendation), HttpStatus.CREATED);
    }
}
