package co.jware.sojka.rest;

import co.jware.sojka.core.domain.party.Candidate;
import co.jware.sojka.core.domain.JobListing;
import co.jware.sojka.core.domain.Recommendation;
import co.jware.sojka.core.domain.Recommendation;
import org.springframework.stereotype.Component;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;


@Component
@Path("/recommend")
public class RecommendEndpoint {
    @GET()
    @Path("/{uuid}")
    @Produces("application/json")
    public Response getRecommendation(@PathParam("uuid") String uuid) {
        Recommendation recommendation = null;
        try {
            recommendation = Recommendation.builder()
                    .receiver(Candidate.NULL)
                    .sender(Candidate.NULL)
                    .subject(JobListing.NULL)
                    .build();
            return Response.ok(recommendation).build();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Response.serverError().build();
    }
}
